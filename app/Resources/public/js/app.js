
$(function() {

    $.extend(true, $.fn.dataTable.defaults, {
        bLengthChange: false,
        info: false,
        pageLength: 5,
        language: {
            url: '/assets/spanish.json'
        }
    });

    $('[data-help-tooltip]').each(function(i, e) {
        $(e).parent().append(
            $('<i>')
                .attr('data-toggle', 'tooltip')
                .attr('title', $(e).data('help-tooltip'))
                .css('margin-left', '10px')
                .addClass('glyphicon glyphicon-info-sign')
                .insertAfter($(e))
        );
    });

    $('[data-table]').each(function (i, e) {
        $(e).DataTable();
    });

    $('.datetimepicker').datetimepicker({
        locale: 'es',
        maxDate: Date.now()
    });

    $.ajaxSetup({
        type: 'POST',
        headers: { Accepts: 'application/json; charset=utf-8' }
    });

    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();

    $('.datepicker').datepicker({ language: 'es' });
    $('.user-redirection').select2({ theme: 'bootstrap' });
    
    $('.betterSelect').select2({
        theme: 'bootstrap'
    });

    $.fn.modal.Constructor.DEFAULTS.keyboard = false;
    $.fn.modal.Constructor.DEFAULTS.background = 'static';

    $.fn.datepicker.defaults.autoclose = true;
    $.fn.datepicker.defaults.format = 'dd/mm/yyyy';
    $.fn.datepicker.defaults.orientation = 'bottom';
    $.fn.datepicker.defaults.language = 'es';
    $.fn.datepicker.defaults.endDate = new Date;
    
    bootbox.setDefaults({
        locale: 'es',
        closeButton: false
    });

    $(document).on('click', '.btn-add[data-target]', function(event) {
        event.preventDefault();

        var holder = $('#' + $(this).data('target'));

        if (!holder.data('counter')) {
            holder.data('counter', holder.children().length);
        }

        var prototype = holder.data('prototype');
        var nameProto = $(this).data('proto-name');
        var protoReg = new RegExp(nameProto, 'g');
        var form = prototype.replace(protoReg, holder.data('counter'));

        holder.data('counter', Number(holder.data('counter')) + 1);
        holder.append(form);

        var callback = $(this).data('callback');
        if (callback && window[callback] !== undefined && typeof(window[callback]) === 'function') {
            window[callback](this, form);
        }
    });

    $(document).on('click', '.btn-remove-item', function(event) {
        event.preventDefault();
        $(this).closest('.removable-item').remove();
    });
    
    $.fn.jsonFill = function (json) {
        var form = $(this);
        $.each(json, function (key, value) {
            form.find('[name*="[' + key + ']"]').val(value);
        });
    };
});

function renderError(errors) {
    var tpl = [];
    tpl.push('<span class="help-block">');
    tpl.push('<ul class="list-unstyled error-list">');

    $.each(errors, function (i, error) {
        tpl.push('<li><span class="glyphicon glyphicon-exclamation-sign"></span>');
        tpl.push(error);
        tpl.push('</li>');
    });

    tpl.push('</ul>');
    tpl.push('</span>');

    return tpl.join(''); 
}

function validateForm(form, response) {
    var config = response[form.attr('name')];
    
    form.find('.error-list').remove();
    form.find('.form-group').removeClass('has-error');

    if (config.vars.valid) {
        return true;
    }

    $.each(config.vars.errors.form.children, function(field, item) {
        if (typeof item.errors === 'undefined') {
            return;
        }
        
        $('#' + config.children[field].vars.id)
            .closest('.form-group')
            .addClass('has-error')
            .append(renderError(item.errors))
        ;
    });
    
    return false;
}

function renderFormErrors (form, error) {
    var error = error.responseJSON.errors.children;
    var formName = $(form).attr('name');
    var hasError = false;
    $.each(error, function(field, item) {
        if(formName == 'requirement_approval') {
            renderApprovalFormErrors(item);
            return;
        }

        if (Object.keys(item).length > 0) {
            hasError = true;
            $('#' + formName + '_' + field).tooltip('destroy')
                .closest('.form-group')
                .addClass('has-error')
                .append(renderError(item.errors));
        }
    });

    if(hasError) {
        bootbox.alert('Tiene campos sin completar');
    }
}

function renderApprovalFormErrors(item) {
    var hasError = false;
    if (typeof item.children !== 'undefined') {
        var subForm = 'requirement_approval_TechEval';
        $.each(item.children, function(sub_field, sub_item) {
            if (Object.keys(sub_item).length > 0) {
                hasError = true;
                $('#' + subForm + '_' + sub_field).tooltip('destroy')
                    .closest('.form-group')
                    .addClass('has-error')
                    .append(renderError(sub_item.errors));
            }
        });
    }

    if(hasError) {
        bootbox.alert('Tiene campos sin completar');
    }
}




