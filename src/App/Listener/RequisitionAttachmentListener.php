<?php

namespace App\Listener;

use App\Entity\Requisition;
use App\Helper\UploadMover;
use App\Model\Attachment;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class RequisitionListener
 * @package App\Listener
 * 
 * @DI\Service("requisition_attachment_listener")
 * @DI\Tag("doctrine.event_listener", attributes={"event"="prePersist"})
 * @DI\Tag("doctrine.event_listener", attributes={"event"="preUpdate"})
 */
class RequisitionAttachmentListener
{
    /**
     * @var string
     * @DI\Inject("%uploads_dir%")
     */
    public $uploadDir;
    
    /**
     * @param LifecycleEventArgs $args
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->processAttachments($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {   
        $this->processAttachments($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    private function processAttachments(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        
        if (!$entity instanceof Requisition) {
            return;
        }
        
        if (empty(array_filter($entity->getAttachment() ?? array()))) {
            return;
        }
        
        $attachment = array_map(function ($attachment) {
            if ($attachment instanceof Attachment) {
                return $attachment;
            }
            
            return UploadMover::move($attachment, $this->uploadDir);
        }, $entity->getAttachment());
        
        $entity->setAttachment(array_filter($attachment));
    }
}