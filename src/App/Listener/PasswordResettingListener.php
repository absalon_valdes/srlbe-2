<?php
namespace App\Listener;

use JMS\DiExtraBundle\Annotation as Di;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\Routing\RouterInterface as Router;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Class AuthenticationFailureListener
 * Listener responsible to change the redirection at the end of the password resetting
 * @package App\Listener
 * @Di\Service("redirect_reset_password_user_listener")
 */
class PasswordResettingListener implements EventSubscriberInterface {
    /**
     * @var Router $router
     * @Di\Inject("router")
     */
    public $router;

    public static function getSubscribedEvents() {
        return [
            FOSUserEvents::RESETTING_RESET_SUCCESS => 'onPasswordResettingSuccess',
        ];
    }

    /**
     * @Di\Observe("fos_user.resetting.reset.success")
     */
    public function onPasswordResettingSuccess(FormEvent $event) {
        $url = $this->router->generate('dashboard');
        $event->setResponse(new RedirectResponse($url));
    }
}