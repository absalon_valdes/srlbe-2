<?php

namespace App\Listener;

use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Routing\RouterInterface as Router;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use App\Repository\UserRepository;
use App\Notification\MessageInterface;
use App\Notification\NotificationManager;
use App\Entity\User;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class AuthenticationListener
 * @package App\Listener
 * @Di\Service()
 */
class AuthenticationListener implements EventSubscriberInterface
{
    /**
     * @Di\Inject("%recaptcha_verify_url%")
     */
    public $path;

    /**
     * @Di\Inject("%recaptcha_secret_key%")
     */
    public $secret;


    /**
     * @var Router $router
     * @Di\Inject("router")
     */
    public $router;

    public static function getSubscribedEvents()
    {
    }

    /**
     * @Di\Observe("security.interactive_login")
     * @param  $event
     */
    public function onInteractiveLogin($event)
    {
        if($event instanceof InteractiveLoginEvent){
            $request = $event->getRequest();
            if($request->get('failedAttempt')){
                $success = $this->isTokenSuccessful($request->get('g-recaptcha-response'));
                if(!$success){
                    Throw new BadCredentialsException('El valor del captcha es incorrecto, intente de nuevo.');
                }

            }
        }

    }

    public function isTokenSuccessful($token){
        if(empty($token)){
            return false;
        }

        $params_string = http_build_query(['secret' => $this->secret,'response' => $token]);
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->path.$params_string,
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $response = @json_decode($response, true);

        if ($response["success"] == true) {
            return true;
        }
        return false;
    }
}