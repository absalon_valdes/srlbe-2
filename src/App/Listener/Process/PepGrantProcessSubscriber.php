<?php
namespace App\Listener\Process;

use App\Entity\PepGrant;
use App\Helper\RedisHandler;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * @Di\Service("workflow.subscriber.pep")
 */
class PepGrantProcessSubscriber
{
    const BACK_OFFICE_NOTIFICATION_KEY = 'PROCESS_UPDATE';

    /**
     * @Di\Inject("redis_handler")
     * @var RedisHandler $redisHandler
     */
    public $redisHandler;
    
    /**
     * @Di\Observe("pep.has_pep")
     *
     * @param GenericEvent $event
     */
    public function sendNotificationBackOffice($event)
    {
        /** @var PepGrant $pep */
        $pep = $event->getSubject();

        $msg = [
            'id'        => $pep->getId(),
            'number'    => $pep->getFormNumber(),
            'type'      => 'PEP',
            'status'    => $pep->getStatus(),
            'updatedAt' => $pep->getUpdatedAt(),
        ];

        $this->redisHandler->push(self::BACK_OFFICE_NOTIFICATION_KEY, $msg);
    }
    
}
