<?php

namespace App\Listener;

use App\Entity\Activity;
use App\Entity\Requisition;
use App\Entity\User;
use App\Entity\WorkflowActivity;
use App\Helper\SlaResolver;
use App\Helper\UserHelper;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Transition;

/**
 * Class VcardListener
 * @Di\Service()
 */
class VcardListener
{
    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @Di\Observe("workflow.vcards.announce.*.*")
     * @param Event $event
     * @param $name
     */
    public function onEnter(Event $event, $name)
    {
        /** @var User $currentUser */
        $currentUser = $this->userHelper->getCurrentUser();

        /** @var Requisition $requisition */
        $requisition = $event->getSubject();
        
        /** @var Transition $transition */
        $transition = $event->getTransition();
        
        /** @var Activity $activity */
        $activity = Activity::create();
        $activity->setWorkflow('vcards');
        $activity->setAssignee($requisition->getAssignee());
        $activity->setCreatedBy($currentUser);
        $activity->setName($transition->getName());

        $this->em->persist($activity);
        
        /** @var Activity $previous */
        if ($previous = $requisition->getActivities()->last()) {
//            $activity->setPrevious($previous);
        }
        
        $requisition->addActivity($activity);
        $requisition->setCreatedAt(new \DateTime);

        $to = $event->getTransition()->getTos()[0];
        $timestamps = $this->slaResolver->getSLA($requisition, $to);
        $requisition->addMetadata('timestamps', $timestamps);

        $this->em->flush();
    }
}
