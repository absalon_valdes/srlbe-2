<?php

namespace App\Listener;

use App\Entity\Product;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Class EasyAdminPreEditListener
 * @package App\Listener
 * @DI\Service()
 */
class EasyAdminPreEditListener
{
    public static $currentImage;

    /**
     * @DI\Observe("easy_admin.pre_update")
     * @param GenericEvent $event
     */
    public function preEdit(GenericEvent $event)
    {
        $entity = $event->getArgument('entity');

        if (null === self::$currentImage || !$entity instanceof Product) {
            return;
        }

        $entity->addImage(self::$currentImage);
    }
}