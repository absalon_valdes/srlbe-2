<?php

namespace App\Notification\Notifier;

use App\Notification\MessageInterface;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("notifier_internal")
 * @Di\Tag("notifier")
 */
class InternalNotifier extends Notifier
{
    const ALLOWED_TAGS = '<p><br><a><strong>';

    /**
     * @Di\Inject("message_repo")
     */
    public $messagesRepo;

    /**
     * @param MessageInterface $message
     * @return mixed|void
     */
    public function notify(MessageInterface $message)
    {
        $internal = $this->messagesRepo->create();
        $internal->setSender($message->getSender());
        $internal->setReceiver($message->getReceiver());
        $internal->setSource($message->getSource());
        $internal->setSubject($message->getSubject());

        $renderedBody = $this->renderBody($message);
        $renderedBody = strip_tags($renderedBody, self::ALLOWED_TAGS);
        $internal->setBody($renderedBody);

        $this->messagesRepo->save($internal);
    }
}
