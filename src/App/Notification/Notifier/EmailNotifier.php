<?php

namespace App\Notification\Notifier;

use App\Notification\MessageInterface;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("notifier_email")
 * @Di\Tag("notifier")
 */
class EmailNotifier extends Notifier
{
    /**
     * @Di\Inject("mailer")
     */
    public $mailer;

    /**
     * @Di\Inject("%notification_sender%")
     */
    public $sender;

    public function notify(MessageInterface $message)
    {
        try {
            $email = \Swift_Message::newInstance();
            $email->setBody(strip_tags($this->renderBody($message)));
            $email->addPart($this->renderBody($message), 'text/html');
            $email->setSubject($message->getSubject());
            $email->setFrom($this->sender);
            $email->setTo($message->getReceiver()->getEmail());

            return $this->mailer->send($email);
        } catch (\Exception $e) {
//            dump($e);
        }

    }
}
