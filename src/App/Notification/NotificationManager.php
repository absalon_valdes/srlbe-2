<?php

namespace App\Notification;

use App\Notification\Notifier\Notifier;
use App\Notification\Notifier\NotifierInterface;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("notification_manager")
 */
class NotificationManager implements NotificationManagerInterface
{
    /**
     * @var Notifier[]
     */
    private $notifiers;

    /**
     * @return Message
     */
    public function createMessage()
    {
        return new Message;
    }

    /**
     * @param NotifierInterface $notifier
     * @return void
     */
    public function addNotifier(NotifierInterface $notifier)
    {
        $this->notifiers[$notifier->getName()] = $notifier;
    }

    /**
     * {@inheritdoc}
     */
    public function notify(MessageInterface $message)
    {
        foreach ($message->getReceivers() as $receiver) {
            $msg = clone $message;
            $msg->setReceiver($receiver);
            $this->doNotify($msg);
        } 
    }

    /**
     * @param MessageInterface $message
     */
    private function doNotify(MessageInterface $message)
    {
        foreach ($this->resolveChannel($message) as $notifier) {
            $notifier->notify($message);
        }
    }

    /**
     * @param MessageInterface $message
     * @throws \InvalidArgumentException
     * @return Notifier[]
     */
    private function resolveChannel(MessageInterface $message)
    {
        $channel = $message->getChannel();

        if ('all' === $channel) {
            return $this->notifiers;
        }

        if (!isset($this->notifiers[$channel])) {
            throw new \InvalidArgumentException(sprintf(
                'Channel %s not found in [%s]', $channel, implode(', ', array_keys($this->notifiers))
            ));
        }

        return [$this->notifiers[$channel]];
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return null;
    }
}
