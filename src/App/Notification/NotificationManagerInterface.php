<?php

namespace App\Notification;

use App\Notification\Notifier\NotifierInterface;

/**
 * Interface NotificationManagerInterface
 * @package App\Notification
 */
interface NotificationManagerInterface extends NotifierInterface
{
    /**
     * @param NotifierInterface $notifier
     * @return mixed
     */
    public function addNotifier(NotifierInterface $notifier);
}
