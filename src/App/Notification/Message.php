<?php

namespace App\Notification;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Message
 * @package App\Notification
 */
class Message implements MessageInterface
{
    /**
     * @var
     */
    private $source;

    /**
     * @var
     */
    private $sender;

    /**
     * @var ArrayCollection
     */
    private $receivers;

    /**
     * @var
     */
    private $data;

    /**
     * @var
     */
    private $subject;

    /**
     * @var
     */
    private $channel;

    /**
     * Message constructor.
     */
    public function __construct()
    {
        $this->receivers = new ArrayCollection;
    }

    /**
     * @param $source
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param User $sender
     * @return $this
     */
    public function setSender(User $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function addReceiver(User $user)
    {
        $this->receivers[] = $user;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getReceivers()
    {
        return $this->receivers;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param User $receiver
     * @return $this
     */
    public function setReceiver(User $receiver)
    {
        $this->receivers->clear();
        $this->receivers->add($receiver);

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getReceiver()
    {
        return $this->receivers->get(0);
    }

    /**
     * @param $channel
     * @return $this
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }
}