<?php

namespace App\Notification\Process;

use App\Entity\Contract;
use App\Entity\Requisition;
use JMS\DiExtraBundle\Annotation as Di;
use Lexik\Bundle\WorkflowBundle\Event\StepEvent;
use Symfony\Component\EventDispatcher\GenericEvent;
use App\Helper\SlaResolver;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class ProductProcessNotifier
 * @package App\Notification\Process
 * @Di\Service("product_process_notifier")
 */
class ProductProcessNotifier extends ProcessNotifier
{
    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * {@inheritdoc}
     */
    public function getData($event)
    {
        /** @var Requisition $requisition */
        if ($event instanceof StepEvent) {
            $requisition = $event->getModel()->getWorkflowObject();
        }

        if ($event instanceof GenericEvent) {
            $requisition = $event->getSubject();
        }

        /** @var Contract $contract */
        $contract = $requisition->getContract();
        /** @var DateTime $responseSla */
        $responseSla = $this->slaResolver->getTotalSla($requisition,$contract);

        return [
            'requisition' => $requisition,
            'delivery_address' => $requisition->getMetadata('delivery_address_2'),
            'contract'    => $contract,
            'approver'    => $contract->getProduct()->getApprover(),
            'supplier'    => $contract->getSupplier()->getRepresentatives()->get(0),
            'requester'   => $requisition->getRequester(),
            'responseTime'=> $responseSla
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getLookupTable()
    {
        return [
            'product.created.reached'   => ['all', 'requester', 'approver'],
            'product.approved.reached'  => ['all', 'requester', ['requester', 'supplier']],
            'product.rejected.reached'  => ['all', 'requester', 'requester'],
            //'product.appeal.reached'    => ['all', 'requester', 'approver'],
            //'product.denied.reached'    => ['all', 'requester', 'requester'],
            'product.accepted.reached'  => ['all', 'requester', 'requester'],
            //'product.confirmed.reached' => ['all', 'requester', 'requester'],
            //'product.closed.reached'  => ['all', 'requester', 'requester'],
            'product.declined.reached'  => ['all', 'requester', 'requester'],
            'product.hold.reached'      => ['all', 'requester', 'approver'],
            'product.reviewed.reached'  => ['all', 'requester', 'requester'],
            //'product.evaluated.reached' => ['all', 'requester', 'supplier'],
            //'product.returned.reached'  => ['all', 'approver', ['approver', 'supplier']],
            //'product.delivered.reached'  => ['all', 'requester', 'supplier'],
            //'product.completed.reached' => ['all', 'requester', 'requester'],
        ];
    }
}