<?php

namespace App\Notification;

use App\Entity\User;

interface MessageInterface
{
    public function setSource($source);
    public function getSource();
    public function setSender(User $sender);
    public function getSender();
    public function addReceiver(User $receiver);
    public function getReceivers();
    public function setReceiver(User $receiver);
    public function getReceiver();
    public function setData($data);
    public function getData();
    public function setSubject($subject);
    public function getSubject();
    public function setChannel($channel);
    public function getChannel();
}