<?php

namespace App\Entity;

use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Loggable
 * @Gedmo\SoftDeleteable
 * @ORM\Table(name="contracts")
 * @ORM\Entity(repositoryClass="App\Repository\ContractRepository")
 * @Serializer\ExclusionPolicy("ALL")
 */
class Contract
{
    use SoftDeleteableEntity;
    use BlameableEntity;
    use TimestampableEntity;
    
    const TYPE_DESCENTRALIZED = 'type.descentralized';
    const TYPE_CENTRALIZED    = 'type.centralized';

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var string
     * @Gedmo\Versioned()
     * @ORM\Column(type="string", nullable=true)
     * @Serializer\Expose()
     */
    protected $costCenter;

    /**
     * TODO: Quitar. Atributo pertenece a Product
     * @ORM\Column(type="boolean")
     * @Serializer\Expose()
     */
    protected $centralized = true;

    /**
     * @var Product
     * @Gedmo\Versioned
     * @Assert\NotNull(message="El producto no existe")
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $product;

    /**
     * @var Supplier
     * @Assert\NotNull(message="El proveedor no existe")
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="App\Entity\Supplier", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @Serializer\Expose()
     */
    protected $supplier;

    /**
     * @Gedmo\Versioned
     * @Assert\NotNull(message="La sucursal no existe")
     * @ORM\ManyToOne(targetEntity="App\Entity\Office", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     * @Serializer\Expose()
     */
    protected $office;

    /**
     * @Gedmo\Versioned
     * @ORM\Embedded(class="App\Entity\DateRange")
     * @Serializer\Expose()
     * @Assert\NotNull(message="Fecha de inicio y/o termino no son válidas")
     * @Assert\Valid()
     */
    protected $validity;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="boolean")
     * @Serializer\Expose()
     */
    protected $expired = false;

    /**
     * @Gedmo\Versioned
     * @ORM\Embedded(class="App\Entity\Money")
     * @Serializer\Expose()
     * @Assert\Valid()
     */
    protected $price;

    /**
     * @var string
     * @Gedmo\Versioned
     * @Assert\NotBlank(message="El numero de contrato no puede estar en blanco")
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $contractNumber;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Serializer\Expose()
     */
    protected $purchaseAgreement;

    /**
     * @var array
     * @Gedmo\Versioned()
     * @Assert\NotBlank(message="Uno o más SLA's estan en blanco o no son válidos")
     * @ORM\Column(type="array", nullable=true)
     * @Assert\Valid()
     */
    protected $slas = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCostCenter()
    {
        return $this->costCenter;
    }

    /**
     * @param string $costCenter
     */
    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;
    }

    /**
     * @return mixed
     */
    public function getCentralized()
    {
        return $this->centralized;
    }

    /**
     * @param mixed $centralized
     */
    public function setCentralized($centralized)
    {
        $this->centralized = $centralized;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return mixed
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @param mixed $office
     */
    public function setOffice($office)
    {
        $this->office = $office;
    }

    /**
     * @return mixed
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * @param mixed $validity
     */
    public function setValidity($validity)
    {
        $this->validity = $validity;
    }

    /**
     * @return mixed
     */
    public function getExpired()
    {
        return $this->expired;
    }

    /**
     * @param mixed $expired
     */
    public function setExpired($expired)
    {
        $this->expired = $expired;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getContractNumber()
    {
        return $this->contractNumber;
    }

    /**
     * @param string $contractNumber
     */
    public function setContractNumber($contractNumber)
    {
        $this->contractNumber = $contractNumber;
    }

    /**
     * @return mixed
     */
    public function getPurchaseAgreement()
    {
        return $this->purchaseAgreement;
    }

    /**
     * @param mixed $purchaseAgreement
     */
    public function setPurchaseAgreement($purchaseAgreement)
    {
        $this->purchaseAgreement = $purchaseAgreement;
    }
    
    /**
     * @param $key
     * @param SLA $sla
     * @return $this
     */
    public function addSLA($key, SLA $sla)
    {
        $this->slas[$key] = $sla;
        
        return $this;
    }

    /**
     * @param $key
     */
    public function removeSLA($key)
    {
        unset($this->slas[$key]);
    }

    /**
     * @param $key
     * @param null $default
     * @return SLA|null
     */
    public function getSLA($key, $default = null)
    {
        return $this->slas[$key] ?? $default ?? null;
    }

    /**
     * @return array
     */
    public function getSlas()
    {
        return $this->slas;
    }

    /**
     * @param array $slas
     */
    public function setSlas($slas)
    {
        $this->slas = $slas;
    }
   
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->contractNumber;
    }

    /**
     * @return User|null
     */
    public function getSupplierUser()
    {
        return $this->supplier->getRepresentatives()->get(0);
    }

    
    public function getSlaProduct()
    {
        return $this->getSLA(ProductStatus::DELIVERED);
    }

    public function getSlaOrder()
    {
        return $this->getSLA(ProductStatus::ORDERED);
    }

    public function getSlaAccept()
    {
        return $this->getSLA(ProductStatus::APPROVED);
    }
    
    public function getSlaApprover()
    {
        return $this->getSLA(ProductStatus::CREATED);
    }
    
    public function getSlaApproverEval()
    {
        return $this->getSLA(ProductStatus::CONFIRMED);
    }
    
    public function getSlaRequesterEval()
    {
        return $this->getSLA(ProductStatus::REVIEWED);
    }
}
