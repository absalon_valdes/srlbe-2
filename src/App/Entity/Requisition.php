<?php

namespace App\Entity;

use ApiBundle\Model\RequisitionDetail;
use App\Form\WorkEvaluationType;
use App\Model\RequisitionInterface;
use App\Model\RequisitionTrail;
use App\Model\Transition;
use App\Workflow\Assigner\AssignableInterface;
use App\Workflow\ProcessTokenInterface;
use App\Workflow\Status\ProductStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Hateoas\Configuration\Annotation as Hateoas;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Tionvel\BudgetBundle\Utils\ChildRequisitionNumber;
use Tionvel\BudgetBundle\Utils\VirtualRequisitionStatus;

/**
 * @Serializer\ExclusionPolicy("ALL")
 * @Serializer\AccessorOrder("alphabetical")
 * @Hateoas\Relation("self", href=@Hateoas\Route("workflow_detail", parameters={"id" = "expr(object.getId())"}))
 * @Hateoas\RelationProvider(name="requisition.actions.provider:addRelations")
 * @Hateoas\Relation("product", embedded="expr(object.getProduct())")
 * @Hateoas\Relation("supplier", embedded="expr(object.getSupplierCompany())")
 * @Hateoas\Relation("office", embedded="expr(object.getOffice())")
 * @Hateoas\Relation("requester", embedded="expr(object.getEmployee())")
 * @Gedmo\Loggable
 * @Gedmo\SoftDeleteable
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="requisitions")
 * @ORM\Entity(repositoryClass="App\Repository\RequisitionRepository")
 */
class Requisition implements
    RequisitionInterface,
    ProcessTokenInterface, 
    AssignableInterface, 
    EntityInterface
{
    use SoftDeleteableEntity;
    use RequisitionTrail;

    /**
     * @todo Remover cuando se actualize el modelo de datos y las constantes
     */
    const TYPE_PRODUCT         = 'requisition.'.self::CLASS_PRODUCT;
    const TYPE_SELF_MANAGEMENT = 'requisition.'.self::CLASS_SELF_MANAGEMENT;
    const TYPE_VCARD           = 'requisition.product.'.self::CLASS_VCARD;
    const TYPE_MAINTENANCE     = 'requisition.'.self::CLASS_MAINTENANCE;
    const TYPE_UNIFORM         = 'requisition.'.self::CLASS_UNIFORM;
    const TYPE_DELIVERY        = 'requisition.'.self::CLASS_DELIVERY;
    const TYPE_WATER           = 'requisition.'.self::CLASS_WATER;

    /**
     * @todo Remover cuando se actualize el modelo de datos y las constantes
     */
    const CLOSED_STATUSES = [
        ProductStatus::CLOSED, 
        ProductStatus::TIMEOUT, 
        ProductStatus::COMPLETED,
        ProductStatus::CLOSED_BY_APPROVER
    ];
    
    /**
     * @ORM\Id @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     * @Serializer\Expose()
     */
    public $id;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $product;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Expose()
     */
    protected $number = 0;

    /**
     * @var Contract
     * @ORM\ManyToOne(targetEntity="App\Entity\Contract")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $contract;

    /**
     * @ORM\Column(type="string")
     * @Serializer\Expose()
     */
    protected $type;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="integer")
     */
    protected $quantity = 1;
    
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $requester;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comments;

    /**
     * @var User
     * @Gedmo\Versioned
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $assignee;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(type="string", length=64)
     * @Serializer\Expose()
     */
    protected $status = ProductStatus::DRAFT;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="datetime")
     */
    protected $stateDueDate;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="change", field="status", value={"product.closed", "product.completed", "product.closed_by_approver", "product.timeout"})
     */
    protected $closedAt;

    /**
     * @var array
     * @ORM\Column(type="array", nullable=true)
     */
    protected $attachment = array();

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="array", nullable=true)
     */
    protected $metadata = array();
    
    /**
     * @var Transition[]
     * @Gedmo\Versioned
     * @ORM\Column(type="array", nullable=true)
     */
    protected $transitions = array();
    
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Activity", mappedBy="process", fetch="EAGER")
     */
    protected $activities;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(type="integer")
     */
    public $lft;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(type="integer")
     */
    public $rgt;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(type="integer")
     */
    public $lvl;

    /**
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="App\Entity\Requisition")
     */
    public $root;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="App\Entity\Requisition", inversedBy="children")
     * @ORM\JoinColumn(nullable=true, name="parent_field_id", onDelete="CASCADE")
     */
    public $parent;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Requisition", mappedBy="parent")
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    public $children;

    /**
     * @var array
     */
    protected $serializedMeta = array();

    /**
     * @var bool
     */
    protected $viewed = false;

    /**
     * @var string
     */
    protected $statusLabel;

    /**
     * @var array
     * @Gedmo\Versioned()
     * @ORM\Column(type="array", nullable=true)
     */
    protected $slas = [];

    /**
     * Requisition constructor.
     */
    public function __construct()
    {
        $this->activities = new ArrayCollection;
    }

    /**
     * @return ArrayCollection
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * @param mixed $activities
     */
    public function setActivities($activities)
    {
        $this->activities = $activities;
    }

    /**
     * @param Activity $activity
     * @return bool
     */
    public function addActivity(Activity $activity)
    {
        $activity->setProcess($this);
        
        return $this->activities->add($activity);
    }

    /**
     * @return Activity
     */
    public function getCurrentActivity()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('state', Activity::STATE_ACTIVE));

        return $this->activities->matching($criteria)->first();
    }

    public function getElapsedTime()
    {
        /** @var Activity $first */
        $first = $this->activities->first();
        
        /** @var Activity $last */
        $last = $this->activities->last();
        
        if ($first === $last && Activity::STATE_ACTIVE === $first->getState()) {
            return (new \DateTime)->diff($first->getCreatedAt());
        }
        
        if (Activity::STATE_ACTIVE === $last) {
            return (new \DateTime)->diff($first->getCreatedAt());
        }
        
        if (Activity::STATE_FINISHED === $last) {
            return $last->finishedAt()->diff($first->getCreatedAt());
        }
    }

    /**
     * @param Contract $contract
     * @return Requisition
     */
    public static function createFromContract(Contract $contract)
    {
        $requisition = new self;
        $requisition->setProduct($contract->getProduct());
        $requisition->setContract($contract);
        $requisition->setStateDueDate(new \DateTime);
        
        return $requisition;
    }

    public function getComments()
    {
        return $this->comments 
            ?? $this->getMetadata('comments') 
            ?? $this->getMetadata('description');
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("inventory_codes")
     */
    public function getInventoryCodes()
    {
        $codes = $this->getMetadata('inventory_code');
        $codes = is_array($codes) ? array_values($codes) : [$codes];
        
        return count($codes) ? $codes : null;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("metadata")
     */
    public function getSerializedMeta()
    {
        return $this->serializedMeta;
    }

    public function setSerializedMeta($serializedMeta)
    {
        $this->serializedMeta = $serializedMeta;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("status_label")
     */
    public function getStatusLabel()
    {
        return $this->statusLabel;
    }

    public function setStatusLabel($statusLabel)
    {
        $this->statusLabel = $statusLabel;
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("updated")
     */
    public function isViewed()
    {
        return $this->viewed;
    }

    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
    }
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getObjectName()
    {
        return __CLASS__;
    }

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        if ('requisition.virtual' === $this->type &&
            class_exists('Tionvel\\BudgetBundle\\Utils\\VirtualRequisitionStatus')
        ) {
            if ($status = VirtualRequisitionStatus::getStatus($this)) {
                return $status;
            }
        }

        return $this->status;
    }

    /**
     * @param $status
     * @return bool
     */
    public function isStatus($status)
    {
        return $this->status === $status;
    }

    /**
     * @return mixed
     */
    public function getWorkflowData()
    {
        return $this->metadata;
    }

    /**
     * @inheritDoc
     */
    public function getProcessName()
    {
        return 'product';
    }

    /**
     * @Serializer\VirtualProperty()
     */
    public function getCostCenter()
    {
        return $this->contract->getCostCenter();
    }

    /**
     * @Serializer\VirtualProperty()
     * @Serializer\SerializedName("dates")
     */
    public function getCalculatedSlas()
    {
        return $this->getMetadata('timestamps');
    }
    
    public function getOffice()
    {
        return $this->contract->getOffice();
    }

    public function getEmployee()
    {
        return $this->requester->getEmployee();
    }

    /**
     * @param Contract|null $contract
     * @return $this
     */
    public function setContract(Contract $contract = null)
    {
        $this->contract = $contract;

        if($contract && empty($this->getSlas())){
            $this->setSlas($contract->getSlas());
        }

        return $this;
    }

    /**
     * @return Contract
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * @param User|null $requester
     * @return $this
     */
    public function setRequester(User $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * @param $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        $meta = $this->metadata;

        if (!isset($meta['details'])) {
            return $this->quantity;
        }

        if (isset($meta['details']['cantidad'])) {
            return $meta['details']['cantidad'];
        }

        if (!isset($meta['details']['caracteristicas'])) {
            return $this->quantity;
        }

        return array_sum(array_column($meta['details']['caracteristicas'], 'cantidad'));
    }

    /**
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     * @return bool
     */
    public function isType($type)
    {
        if ($this->product && $this->belongsToCategory('Impresora')) {
            return self::TYPE_MAINTENANCE === $type;
        }
            
        return $type === $this->type;
    }

    /**
     * @param Product|null $product
     * @return $this
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param string $comments
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    /**
     * @param $metadata
     * @return $this
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * @param string $key
     * @return null
     */
    public function getMetadata($key = null)
    {
        return $key ? $this->metadata[$key] ?? null : $this->metadata;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addMetadata($key, $value)
    {
        $this->metadata[$key] = $value;

        return $this;
    }

    /**
     * @param $key
     * @return bool
     */
    public function hasMetadata($key)
    {
        return isset($this->metadata[$key]);
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addSubMetadata($key, $value)
    {
        if (!$this->hasMetadata($key)) {
            $this->metadata[$key] = [];
        }

        $this->metadata[$key][] = $value;

        return $this;
    }

    /**
     * @param $key
     */
    public function removeMetadata($key)
    {
        if ($this->hasMetadata($key)) {
            unset($this->metadata[$key]);
        }
    }

    /**
     * @param $number
     * @return $this
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        if ($this->hasMetadata('virtual_number')) {
            return $this->getMetadata('virtual_number');
        }

        // Numero de solicitud agrupado por proveedor
        if ($this->parent && $this->parent->getChildren()) {
            if (class_exists('Tionvel\\BudgetBundle\\Utils\\ChildRequisitionNumber')) {
                if ($number = ChildRequisitionNumber::getNumber($this)) {
                    return $number;
                }
            }
        }

        return $this->number;
    }

    /**
     * @return mixed
     */
    public function isCentralized()
    {
        if ($this->isType(Requisition::TYPE_VCARD)) {
            return false;    
        }
        
        if ($this->isType(Requisition::TYPE_MAINTENANCE)) {
            return $this->product->isCentralized();
        }
        
        return (bool) $this->product->getApprover();
    }

    /**
     * @param $stateDueDate
     * @return $this
     */
    public function setStateDueDate($stateDueDate)
    {
        $this->stateDueDate = $stateDueDate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStateDueDate()
    {
        return $this->stateDueDate;
    }

    /**
     * Set closedAt
     *
     * @param \DateTime $closedAt
     *
     * @return Requisition
     */
    public function setClosedAt($closedAt)
    {
        $this->closedAt = $closedAt;

        return $this;
    }

    /**
     * Get closedAt
     *
     * @return \DateTime
     */
    public function getClosedAt()
    {
        return $this->closedAt;
    }

    /**
     * @param UserInterface $assignee
     * @return Requisition
     */
    public function setAssignee(UserInterface $assignee = null)
    {
        $this->assignee = $assignee;

        return $this;
    }

    /**
     * Get asignee
     *
     * @return \App\Entity\User
     */
    public function getAssignee()
    {
        return $this->assignee;
    }

    /**
     * @return array
     */
    public function getAttachment()
    {
        return array_filter($this->attachment ?? array());
    }

    /**
     * @param array $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * @return mixed
     */
    public function getTransitions()
    {
        return $this->transitions ?? array();
    }

    /**
     * @param Transition $transition
     * @return $this
     */
    public function addTransition(Transition $transition)
    {
        $this->transitions[] = $transition;
        
        return $this;
    }

    /**
     * @return void
     */
    public function resetInboxStatus()
    {
        $this->metadata['inbox_status'] = [];
    }

    /**
     * @param User $user
     * @return bool
     */
    public function getInboxStatus(User $user)
    {
        $statuses = $this->metadata['inbox_status'] ?? [];
        
        return isset($statuses[$user->getId()]) ? null : sprintf('%s.inbox', $this->status);
    }

    /**
     * @param User $user
     */
    public function clearInboxStatus(User $user)
    {
        $uid = $user->getId();
        
        if (isset($this->metadata['inbox_status'][$uid])) {
            return;
        }
        
        $this->metadata['inbox_status'][$uid] = true;
    }

    /**
     * @return bool
     */
    public function hasProblems()
    {
        if (!$this->hasMetadata('evaluation')) {
            return false;
        } 
        
        $metadata = $this->getMetadata('evaluation');
        
        if (!is_array($metadata) || array_key_exists('evaluation', $metadata)) {
            return false;
        }
        
        foreach ($metadata as $evaluation) {
            if (WorkEvaluationType::EVALUATION_NOT_OK === $evaluation['evaluation']) {
                return true;
            }
        }
        
        return false;
    }

    /**
     * @return User
     */
    public function getApprover()
    {
        return $this->product->getApprover();
    }

    /**
     * @return User
     */
    public function getSupplier()
    {
        if (!$suppliers = $this->contract->getSupplier()) {
            return null;
        }
        
        if (!$representatives = $suppliers->getRepresentatives()) {
            return null;
        }
        
        return $representatives->get(0);
    }

    public function getSupplierCompany()
    {
        return $this->contract->getSupplier();
    }

    /**
     * @param string $status
     * @return Transition|null
     */
    public function getTransition(string $status, $all = false)
    {
        $transitions = [];
        
        /** @var Transition $transition */
        foreach ($this->getTransitions() as $transition) {
            if ($transition->getStatus() === $status) {
                $transitions[] = $transition;
            }    
        }

        if($all){
            return $transitions;
        }
        return end($transitions);
    }

    /**
     * @param $a
     * @param $b
     * @return bool|\DateInterval|null
     */
    public function getTransitionsInterval($a, $b)
    {
        $x = $this->getTransition($a);
        $y = $this->getTransition($b);
        
        if ($x instanceof Transition && $y instanceof Transition) {
            return $x->getDatetime()->diff($y->getDatetime());
        }
        
        return null;
    }

    /**
     * @return Transition|null
     */
    public function getLastTransition()
    {
        return end($this->transitions);
    }

    /**
     * @return SLA|SLA[]|null
     */
    public function getCurrentStateSLA()
    {
        return $this->contract->getSla($this->getStatus());
    }

    /**
     * @return \DateInterval|null
     */
    public function getJobCompletionInterval()
    {
        if (!$started = $this->getTransition(ProductStatus::ACCEPTED)) {
            return null;
        }

        if (!$finished = $this->getJobCompletionDatetime()) {
            return null;
        }

        $started = $started->getDatetime();
        $finished = $this->getJobCompletionDatetime();
        
        return $finished->diff($started);
    }

    /**
     * @return \DateTime|null
     */
    public function getJobCompletionDatetime()
    {
        $returned = $this->getTransition(ProductStatus::RETURNED);
        
        if ($returned && $this->getLastTransition() === $returned) {
            return $returned->getDatetime();
        }
        
        if ($evaluation = $this->getMetadata('evaluation')) {
            $evaluation = end($evaluation);
            
            // BC fix
            if ($evaluation instanceof \DateTime) {
                return $evaluation;
            }
            
            if ($evaluation['evaluated_at'] instanceof \DateTime) {
                return $evaluation['evaluated_at'];
            }
        }

        if ($confirmation = $this->getMetadata('confirmation')) {
            if (($deliveredAt = $confirmation['delivered_at']) instanceof \DateTime) {
                return $deliveredAt;
            }
            
            if ($confirmed = $this->getTransition(ProductStatus::CONFIRMED)) {
                return $confirmed->getDatetime();
            }
            
            if ($reviewed = $this->getTransition(ProductStatus::REVIEWED)) {
                return $reviewed->getDatetime();
            }
        }        
        
        return null;
    }

    /**
     * @param $name
     * @return bool
     */
    public function belongsToCategory($name)
    {
        return $this->product->belongsToCategory($name);
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return in_array($this->getStatus(), self::CLOSED_STATUSES, true);
    }

    /**
     * @return bool
     */
    public function isRejected()
    {
        return (bool) $this->getTransition(ProductStatus::REJECTED);
    }
    
    /**
     * @return bool
     */
    public function inProgress()
    {
        return $this->getTransition(ProductStatus::ORDERED) || $this->getTransition(ProductStatus::ACCEPTED);
    }


    /**
     * @param null $status
     * @return string
     */
    public function getTranslatableStatus($status = null)
    {
        return sprintf('%s.%s.%s', $status ?? $this->status, 
            str_replace('requisition.', '', $this->type), 
            $this->getContract()->getCentralized() ? 'centralized' : 'descentralized'
        );
    }

    public function isRequester(User $user)
    {
        return $user === $this->requester;
    }

    public function isApprover(User $user)
    {
        return $user === $this->getApprover();
    }

    public function isSupplier(User $user)
    {
        return $user === $this->getSupplier();
    }

    public function getUserRole(User $user)
    {
        if ($this->isRequester($user)) {
            return 'requester';
        }
        
        if ($this->isApprover($user)) {
            return 'approver';
        }
        
        if ($this->isSupplier($user)) {
            return 'supplier';
        }
        
        return null;
    }

    public function setTransitions($transitions){
        $this->transitions = $transitions;
    }


    /**
     * @param Supplier|null $supplier
     * @return $this
     */
    public function setShipmentSupplier(Supplier $supplier = null)
    {
        $this->shipmentSupplier = $supplier;

        return $this;
    }

    /**
     * @return Supplier
     */
    public function getShipmentSupplier()
    {
        return $this->shipmentSupplier;
    }

    /**
     * @return mixed
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * @param mixed $lft
     */
    public function setLft($lft)
    {
        $this->lft = $lft;
    }

    /**
     * @return mixed
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * @param mixed $rgt
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;
    }

    /**
     * @return mixed
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * @param mixed $lvl
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * @param mixed $root
     */
    public function setRoot($root)
    {
        $this->root = $root;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }


    /**
     * @param $key
     * @param null $default
     * @return SLA|null
     */
    public function getSLA($key, $default = null)
    {
        return $this->slas[$key] ?? $default ?? null;
    }

    /**
     * @return array
     */
    public function getSlas()
    {
        return $this->slas;
    }

    /**
     * @param array $slas
     */
    public function setSlas($slas)
    {
        $this->slas = $slas;
    }

    public function getSlaProduct()
    {
        return $this->getSLA(ProductStatus::DELIVERED);
    }

    public function getSlaOrder()
    {
        return $this->getSLA(ProductStatus::ORDERED);
    }

    public function getSlaAccept()
    {
        return $this->getSLA(ProductStatus::APPROVED);
    }

    public function getSlaApprover()
    {
        return $this->getSLA(ProductStatus::CREATED);
    }

    public function getSlaApproverEval()
    {
        return $this->getSLA(ProductStatus::CONFIRMED);
    }

    public function getSlaRequesterEval()
    {
        return $this->getSLA(ProductStatus::REVIEWED);
    }

    /**
     * @return bool
     */
    public function isMaintenance()
    {
        return $this->isType(Requisition::TYPE_MAINTENANCE);
    }

    /**
     * @return bool
     */
    public function withoutApprover()
    {
        /** @var Contract $contract */
        $contract = $this->getContract();

        /** @var Product $product */
        $product = $contract->getProduct();

        $centralized = $contract->getCentralized();

        if ($this->getParent()) {
            return true;
        }

        if ($this->isType(Requisition::TYPE_MAINTENANCE)) {
            return !$product->isCentralized();
        }

        return !$centralized;
    }

    /**
     * @return bool
     */
    public function isEvaluationOk()
    {
        $evaluation = $this->getMetadata('evaluation');

        if (null === $evaluation) {
            return false;
        }

        return WorkEvaluationType::EVALUATION_REJECT !== end($evaluation)['evaluation'];
    }

    /**
     * @return bool
     */
    public function revalidateNext()
    {
        if ($this->isType(Requisition::TYPE_MAINTENANCE)) {
            return false; // -> reviewed
        }
        return !$this->withoutApprover();
    }
}
