<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * @ORM\Table(name="declined_requisitions")
 * @ORM\Entity(repositoryClass="App\Repository\DeclinedRequisitionRepository")
 * @Gedmo\SoftDeleteable()
 */
class DeclinedRequisition
{
    use SoftDeleteableEntity;
    
    /**
     * @ORM\Id @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Requisition")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     *
     * @var Requisition
     */
    protected $requisition;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     *
     * @var User
     */
    protected $user;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     *
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     *
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * Get id
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set requisition
     *
     * @param \App\Entity\Requisition $requisition
     *
     * @return DeclinedRequisition
     */
    public function setRequisition(Requisition $requisition = null)
    {
        $this->requisition = $requisition;

        return $this;
    }

    /**
     * Get requisition
     *
     * @return \App\Entity\Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * Set user
     *
     * @param \App\Entity\User $user
     *
     * @return DeclinedRequisition
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return DeclinedRequisition
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return DeclinedRequisition
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
