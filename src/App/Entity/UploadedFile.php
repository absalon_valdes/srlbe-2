<?php

namespace App\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile as BaseUploadedFile;

class UploadedFile implements \JsonSerializable
{
    protected $name;
    protected $size;
    protected $mimeType;
    protected $originalName;

    public static function createFromFile(BaseUploadedFile $file, $dest)
    {
        $orgName = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $storageName = sha1($orgName.time()).($ext ? '.'.$ext : null);
        
        $file->move($dest, $storageName);
        
        $uploaded = new self;
        $uploaded->setName($storageName);
        $uploaded->setOriginalName($orgName);
        $uploaded->setSize($file->getSize());
        $uploaded->setMimeType($file->getClientMimeType());
        
        return $uploaded;
    }
    
    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @param mixed $originalName
     */
    public function setOriginalName($originalName)
    {
        $this->originalName = $originalName;
    }

    /**
     * @return mixed
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param mixed $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'size' => $this->size,
            'mimeType' => $this->mimeType,
            'originalName' => $this->originalName
        ];
    }
}