<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class WorkflowActivity
 * @ORM\Entity(repositoryClass="App\Repository\ActivityRepository")
 * @ORM\Table(name="workflow_activities")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable()
 */
class Activity
{
    const STATE_ACTIVE = 'state.active';
    const STATE_FINISHED = 'state.finished';
    
    use AccesorTraits;
    use BlameableEntity;
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var string
     * @ORM\Id @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $workflow;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @var Requisition
     * @ORM\ManyToOne(targetEntity="App\Entity\Requisition", inversedBy="activities", cascade={"all"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $process;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $state;

    /**
     * @var WorkflowActivity
     * @ORM\OneToOne(targetEntity="App\Entity\Activity")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $previous;

    /**
     * @var WorkflowActivity
     * @ORM\OneToOne(targetEntity="App\Entity\Activity")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $next;
    
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $assignee;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $startedAt;
    
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $finishedAt;
    
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\ActivityAttribute", mappedBy="activity", cascade={"all"})
     */
    protected $attributes;

    /**
     * Activity constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime;
        $this->attributes = new ArrayCollection;
    }

    /**
     * @return Activity
     */
    public static function create()
    {
        $instance = new self;
        $instance->start();
        
        return $instance;
    }

    /**
     * @return void
     */
    public function start()
    {
        $this->startedAt = new \DateTime;
        $this->state = self::STATE_ACTIVE;
    }

    /**
     * @return void
     */
    public function finish()
    {
        $this->finishedAt = new \DateTime;
        $this->state = self::STATE_FINISHED;
    }

    /**
     * @return bool|\DateInterval
     */
    public function elapsed()
    {
        if (null === $this->finishedAt) {
            return (new \DateTime)->diff($this->createdAt);
        }
        
        return $this->finishedAt->diff($this->createdAt);
    }

    /**
     * @param Activity $previous
     * @return $this
     */
    public function setPrevious(Activity $previous)
    {
        $previous->setNext($this);
        $previous->finish();
        
        $this->previous = $previous;
        
        return $this;
    }

    /**
     * @param $key
     * @return boolean
     */
    public function hasAttribute($key)
    {
        return !$this->attributeQuery($key)->isEmpty();
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getAttribute($key)
    {
        return $this->attributeQuery($key)->first();
    }

    /**
     * @param $key
     * @return mixed
     */
    private function attributeQuery($key)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('key', $key))
        ;
        
        return $this->attributes->matching($criteria);
    }
}