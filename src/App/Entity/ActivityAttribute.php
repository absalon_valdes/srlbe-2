<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class ActivityAttributes
 * @ORM\Entity(repositoryClass="App\Repository\ActivityAttributeRepository")
 * @ORM\Table(name="workflow_activity_attributes")
 * @Gedmo\Loggable()
 * @Gedmo\SoftDeleteable()
 */
class ActivityAttribute
{
    use AccesorTraits;
    use BlameableEntity;
    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @var string
     * @ORM\Id @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Activity
     * @ORM\ManyToOne(targetEntity="App\Entity\Activity", inversedBy="attributes", fetch="EAGER")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $activity;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $key;

    /**
     * @var string
     * @ORM\Column(type="object")
     */
    protected $value;

    /**
     * @param $type
     * @param $key
     * @param $value
     * @return ActivityAttribute
     */
    public static function create($type, $key, $value)
    {
        $instance = new self;
        $instance->setType($type);
        $instance->setKey($key);
        $instance->setValue($value);
        
        return $instance;
    }
}