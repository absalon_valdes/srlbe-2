<?php 

namespace App\Entity;

use App\Model\Company;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="pep_grants")
 * @ORM\Entity(repositoryClass="App\Repository\PepGrantRepository")
 */
class PepGrant
{
    const FORM_NUMBER_START = 4999;

    const MODE_PRIVATE  = 'pep.private';
    const MODE_DIRECT   = 'pep.direct';
    const MODE_PERSON   = 'pep.person';

    const RANGE_GE_2KUF = 'pep.ge_2kuf';
    const RANGE_LT_2KUF = 'pep.lt_2kuf';

    /**
     * @ORM\Id @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $status;

    /**
     * @ORM\Column(type="string")
     */
    protected $purchaseMode;

    /**
     * @ORM\Column(type="string")
     */
    protected $purchaseRange;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\Column(type="array")
     */
    protected $companies;

    /**
     * @ORM\Column(type="array")
     */
    protected $certification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $requester;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    protected $updatedAt;

    /**
     * PepGrant constructor.
     */
    public function __construct()
    {
        $this->companies = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFormNumber()
    {
        return sprintf("%'.08d", $this->id + self::FORM_NUMBER_START);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFormNumber();
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $purchaseMode
     * @return $this
     */
    public function setPurchaseMode($purchaseMode)
    {
        $this->purchaseMode = $purchaseMode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseMode()
    {
        return $this->purchaseMode;
    }

    /**
     * @param $purchaseRange
     * @return $this
     */
    public function setPurchaseRange($purchaseRange)
    {
        $this->purchaseRange = $purchaseRange;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPurchaseRange()
    {
        return $this->purchaseRange;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $companies
     * @return $this
     */
    public function setCompanies($companies)
    {
        $this->companies = $companies;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @param $certification
     * @return $this
     */
    public function setCertification($certification)
    {
        $this->certification = $certification;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCertification()
    {
        return $this->certification;
    }

    /**
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param User|null $requester
     * @return $this
     */
    public function setRequester(User $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * @param $rut
     * @return mixed
     */
    public function getCompanyByRut($rut)
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('rut', $rut));

        return $this->companies->matching($criteria)->first();
    }

    /**
     * @param Company $selected
     * @return $this
     */
    public function selectCompany(Company $selected)
    {
        $isSelected = function($company) use ($selected) {
            return $company->setSelected($company === $selected);
        };

        $this->companies = $this->companies->map($isSelected);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSelectedCompany()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('selected', true));

        return $this->companies->matching($criteria)->first();
    }

    /**
     * @return bool
     */
    public function hasPep()
    {
        $companies = clone $this->companies;
        
        $companies->filter(function (Company $company) {
            return $company->hasPeps();
        });
        
        return !!$companies->count();
    }

    /**
     * @return string
     */
    public function shortDescription()
    {
        return strlen($this->description) > 100 ? 
            substr($this->description, 0, 100).'...' : $this->description;
    }
}
