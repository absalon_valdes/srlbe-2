<?php

namespace App\Entity;

use App\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class DateRange
 * @package App\Entity
 *
 * @Assert\DateRange()
 * @ORM\Embeddable
 */
class DateRange
{
    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $start;

    /**
     * @var \DateTime
     * @ORM\Column(type="date")
     */
    protected $end;

    /**
     * DateRange constructor.
     * @param \DateTime $start
     * @param \DateTime $end
     */
    public function __construct(\DateTime $start, \DateTime $end)
    {
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * @param \DateTime $start
     * @param \DateTime $end
     * @return DateRange|null
     */
    public static function create($start, $end)
    {
        if (!$start instanceof \DateTime || !$end instanceof \DateTime) {
            return null;
        }

        return new self($start, $end);
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s - %s', $this->start->format('d/m/Y H:i:s'), $this->end->format('d/m/Y H:i:s'));
    }
}