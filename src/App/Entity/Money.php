<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Money
 * @ORM\Embeddable
 */
class Money
{
    const CURRENCY_UF  = 'currency.uf';
    const CURRENCY_CLP = 'currency.clp';
    const CURRENCY_USD = 'currency.usd';

    /**
     * @var integer
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric", message="El valor {{ value }} debe ser numérico")
     */
    protected $value;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Choice(
     *     choices={"currency.usd", "currency.clp", "currency.uf"},
     *     message="El tipo de moneda no es válido"
     * )
     */
    protected $currency;

    /**
     * Money constructor.
     * @param float|int $value
     * @param string $currency
     */
    public function __construct($value, string $currency)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    /**
     * @param float|int $value
     * @param string $currency
     * @return Money
     */
    public static function create($value, string $currency)
    {
        return new self($value, $currency);
    }

    /**
     * @param $string
     * @return Money
     */
    public static function fromString($string)
    {
        $type = self::getMoneyType($string);
        $value = str_replace('/\D/', '', $string);

        return self::create($value, $type);
    }

    /**
     * @param $money
     * @return string
     */
    public static function getMoneyType($money)
    {
        if (0 === stripos(strtolower($money), 'peso')) {
            return Money::CURRENCY_CLP;
        }
        
        if (0 === stripos(strtolower($money), 'usd')) {
            return Money::CURRENCY_USD;
        }
        
        if (0 === stripos(strtolower($money), 'uf')) {
            return Money::CURRENCY_UF;
        }
        
        return 'undefined';
    }
    
    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s %s', $this->value, $this->currency);
    }
}