<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateVcardRequisitionsCommand
 *
 * * * * * * /usr/local/bin/php /www/xxx/bin/console srl:update:vcards
 */
class UpdateVcardRequisitionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('srl:update:vcards');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('update_vcard_requisitions')->update();
    }
}