<?php

namespace App\Command;

use App\Entity\Employee;
use App\Entity\Requisition;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Dump\Container;

class FixRequisitionMetadataCommand extends ContainerAwareCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('fix:requisition:metadata')
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $c = $this->getContainer();
        $em = $c->get('doctrine')->getManager();
        /** @var Requisition $requisition */
        $requisition = $em->getRepository('App:Requisition')->findOneByNumber(18406);
        $metadata = $requisition->getMetadata();
        $transition = $requisition->getTransitions();
        $metadata['evaluation'][0]['evaluated_at'] = $transition[8]->getDateTime();
        $requisition->setMetadata($metadata);
        $em->flush();
    }
}
