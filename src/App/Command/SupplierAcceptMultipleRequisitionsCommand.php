<?php

namespace App\Command;

use App\Entity\Requisition;
use App\Helper\UserHelper;
use App\Notification\MessageInterface;
use App\Notification\NotificationManager;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\EventDispatcher\EventDispatcherInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use Doctrine\DBAL\Types\Type;
use App\Model\Transition;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class SupplierAcceptMultipleRequisitionsCommand extends ContainerAwareCommand
{
    /** @var  EntityManager**/
    protected $em;

    /**
     * @var RequisitionRepository
     */
    protected $requisitionRepo;

    /**
     * @var ProcessInstance
     */
    public $workflow;

    /**
     * @var UserHelper
     */
    protected $userHelper;

    /**
     * @var NotificationManager
     */
    public $notificator;

    /**
     * @var TranslatorInterface
     */
    public $translator;

    /**
     * @var $suppliers
     */
    protected $suppliers = [];

    const TYPE_ACTION         = 'massive.accept.requisition';

    protected function configure()
    {
        $this->setName('srl:accept:requisitions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Container $container */
        $container = $this->getContainer();

        $this->requisitionRepo = $container->get('requisition_repo');

        $this->workflow = $container->get('workflow.product');

        $this->em = $container->get('doctrine')->getManager();

        $this->notificator = $container->get('notification_manager');

        $this->translator = $container->get('translator');

        $qb = $this->requisitionRepo->createQueryBuilder('r');

        $context = $container->get('router')->getContext();
        $context->setHost(str_replace('http://','',$container->getParameter('base_url')));
        $context->setScheme('http');
        $context->setBaseUrl($container->getParameter('base_url'));

        /** @var UserHelper userHelper */
        $this->userHelper = $container->get('user_helper');

        /** @var [] Requisition $requisitions */
        $requisitions = $qb
            ->select('r')
            ->where('r.type = :type')
            ->andWhere($qb->expr()->in('r.status', [ProductStatus::APPROVED]))
            ->orderBy('r.createdAt', 'desc')
            ->setParameter('type',Requisition::TYPE_UNIFORM)
            ->getQuery()
            ->getResult();

        $output->writeln(sprintf('Aceptando solicitudes masivamente... %s', $total = count($requisitions)));
        $progress = new ProgressBar($output, $total);
        $progress->setFormat('very_verbose');
        
        $this->em->getConnection()->beginTransaction();

        try {
            foreach ($requisitions as $requisition) {

                $requisition->addMetadata('massive_accept', new \DateTime());
                $supplierUser = $requisition->getContract()->getSupplierUser();
                $this->userHelper->impersonateUser($supplierUser->getUsername());
                if (!$this->workflow->proceed($requisition, 'accept')->getSuccessful()) {
                    throw new \Exception();
                }
                $this->requisitionRepo->save($requisition);

                if (!$this->workflow->proceed($requisition, 'deliver')->getSuccessful()) {
                    throw new \Exception();
                }

                $requisition->addMetadata('work_order', $workOrder = time());
                $this->requisitionRepo->save($requisition);

                $this->addRequisition($supplierUser);
                $progress->advance();
            }
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $output->writeln($e->getMessage());
        }

        $this->notifySuppliers();
        $progress->finish();
        $output->writeln(['', "{$total} Solicitudes aceptadas"]);
    }

    function addRequisition($supplier){
        $username = $supplier->getUsername();
        if(!array_key_exists($username,$this->suppliers)){
            $this->suppliers[$username] = ['quantity' => 1,'supplier' => $supplier];
        }else{
            $this->suppliers[$username]['quantity'] = ++$this->suppliers[$username]['quantity'];
        }
    }

    function notifySuppliers(){
        foreach ($this->suppliers as $s){
            /** @var MessageInterface $message */
            $message = $this->notificator->createMessage()
                ->setSubject($this->translator->trans(self::TYPE_ACTION))
                ->setSource(self::TYPE_ACTION)
                ->setData($s)
                ->setChannel('email')
                ->setSender($s["supplier"])
                ->addReceiver($s["supplier"])
            ;
            $this->notificator->notify($message);
        }
    }
}