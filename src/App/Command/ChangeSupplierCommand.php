<?php

namespace App\Command;

use App\Repository\RequisitionRepository;
use App\Repository\SupplierRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

class ChangeSupplierCommand extends ContainerAwareCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('change-supplier');
    }
    
    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Container $container */
        $container = $this->getContainer();
        
        /** @var RequisitionRepository $requisitions */
        $requisitions = $container->get('requisition_repo');
    
        /** @var SupplierRepository $suppliers */
        $suppliers = $container->get('supplier_repo');
        
        $numbers = file(
            __DIR__.'/../../../drop/20170303161000/solicitudes_cambio_proveedor.txt', 
            FILE_IGNORE_NEW_LINES
        );

        $toChange = $requisitions->createQueryBuilder('r')
            ->where('r.number in (:numbers)')
            ->setParameter('numbers', $numbers)
            ->getQuery()
            ->getResult()
        ;

        $newSupplier = $suppliers->findOneByRut('89912300-K');

//	print_r([get_class($newSupplier), count($toChange)]); return;

        $changer = $container->get('requisition_supplier_changer');
        $changer->changeSupplier($toChange, $newSupplier);
    }
}
