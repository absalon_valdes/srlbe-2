<?php

namespace App\Command;

use App\Entity\SLA;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Container;

class FixWorkflowStatesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('fix-contracts');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Container $c */
        $c = $this->getContainer();
        
        /** @var Connection $db */
        $db = $c->get('database_connection');
        
        $stmt = $db->executeQuery('select * from contracts where slas is null limit 0, 125000');
        
        $toUpdate = [];
        
        foreach ($stmt->fetchAll() as $item) {
            $toUpdate[$item['id']] = serialize([
                ProductStatus::DELIVERED => SLA::create($item['sla_product_value'],  $item['sla_product_type']),
                ProductStatus::APPROVED  => SLA::create($item['sla_accept_value'],   $item['sla_accept_type']),
                ProductStatus::CREATED   => SLA::create($item['sla_approver_value'], $item['sla_approver_type']),
                ProductStatus::ORDERED   => SLA::create($item['sla_order_value'],    $item['sla_order_type']),
                ProductStatus::CONFIRMED => SLA::create($item['sla_approver_eval_value'], $item['sla_approver_eval_type']),
                ProductStatus::REVIEWED  => SLA::create($item['sla_requester_eval_value'], $item['sla_requester_eval_type']),
                'en_despacho'            => SLA::create(1, SLA::CALENDAR_HOUR),
            ]);
        }
        
        $output->writeln(sprintf('Actualizando SLAS...%s', $total = count($toUpdate)));
        $progress = new ProgressBar($output, $total);
        $progress->setFormat('very_verbose');
        
        $db->beginTransaction();

        try {
            foreach ($toUpdate as $id => $slas) {
                $db->update('contracts', ['slas' => $slas], ['id' => $id]);
                $progress->advance();
            }
            
            $db->commit();
        } catch (\Exception $e) {
            $db->rollBack();
            $output->writeln($e->getMessage());
        }
        
        $progress->finish();
        $output->writeln(['', 'SLAS actualizados']);
    }
}