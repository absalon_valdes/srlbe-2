<?php

namespace App\Command;

use App\Entity\Money;
use App\Entity\SLA;
use App\Util\RutUtils;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportContractsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('srl:import:contracts')
            ->addArgument('file', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!file_exists($file = $input->getArgument('file'))) {
            throw new \InvalidArgumentException('Fixtures file not found');
        }

        $container = $this->getContainer();
        $processor = $container->get('excel_batch_processor');
        $importer = $container->get('contracts_importer');
        
        $processor->process($file, 'catalogo_', function ($batch) use ($importer) {
            $importer->import($this->prepareContracts($batch));
        });
    }
    
    public function prepareContracts($array)
    {
        return array_map([$this, 'createFromArray'], array_filter($array, function ($item) {
            return !empty($item[0]);
        }));
    }

    private function createFromArray(array $item, $dateFormat = 'Y-m-d')
    {
        return  [
            'id_producto' => strtoupper($this->cleanString($item[0])),
            'id_proveedor' => RutUtils::normalize((int) $item[1]),
            'id_unidad' => is_numeric(trim($item[6])) ? (int) trim($item[6]) : trim($item[6]),
            'fecha_inicio' => \DateTime::createFromFormat($dateFormat, $item[3]),
            'fecha_fin' => \DateTime::createFromFormat($dateFormat, $item[4]),
            'numero_contrato' => $item[2],
            'acuerdo_compra' => $item[5], 
            'centro_costo' => $item[7],
            'precio' => $item[9],
            'moneda' => Money::getMoneyType($item[8]),
            'centralizado' => true,

            'sla_eval_tipo' => SLA::getSlaType($item[10] ?? SLA::BUSINESS_HOUR),
            'sla_eval_valor' => SLA::getSlaValue($item[10] ?? 0),
            'sla_order_tipo' => SLA::getSlaType($item[11] ?? SLA::BUSINESS_HOUR),
            'sla_order_valor' => SLA::getSlaValue($item[11] ?? 0),
            'sla_accept_tipo' => SLA::getSlaType($item[12] ?? SLA::BUSINESS_HOUR),
            'sla_accept_valor' => SLA::getSlaValue($item[12] ?? 0),
            'sla_prod_tipo' => SLA::getSlaType($item[13] ?? SLA::BUSINESS_HOUR),
            'sla_prod_valor' => SLA::getSlaValue($item[13] ?? 0),
            'sla_delivery_tipo' => SLA::getSlaType($item[14] ?? SLA::BUSINESS_HOUR),
            'sla_delivery_valor' => SLA::getSlaValue($item[14] ?? 0),
/*
            'sla_eval_tipo' => SLA::getSlaType($item[10]),
            'sla_eval_valor' => SLA::getSlaValue($item[10]),
            'sla_order_tipo' => SLA::getSlaType($item[11]),
            'sla_order_valor' => SLA::getSlaValue($item[11]),
            'sla_accept_tipo' => SLA::getSlaType($item[12]),
            'sla_accept_valor' => SLA::getSlaValue($item[12]),
            'sla_prod_tipo' => SLA::getSlaType($item[13]),
            'sla_prod_valor' => SLA::getSlaValue($item[13]),
            'sla_delivery_tipo' => SLA::getSlaType($item[14]),
            'sla_delivery_valor' => SLA::getSlaValue($item[14]),
*/
        ];
    }
    private function cleanString($text)
    {
        return trim(preg_replace('/[^A-Za-z0-9\-\_]/', '', $text));
    }

}
