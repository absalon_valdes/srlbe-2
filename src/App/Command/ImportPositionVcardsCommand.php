<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportPositionVcardsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('srl:import:vcards-pos')
            ->addArgument('file', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$file = $input->getArgument('file')) {
            throw new \InvalidArgumentException(sprintf('File %s not found', $file));
        }
        
        $type = \PHPExcel_IOFactory::identify($file);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($file);
        
        $data = $excel->getActiveSheet()->toArray();
        
        array_shift($data);
        
        $data = array_map(function ($i) {
            return [
                'position' => strtoupper(str_replace([' ', '-', '_'], '', $i[0])),
                'vcard' => $i[1]
            ];
        }, $data);
        
        $grouped = array_map(function ($i) {
            return array_column($i, 'vcard');
        }, array_group_by($data, 'position'));

        $varDir = $this->getContainer()->getParameter('kernel.var_dir');
        $cache = sprintf('%s/data/vcards.php.cache', $varDir);

        file_put_contents($cache, sprintf('<?php return %s;', var_export($grouped, true)));
    }
}