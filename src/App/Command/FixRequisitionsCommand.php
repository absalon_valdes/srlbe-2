<?php

namespace App\Command;

use App\Entity\Requisition;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FixRequisitionsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('fix-requisitions');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $c = $this->getContainer();
        
        /** @var EntityManager $em */
        $em = $c->get('doctrine.orm.entity_manager');
        
        /** @var Connection $pdo */
        $pdo = $c->get('database_connection');

        $stmt = $pdo->prepare('select id from requisitions');
        $stmt->execute();
        
        foreach ($stmt->fetchAll() as $index => $req) {
            echo $index.' - '.$req['id'].PHP_EOL;
            $em->getRepository(Requisition::class)->find($req['id']);
        }
    }
}
