<?php

namespace App\Command;

use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UpdateProductDescriptionCommand
 */
class UpdateProductDescriptionCommand extends ContainerAwareCommand
{
    private $instruction = 'La presente solicitud no constituye una ejecución obligatoria. Esta debe ser confirmada luego de que el proveedor realice la visita y entregue la cotización (en caso de ser necesaria la visita). Recuerde que este proceso debe ser gestionado por usted con el proveedor, con cargo al presupuesto de su sucursal (obras menores u operacional, según sea el caso). Para evitar inconvenientes al momento de realizar el pago, le sugerimos revisar el saldo disponible a la fecha antes de ingresar una solicitud.';

    private $description = 'La presente ficha representa una SOLICITUD DE COTIZACIÓN.
Los servicios solicitados serán cargados al centro de costos de la sucursal, ya que corresponde a una compra descentralizada.

Por ejemplo, si solicita pintura o gasfitería, este servicio debe ser cancelado con el presupuesto de obras menores del que dispone cada sucursal.

Una vez emitida esta Solicitud de Cotización, debe coordinar la visita con el proveedor.

Le recordamos que, de acuerdo a la normativa vigente, cada vez que realice una compra debe preparar la documentación reglamentaria.';

    private $em;

    protected function configure()
    {
        $this->setName('srl:update:products');
        $this->addArgument('file', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $file = $input->getArgument('file');

        if (null === $file) {
            throw new \InvalidArgumentException;
        }

        $codes = $this->getCodesFromFile($file);

        if (null === $codes) {
            throw new \InvalidArgumentException;
        }

        foreach($codes as $code) {
            $output->writeln("Actualizando producto {$code}");
            if(!$this->updateProduct($code)) {
                $output->writeln("El producto {$code} no existe");
            }
        }
        $this->em->flush();
    }

    private function getCodesFromFile($filename)
    {
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        $toArray = $worksheet ? $worksheet->toArray() : array();
        array_shift($toArray);

        $toArray = array_filter($toArray, function ($item) {
            return !empty($item[0]);
        });

        $codes = [];
        foreach (array_filter($toArray) as $item) {
            $codes[] = $item[0];
        }

        return array_filter($codes);
    }

    private function updateProduct($code) {

        /** @var Product $product */
        $product = $this->em->getRepository(Product::class)->findOneByCode($code);

        if(!$product)
            return false;

        $product->setDescription($this->description);
        $product->setInstructions($this->instruction);

        $this->em->persist($product);

        return true;
    }
}