<?php

namespace App\Command;

use App\Import\ProductImporter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportProductsCommand extends ContainerAwareCommand
{
    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('srl:import:products')
            ->addOption('source', null, InputOption::VALUE_OPTIONAL)
            ->addOption('images', null, InputOption::VALUE_OPTIONAL)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getOption('source');
        $images = $input->getOption('images');
        
        if (null === $source && null === $images) {
            throw new \InvalidArgumentException;
        }
        
        if (null !== $images) {
            ProductImporter::setImagesDirectory($images);
        }
        
        if (null !== $source) {
            $data = $this->processSource($source);
        } else {
            ProductImporter::setOnlyImages(true);
            $data = $this->getProductCodes();
        }
        
        $this->getContainer()->get('product_importer')->import($data);
    }

    private function getProductCodes()
    {
        $pdo = $this->getContainer()->get('doctrine.dbal.default_connection');
        
        $products = array_map(function ($product) {
            $product['imagenes'] = $product['documentos'] = '';
            
            return $product;
        }, $pdo->fetchAll('select code as codigo_tecnico from products'));
        
        return ['data' => $products];
    }

    /**
     * @param $filename
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    private function processSource($filename)
    {
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        $toArray = $worksheet ? $worksheet->toArray() : array();
        array_shift($toArray);
        
        $toArray = array_filter($toArray, function ($item) {
            return !empty($item[0]);
        });
        
        $products = [];
        foreach (array_filter($toArray) as $item) {
            
            if (!isset($nuevos[$item[0]])) {
                $nuevos[$item[0]] = [];
            }
            
            $products[$item[0]][] = [
                'guid'                      => null,
                'nombre'                    => $item[2],
                'categoria'                 => preg_replace('/\s+/', ' ',$item[0]),
                'codigo_tecnico'            => $item[1],
                'especificaciones_tecnicas' => $item[7],
                'instrucciones'             => $item[6],
                'evaluador'                 => empty($item[5]) ? null : $item[5],
                'precauciones'              => '',
                'descripcion'               => $item[3] ?? 'Sin descripción',
                'imagenes'                  => '',
                'documentos'                => '',
                'normas_aplicacion'         => '',
                'centralizado'              => strtolower(trim($item[4])) === 'con'
            ]; 
        }
        
        return array_filter($products);
    }
}