<?php

namespace App\Command;

use App\Util\RutUtils;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ImportSuppliersCommand extends ContainerAwareCommand
{
    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this
            ->setName('srl:import:suppliers')
            ->addArgument('file', InputArgument::OPTIONAL)
        ;
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $source = $input->getArgument('file');
        $data = $this->processSource($source);
        
        $this->getContainer()
            ->get('supplier_importer')
            ->import($data)
        ;
    }

    /**
     * @param $filename
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    private function processSource($filename)
    {
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(false);
        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        $toArray = $worksheet ? $worksheet->toArray() : array();
        array_shift($toArray);
        
        $toArray = array_filter($toArray, function ($item) {
            return !empty($item[0]);
        });
        
        $toArray = array_map([$this, 'parseSupplier'], $toArray);
        
        $suppliers = [];
        foreach (array_filter($toArray) as $supplier) {
            $suppliers[$supplier['rut']] = $supplier;
        }
        
        return $suppliers;
    }

    private function parseSupplier($item)
    {
        $rut = explode('-', $item[0])[0];
        $rut = RutUtils::normalize($rut);
        
        return [
            'rut'                 => $rut,
            'nombre'              => trim($item[2]),
            'telefono'            => preg_replace('/\s+/', ' ',$item[6]),
            'direccion'           => trim($item[3]),
            'representantes'      => explode('-', $rut)[0],
            'email_representante' => $item[5],
            'comuna'              => $item[4],
            'extracto'            => $item[8] ?? '-',
            'sitio_web'           => $item[7],
            'email'               => $item[5],
            'tipo'                => '',
        ];
    }
}