<?php

namespace App\Command;

use App\Pipeline\Builder;
use App\Pipeline\Pipeline;
use App\Pipeline\Registry;
use App\Pipeline\Task\Reader\ExcelReader;
use App\Pipeline\Task\Transformer\ColumnMapping;
use App\Pipeline\Task\Transformer\ColumnTransform;
use App\Pipeline\Task\Transformer\ObjectMapping;
use App\Pipeline\Task\Transformer\RelationshipMapping;
use App\Pipeline\Task\Transformer\RemoveHeader;
use App\Pipeline\Task\Transformer\VirtualColumn;
use App\Pipeline\Task\Writer\ArrayWriter;
use App\Pipeline\Task\Writer\DoctrineDBALWriter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Yaml;

class ProcessPipelineCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('pipeline:run')
            ->addOption('pipeline', null, InputOption::VALUE_REQUIRED)
            ->addOption('param', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $co = $this->getContainer();
        $rootDir = $co->getParameter('kernel.root_dir');
        $pipelinesConfig = $rootDir.'/config/pipelines/contract_importer.yml';
        $pipelines = Yaml::parse(file_get_contents($pipelinesConfig));
        
        $params = [];
        
        foreach ($input->getOption('param') as $param) {
            $parts = explode(':', $param);
            $params[$parts[0]] = $parts[1];
        }
        
        // Services
        $a = new ExcelReader($co);
        $b = new ColumnMapping($co);
        $c = new ColumnTransform($co);
        $d = new RemoveHeader($co);
        $e = new VirtualColumn($co);
        $f = new RelationshipMapping($co);
        $g = new ObjectMapping($co);
        $h = new DoctrineDBALWriter($co);

        // Registry
        $registry = new Registry();
        foreach ([$a, $b, $c, $d, $e, $f, $g, $h] as $item) {
            $registry->registerTask($item);
        }

        $pipelines = [];
        $builder = new Builder($registry);
        foreach ($pipelines['pipelines'] as $k => $v) {
            $tasks[] = $v['src'];
            $tasks = array_merge($tasks, $v['tasks']);
            $tasks[] = $v['dest'];
        }
    }
}