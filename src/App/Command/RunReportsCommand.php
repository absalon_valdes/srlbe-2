<?php

namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;

class RunReportsCommand extends ContainerAwareCommand
{
    /**
     * @var LoggerInterface
     * @Di\Inject("logger")
     */
    public $logger;

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('reports:generate')
            ->addOption('type', null, InputOption::VALUE_OPTIONAL);
    }
    
    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try{
            $c = $this->getContainer();
            $env = $c->getParameter('kernel.environment');
            /**@var Translator $translator **/
            $translator = $c->get('translator');
            $this->logger = $c->get('logger');
            $type =  $input->getOption('type') ?? null;
            $reportsDir = sprintf('%s/reports', $c->getParameter('kernel.var_dir'));
            if($type){
                $reportsDir = $reportsDir.'/multiple';
            }

            if (!@mkdir($reportsDir, 0777, true) && !is_dir($reportsDir)) {
                throw new \RuntimeException(sprintf(
                    'Cannot create folder %s', $reportsDir
                ));
            }

            $filename = sprintf('%s/reporte-bi-%s-%s-%s.csv', $reportsDir,$type? $translator->trans($type) : 'general', date('YmdHis'), $env);
            $c->get('requisition.exporter')->export($type)->getFile($filename);
        }
        catch (\Exception $e) {
            $this->logger->error("REPORTE: {$e->getMessage()}");
        }

    }
}
