<?php

namespace App\Import;

use App\Entity\Employee;
use App\Entity\Position;
use App\Import\Helper\RenderImporterErrorMessage;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("employees_importer")
 */
class EmployeeImporter extends Importer
{
    const NUM_TRANSACTIONS = 20;
    const DEFAULT_ROL = 'ROLE_EMPLOYEE';

    /**
     * @Inject("employee_repo")
     */
    public $employeesRepo;

    /**
     * @Inject("position_repo")
     */
    public $positionsRepo;

    /**
     * @Inject("office_repo")
     */
    public $officeRepo;

    /**
     * @Inject("fos_user.user_manager")
     */
    public $userManager;

    /**
     * @var RenderImporterErrorMessage $errorMessageRender
     * @Inject("render_importer_error_message")
     */
    public $errorMessageRender;

    protected function transform(array $row)
    {
        if (array_key_exists('rut', $row[0])) {
            return $row;
        }

        return [
            'rut' => function ($row) {
                return strtolower((trim($row[0])));
            },
            'nombre' => function ($row) {
                return ucwords(strtolower(trim($row[1])));
            },
            'apellido_paterno' => function ($row) {
                return ucfirst(strtolower($row[2]));
            },
            'apellido_materno' => function ($row) {
                return ucfirst(strtolower($row[3]));
            },
            'puesto' => function ($row) {
                return trim($row[5]);
            },
            'id_dependencia' => function ($row) {
                return trim($row[7]);
            },
            'id_unidad' => function ($row) {
                return trim($row[6]);
            },
            'telefono' => function ($row) {
                return trim($row[4]);
            },
            'email' => function ($row) {
                return strtolower(trim($row[7]));
            },
        ];
    }

    protected function doImport(array $data)
    {
        $ruts = array_column($data, 'rut');
//        $devRoles = [
//            '16774151-1',
//            '16774151-2',
//            '16774151-3',
//            '16774151-4',
//            '16774151-5',
//            '16774151-6',
//            '16774151-7',
//            '16774151-8',
//            '16774151-9',
//            '12482470-2',
//        ];
//
//        $ruts = array_merge($ruts, $devRoles);
//
//        $this->employeesRepo->removeInactive($ruts);

        $violations = [];
        foreach ($data as $employee) {
//            dump($employee);
            $violations[] = $this->createEmployee($employee);
        }

        $this->employeesRepo->flush();

        return $violations;
    }

    private function createEmployee(array $row)
    {
        if (isset($row['email']) && $this->hasReference('email')) {
            return;
        }

        if (!isset($row['rut']) || $this->hasReference($row['rut'])) {
            return;
        }

        $rut = $row['rut'];

        if (!$employee = $this->employeesRepo->findOneByRut($rut)) {
            $employee = new Employee;
            $employee->setRut($rut);
        }

        $employee->setName(ucwords(strtolower($row['nombre'])));
        $employee->setLastName(ucwords(strtolower($row['apellido_paterno'])));
        $employee->setSecondLastName(ucwords(strtolower($row['apellido_materno'])));
        $employee->setPhone($row['telefono']);

        $office = $this->officeRepo->findOneByCode(trim($row['id_sucursal']));

        if (!$office) {
            $message  = sprintf('[Importador Funcionarios] RUT: %s - Error unidad codigo %s no existe', $rut, $row['id_sucursal']);
            return $this->errorMessageRender->renderError($row, $message, true);
        }

        $employee->setOffice($office);
        $position = $this->getPosition(ucwords(strtolower($row['puesto'])));
        $employee->setPosition($position);

        $email = $row['email'];
        $violation = $this->validateEntity($employee);

        if (!$violation['error']) {
            if (!empty($email)) {
                $this->employeesRepo->persist($employee, true);
                $employee->setUser($this->createUser($email, $rut, $employee));
                $this->employeesRepo->persist($employee, true);
            }
        }

        return $violation;
    }


    private function getPosition($title)
    {
        if ($position = $this->positionsRepo->findOneByTitle($title)) {
            return $position;
        }

        $hash = sha1($title);

        if (!$this->hasReference($hash)) {
            $position = new Position;
            $position->setTitle($title);

            $this->positionsRepo->persist($position);
            $this->addReference($hash, $position);
        }

        return $this->getReference($hash);
    }

    private function createUser($email, $rut, $employee)
    {
        $user = $this->userManager->findUserByEmail($email);
        if ($user) {
            return $user;
        }

        $username = explode('@', $email)[0];
        $user = $this->userManager->findUserByUsername($username);

        if ($user) {
            return $user;
        }

        $hash = sha1($rut.$username);

        if (!$this->hasReference($hash)) {
            $user = $this->userManager->createUser();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setRoles(array(self::DEFAULT_ROL));
            $user->setEnabled(true);
            $user->setIsEmployee(true);
            $user->setEmployee($employee);
            $password = explode('-', $rut)[0];
            $user->setPlainPassword($password);

            $this->userManager->updateUser($user);
            $this->addReference($hash, $user);
        }

        return $this->getReference($hash);
    }
}
