<?php

namespace App\Import;

use App\Entity\Supplier;
use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\Repository;
use Behat\Transliterator\Transliterator;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\UserManager;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class SupplierImporter
 * @package App\Import
 * 
 * @Di\Service("supplier_importer")
 */
class SupplierImporter
{
    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $manager;

    /**
     * @var Repository
     * @Di\Inject("supplier_repo")
     */
    public $supplierRepo;

    /**
     * @var CategoryRepository
     * @Di\Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @var UserManager
     * @Di\Inject("fos_user.user_manager")
     */
    public $userManager;

    /**
     * @Di\Inject("security.password_encoder")
     */
    public $encoder;

    /**
     * @param $data
     */
    public function import($data)
    {
        foreach ($data as $rut => $sup) {
            if (!isset($sup['email']) || !$sup['email']) {
                continue;
            }
            
            $supplier = $this->supplierRepo->findOneByRut($rut) ?? new Supplier;
            $supplier->setName($sup['nombre']);
            $supplier->setRut($rut);
            $supplier->setPhone($sup['telefono']);
            $supplier->setWebsite($sup['sitio_web']);
            $supplier->setEmail($sup['email']);
            $supplier->setCity($this->cleanCity($sup['comuna']));
            $supplier->setStreet($sup['direccion']);
            $supplier->setExcerpt($sup['extracto']);

            $this->addRepresentative($supplier, $sup);

            $this->manager->persist($supplier);
        }

        $this->manager->flush();
    }

    /**
     * @param $cityName
     * @return mixed
     */
    private function cleanCity($cityName)
    {
        $citySlug = Transliterator::urlize($cityName);

        $city = $this->categoryRepo->createQueryBuilder('c')
            ->where('c.slug like :slug')
            ->andWhere('c.lvl = 2')
            ->setParameter('slug', '%'.$citySlug.'%')
            ->getQuery()
            ->getResult()
        ;

        foreach ($city as $c) {
            if (preg_match('/'.$citySlug.'$/', $c->getSlug())) {
                return $c;
            }
        }

        return null;
    }

    /**
     * @param $supplier
     * @param $data
     */
    private function addRepresentative(Supplier $supplier, $data)
    {
        $username = explode('-', trim($data['rut']))[0];
        
        /** @var User $user */
        if ($user = $this->userManager->findUserByUsername($username)) {
            return;
        }

        $user = $this->userManager->createUser();
        $user->setUsername($username);
        
        $sup['email_representante'] = empty($data['email_representante']) ? 
            sprintf('%s@algo.com', $user) : $data['email_representante'];
        
        $user->setEmail($data['email_representante']);
        $user->setRoles(['ROLE_SUPPLIER']);
        $user->setEnabled(true);

        $password = $this->encoder->encodePassword($user, $username);
        $user->setPassword($password);

        $this->userManager->updateUser($user);
        $supplier->addRepresentative($user);
    }
}