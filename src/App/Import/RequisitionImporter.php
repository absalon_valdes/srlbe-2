<?php

namespace App\Import;

use App\Entity\Employee;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Helper\RequisitionTypeResolver;
use App\Helper\UserHelper;
use App\Import\Helper\RenderImporterErrorMessage;
use App\Repository\ContractRepository;
use App\Repository\EmployeeRepository;
use App\Repository\ProductRepository;
use App\Repository\Repository;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("requisition_importer")
 */
class RequisitionImporter extends Importer
{
    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var ProductRepository
     * @Di\Inject("product_repo")
     */
    public $productRepo;

    /**
     * @var Repository
     * @Di\Inject("supplier_repo")
     */
    public $supplierRepo;

    /**
     * @var EmployeeRepository
     * @Di\Inject("employee_repo")
     */
    public $employeeRepo;

    /**
     * @var ContractRepository
     * @Di\Inject("contract_repo")
     */
    public $contractRepo;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RenderImporterErrorMessage
     * @Di\Inject("render_importer_error_message")
     */
    public $errorMessageRender;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function transform(array $data)
    {
        return [
            'created_at' => function ($row) {
                return \DateTime::createFromFormat('d/m/Y H:i', sprintf('%s %s', trim($row[0]), trim($row[1])));
            },
            'product_code' => function ($row) {
                return trim($row[2]);
            },
            'inventory_codes' => function ($row) {
                $inventoryCodes = trim($row[3]).',' ?? ',';

                return  explode(',', $inventoryCodes);
            },
            'comments' => function ($row) {
                return trim($row[4]);
            },
            'supplier_rut' => function ($row) {
                return trim($row[5]);
            },
            'requester_rut' => function ($row) {
                return trim($row[6]);
            },
            'quantity' => function ($row) {
                return trim($row[7]);
            },
        ];
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    protected function doImport(array $data)
    {
        $currentUser = $this->userHelper->getCurrentUser();
        $violations = [];
        foreach ($data as $requisition) {
            $violations[] = $this->importRequisition($requisition);
        }

        //Return to the current user
        $this->userHelper->impersonateUser($currentUser->getUsername());

        return $violations;
    }

    private function importRequisition($requisitionData)
    {
        /** @var Employee $requester */
        $requester = $this->employeeRepo->findOneByRut($requisitionData['requester_rut']);
        if (!$requester) {
            $message = sprintf('[Importador Pedidos] - Demandante %s no existe', $requisitionData['requester_rut']);

            return $this->errorMessageRender->renderError($requisitionData, $message);
        }

        /** @var Supplier $supplier */
        $supplier = $this->supplierRepo->findOneByRut($requisitionData['supplier_rut']);
        if (!$supplier) {
            $message = sprintf('[Importador Pedidos] - Proveedor %s no existe', $requisitionData['supplier_rut']);

            return $this->errorMessageRender->renderError($requisitionData, $message);
        }

        /** @var Product $product */
        $product = $this->productRepo->findOneByCode($requisitionData['product_code']);
        if (!$product) {
            $message = sprintf('[Importador Pedidos] - Producto %s no existe', $requisitionData['product_code']);

            return $this->errorMessageRender->renderError($requisitionData, $message);
        }

        $contract = $this->contractRepo->findOneBy(
            [
                'product' => $product,
                'office' => $requester->getOffice(),
                'supplier' => $supplier,
            ]
        );
        if (!$contract) {
            $message = sprintf(
                '[Importador Pedidos] - Contrato con proveedor %s, producto %s no existe en la sucursal %s',
                $requisitionData['supplier_code'],
                $requisitionData['product_code'],
                $requester->getOffice()->getName()
            );

            return $this->errorMessageRender->renderError($requisitionData, $message);
        }

        /** @var Requisition $requisition */
        $requisition = $this->requisitionRepo->create();
        $requisition->setType($this->typeResolver->getType($product));
        $requisition->setProduct($product);
        $requisition->setContract($contract);
        $requisition->setStatus(ProductStatus::DRAFT);
        $requisition->setQuantity($requisitionData['quantity']);
        $requisition->setComments($requisitionData['comments']);
        $requisition->setRequester($requester->getUser());
        $requisition->setStateDueDate(new \DateTime());
        $requisition->setCreatedAt($requisitionData['created_at']);
        if (is_array($requisitionData['inventory_codes'])) {
            $requisition->addMetadata('inventory_code', array_filter($requisitionData['inventory_codes']));
        }

        //impersonate user to start the workflow process
        $this->userHelper->impersonateUser($requester->getUser()->getUsername());

        $this->requisitionRepo->persist($requisition, true);
        if (!$this->workflow->start($requisition)->getSuccessful()) {
            $message = sprintf('[Importador Pedidos] - Error al workflow en el paso %s', ProductStatus::DRAFT);

            return $this->errorMessageRender->renderError($requisitionData, $message);
        }

        if (!$this->workflow->proceed($requisition, 'create')) {
            $message = sprintf('[Importador Pedidos] - Error al workflow en el paso %s', ProductStatus::CREATED);

            return $this->errorMessageRender->renderError($requisitionData, $message);
        }
    }
}
