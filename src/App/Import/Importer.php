<?php
namespace App\Import;

use App\Loader\LoaderInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Importer
 * @package App\Import
 */
abstract class Importer
{
    /**
     * @var LoaderInterface
     * @Di\Inject("file_loader")
     */
    public $loader;

    /**
     * @var ValidatorInterface
     * @Di\Inject("validator")
     */
    public $validator;

    /**
     * Importer constructor.
     */
    public function __construct()
    {
        $this->referenceBag = new ReferenceBag;
    }

    /**
     * @param $data
     */
    protected function beforeImport($data)
    {
    }

    /**
     * @param $dataOrigin
     * @return mixed
     */
    public function import($dataOrigin)
    {
        $isFile = is_string($dataOrigin);
        $data = $isFile ? $this->loader->load($dataOrigin) : $dataOrigin;

        $mapping = $this->transform($data);

        $transformed = [];
        foreach ($data as $row) {
            if (!$isFile) {
                $transformed[] = $row;
                continue;
            }

            $new = [];
            foreach ($mapping as $key => $tamper) {
                $new[$key] = $tamper($row);
            }
            
            $transformed[] = $new;
        }

        return $this->doImport($transformed);
    }

    /**
     * @param $entity
     * @return array|null
     */
    protected function validateEntity($entity)
    {
        $validation = $this->validator->validate($entity);

        if (0 === count($validation)) {
            return null;
        }

        return [
            'entity' => $entity,
            'error'  => $validation,
        ];
    }

    /**
     * @param array $data
     * @return mixed
     */
    abstract protected function doImport(array $data);

    /**
     * @param array $data
     * @return mixed
     */
    abstract protected function transform(array $data);

    /**
     * @param $id
     * @return bool
     */
    protected function hasReference($id)
    {
        return $this->referenceBag->hasReference($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    protected function getReference($id)
    {
        return $this->referenceBag->getReference($id);
    }

    /**
     * @param $id
     * @param $object
     */
    protected function setReference($id, $object)
    {
        $this->referenceBag->setReference($id, $object);
    }

    /**
     * @param $id
     * @param $object
     */
    protected function addReference($id, $object)
    {
        $this->referenceBag->addReference($id, $object);
    }
}
