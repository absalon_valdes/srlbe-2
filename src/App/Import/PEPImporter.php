<?php

namespace App\Import;

use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class PEPImporter
 * @package App\Import
 * 
 * @DI\Service("pep_importer")
 */
class PEPImporter implements ImporterInterface
{
    /**
     * {@inheritdoc}
     */
    public function import($filename)
    {
        try {
            $sheets = $this->loadExcel($filename);
            
            if (count($sheets) !== 2) {
                throw new \InvalidArgumentException('Formato inválido');
            }


            
        } catch (\PHPExcel_Reader_Exception $e) {
            
        }
    }

    /**
     * @param $filename
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    private function loadExcel($filename)
    {
        $type = \PHPExcel_IOFactory::identify($filename);
        $reader = \PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(true);
        $excel = $reader->load($filename);

        $arrayData = array_map(function (\PHPExcel_Worksheet $sheet) {
            return $sheet ? $sheet->toArray() : array();
        }, $excel->getAllSheets());

        return $arrayData;
    }
}