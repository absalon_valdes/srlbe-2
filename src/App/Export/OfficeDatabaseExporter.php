<?php

namespace App\Export;

use App\Entity\Office;
use App\Repository\OfficeRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Translation\Translator;

/**
 * Class OfficeDatabaseExporter
 * @package App\Export
 * @DI\Service("office_exporter")
 * @DI\Tag("exporter", attributes={"alias"="offices"})
 */
class OfficeDatabaseExporter extends Exporter
{
    /**
     * @var OfficeRepository
     * @DI\Inject("office_repo")
     */
    public $officeRepo;

    /**
     * @var Translator
     * @DI\Inject("translator")
     */
    public $translator;

    /**
     * OfficeDatabaseExporter constructor.
     */
    public function __construct()
    {
        $this->fileName = sprintf('base-datos-sucursales-%s.xls', time());
        $this->tabName = 'Sucursales';
        $this->title = 'Base de datos sucursales'.date('d-m-Y H:i:s'); 
    }

    /**
     * @inheritdoc
     */
    protected function fetchData($type, $data)
    {
        $this->data = $this->officeRepo->findAll();
    }
    
    /**
     * @inheritdoc
     */
    protected function fillRows()
    {
        $index = 2;
        
        /** @var Office $office */
        foreach ($this->data as $office) {
            $this->report->getActiveSheet()
                ->setCellValue('A'.$index, $office->getCode())
                ->setCellValue('B'.$index, $office->getDependencyCode())
                ->setCellValue('C'.$index, $office->getName())
                ->setCellValue('D'.$index, $office->getAddress())
                ->setCellValue('E'.$index, $office->getCostCenter())
                ->setCellValue('F'.$index, $this->translator->trans($office->getType()))
                ->setCellValue('G'.$index, $office->getCity()->getName())
            ;
            
            ++$index;
        }
        
        foreach (range('A', 'G') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }
    
    /**
     * @inheritdoc
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Código')
            ->setCellValue('B1', 'Código de dependencia')
            ->setCellValue('C1', 'Nombre')
            ->setCellValue('D1', 'Dirección')
            ->setCellValue('E1', 'Centro de costo')
            ->setCellValue('F1', 'Tipo')
            ->setCellValue('G1', 'Comuna')
        ;
    }
}