<?php

namespace App\Export;
use App\Entity\Requisition;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class RequisitionDailyExporter
 * @package App\Export
 * 
 * @DI\Service("requisition.exporter.daily")
 */
class RequisitionDailyExporter extends RequisitionExporter
{
    public function fetchData($type, $data)
    {
        $types = !$type ? [
            Requisition::TYPE_MAINTENANCE,
            Requisition::TYPE_PRODUCT,
            Requisition::TYPE_SELF_MANAGEMENT,
        ] : [$type];

        $start = new \DateTime;
        $end = new \DateTime;
        
        $this->data = $this->requisitionsRepo->createQueryBuilder('r')
            ->where('r.type in (:types)')
            ->andWhere('r.createdAt between :start_date and :end_date')
            ->andWhere('r.deletedAt is null')
            ->orderBy('r.number', 'desc')
            ->setParameter('types', $types)
            ->setParameter('start_date', $start->setTime(0, 0))
            ->setParameter('end_date', $end->setTime(23, 59))
            ->getQuery()
            ->getResult()
        ;
            
        $type = !$type ? 'all' : $type;

        $reportFilename = [
            Requisition::TYPE_MAINTENANCE => 'mantenciones',
            Requisition::TYPE_PRODUCT => 'pedidos',
            Requisition::TYPE_SELF_MANAGEMENT => 'autogestion',
            'all' => 'general'
        ];

        $this->fileName = sprintf('reporte-%s-%s.xls', $reportFilename[$type], time());
        $this->tabName = ucfirst($reportFilename[$type]);
        $this->title = sprintf('Reporte de ficha de %s (%s)', $reportFilename[$type], date('d-m-Y H:i:s'));

        $this->setColumnInfo();
        $this->setHeaderInfo();
    }
}