<?php

namespace App\Export;


use App\Entity\Category;
use App\Entity\Contract;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Helper\SlaCalculator;
use App\Util\DateUtils;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("backoffice.requisition.exporter")
 */
class RequisitionBackOfficeExporter extends Exporter
{

    /**
     * @var SlaCalculator $slaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    public function __construct()
    {
        $this->fileName = sprintf('reporte-pedidos-%s.xls', time());
        $this->tabName = 'Pedidos';
        $this->title = 'Reporte de Pedidos '.date('d-m-Y H:i:s');
    }

    /**
     * @param $type
     * @param $data
     * @return mixed
     * @internal param type $string
     */
    protected function fetchData($type, $data)
    {
        $this->data = $data;
    }

    protected function fillRows()
    {

        $productCategory = function (Product $product) {
            if ($category = $product->getCategory()) {
                return $category->getName();
            }

            return null;
        };

        $requisitionRequester = function (Requisition $requisition) {
            if ($requester = $requisition->getRequester()) {
                return $requester->getEmployee();
            }

            return null;
        };

        $employeeOffice = function (Employee $employee = null) {
            if ($employee && ($office = $employee->getOffice())) {
                return $office;
            }

            return null;
        };
        
        $rowCount = 2;
        /** @var Requisition $requisition */
        foreach ($this->data as $requisition) {

            /** @var Contract $contract */
            $contract = $requisition->getContract();

            /** @var Employee $requester */
            $requester = $requisitionRequester($requisition);

            if (is_null($requester)) {
                continue;
            }

            /** @var Office $requesterOffice */
            $requesterOffice = $employeeOffice($requester);
            
            $category = $productCategory($requisition->getProduct());


            /* @var \DateTime $estimatedSLA */
            $startDate = $requisition->getCreatedAt();
            
            $daysSlaEstimated = $this->slaIndicator($requisition);

            $this->report->getActiveSheet()
                ->setCellValue('A'.$rowCount, $startDate->format('d/m/Y'))
                ->setCellValue('B'.$rowCount, $requisition->getNumber())
                ->setCellValue('C'.$rowCount, $category)
                ->setCellValue('D'.$rowCount, $this->translator->trans($requisition->getTranslatableStatus()))
                ->setCellValue('E'.$rowCount, $daysSlaEstimated)
                ->setCellValue('F'.$rowCount, $requesterOffice ? $requesterOffice->getName() : null)
                ->setCellValue('G'.$rowCount, $requester ? $requester->getFullName() : null)
            ;

            ++$rowCount;
        }

        foreach (range('A', 'F') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Fecha de ingreso')
            ->setCellValue('B1', 'Solicitud')
            ->setCellValue('C1', 'Tipo')
            ->setCellValue('D1', 'Estado solicitud')
            ->setCellValue('E1', 'SLA')
            ->setCellValue('F1', 'Sucursal')
            ->setCellValue('G1', 'Usuario')
        ;
    }

    /**
     * @param Requisition $requisition
     * @return string
     */
    public function slaIndicator(Requisition $requisition)
    {
        if ($requisition->isRejected()) {
            return '-';
        }

        if ($requisition->isClosed()) {
            $when = null;
            $result = $this->remainingSla($requisition);
        } elseif ($requisition->isStatus(ProductStatus::RETURNED)) {
            $when = $this->slaResolver->resolveSLA($requisition);
            $result = $when->diff(new \DateTime);
        } else {
            $when = $requisition->getStateDueDate();
            $result = $when->diff(new \DateTime);
        }

        $sla = DateUtils::dateIntervalFormat($result);

        return $sla;

    }

    /**
     * @param Requisition $requisition
     * @return \DateInterval
     */
    private function remainingSla(Requisition $requisition)
    {
        $accepted = $requisition->getTransition(ProductStatus::ACCEPTED);
        $delivered = $requisition->getTransition(ProductStatus::DELIVERED);

        // BC
        if (!$accepted instanceof Transition && !$delivered instanceof Transition) {
            return (new \DateTime)->diff(new \DateTime);
        }

        $diff = $delivered->getDatetime();
        $diff = $diff->diff($accepted->getDatetime());

        return $diff;
    }
}
