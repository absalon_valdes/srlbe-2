<?php

namespace App\Export;

use App\Dashboard\Query\Merge\ApproverAll;
use App\Dashboard\Query\Requisition\ApproverVcardAll;
use App\Entity\Category;
use App\Entity\Contract;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Entity\SLA;
use App\Model\VcardRequisition;
use App\Entity\Supplier;
use App\Entity\User;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\RequisitionRepository;
use App\Dashboard\Query\Requisition\SupplierAll;
use App\Util\DateUtils;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use JMS\Serializer\Serializer;

/**
 * Class ApproverVCardRequisitionExporter.
 *
 * @Di\Service("approver.vcard.requisition.exporter")
 */
class ApproverVCardRequisitionExporter extends Exporter
{
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;
    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var ApproverVcardAll
     * @Di\Inject("app.dashboard.query.requisition.approver_vcard_all")
     */
    public $queryApprover;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;
    /**
     * @var Container
     * @Di\Inject("service_container")
     */
    public $container;

    /**
     */
    public $serializer;

    private $possibleStatuses = [
        'borrador',
        'evaluacion',
        'cerrado',
        'aceptado',
        'rechazado',
        'denegado',
        'apelacion',
        'ejecucion',
        'en_despacho',
        'solicitud_evaluada',
        'proveedor_calificado',
        'solicitud_completa'
    ];

    protected function fetchData($type, $data)
    {
        $this->data = $this->queryApprover->getResult();

        $this->fileName = sprintf('reporte-%s-%s.xls', 'general', date('dmYHi'));
        $this->tabName = ucfirst('general');
        $this->title = sprintf('Reporte de ficha de %s (%s)', 'general', date('d-m-Y H:i:s'));

        $this->setColumnInfo();
        $this->setHeaderInfo();
    }

    protected function setColumnInfo()
    {
        $phone = function (Employee $e = null) {
            if($e){
                $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $e->getPhone()));
                return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
            }
            return null;
        };

        $this->serializer = $this->container->get('jms_serializer');

        $requisitionsData = [];
        /** @var Requisition $requisition */
        foreach ($this->data as $i => $requisition) {
            $requisitionData = [];

            /**@var VcardRequisition $vcards**/
            $vcards = $this->serializer->deserialize($requisition->getMetadata('vcards'), VcardRequisition::class, 'json');


            $timeStamps = $requisition->getMetadata('timestamps');
            $certify = json_decode($requisition->getMetadata('certificacion') ?? "{}",true);

            /** @var Product $product */
            $product = $requisition->getProduct();

            /** @var User $user */
            $user = $requisition->getRequester();

            /** @var Employee $requester */
            $requester = $vcards->getEmployee();

            /** @var Office $requesterOffice */
            $requesterOffice = $vcards->getOffice();

            /* @var \DateTime $startDate */
            $startDate = $timeStamps["requester"]["started"];

            $endedAt = null;
            //Support for legacy evaluation metadata format
            if ($eval = $requisition->getMetadata('evaluation')) {
                if (isset($eval['evaluation'])) {
                    $endedAt = $eval['evaluated_at'];
                } else {
                    $endedAt = end($eval)['evaluated_at'];
                }
            }

            if ($requisition->getStatus() == 'solicitud_completa' && array_key_exists('delivered_at',$certify)) {
                $endedAt = new \DateTime($certify["delivered_at"]);
            }

            if ($requisition->getStatus() == 'cerrado') {
                $declined = $requisition->getActivities()[count($requisition->getActivities())-1];
                $endedAt = $declined->getCreatedAt();
            }

            $requisitionData[] = $this->translator->trans($requisition->getType());
            $requisitionData[] = $requisition->getNumber();
            $requisitionData[] = $product->getCode();
            $requisitionData[] = $product->getName();
            $requisitionData[] = $requisition->getQuantity();
            $requisitionData[] = $requester && $requester ? $requester->getRut() : null;
            $requisitionData[] = $requester ? $requester->getFullName() : null;
            $requisitionData[] = $phone($requester);
            $requisitionData[] = $user ? $user->getEmail() : '';
            $requisitionData[] = $requesterOffice ? $requesterOffice->getAddress() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCity()->getName() : null;
            $requisitionData[] = $requesterOffice ? $this->regionToRoman($requesterOffice) : null;
            $requisitionData[] = $requesterOffice->getCity()->getParent() ? $requesterOffice->getCity()->getParent()->getName() : null;
            $requisitionData[] = $requester ? $requester->getPosition()->getTitle() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCode() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getName() : null;
            $requisitionData[] = $vcards->getCostCenter();
            $requisitionData[] = $requester ? $this->translator->trans($requesterOffice->getType()) : null;
            $requisitionData[] = $requisition->isClosed() ? 'CERRADO' : 'ABIERTO';
            $requisitionData[] = $this->translator->trans(str_replace('requisition',$requisition->getStatus(),$requisition->getType()).'.centralized');
            $requisitionData[] = $startDate->format('d/m/Y H:i:s');//fecha creacion

            /***** SLA ejecución proveedor ***/
            $estimadedExecution = $this->slaCalculator->getEstimate(
                $startDate,
                $requisition->getContract()->getSLA(ProductStatus::APPROVED),
                $requisition->getContract()->getSLA(ProductStatus::CONFIRMED),
                $requisition->getContract()->getSLA(ProductStatus::DELIVERED),
                $requisition->getContract()->getSLA(ProductStatus::REVIEWED),
                $requisition->getContract()->getSLA('en_despacho')
                );

            $daysSlaEstimated = $this->getDaysInFormat($estimadedExecution, $startDate);
            $inSla = $requisition->isClosed() ? $estimadedExecution > $requisition->getClosedAt() : $estimadedExecution > new \DateTime();
            $slaTimeout = $requisition->isClosed() ? DateUtils::dateIntervalFormat($startDate->diff($requisition->getClosedAt())) : DateUtils::dateIntervalFormat($estimadedExecution->diff((new \DateTime)));

            $requisitionData[] = $estimadedExecution->format('d/m/Y H:i:s');
            $requisitionData[] = $endedAt ? $endedAt->format('d/m/Y H:i:s') : null;
            $requisitionData[] = $inSla ? 'DENTRO SLA' : 'FUERA SLA'; //calcular con esta fecha estimada
            $requisitionData[] = $inSla && !$requisition->isClosed() ? $this->getDaysInFormat($estimadedExecution) : 0;
            $requisitionData[] = $daysSlaEstimated;
            $requisitionData[] = !$requisition->isClosed() ? DateUtils::dateIntervalFormat($startDate->diff((new \DateTime))) : DateUtils::dateIntervalFormat($startDate->diff($endedAt));
            $requisitionData[] = !$inSla ? $slaTimeout : 0;

            $requisitionData[] = "SLA Ejecución del proveedor";
            $requisitionData[] = $this->addValues($requisition->getContract()->getSLA(ProductStatus::APPROVED), $requisition->getContract()->getSLA(ProductStatus::CONFIRMED),$requisition->getContract()->getSLA(ProductStatus::DELIVERED));
            $requisitionData[] = $this->translator->trans($requisition->getContract()->getSlaProduct()->getType());

            $requisitionData[] = $this->translator->trans($requisition->getCreatedAt()->format('F')) . " " . $requisition->getCreatedAt()->format('Y');//mes creacion

            /***** SLA ejecución proveedor ***/
            $requisitionsData[] = $requisitionData;
        }
        $this->columns = $requisitionsData;

    }

    protected function setHeaderInfo()
    {
        $headerInfo = [];
        $headerInfo[] = 'Tipo';
        $headerInfo[] = 'N° solicitud';
        $headerInfo[] = 'Codigo técnico';
        $headerInfo[] = 'Producto';
        $headerInfo[] = 'Cantidad';
        $headerInfo[] = 'RUT Solicitante';
        $headerInfo[] = 'Solicitante';
        $headerInfo[] = 'Anexo';
        $headerInfo[] = 'Correo electrónico';
        $headerInfo[] = 'Dirección';
        $headerInfo[] = 'Comuna';
        $headerInfo[] = 'N° Región';
        $headerInfo[] = 'Región';
        $headerInfo[] = 'Cargo Solicitante';
        $headerInfo[] = 'Codigo Unidad';
        $headerInfo[] = 'Nombre Sucursal';
        $headerInfo[] = 'Centro de costo';
        $headerInfo[] = 'Tipo unidad';
        $headerInfo[] = 'Estado';
        $headerInfo[] = 'Estado en sistema';
        $headerInfo[] = 'Fecha de creación';
        $headerInfo[] = 'Fecha posible de entrega';
        $headerInfo[] = 'Fecha entrega real';
        $headerInfo[] = 'En SLA';
        $headerInfo[] = 'Días SLA restante';
        $headerInfo[] = 'SLA total esperado';
        $headerInfo[] = 'SLA total real';
        $headerInfo[] = 'Tpo Excedido respecto de SLA';

        $headerInfo[] = "Tipo SLA";
        $headerInfo[] = "SLA esperado";
        $headerInfo[] = "SLA unidad";
        $headerInfo[] = 'Mes creación';
        //$headerInfo[] = 'Nro orden de trabajo';
        $this->headers = $headerInfo;
    }

    protected function fillRows()
    {
        $this->fillCellValues($this->columns);
    }

    /**
     * @param SLA[] $slas
     * @return integer
     */
    private function addValues(...$slas)
    {
        $result = 0;
        foreach (array_filter($slas) as $sla) {
            if (!$sla instanceof SLA) {
                continue;
            }
            $result += $sla->getValue();
        }
        return $result;
    }

    private function regionToRoman(Office $office)
    {
        if (!$city = $office->getCity()) {
            return null;
        }

        /** @var Category $region */
        if (!$region = $city->getParent()) {
            return null;
        }

        $regionToRoman = [
            'arica' => 'XV',
            'tarapaca' => 'I',
            'antofagasta' => 'II',
            'atacama' => 'III',
            'coquimbo' => 'IV',
            'valparaiso' => 'V',
            'libertador' => 'VI',
            'maule' => 'VII',
            'biobio' => 'VIII',
            'araucania' => 'IX',
            'rios' => 'XIV',
            'lagos' => 'X',
            'aisen' => 'XI',
            'magallanes' => 'XII',
            'metropolitana' => 'XIII',
        ];

        foreach ($regionToRoman as $slug => $no) {
            if ((bool)strstr($region->getSlug(), $slug)) {
                return $no;
            }
        }
        return null;
    }

    /**
     * @throws \PHPExcel_Exception
     */
    protected function fillColumnHeaders()
    {
        foreach ($this->headers as $key => $value) {
            $this->report->setActiveSheetIndex(0)
                ->setCellValue($this->getNameFromNumber($key) . "1", $value);
        }
    }

    private function fillCellValues($requisitionsData)
    {
        $cRow = 2;
        foreach ($requisitionsData as $row) {
            foreach ($row as $key => $value) {
                $this->report->getActiveSheet()
                    ->setCellValue($this->getNameFromNumber($key) . $cRow, $value);
            }
            ++$cRow;
        }
    }

    private function filterStates($states, $status, $contractSlas)
    {
        $s = $result = null;
        $result = [
            "status" => $status,
            "createdAt" => null,
            "finishedAt" => null,
            "slaExpectedValue" => array_key_exists($status, $contractSlas) ? $contractSlas[$status]->getValue() : null,
            "slaExpectedType" => array_key_exists($status, $contractSlas) ? $contractSlas[$status]->getType() : null,
            "sla" => array_key_exists($status, $contractSlas) ? $contractSlas[$status] : null
        ];

        foreach ($states as $state) {
            $stateN = $state->getProcessName() . "." . $state->getStepName();
            if ($stateN != $status) {
                continue;
            }
            $s[] = $state;
        }

        if (count($s) > 0 && $s !== null) {
            $result["createdAt"] = $s[0]->getCreatedAt();
            $result["finishedAt"] = $s[count($s) - 1]->getFinishedAt();
            $result["quantity"] = count($s);
        }

        return $result;
    }

    private function getNameFromNumber($num)
    {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
}
