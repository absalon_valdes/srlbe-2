<?php

namespace App\Export;

use App\Helper\PepQuery;
use App\Util\RutUtils;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class PepDatabaseExporter
 * @package App\Export
 * @DI\Service("pep_database_exporter")
 * @DI\Tag("exporter", attributes={"alias"="pep"})
 */
class PepDatabaseExporter extends Exporter
{
    /**
     * @var PepQuery
     * @DI\Inject("pep_query_helper")
     */
    public $pepQueryHelper;

    /**
     * @var TranslatorInterface
     * @DI\Inject("translator")
     */
    public $translator;

    /**
     * PepDatabaseExporter constructor.
     */
    public function __construct()
    {
        $this->fileName = sprintf('base-datos-pep-%s.xls', time());
        $this->tabName = 'PEP';
        $this->title = 'Base de datos de PEP'.date('d-m-Y H:i:s');
    }

    /**
     * @inheritdoc
     */
    protected function fetchData($type, $data)
    {
        $this->data = $this->pepQueryHelper->getPEPDatabase();
    }

    /**
     * @inheritdoc
     */
    protected function fillRows()
    {
        $index = 2;
        
        foreach ($this->data as $pepType => $peps) {
            foreach ($peps as $rut => $pep) {
                $parts = explode(',', $pep);
                
                $this->report->getActiveSheet()
                    ->setCellValue('A'.$index, $this->translator->trans('pep_type_'.$pepType))
                    ->setCellValue('B'.$index, RutUtils::normalize($rut))
                    ->setCellValue('C'.$index, sprintf('%s %s, %s', $parts[2], $parts[3], $parts[4]))
                ;    
                
                ++$index;
            }
        }
        
        foreach (range('A', 'C') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @inheritdoc
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Tipo de PEP')
            ->setCellValue('B1', 'Rut')
            ->setCellValue('C1', 'Nombre completo')
        ;
    }
}