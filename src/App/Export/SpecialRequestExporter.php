<?php

namespace App\Export;

use App\Entity\request;
use App\Entity\SLA;
use App\Entity\SpecialRequest;
use App\Repository\SpecialRequestRepository;
use App\Util\FunctionUtils;
use App\Helper\requestTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\requestRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Util\DateUtils;

/**
 * Class SpecialRequestExporter.
 *
 * @Di\Service("special.request.exporter")
 */
class SpecialRequestExporter
{
    const TIMEOUT = 60 * 2;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;


    /**
     * @var SpecialRequestRepository
     * @Di\Inject("special_request_repo")
     */
    public $specialRequestRepo;


    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var \Redis
     * @Di\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var string
     * @Di\Inject("%kernel.cache_dir%")
     */
    public $varDir;

    /**
     * @var array
     */
    private $historyPrefetch;

    /**
     * @var request
     */
    private $currentRequest;

    /**
     * @var array
     */
    private $currentComments;

    /**
     * @var array
     */
    private $columnNames = [];

    /**
     * @var string
     */
    private $fileName = "";

    /**
     * @var Factory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;

    /**
     * @var array
     */
    private $possibleStatuses = [
        SpecialRequest::STATE_EVALUATION,
        SpecialRequest::STATE_IN_PROGRESS,
        SpecialRequest::STATE_REJECTED,
        SpecialRequest::STATE_APPEALED,
        SpecialRequest::STATE_REJECTED_FINAL,
        SpecialRequest::STATE_CLOSED,
    ];

    /**
     *
     */
    public function export()
    {

        $reportFilename = 'pedido-especial';

        $cacheDir = $this->varDir.'/exporter_cache';

        if (!@mkdir($cacheDir, 0777, true) && !is_dir($cacheDir)) {
            throw new \RuntimeException(sprintf('Cannot create folder %s', $cacheDir));
        }

        $basefilename = sprintf('reporte-%s-%s', $reportFilename, date('YmdHi'));

        $csvname = $cacheDir.'/'.$basefilename.'.csv';

        if (!file_exists($csvname)/* && (time() - self::TIMEOUT) < filemtime($csvname)*/) {
            $file = new \SplFileObject($csvname, 'w');
            $this->writeData($file, $this->getData());

        }
        $this->fileName = $csvname;
        return $this;
    }

    public function getResponse(){

        $basefilename = basename($this->fileName);
        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($this->fileName);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $basefilename,
            iconv('UTF-8', 'ASCII//TRANSLIT', $basefilename)
        );

        return $response;
    }

    /**
     * @param string $filepath
     * @return mixed
     */
    public function getFile($filepath)
    {
        try{
            $file = file_get_contents($this->fileName);
            file_put_contents($filepath, $file);
            return $this;
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return bool|string
     */
    private function getData()
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('r.id as request_uid')
            ->addSelect('r.number as request_number')
            ->addSelect('r.comments as request_comments')
            ->addSelect('r.state as request_state')
            ->addSelect('r.created_at as request_created_at')
            ->addSelect('r.date as request_date')
            ->addSelect('r.description as request_description')
            ->addSelect('r.request as request_detail')
            ->addSelect('monthname(r.created_at) as request_month')
            ->addSelect('year(r.created_at) as request_year')
            ->addSelect('u.email as requester_email')
            ->addSelect('e.rut as requester_rut')
            ->addSelect('concat_ws(" ", e.name, e.last_name, e.second_last_name) as requester_full_name')
            ->addSelect('e.phone as requester_phone')
            ->addSelect('x.title as requester_position')
            ->addSelect('o.name as requester_office_name')
            ->addSelect('o.address as requester_office_address')
            ->addSelect('o.code as requester_office_code')
            ->addSelect('o.cost_center as requester_office_cost_center')
            ->addSelect('o.type as requester_office_type')
            ->addSelect('k.name as requester_office_city')
            ->addSelect('l.name as requester_office_region_name')
            ->addSelect('l.slug as requester_office_region_slug')
            ->addSelect('o.name as request_office_name')
            ->addSelect('o.address as request_office_address')
            ->addSelect('o.code as request_office_code')
            ->addSelect('o.cost_center as request_office_cost_center')
            ->addSelect('o.type as request_office_type')
            ->addSelect('ck.name as request_office_city')
            ->addSelect('cl.name as request_office_region_name')
            ->addSelect('cl.slug as request_office_region_slug')
            ->addSelect('concat_ws(" ", y.name, y.last_name, y.second_last_name) as approver_full_name')
            ->addSelect('y.rut as approver_rut')
            ->addSelect('z.name as supplier_name')
            ->addSelect('z.rut as supplier_rut')
            ->from('special_requests', 'r')
            ->leftJoin('r', 'users',      'u', 'u.id = r.requester_id')
            ->leftJoin('u', 'employees',  'e', 'e.id = u.employee_id')
            ->leftJoin('e', 'offices',    'o', 'o.id = e.office_id')
            ->leftJoin('o', 'categories', 'k', 'k.id = o.city_id')
            ->leftJoin('k', 'categories', 'l', 'l.id = k.parent_id')
            ->leftJoin('o', 'categories', 'ck', 'ck.id = o.city_id')
            ->leftJoin('k', 'categories', 'cl', 'cl.id = ck.parent_id')
            ->leftJoin('e', 'positions',  'x', 'x.id = e.position_id')
            ->leftJoin('r', 'users',      'w', 'w.id = r.assignee_id')
            ->leftJoin('w', 'employees',  'y', 'y.id = w.employee_id')
            ->leftJoin('r', 'suppliers',  'z', 'z.id = r.supplier_id')
            ->andWhere('r.deleted_at is null')
            ->andWhere("r.state <> 'state.cart'")
            ->orderBy('r.number', 'desc');

        if (!$this->redis->exists($key = sha1($query->getSQL()))) {

            $this->redis->setex($key, self::TIMEOUT, $query->execute()->fetchAll(\PDO::FETCH_ASSOC));
        }

        return $this->redis->get($key);
    }

    /**
     * @param \SplFileObject $file
     * @param $data
     */
    public function writeData(\SplFileObject $file, $data)
    {
        $BOM = "\xEF\xBB\xBF";
        $file->fwrite($BOM);
        $accessor = PropertyAccess::createPropertyAccessor();

        $getValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentrequest, $key);
        };

        $getRegionNumber = function ($regionName) {
            $regionToRoman = [
                'arica' => 'XV',
                'tarapaca' => 'I',
                'antofagasta' => 'II',
                'atacama' => 'III',
                'coquimbo' => 'IV',
                'valparaiso' => 'V',
                'libertador' => 'VI',
                'maule' => 'VII',
                'biobio' => 'VIII',
                'araucania' => 'IX',
                'rios' => 'XIV',
                'lagos' => 'X',
                'aisen' => 'XI',
                'magallanes' => 'XII',
                'metropolitana' => 'XIII',
            ];

            foreach ($regionToRoman as $slug => $no) {
                if ((bool)strstr($regionName, $slug)) {
                    return $no;
                }
            }

            return null;
        };

        $getrequestTypeString = function ($type) {
            return $this->translator->trans($type);
        };

        $getRequestStateString = function ($state = null) use ($getValue) {
            $state = !$state ? $getValue('[request_state]') : $state;
            $cent = 'centralized';
            return $this->translator->trans(
                $state.'.special_request.'.$cent
            );
        };

        $getOfficeTypeString = function ($officeType) {
            return $this->translator->trans($officeType);
        };

        $getEmployeePhone = function ($p) {
            return $p;
        };

        $getCreationDate = function ($startedAt) use ($getValue) {
            $month = $startedAt->format('F');
            $year = $startedAt->format('Y');

            return $this->translator->trans($month).' '.$year;
        };

        $unserialize = function ($key) use ($getValue) {
            return @unserialize($getValue($key) ?? 'a:0:{}');
        };

        $isClosed = function () use ($getValue) {
            return in_array($getValue('[request_state]'), SpecialRequest::CLOSED_STATUSES, true);
        };

        $formatMysqlDatetime = function ($datetime) {
            return date('d/m/Y H:i:s', strtotime($datetime));
        };

        $mysqlDatetimeToObject = function ($datetime,$format = 'Y/m/d H:i:s') {
            if(!$datetime){
                return null;
            }
            return \DateTime::createFromFormat($format, $datetime);
        };

        $getKey = function($key){
            $this->columnNames[] = $key;
            return $key;
        };

        $getReqType = FunctionUtils::memoize($getRequestStateString);
        $getRN = FunctionUtils::memoize($getRegionNumber);
        $getTypeOffice = FunctionUtils::memoize($getOfficeTypeString);
        $getStatusString = $getRequestStateString;

        if(!count($data)){
            $file->fputcsv($this->getColumnNames(),';');
        }

        foreach ((array) $data as $i => $request) {

            $this->currentrequest = $request;
            $this->currentComments = $unserialize('[request_comments]');

            $startedAt = $mysqlDatetimeToObject($getValue('[request_created_at]'),'Y-m-d H:i:s');
            $date = $mysqlDatetimeToObject($getValue('[request_date]'),'Y-m-d');
            $endedAt   = $isClosed() ? $mysqlDatetimeToObject($getValue('[request_closed_at]'),'Y-m-d H:i:s'): new \DateTime();


            try {
                $rowData[$getKey('N° solicitud')] = $getValue('[request_number]');
                $rowData[$getKey('RUT Solicitante')] = $getValue('[requester_rut]') ?? 'Sin Información';
                $rowData[$getKey('Solicitante')] = $getValue('[requester_full_name]') ?? 'Sin Información';
                $rowData[$getKey('Anexo')] = $getEmployeePhone($getValue('[requester_phone]')) ?? 'Sin Información';
                $rowData[$getKey('Correo electrónico')] = $getValue('[requester_email]');
                $rowData[$getKey('Dirección')] = $getValue('[request_office_address]');
                $rowData[$getKey('Comuna')] = $getValue('[request_office_city]');
                $rowData[$getKey('N° Región')] = $getRN($getValue('[request_office_region_slug]'));
                $rowData[$getKey('Región')] = $getValue('[request_office_region_name]');
                $rowData[$getKey('Cargo Solicitante')] = $getValue('[requester_position]') ?? 'Sin Información';
                $rowData[$getKey('Codigo Unidad')] = $getValue('[request_office_code]');
                $rowData[$getKey('Nombre Sucursal')] = $getValue('[request_office_name]');
                $rowData[$getKey('Centro de costo')] = $getValue('[request_office_cost_center]');
                $rowData[$getKey('Estado')] = $isClosed() ? 'CERRADO' : 'ABIERTO';
                $rowData[$getKey('Estado en sistema')] = $getStatusString;
                $rowData[$getKey('Fecha de creación')] = $startedAt->format('d/m/Y H:i:s');
                $rowData[$getKey('Fecha entrega real')] =  ''; //cuando el evaluador certifico

                $rowData[$getKey('RUT Evaluador')] = $getValue('[approver_rut]');
                $rowData[$getKey('Nombre Evaluador')] = $getValue('[approver_full_name]');
                $rowData[$getKey('Dirección unidad demandante')] = $getValue('[requester_office_address]');
                $rowData[$getKey('Tipo unidad')] = $getTypeOffice($getValue('[requester_office_type]'));
                $rowData[$getKey('RUT Proveedor')] = $getValue('[supplier_rut]');
                $rowData[$getKey('Nombre Proveedor')] = $getValue('[supplier_name]');
                $rowData[$getKey('Mes creación')] = $getCreationDate($startedAt);
                $rowData[$getKey('¿Qué necesita solicitar?')] = $getValue('[request_detail]');
                $rowData[$getKey('¿Para cuándo lo necesita?')] = $date->format('d/m/Y');
                $rowData[$getKey('Descripción de lo que necesita')] = $getValue('[request_description]');

                if ($i === 0) {
                    $file->fputcsv($this->columnNames,';');
                }
                $this->columnNames = [];
                $file->fputcsv($rowData,';');
            } catch (\Exception $e) {
//                dump($e->getMessage());
            }
        }
    }

    function getColumnNames(){
        $columns =  [
            'N° solicitud',
            'RUT Solicitante',
            'Solicitante',
            'Anexo',
            'Correo electrónico',
            'Dirección',
            'Comuna',
            'N° Región',
            'Región',
            'Cargo Solicitante',
            'Codigo Unidad',
            'Nombre Sucursal',
            'Centro de costo',
            'Estado',
            'Estado en sistema',
            'Fecha de creación',
            'Fecha entrega real',
            '¿Qué necesita solicitar?',
            '¿Para cuándo lo necesita?',
            'Descripción de lo que necesita',
        ];


        $columns[] = 'RUT Evaluador';
        $columns[] = 'Nombre Evaluador';
        $columns[] = 'Dirección unidad demandante';
        $columns[] = 'Tipo unidad';
        $columns[] = 'RUT Proveedor';
        $columns[] = 'Nombre Proveedor';
        $columns[] = 'Cantidad';
        $columns[] = 'Proveedor de despacho';
        $columns[] = 'Mes creación';
        $columns[] = 'Rojo';
        $columns[] = 'Gris';
        $columns[] = 'Mesón rojo';
        $columns[] = 'Mesón gris';
        $columns[] = 'Cajero rojo';
        $columns[] = 'Cajero gris';
        return $columns;
    }

}