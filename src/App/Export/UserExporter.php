<?php

namespace App\Export;


use App\Entity\Employee;
use App\Entity\User;
use App\Repository\UserRepository;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UserExporter.
 *
 * @Di\Service("user.exporter")
 */
class UserExporter extends Exporter
{
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var UserRepository $userRepo
     * @Di\Inject("user_repo")
     */
    public $userRepo;
    

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;
    
public function __construct()
    {
        $this->fileName = sprintf('reporte-usuarios-%s.xls', time());
        $this->tabName = 'Usuarios';
        $this->title = 'Reporte de pep '.date('d-m-Y H:i:s');
    }

    protected function fetchData($type, $data)
    {
        $this->em->getFilters()->disable('soft_deleteable');
        $this->data = $this->userRepo->findAll();
    }

    protected function fillRows()
    {
        
        
        $rowCount = 2;
        /** @var User $user */
        foreach ($this->data as $user) {
            
            /** @var Employee $employee */
            $employee = $user->getEmployee();
            
            $roles = array_map(function ($item) {
                if (in_array($item, ['ROLE_USER', 'ROLE_SYSTEM', 'ROLE_EMPLOYEE'])) {
                    return;
                }
                return $this->translator->trans($item);                
            }, $user->getRoles());
                    
            $rut      = $employee ? $employee->getRut() : null;
            $fullName = $employee ? $employee->getFullName() : null;
            
            $this->report->getActiveSheet()
                ->setCellValue('A'.$rowCount, $user->getUsername())
                ->setCellValue('B'.$rowCount, $rut)
                ->setCellValue('C'.$rowCount, $fullName)
                ->setCellValue('D'.$rowCount, implode(',', array_filter($roles)))
            ;

            ++$rowCount;
        }

        foreach (range('A', 'D') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @throws \PHPExcel_Exception
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'N° solicitud')
            ->setCellValue('B1', 'RUT')
            ->setCellValue('C1', 'Nombre')
            ->setCellValue('D1', 'Roles')
        ;
    }
}
