<?php

namespace App\Export;

use App\Entity\Requisition;
use App\Entity\SLA;
use App\Util\FunctionUtils;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Util\DateUtils;

/**
 * Class SingleRequisitionExporter.
 *
 * @Di\Service("single.requisition.exporter")
 */
class SingleRequisitionExporter
{
    const TIMEOUT = 60 * 2;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var \Redis
     * @Di\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var string
     * @Di\Inject("%kernel.cache_dir%")
     */
    public $varDir;

    /**
     * @var array
     */
    private $historyPrefetch;

    /**
     * @var Requisition
     */
    private $currentRequisition;

    /**
     * @var array
     */
    private $currentContractSlas;

    /**
     * @var array
     */
    private $currentMetadata;

    /**
     * @var array
     */
    private $currentHistory;

    /**
     * @var array
     */
    private $columnNames = [];

    /**
     * @var string
     */
    private $fileName = "";

    /**
     * @var Factory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;

    /**
     * @param string $uid
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function export($uid)
    {

        $cacheDir = $this->varDir.'/exporter_cache';

        if (!@mkdir($cacheDir, 0777, true) && !is_dir($cacheDir)) {
            throw new \RuntimeException(sprintf('Cannot create folder %s', $cacheDir));
        }

        $basefilename = sprintf('detalle-solicitud-%s', date('YmdHi'));
        $csvname = $cacheDir.'/'.$basefilename.'.csv';
        $cacheFilename = $cacheDir.'/'.$basefilename.'.xlsx';

        if (!(file_exists($cacheFilename) && time() - self::TIMEOUT < filemtime($cacheFilename))) {
            $file = new \SplFileObject($csvname, 'w');

            $this->writeData($file, $this->getData($uid));

            $reader = \PHPExcel_IOFactory::createReader('CSV');
            $excel = $reader->load($file->getRealPath());

            $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $writer->save($cacheFilename);

            @unlink($file->getRealPath());
        }

        $this->fileName = $cacheFilename;
        return $this;
    }

    public function getResponse(){

        $basefilename = basename($this->fileName);
        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($this->fileName);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $basefilename,
            iconv('UTF-8', 'ASCII//TRANSLIT', $basefilename)
        );

        return $response;
    }

    /**
     * @param $filepath
     */
    public function getFile($filepath)
    {
        try{
            $file = file_get_contents($this->fileName);
            file_put_contents($filepath,$file);
            return $this;
        }catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $uid
     * @return bool|string
     */
    private function getData($uid)
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('r.id as requisition_uid')
            ->addSelect('r.number as requisition_number')
            ->addSelect('r.type as requisition_type')
            ->addSelect('r.metadata as requisition_metadata')
            ->addSelect('r.transitions as requisition_transitions')
            ->addSelect('r.status as requisition_state')
            ->addSelect('r.created_at as requisition_created_at')
            ->addSelect('r.closed_at as requisition_closed_at')
            ->addSelect('r.quantity as requisition_quantity')
            ->addSelect('monthname(r.created_at) as requisition_month')
            ->addSelect('year(r.created_at) as requisition_year')
            ->addSelect('c.contract_number as contract_number')
            ->addSelect('c.centralized as contract_type')
            ->addSelect('r.slas as contract_slas')
            ->addSelect('u.email as requester_email')
            ->addSelect('e.rut as requester_rut')
            ->addSelect('concat_ws(" ", e.name, e.last_name, e.second_last_name) as requester_full_name')
            ->addSelect('e.phone as requester_phone')
            ->addSelect('x.title as requester_position')
            ->addSelect('o.name as requester_office_name')
            ->addSelect('o.address as requester_office_address')
            ->addSelect('o.code as requester_office_code')
            ->addSelect('o.cost_center as requester_office_cost_center')
            ->addSelect('o.type as requester_office_type')
            ->addSelect('k.name as requester_office_city')
            ->addSelect('l.name as requester_office_region_name')
            ->addSelect('l.slug as requester_office_region_slug')
            ->addSelect('p.name as product_name')
            ->addSelect('p.code as product_code')
            ->addSelect('t.name as product_category')
            ->addSelect('concat_ws(" ", y.name, y.last_name, y.second_last_name) as approver_full_name')
            ->addSelect('y.rut as approver_rut')
            ->addSelect('z.name as supplier_name')
            ->addSelect('z.rut as supplier_rut')
            ->from('requisitions', 'r')
            ->leftJoin('r', 'contracts',  'c', 'c.id = r.contract_id')
            ->leftJoin('r', 'users',      'u', 'u.id = r.requester_id')
            ->leftJoin('u', 'employees',  'e', 'e.id = u.employee_id')
            ->leftJoin('c', 'products',   'p', 'p.id = c.product_id')
            ->leftJoin('p', 'categories', 't', 't.id = p.category_id')
            ->leftJoin('e', 'offices',    'o', 'o.id = e.office_id')
            ->leftJoin('o', 'categories', 'k', 'k.id = o.city_id')
            ->leftJoin('k', 'categories', 'l', 'l.id = k.parent_id')
            ->leftJoin('e', 'positions',  'x', 'x.id = e.position_id')
            ->leftJoin('p', 'users',      'w', 'w.id = p.approver_id')
            ->leftJoin('w', 'employees',  'y', 'y.id = w.employee_id')
            ->leftJoin('c', 'suppliers',  'z', 'z.id = c.supplier_id')
            ->where("r.id = :uid")
            ->andWhere('r.deleted_at is null')
            ->orderBy('r.number', 'desc')
            ->setParameter('uid', [$uid], Connection::PARAM_STR_ARRAY)
        ;

        $this->historyPrefetch = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('s.workflow_identifier as process_uid')
            ->addSelect('s.process_name as process_name')
            ->addSelect('s.step_name as process_step')
            ->addSelect('concat_ws(".", s.process_name, s.step_name) as process_step_full')
            ->addSelect('s.created_at as process_created_at')
            ->addSelect('s.finished_at as process_finished_at')
            ->from('workflow_state', 's')
            ->execute()
            ->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_ASSOC)
        ;

        if (!$this->redis->exists($key = sha1($query->getSQL()))) {
            $this->redis->setex($key, self::TIMEOUT, $query->execute()->fetchAll(\PDO::FETCH_ASSOC));
        }

        return $this->redis->get($key);
    }

    /**
     * @param \SplFileObject $file
     * @param $data
     */
    public function writeData(\SplFileObject $file, $data)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        $getValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentRequisition, $key);
        };

        $getRegionNumber = function ($regionName) {
            $regionToRoman = [
                'arica' => 'XV',
                'tarapaca' => 'I',
                'antofagasta' => 'II',
                'atacama' => 'III',
                'coquimbo' => 'IV',
                'valparaiso' => 'V',
                'libertador' => 'VI',
                'maule' => 'VII',
                'biobio' => 'VIII',
                'araucania' => 'IX',
                'rios' => 'XIV',
                'lagos' => 'X',
                'aisen' => 'XI',
                'magallanes' => 'XII',
                'metropolitana' => 'XIII',
            ];

            foreach ($regionToRoman as $slug => $no) {
                if ((bool)strstr($regionName, $slug)) {
                    return $no;
                }
            }

            return null;
        };

        $getRequisitionTypeString = function ($type) {
            return $this->translator->trans($type);
        };

        $getRequisitionStateString = function ($state = null) use ($getValue) {
            $state = !$state ? $getValue('[requisition_state]') : $state;
            $cent = $getValue('[contract_type]');
            $type = $getValue('[requisition_type]');

            return $this->translator->trans(
                $state.'.'.str_replace('requisition.', '', $type).'.'.($cent ? 'centralized' : 'descentralized')
            );
        };

        $getOfficeTypeString = function ($officeType) {
            return $this->translator->trans($officeType);
        };

        $getStatus = function($status){
            return $this->translator->trans($status);
        };

        $getSLAType = function ($sla) {
            return $this->translator->trans($sla->getType());
        };

        $getEmployeePhone = function ($p) {
            if (!$p) {
                return null;
            }

            $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $p));
            return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
        };

        $getMetadata = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentMetadata, $key);
        };

        $getCreationDate = function () use ($getValue) {
            $month = $getValue('[requisition_month]');
            $year = $getValue('[requisition_year]');

            return $this->translator->trans($month).' '.$year;
        };

        $getRequisitionQuantity = function () use ($getValue) {
            $mock = new Requisition();
            $mock->setMetadata($this->currentMetadata);
            $mock->setQuantity($getValue('[requisition_quantity]'));

            return $mock->getQuantity();
        };

        $unserialize = function ($key) use ($getValue) {
            return @unserialize($getValue($key) ?? 'a:0:{}');
        };

        $getWorkflowHistory = function () use ($getValue) {
            $oid = Requisition::class.$getValue('[requisition_uid]');
            $workflowId = Uuid::uuid5(Uuid::NAMESPACE_DNS, $oid)->toString();
            return $this->historyPrefetch[$workflowId];
        };

        $checkNewFormat = function () use ($getMetadata) {
            if (!$getMetadata('[details]')) {
                return false;
            }

            return (bool) $getMetadata('[details][caracteristicas]');
        };

        $totalByColor = function ($color) use ($getMetadata, $checkNewFormat) {
            if (!$checkNewFormat()) {
                return null;
            }

            $item = $getMetadata('[details][caracteristicas]') ?? [];

            return array_reduce($item, function ($carry, $item) use ($color) {
                if (isset($item['color']) && $color === $item['color']) {
                    $carry += $item['cantidad'] ?? 0;
                }

                return $carry;
            }, 0);
        };

        $totalByColorAndModel = function ($color, $model) use ($getMetadata, $checkNewFormat) {
            if (!$checkNewFormat()) {
                return null;
            }

            $items = $getMetadata('[details][caracteristicas]') ?? [];

            return array_reduce($items, function ($carry, $item) use ($color, $model) {
                if ((isset($item['color']) && $color === $item['color']) &&
                    (isset($item['modelo']) && $model === $item['modelo'])
                ) {
                    $carry += $item['cantidad'] ?? 0;
                }

                return $carry;
            }, 0);
        };

        $isClosed = function () use ($getValue) {
            return in_array($getValue('[requisition_state]'), Requisition::CLOSED_STATUSES, true);
        };

        $formatMysqlDatetime = function ($datetime) {
            return date('d/m/Y H:i:s', strtotime($datetime));
        };

        $mysqlDatetimeToObject = function ($datetime,$format = 'Y/m/d H:i:s') {
            if(!$datetime){
                return null;
            }
            return \DateTime::createFromFormat($format, $datetime);
        };


        $leftCartAt = function () use ($formatMysqlDatetime) {
            $index0 = 1 === count($this->currentHistory) ? 0 : 1;
            return $formatMysqlDatetime($this->currentHistory[$index0]['process_created_at']);
        };

        $getSLAValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentContractSlas, "[$key]");
        };

        $estimadedSLA = function ($start,$status = null) use ($getSLAValue,$accessor){
            if(!$status){
                return $this->slaCalculator->getEstimate(
                    $start,
                    $getSLAValue(ProductStatus::DELIVERED),
                    $getSLAValue(ProductStatus::APPROVED),
                    $getSLAValue(ProductStatus::CREATED),
                    $getSLAValue(ProductStatus::CONFIRMED),
                    $getSLAValue(ProductStatus::REVIEWED)
                );
            }

            return $accessor->isReadable($this->currentContractSlas, "[$status]") ?
                $this->slaCalculator->getEstimate($start,$getSLAValue($status)) : null;
        };

        $inSLA = function($estimatedTo,$endedAt = null){
            return $endedAt ? $estimatedTo > $endedAt : $estimatedTo > new \DateTime();

        };

        $getKey = function($key){
            $this->columnNames[] = $key;
            return $key;
        };

        $getReqType = FunctionUtils::memoize($getRequisitionTypeString);
        $getRN = FunctionUtils::memoize($getRegionNumber);
        $getTypeOffice = FunctionUtils::memoize($getOfficeTypeString);
        $getSLATypeString = FunctionUtils::memoize($getSLAType);
        $getStatusString = FunctionUtils::memoize($getRequisitionStateString);


        foreach ((array) $data as $i => $requisition) {

            $this->currentRequisition = $requisition;
            $this->currentMetadata = $unserialize('[requisition_metadata]');
            $this->currentHistory = $getWorkflowHistory();
            $this->currentContractSlas = $unserialize('[contract_slas]');

            $startedAt = $mysqlDatetimeToObject($leftCartAt(),'d/m/Y H:i:s');
            $endedAt   = $isClosed() ? $mysqlDatetimeToObject($getValue('[requisition_closed_at]'),'Y-m-d H:i:s'): new \DateTime();
            $estimatedEnd = $estimadedSLA($startedAt);
            $inSla = $inSLA($estimatedEnd,$endedAt);

            try {
                $rowData[$getKey('N° solicitud')] = $getValue('[requisition_number]');
                $rowData[$getKey('Tipo')] = $getReqType($getValue('[requisition_type]'));
                $rowData[$getKey('RUT Solicitante')] = $getValue('[requester_rut]');
                $rowData[$getKey('Solicitante')] = $getValue('[requester_full_name]');
                $rowData[$getKey('Anexo')] = $getEmployeePhone($getValue('[requester_phone]'));
                $rowData[$getKey('Correo electrónico')] = $getValue('[requester_email]');
                $rowData[$getKey('Dirección')] = $getValue('[requester_office_address]');
                $rowData[$getKey('Comuna')] = $getValue('[requester_office_city]');
                $rowData[$getKey('N° Región')] = $getRN($getValue('[requester_office_region_slug]'));
                $rowData[$getKey('Región')] = $getValue('[requester_office_region_name]');
                $rowData[$getKey('Cargo Solicitante')] = $getValue('[requester_position]');
                $rowData[$getKey('Codigo Unidad')] = $getValue('[requester_office_code]');
                $rowData[$getKey('Nombre Sucursal')] = $getValue('[requester_office_name]');
                $rowData[$getKey('Centro de costo')] = $getValue('[requester_office_cost_center]');
                $rowData[$getKey('Estado')] = $isClosed() ? 'CERRADO' : 'ABIERTO';
                $rowData[$getKey('Estado en sistema')] = $getRequisitionStateString();
                $rowData[$getKey('Fecha de creación')] = $formatMysqlDatetime($getValue('[requisition_created_at]'));
                $rowData[$getKey('Fecha posible de entrega')] = $estimatedEnd->format('d/m/Y H:i:s');
                $rowData[$getKey('Fecha entrega real')] = $isClosed() ? $formatMysqlDatetime($getValue('[requisition_closed_at]')) : '';
                $rowData[$getKey('Codigo técnico')] = $getValue('[product_code]');
                $rowData[$getKey('Producto')] = $getValue('[product_name]');
                $rowData[$getKey('Categoría')] = $getValue('[product_category]');
                if($getValue('[contract_type]')) {
                    $rowData[$getKey('RUT Evaluador')] = $getValue('[approver_rut]');
                    $rowData[$getKey('Nombre Evaluador')] = $getValue('[approver_full_name]');
                }
                $rowData[$getKey('Dirección unidad demandante')] = $getValue('[requester_office_address]');
                $rowData[$getKey('Tipo unidad')] = $getTypeOffice($getValue('[requester_office_type]'));
                $rowData[$getKey('RUT Proveedor')] = $getValue('[supplier_rut]');
                $rowData[$getKey('Nombre Proveedor')] = $getValue('[supplier_name]');
                $rowData[$getKey('Cantidad')] = $getRequisitionQuantity();
                $rowData[$getKey('Mes creación')] = $getCreationDate();

                if($checkNewFormat()){
                    $rowData[$getKey('comentarios')] = $getMetadata('[details][comentarios]');
                    $rowData[$getKey('Rojo')] = $totalByColor('rojo');
                    $rowData[$getKey('Gris')] = $totalByColor('gris');
                    $rowData[$getKey('Mesón rojo')] = $totalByColorAndModel('rojo', 'meson');
                    $rowData[$getKey('Mesón gris')] = $totalByColorAndModel('gris', 'meson');
                    $rowData[$getKey('Cajero rojo')] = $totalByColorAndModel('rojo', 'cajero');
                    $rowData[$getKey('Cajero gris')] = $totalByColorAndModel('gris', 'cajero');
                }else{
                    if($details = $getMetadata('[details]')){
                        foreach ($details as $key => $value){
                            $rowData[$getKey($key)] = $value;
                        }
                    }
                }

                if ($i === 0) {
                    $file->fputcsv($this->columnNames);
                }
                $this->columnNames = [];
                $file->fputcsv($rowData);
            } catch (\Exception $e) {
                //dump($e->getMessage());
            }
        }
    }

}