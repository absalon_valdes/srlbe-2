<?php

namespace App\Export;

use App\Entity\Product;
use App\Repository\ProductRepository;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ProductDatabaseExporter
 * @package App\Export
 * @DI\Service("product_exporter")
 * @DI\Tag("exporter", attributes={"alias"="products"})
 */
class ProductDatabaseExporter extends Exporter
{
    /**
     * @var ProductRepository
     * @DI\Inject("product_repo")
     */
    public $productRepo;

    /**
     * ProductDatabaseExporter constructor.
     */
    public function __construct()
    {
        $this->fileName = sprintf('base-datos-productos-%s.xls', time());
        $this->tabName = 'Productos';
        $this->title = 'Base de datos de productos '.date('d-m-Y H:i:s');
    }

    /**
     * @inheritdoc
     */
    protected function fetchData($type, $data)
    {
        $this->data = $this->productRepo->findAll();
    }
    
    /**
     * @inheritdoc
     */
    protected function fillRows()
    {
        $index = 2;
        
        usort($this->data, function ($a, $b) {
            if (!$c = strcmp($a->getCategoryPath(), $b->getCategoryPath())) {
                $c = strcmp($a->getCode(), $b->getCode());
            }

            return $c;
        });
        
        $products = [];

        foreach ($this->data as $product) {
            $products[$product->getCode()] = $product;
        }

        /** @var Product $product */
        foreach ($products as $product) {
            $this->report->getActiveSheet()
                ->setCellValue('A'.$index, $product->getCode())
                ->setCellValue('B'.$index, $product->getName())
                ->setCellValue('C'.$index, $product->getUnitOfMeasure())
                ->setCellValue('D'.$index, $product->getApproverId())
                ->setCellValue('E'.$index, $product->getCategoryPath())
                ->setCellValue('F'.$index, $product->getDescription())
            ;
            ++$index;
        }
        
        foreach (range('A', 'F') as $columnID) {
            $this->report
                ->getActiveSheet()
                ->getColumnDimension($columnID)
                ->setAutoSize(true)
            ;
        }
    }

    /**
     * @inheritdoc
     */
    protected function fillColumnHeaders()
    {
        $this->report->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Código técnico')
            ->setCellValue('B1', 'Nombre')
            ->setCellValue('C1', 'Unidad de medida')
            ->setCellValue('D1', 'Evaluador')
            ->setCellValue('E1', 'Categoría')
            ->setCellValue('F1', 'Descripción')
        ;
    }
}