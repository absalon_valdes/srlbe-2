<?php

namespace App\Export;

use App\Entity\Requisition;
use App\Entity\SLA;
use App\Helper\UserHelper;
use App\Util\FunctionUtils;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\RequisitionRepository;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use BE\UniformBundle\Twig\DeliveryEstimate;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as Di;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use App\Util\DateUtils;

/**
 * Class SupplierUniformRequisitionExporter.
 *
 * @Di\Service("supplier.uniform.requisition.exporter")
 */
class SupplierUniformRequisitionExporter
{
    const TIMEOUT = 60 * 2;

    /**
     * @var string
     * @Di\Inject("%compostura_code%")
     */
    public $compProductCode;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine")
     */
    public $doctrine;

    /**
     * @var \Redis
     * @Di\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var string
     * @Di\Inject("%kernel.cache_dir%")
     */
    public $varDir;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var array
     */
    private $historyPrefetch;

    /**
     * @var Requisition
     */
    private $currentRequisition;

    /**
     * @var array
     */
    private $currentContractSlas;

    /**
     * @var array
     */
    private $currentMetadata;

    /**
     * @var array
     */
    private $currentHistory;

    /**
     * @var array
     */
    private $columnNames = [];

    /**
     * @var string
     */
    private $fileName = "";

    /**
     * @var Factory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;

    /**
     * @return SupplierUniformRequisitionExporter
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function export()
    {

        $cacheDir = $this->varDir . '/exporter_cache';

        if (!@mkdir($cacheDir, 0777, true) && !is_dir($cacheDir)) {
            throw new \RuntimeException(sprintf('Cannot create folder %s', $cacheDir));
        }

        $basefilename = sprintf('reporte-uniformes-%s', date('YmdHi'));
        $csvname = $cacheDir . '/' . $basefilename . '.csv';
        $cacheFilename = $cacheDir . '/' . $basefilename . '.xlsx';

        if (!(file_exists($cacheFilename) && time() - self::TIMEOUT < filemtime($cacheFilename))) {
            $file = new \SplFileObject($csvname, 'w');

            $this->writeData($file, $this->getData());

            $reader = \PHPExcel_IOFactory::createReader('CSV');
            $excel = $reader->load($file->getRealPath());

            $writer = \PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $writer->save($cacheFilename);

            @unlink($file->getRealPath());
        }

        $this->fileName = $cacheFilename;
        return $this;
    }

    public function getResponse()
    {

        $basefilename = basename($this->fileName);
        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($this->fileName);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $basefilename,
            iconv('UTF-8', 'ASCII//TRANSLIT', $basefilename)
        );

        return $response;
    }

    /**
     * @param $filepath
     */
    public function getFile($filepath)
    {
        try {
            $file = file_get_contents($this->fileName);
            file_put_contents($filepath, $file);
            return $this;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return bool|string
     */
    private function getData()
    {
        $query = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('r.id as requisition_uid')
            ->addSelect('r.number as requisition_number')
            ->addSelect('r.type as requisition_type')
            ->addSelect('r.metadata as requisition_metadata')
            ->addSelect('r.transitions as requisition_transitions')
            ->addSelect('r.status as requisition_state')
            ->addSelect('r.created_at as requisition_created_at')
            ->addSelect('r.closed_at as requisition_closed_at')
            ->addSelect('r.quantity as requisition_quantity')
            ->addSelect('r.state_due_date as requisition_state_due_date')
            ->addSelect('monthname(r.created_at) as requisition_month')
            ->addSelect('year(r.created_at) as requisition_year')
            ->addSelect('c.contract_number as contract_number')
            ->addSelect('c.centralized as contract_type')
            ->addSelect('c.slas as contract_slas')
            ->addSelect('u.email as requester_email')
            ->addSelect('e.rut as requester_rut')
            ->addSelect('concat_ws(" ", e.name, e.last_name, e.second_last_name) as requester_full_name')
            ->addSelect('e.phone as requester_phone')
            ->addSelect('e.second_phone as requester_phone_2')
            ->addSelect('e.alternate_address as requester_alternate_address')
            ->addSelect('x.title as requester_position')
            ->addSelect('o.name as request_office_name')
            ->addSelect('f.address as request_office_address')
            ->addSelect('f.address as requester_office_address')
            ->addSelect('o.address as request_office_address_legacy')
            ->addSelect('o.address as requester_office_address_legacy')
            ->addSelect('co.code as request_office_code')
            ->addSelect('co.cost_center as request_office_cost_center')
            ->addSelect('o.type as request_office_type')
            ->addSelect('o.type as requester_office_type')
            ->addSelect('k.name as request_office_city')
            ->addSelect('l.name as request_office_region_name')
            ->addSelect('l.slug as request_office_region_slug')
            ->addSelect('p.name as product_name')
            ->addSelect('p.code as product_code')
            ->addSelect('p.id as product_id')
            ->addSelect('t.name as product_category')
            ->addSelect('concat_ws(" ", y.name, y.last_name, y.second_last_name) as approver_full_name')
            ->addSelect('y.rut as approver_rut')
            ->addSelect('z.name as supplier_name')
            ->addSelect('z.rut as supplier_rut')
            ->from('requisitions', 'r')
            ->leftJoin('r', 'contracts', 'c', 'c.id = r.contract_id')
            ->leftJoin('r', 'users', 'u', 'u.id = r.requester_id')
            ->leftJoin('u', 'employees', 'e', 'e.id = u.employee_id')
            ->leftJoin('c', 'products', 'p', 'p.id = r.product_id')
            ->leftJoin('p', 'categories', 't', 't.id = p.category_id')
            ->leftJoin('c', 'offices', 'co', 'co.id = c.office_id')
            ->leftJoin('e', 'offices', 'o', 'o.id = e.office_id')
            ->leftJoin('e', 'offices', 'f', 'f.id = e.delivery_address_id')
            ->leftJoin('o', 'categories', 'k', 'k.id = o.city_id')
            ->leftJoin('k', 'categories', 'l', 'l.id = k.parent_id')
            ->leftJoin('e', 'positions', 'x', 'x.id = e.position_id')
            ->leftJoin('p', 'users', 'w', 'w.id = p.approver_id')
            ->leftJoin('w', 'employees', 'y', 'y.id = w.employee_id')
            ->leftJoin('z', 'supplier_representatives', 'sr', 'z.id = sr.supplier_id')
            ->leftJoin('c', 'suppliers', 'z', 'z.id = c.supplier_id')
            ->where("r.type = :type")
            ->andWhere('r.deleted_at is null')
            ->andWhere("r.status <> 'product.draft'")
            ->orderBy('r.updated_at', 'desc')
            ->setParameter('type', [Requisition::TYPE_UNIFORM], Connection::PARAM_STR_ARRAY)
        ;

        if ($this->userHelper->isGranted('ROLE_SUPPLIER')) {
            $query
                ->andWhere("sr.user_id = :supplier_user")
                ->setParameter('supplier_user', [$this->userHelper->getCurrentUser()->getId()], Connection::PARAM_INT_ARRAY)
            ;
        }

        $this->historyPrefetch = $this->em->getConnection()
            ->createQueryBuilder()
            ->addSelect('s.workflow_identifier as process_uid')
            ->addSelect('s.process_name as process_name')
            ->addSelect('s.step_name as process_step')
            ->addSelect('concat_ws(".", s.process_name, s.step_name) as process_step_full')
            ->addSelect('s.created_at as process_created_at')
            ->addSelect('s.finished_at as process_finished_at')
            ->from('workflow_state', 's')
            ->execute()
            ->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_ASSOC);

        if (!$this->redis->exists($key = sha1($query->getSQL()))) {
            $this->redis->setex($key, self::TIMEOUT, $query->execute()->fetchAll(\PDO::FETCH_ASSOC));
        }

        return $this->redis->get($key);
    }

    /**
     * @param \SplFileObject $file
     * @param $data
     */
    public function writeData(\SplFileObject $file, $data)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        $getValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentRequisition, $key);
        };

        $getRegionNumber = function ($regionName) {
            $regionToRoman = [
                'arica' => 'XV',
                'tarapaca' => 'I',
                'antofagasta' => 'II',
                'atacama' => 'III',
                'coquimbo' => 'IV',
                'valparaiso' => 'V',
                'libertador' => 'VI',
                'maule' => 'VII',
                'biobio' => 'VIII',
                'araucania' => 'IX',
                'rios' => 'XIV',
                'lagos' => 'X',
                'aisen' => 'XI',
                'magallanes' => 'XII',
                'metropolitana' => 'XIII',
            ];

            foreach ($regionToRoman as $slug => $no) {
                if ((bool)strstr($regionName, $slug)) {
                    return $no;
                }
            }

            return null;
        };

        $getRequisitionTypeString = function ($type) {
            return $this->translator->trans($type);
        };

        $getRequisitionStateString = function ($state = null) use ($getValue) {
            $state = !$state ? $getValue('[requisition_state]') : $state;
            $cent = $getValue('[contract_type]');
            $type = $getValue('[requisition_type]');

            return $this->translator->trans(
                $state . '.' . str_replace('requisition.', '', $type) . '.' . ($cent ? 'centralized' : 'descentralized')
            );
        };

        $getOfficeTypeString = function ($officeType) {
            return $this->translator->trans($officeType);
        };

        $getStatus = function ($status) {
            return $this->translator->trans($status);
        };

        $getSLAType = function ($sla) {
            return $this->translator->trans($sla->getType());
        };

        $getEmployeePhone = function ($p) {
            if (!$p) {
                return null;
            }

            $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $p));
            return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
        };

        $getMetadata = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentMetadata, $key);
        };

        $getCreationDate = function () use ($getValue) {
            $month = $getValue('[requisition_month]');
            $year = $getValue('[requisition_year]');

            return $this->translator->trans($month) . ' ' . $year;
        };

        $getRequisitionQuantity = function () use ($getValue) {
            $mock = new Requisition();
            $mock->setMetadata($this->currentMetadata);
            $mock->setQuantity($getValue('[requisition_quantity]'));

            return $mock->getQuantity();
        };

        $getRealNumber = function () use ($getValue) {
            $mock = new Requisition();
            $mock->setNumber($getValue('[requisition_number]'));
            $mock->setMetadata($this->currentMetadata);

            return $mock->getNumber();
        };

        $unserialize = function ($key) use ($getValue) {
            return @unserialize($getValue($key) ?? 'a:0:{}');
        };

        $getWorkflowHistory = function () use ($getValue) {
            $oid = Requisition::class . $getValue('[requisition_uid]');
            $workflowId = Uuid::uuid5(Uuid::NAMESPACE_DNS, $oid)->toString();
            return $this->historyPrefetch[$workflowId];
        };

        $checkNewFormat = function () use ($getMetadata) {
            if (!$getMetadata('[details]')) {
                return false;
            }

            return (bool)$getMetadata('[details][caracteristicas]');
        };

        $isClosed = function () use ($getValue) {
            return in_array($getValue('[requisition_state]'), Requisition::CLOSED_STATUSES, true);
        };

        $formatMysqlDatetime = function ($datetime) {
            return date('d/m/Y H:i:s', strtotime($datetime));
        };

        $mysqlDatetimeToObject = function ($datetime, $format = 'Y/m/d H:i:s') {
            if (!$datetime) {
                return null;
            }
            return \DateTime::createFromFormat($format, $datetime);
        };


        $leftCartAt = function () use ($formatMysqlDatetime) {
            $index0 = 1 === count($this->currentHistory) ? 0 : 1;
            return $formatMysqlDatetime($this->currentHistory[$index0]['process_created_at']);
        };

        $getSLAValue = function ($key) use ($accessor) {
            return $accessor->getValue($this->currentContractSlas, "[$key]");
        };

        $estimadedSLA = function ($start, $status = null) use ($getSLAValue, $accessor) {
            if (!$status) {
                return $this->slaCalculator->getEstimate(
                    $start,
                    $getSLAValue(ProductStatus::DELIVERED),
                    $getSLAValue(ProductStatus::APPROVED),
                    $getSLAValue(ProductStatus::CREATED),
                    $getSLAValue(ProductStatus::CONFIRMED),
                    $getSLAValue(ProductStatus::REVIEWED)
                );
            }

            return $accessor->isReadable($this->currentContractSlas, "[$status]") ?
                $this->slaCalculator->getEstimate($start, $getSLAValue($status)) : null;
        };

        $inSLA = function ($estimatedTo, $endedAt = null) {
            return $endedAt ? $estimatedTo > $endedAt : $estimatedTo > new \DateTime();

        };

        $getKey = function ($key) {
            $this->columnNames[] = $key;
            return $key;
        };

        $getReqType = FunctionUtils::memoize($getRequisitionTypeString);
        $getRN = FunctionUtils::memoize($getRegionNumber);
        $getTypeOffice = FunctionUtils::memoize($getOfficeTypeString);
        $getSLATypeString = FunctionUtils::memoize($getSLAType);
        $getStatusString = FunctionUtils::memoize($getRequisitionStateString);

        $deliveryEstimate = new DeliveryEstimate($this->slaCalculator,$this->doctrine);

        $baseReqCols = [
            'alternativas',
            'busto',
            'cabeza',
            'cadera',
            'cantidad_faldas',
            'cantidad_pantalones',
            'cintura',
            'cintura_veston',
            'comentarios',
            'cuello',
            'discapacidad',
            'descripcion_discapacidad',
            'entrepierna',
            'espalda',
            'estatura',
            'falda',
            'hombro',
            'largo_blazer',
            'largo_blusa',
            'largo_chaqueta',
            'largo_chaquetilla',
            'largo_chaqueton',
            'largo_gillette',
            'largo_manga',
            'largo_manga_blazer',
            'largo_manga_blusa',
            'largo_manga_camisa',
            'largo_manga_chaquetilla',
            'largo_manga_chaqueton',
            'largo_manga_veston',
            'largo_total_blazer',
            'largo_total_camisa',
            'largo_total_casaca',
            'largo_total_chaqueta',
            'largo_total_falda',
            'largo_total_manga',
            'largo_total_pantalon',
            'largo_total_veston',
            'observaciones',
            'pantalon',
            'pantalon_pretina',
            'pecho',
            'pecho_veston',
            'peso',
            'pie',
            'talla_blazer',
            'talla_blazer_gillet',
            'talla_blusa',
            'talla_buzo',
            'talla_calcetin',
            'talla_calzado',
            'talla_camisa',
            'talla_casaca',
            'talla_chaquetilla',
            'talla_embarazada',
            'talla_falda',
            'talla_pantalon',
            'talla_pantalon_falda',
            'talla_veston'
        ];

        $compReqCols = [
            'prenda',
            'talla_recibida',
            'talla_requerida',
            'largo_de_manga',
            'largo_total',
            'comentarios',
        ];

        foreach ((array)$data as $i => $requisition) {

            $this->currentRequisition = $requisition;
            $this->currentMetadata = $unserialize('[requisition_metadata]');
            $this->currentHistory = $getWorkflowHistory();
            $this->currentContractSlas = $unserialize('[contract_slas]');

            $startedAt = $mysqlDatetimeToObject($leftCartAt(), 'd/m/Y H:i:s');
            $endedAt = $isClosed() ? $mysqlDatetimeToObject($getValue('[requisition_closed_at]'), 'Y-m-d H:i:s') : new \DateTime();
            $estimatedEnd = $estimadedSLA($startedAt);
            $inSla = $inSLA($estimatedEnd, $endedAt);
            $estimatedDeliveryEnd  = $deliveryEstimate->deliveryDate($getValue('[product_id]'),$getValue('[requisition_state_due_date]'));

            try {
                $rowData[$getKey('N° solicitud')] = $getRealNumber();
                $rowData[$getKey('Tipo')] = $getReqType($getValue('[requisition_type]'));
                $rowData[$getKey('RUT Solicitante')] = $getValue('[requester_rut]');
                $rowData[$getKey('Solicitante')] = $getValue('[requester_full_name]');
                $rowData[$getKey('Anexo')] = $getEmployeePhone($getValue('[requester_phone]'));
                $rowData[$getKey('Telefono Secundario')] = $getEmployeePhone($getValue('[requester_phone_2]'));
                $rowData[$getKey('Correo electrónico')] = $getValue('[requester_email]');
                if($getMetadata('[delivery_address_2]')){
                    $rowData[$getKey('Dirección de despacho')] = $getMetadata('[delivery_address_2].office.address');
                    $rowData[$getKey('Datos adicionales de dirección')] = $getMetadata('[delivery_address_2].detail');
                    $rowData[$getKey('Comuna')] = $getMetadata('[delivery_address_2].office.city');
                    $rowData[$getKey('N° Región')] = $getRN($getMetadata('[delivery_address_2].office.city.parent.slug'));
                    $rowData[$getKey('Región')] = $getMetadata('[delivery_address_2].office.city.parent.name');
                    $rowData[$getKey('Cargo Solicitante')] = $getValue('[requester_position]');
                    $rowData[$getKey('Codigo Unidad')] = $getMetadata('[delivery_address_2].office.code');
                    $rowData[$getKey('Nombre Sucursal')] = $getMetadata('[delivery_address_2].office.name');
                    $rowData[$getKey('Centro de costo')] = $getMetadata('[delivery_address_2].office.costCenter');
                    $rowData[$getKey('Estado')] = $isClosed() ? 'CERRADO' : 'ABIERTO';
                    $rowData[$getKey('Estado en sistema')] = $getRequisitionStateString();
                    $rowData[$getKey('Fecha de creación')] = $startedAt->format('d/m/Y H:i:s');
                    $tmp ='';
                    if(isset($estimatedDeliveryEnd['deliveryDate'])&&isset($estimatedDeliveryEnd['deliveryDate']['deliveryAt'])){
                        $tmp = $estimatedDeliveryEnd['deliveryDate']['deliveryAt']->format('d/m/Y H:i:s');
                    }
                    $rowData[$getKey('Fecha posible de entrega')] = $tmp;
                    $rowData[$getKey('Fecha entrega real')] = $isClosed() ? $formatMysqlDatetime($getValue('[requisition_closed_at]')) : '';
                    $rowData[$getKey('Codigo técnico')] = $getValue('[product_code]');
                    $rowData[$getKey('Producto')] = $getValue('[product_name]');
                    $rowData[$getKey('Categoría')] = $getValue('[product_category]');
                }else{
                    $rowData[$getKey('Dirección de despacho')] = $getValue('[request_office_address]');
                    $rowData[$getKey('Datos adicionales de dirección')] = $getValue('[requester_alternate_address]');
                    $rowData[$getKey('Comuna')] = $getValue('[request_office_city]');
                    $rowData[$getKey('N° Región')] = $getRN($getValue('[request_office_region_slug]'));
                    $rowData[$getKey('Región')] = $getValue('[request_office_region_name]');
                    $rowData[$getKey('Cargo Solicitante')] = $getValue('[requester_position]');
                    $rowData[$getKey('Codigo Unidad')] = $getValue('[request_office_code]');
                    $rowData[$getKey('Nombre Sucursal')] = $getValue('[request_office_name]');
                    $rowData[$getKey('Centro de costo')] = $getValue('[request_office_cost_center]');
                    $rowData[$getKey('Estado')] = $isClosed() ? 'CERRADO' : 'ABIERTO';
                    $rowData[$getKey('Estado en sistema')] = $getRequisitionStateString();
                    $rowData[$getKey('Fecha de creación')] = $startedAt->format('d/m/Y H:i:s');
                    $tmp ='';
                    if(isset($estimatedDeliveryEnd['deliveryDate'])&&isset($estimatedDeliveryEnd['deliveryDate']['deliveryAt'])){
                        $tmp = $estimatedDeliveryEnd['deliveryDate']['deliveryAt']->format('d/m/Y H:i:s');
                    }
                    $rowData[$getKey('Fecha posible de entrega')] = $tmp;
                    $rowData[$getKey('Fecha entrega real')] = $isClosed() ? $formatMysqlDatetime($getValue('[requisition_closed_at]')) : '';
                    $rowData[$getKey('Codigo técnico')] = $getValue('[product_code]');
                    $rowData[$getKey('Producto')] = $getValue('[product_name]');
                    $rowData[$getKey('Categoría')] = $getValue('[product_category]');
                }

                if ($getValue('[contract_type]')) {
                    $rowData[$getKey('RUT Evaluador')] = $getValue('[approver_rut]');
                    $rowData[$getKey('Nombre Evaluador')] = $getValue('[approver_full_name]');
                }
                //$rowData[$getKey('Dirección unidad demandante')] = $getValue('[requester_office_address]');
                $rowData[$getKey('Tipo unidad')] = $getTypeOffice($getValue('[requester_office_type]'));
                $rowData[$getKey('RUT Proveedor')] = $getValue('[supplier_rut]');
                $rowData[$getKey('Nombre Proveedor')] = $getValue('[supplier_name]');
                $rowData[$getKey('Cantidad')] = $getRequisitionQuantity();
                $rowData[$getKey('Mes creación')] = $getCreationDate();

                $baseMeta = [];
                $compMeta = [];
                if ($this->compProductCode === $getValue('[product_code]')) {
                    if ($parentId = $getMetadata('[parent]')) {
                        if ($parent = $this->requisitionsRepo->find($parentId)) {
                            $baseMeta = $parent->getMetadata('details');
                        }
                    }
                } else {
                    $baseMeta = $getMetadata('[details]');
                }

                foreach ($baseReqCols as $key) {
                    if ('comentarios' === $key) {
                        $tt = $this->translator->trans('pedido_uniforme_'.$key);
                    } else {
                        $tt = $this->translator->trans($key);
                    }

                    $rowData[$getKey($tt)] = $key === 'cantidad' ? $getRequisitionQuantity() : $baseMeta[$key] ?? null;
                }

                if ($this->compProductCode === $getValue('[product_code]')) {
                    $compMeta = $getMetadata('[details]');
                }

                foreach ($compReqCols as $key) {
                    $rowData[$getKey($this->translator->trans('compostura_' . $key))]
                        = $key === 'cantidad' ? $getRequisitionQuantity() : $compMeta[$key] ?? null;
                }

                if ($i === 0) {
                    $file->fputcsv($this->columnNames);
                }

                $this->columnNames = [];
                $file->fputcsv($rowData);
            } catch (\Exception $e) {
                //    dump($e->getMessage());
            }
        }
    }
}
