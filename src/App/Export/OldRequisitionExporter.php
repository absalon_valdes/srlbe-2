<?php

namespace App\Export;

use App\Entity\Category;
USe App\Entity\Contract;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Entity\User;
use App\Helper\RequisitionTypeResolver;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Repository\RequisitionRepository;
use App\Util\DateUtils;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;
use Lexik\Bundle\WorkflowBundle\Entity\ModelState;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class OldRequisitionExporter.
 *
 * @Di\Service("old.requisition.exporter")
 */
class OldRequisitionExporter extends Exporter
{
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;
    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionsRepo;

    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    public $typeResolver;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $em;


    private $possibleStatuses = [
        ProductStatus::CREATED,
        ProductStatus::ORDERED,
        ProductStatus::REJECTED,
        ProductStatus::APPEAL,
        ProductStatus::DENIED,
        ProductStatus::APPROVED,
        ProductStatus::ACCEPTED,
        ProductStatus::CONFIRMED,
        ProductStatus::DECLINED,
        ProductStatus::HOLD,
        ProductStatus::DELIVERED,
        ProductStatus::REVIEWED,
        ProductStatus::RETURNED,
        ProductStatus::EVALUATED,
        ProductStatus::CLOSED,
        ProductStatus::COMPLETED,
        ProductStatus::TIMEOUT
    ];

    protected function fetchData($type, $data)
    {
        $types = !$type ? [
            Requisition::TYPE_MAINTENANCE,
            Requisition::TYPE_PRODUCT,
            Requisition::TYPE_SELF_MANAGEMENT,
        ] : [$type];

        $this->data = $data;

        if (!$data) {
            $this->em->getFilters()->disable('soft_deleteable');
            
            $this->data = $this->requisitionsRepo->createQueryBuilder('r')
                ->where('r.type in (:types)')
                ->andWhere('r.deletedAt is null')
                ->orderBy('r.number', 'desc')
                ->setParameter('types', $types)
                ->getQuery()
                ->getResult()
            ;
        }
        
        $type = !$type ? 'all' : $type;

        $reportFilename = [
            Requisition::TYPE_MAINTENANCE => 'mantenciones',
            Requisition::TYPE_PRODUCT => 'pedidos',
            Requisition::TYPE_SELF_MANAGEMENT => 'autogestion',
            'all' => 'general'
        ];

        $this->fileName = sprintf('reporte-%s-%s.xls', $reportFilename[$type], time());
        $this->tabName = ucfirst($reportFilename[$type]);
        $this->title = sprintf('Reporte de ficha de %s (%s)', $reportFilename[$type], date('d-m-Y H:i:s'));

        $this->setColumnInfo();
        $this->setHeaderInfo();
    }

    protected function setColumnInfo(){
        $contractType = function (Contract $contract) {
            return $this->translator->trans(
                $contract->getCentralized() ?
                    Contract::TYPE_CENTRALIZED :
                    Contract::TYPE_DESCENTRALIZED
            );
        };

        $productCategory = function (Product $product) {
            if ($category = $product->getCategory()) {
                return $category->getName();
            }

            return null;
        };

        $productApprover = function (Product $product) {
            if (($approver = $product->getApprover()) &&
                ($employee = $approver->getEmployee())) {
                return $employee;
            }

            return null;
        };

        $employeeOffice = function (Employee $employee = null) {
            if ($employee && ($office = $employee->getOffice())) {
                return $office;
            }

            return null;
        };

        $requisitionState = function (Requisition $requisition, $state) {
            $states = $this->workflow->getHistory($requisition);
            foreach ($states as $s){
                $n = $s->getProcessName().".".$s->getStepName();
                if($n == $state){
                    return $s;
                }
            }
            return null;
        };

        $initStatus = function (Requisition $requisition) {
            switch ($requisition->getType()) {
                case Requisition::TYPE_MAINTENANCE:
                    return ProductStatus::APPROVED;
                case Requisition::TYPE_PRODUCT:
                    return $requisition->isCentralized() ? ProductStatus::CREATED : ProductStatus::APPROVED;
                case Requisition::TYPE_SELF_MANAGEMENT:
                    return ProductStatus::CREATED;
            }
    
            return null;
        };
        
        $checkNuevoFormato = function (Requisition $requisition) {
            if (!$requisition->hasMetadata('details')) {
                return false;
            }
            
            $detalles = $requisition->getMetadata('details');
            
            return isset($detalles['caracteristicas']);
        };
        
        $totalPorColor = function (Requisition $requisition, $color) use ($checkNuevoFormato) {
            if (!$checkNuevoFormato($requisition)) {
                return null;
            }
            
            $items = $requisition->getMetadata('details')['caracteristicas'];
            
            return array_reduce($items, function ($carry, $item) use ($color) {
                if (isset($item['color']) && $color === $item['color']) {
                    $carry += $item['cantidad'] ?? 0;
                }
                
                return $carry;
            }, 0);
        };
        
        $totalPorColorModelo = function (Requisition $requisition, $color, $modelo) use ($checkNuevoFormato) {
            if (!$checkNuevoFormato($requisition)) {
                return null;
            }
            
            $items = $requisition->getMetadata('details')['caracteristicas'];
            
            return array_reduce($items, function ($carry, $item) use ($color, $modelo) {
                if ((isset($item['color']) && $color === $item['color']) &&
                    (isset($item['modelo']) && $modelo === $item['modelo'])
                ) {
                    $carry += $item['cantidad'] ?? 0;
                }
                
                return $carry;
            }, 0);
        };

        $requisitionStatus = function (Requisition $requisition,$status = null) use ($contractType) {
            $type = $requisition->getType();
            $status = $status ?: $requisition->getStatus();
            $centr = $requisition->isCentralized() ? Contract::TYPE_CENTRALIZED : Contract::TYPE_DESCENTRALIZED;

            return sprintf('%s.%s.%s', $status, str_replace('requisition.', '', $type), str_replace('type.', '', $centr));
        };

        $requisitionsData = [];
        /** @var Requisition $requisition */
        foreach ($this->data as $i => $requisition) {
            $requisitionData = [];
            $initialStatus = $initStatus($requisition);

            //Requisition hasn't been created yet (product.draft status)
            if (!$requisitionState($requisition,$initialStatus)) {
                continue;
            }
            
            $notReceived = $endedAt = $supplierRating = null;
            
            /** @var ModelState[] $history */
            $history = $this->workflow->getHistory($requisition);
            
            $evaluation = $requisition->getMetadata('evaluation');
            
            /** @var \DateTime $startDate */
            $startDate = $history[1 === count($history) ? 0 : 1]->getCreatedAt();
            
            //Support for legacy evaluation metadata format
            if ($eval = $requisition->getMetadata('evaluation')) {
                if (isset($eval['evaluation'])) {
                    $notReceived = $eval['evaluation'];
                    $endedAt = $eval['evaluated_at'];
                } else {
                    if (is_array($evaluation) && isset(end($evaluation)['evaluated_at'])) {
                        $notReceived = $this->translator->trans(end($evaluation)['evaluation'] ?? '');
                        $endedAt = end($evaluation)['evaluated_at'];    
                    }
                }
            }

            if (!$endedAt instanceof \DateTime) {
                if ($requisition->getMetadata('rejection') || 'product.closed' === $requisition->getStatus()) {
                    $endedAt = $requisition->getClosedAt();
                }
            }

            /** @var Contract $contract */
            $contract = $requisition->getContract();

            $states = $this->workflow->getHistory($requisition);

            /** @var Product $product */
            $product = $requisition->getProduct();

            /** @var User $user*/
            $user = $requisition->getRequester();

            /** @var Employee $requester */
            $requester = $user ? $user->getEmployee() : null;

            /** @var Office $requesterOffice */
            $requesterOffice = $employeeOffice($requester);

            /** @var Category $category */
            $category = $productCategory($product);

            /** @var Employee $approver */
            $approver = $productApprover($product);

            /** @var Supplier $supplier */
            $supplier = $requisition->getContract()->getSupplier();

            /* @var \DateTime $estimatedSLA */
            $estimatedSLA = $this->slaCalculator->getEstimate(
                $requisitionState($requisition,$initialStatus)->getCreatedAt(),
                $contract->getSlaProduct(),
                $contract->getSlaAccept(),
                $contract->getSlaApprover(),
                $contract->getSlaApproverEval(),
                $contract->getSlaRequesterEval());


            if($endedAt){
                $supplierRating = $requisition->getMetadata('supplier_rating');
            }

            if($requisition->getStatus() == ProductStatus::CLOSED){
                $endedAt = $requisition->getClosedAt();
            }

            $daysSlaEstimated = $this->getDaysInFormat($estimatedSLA, $startDate);
            //$inSla = $requisition->isClosed() ? $estimatedSLA > $requisition->getClosedAt() : $estimatedSLA > new \DateTime();
            $inSla = $endedAt ? $estimatedSLA > $endedAt : $estimatedSLA > new \DateTime();
            $slaTimeout = $requisition->isClosed() ? DateUtils::dateIntervalFormat($startDate->diff($requisition->getClosedAt())) : DateUtils::dateIntervalFormat($estimatedSLA->diff((new \DateTime)));

            $phone = function (Employee $e = null) {
                if($e){
                    $parts = explode('/', str_replace(['\\', '//', '-', ')'], '/', $e->getPhone()));
                    return trim($parts[count($parts) === 1 ? 0 : count($parts) - 1]);
                }
                return null;
            };

            $requisitionData[] =  $requisition->getNumber();
            $requisitionData[] = $this->translator->trans($requisition->getType());
            $requisitionData[] = $requester && $requester ? $requester->getRut() : null;
            $requisitionData[] = $requester ? $requester->getFullName() : null;
            $requisitionData[] = $phone($requester);
            $requisitionData[] = $user ? $user->getEmail() : '';
            $requisitionData[] = $requesterOffice ? $requesterOffice->getAddress(): null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCity()->getName() : null;
            $requisitionData[] = $requesterOffice ? $this->regionToRoman($requesterOffice) : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCity()->getParent()->getName() : null;
            $requisitionData[] = $requester ? $requester->getPosition()->getTitle() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCode() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getName() : null;
            $requisitionData[] = $requesterOffice ? $requesterOffice->getCostCenter() : null;
            $requisitionData[] = $notReceived;
            $requisitionData[] = $requisition->isClosed() ? 'CERRADO' : 'ABIERTO';
            $requisitionData[] = $this->translator->trans($requisitionStatus($requisition));
            $requisitionData[] = $requisition->getCreatedAt()->format('d/m/Y H:i:s');
            $requisitionData[] = $estimatedSLA->format('d/m/Y  H:i:s');
            $requisitionData[] = $endedAt ? $endedAt->format('d/m/Y H:i:s') : null;
            $requisitionData[] = $inSla ? 'DENTRO SLA' : 'FUERA SLA';
            $requisitionData[] = $inSla && !$requisition->isClosed() ? $this->getDaysInFormat($estimatedSLA) : 0;
            $requisitionData[] = $daysSlaEstimated;
            $requisitionData[] = !$requisition->isClosed() ? DateUtils::dateIntervalFormat($startDate->diff((new \DateTime))) : DateUtils::dateIntervalFormat($startDate->diff($endedAt));
            $requisitionData[] = !$inSla ? $slaTimeout : 0;

            $acceptedInfo = $productInfo = $approvedInfo = $declined = null;
            foreach ($this->possibleStatuses as $status){

                $diff = null;
                $slaData = $this->filterStates($states, $status,$contract->getSlas(),$endedAt);

                $typeRequisition = $requisitionStatus($requisition,$status);
                if($slaData["createdAt"] && $slaData["finishedAt"] ){
                    $diff = $slaData["createdAt"]->diff($slaData["finishedAt"]);
                }elseif($slaData["createdAt"]){
                    $diff = $slaData["createdAt"]->diff((new \DateTime));
                }
                if($status == ProductStatus::ACCEPTED){
                    $acceptedInfo["createdAt"] = $slaData["createdAt"];
                    $acceptedInfo["sla"] = $slaData["sla"];
                }elseif($status == ProductStatus::DELIVERED){
                    $productInfo["createdAt"] = $slaData["createdAt"];
                    $productInfo["sla"] = $slaData["sla"];
                }elseif($status == ProductStatus::APPROVED){
                    $approvedInfo["createdAt"] = $slaData["createdAt"];
                    $approvedInfo["finishedAt"] = $slaData["finishedAt"];
                    $approvedInfo["sla"] = $slaData["sla"];
                }

                $excessSlaTime = null;
                if(!is_null($slaData["slaExpectedValue"]) && !is_null($diff)){
                    if( strpos($slaData["slaExpectedType"],'day') &&
                        ($diff->d > $slaData["slaExpectedValue"] || $diff->m > 0  || $diff->y > 0) ){
                        $excessSlaTime = DateUtils::dateIntervalFormat(
                            $this->slaCalculator->getEstimate($slaData["createdAt"],
                                $slaData["sla"])->diff($slaData["finishedAt"] ?? (new \DateTime)));
                    }elseif (strpos($slaData["slaExpectedType"],'hour') &&
                        ($diff->h > $slaData["slaExpectedValue"] || $diff->d > 0  || $diff->m > 0  || $diff->y > 0) ){
                        $excessSlaTime = DateUtils::dateIntervalFormat(
                            $this->slaCalculator->getEstimate($slaData["createdAt"],
                                $slaData["sla"])->diff($slaData["finishedAt"] ?? (new \DateTime)));
                    }
                }

                $fAt = $slaData["finishedAt"];
                if($fAt){
                    $fAt = $fAt->format('d/m/Y H:i:s');
                }elseif($status == ProductStatus::COMPLETED && $slaData["createdAt"]){
                    $fAt = 'N/A';
                }
                $requisitionData[] = $this->translator->trans("sla.".$typeRequisition);
                $requisitionData[] = $slaData["slaExpectedValue"];
                $requisitionData[] = $slaData["slaExpectedType"] ? $this->translator->trans($slaData["slaExpectedType"]) : null;
                $requisitionData[] = $diff && strpos($status,'completed') == false ? DateUtils::dateIntervalFormat($diff) : null;
                $requisitionData[] = $slaData["createdAt"] ? $slaData["createdAt"]->format('d/m/Y H:i:s') : null;
                $requisitionData[] = $fAt;
                if($status == ProductStatus::EVALUATED){
                    $requisitionData[] = $endedAt ? $endedAt->format('d/m/Y H:i:s') : null;
                }

                if($status == ProductStatus::RETURNED){
                    $requisitionData[] = $slaData["createdAt"] ? $slaData["quantity"] : null;
                }
                if($status == ProductStatus::DECLINED && $slaData["createdAt"]){
                    $declined = true;
                }
                $requisitionData[] = $excessSlaTime;
            }

            /***** SLA ejecución proveedor ***/
            $estimadedDate1 = $this->slaCalculator->getEstimate(
                $requisitionState($requisition,$initialStatus)->getCreatedAt(),
                $contract->getSlaProduct(),
                $contract->getSlaAccept(),
                $contract->getSlaApprover()); //tiempo estimado de acuerdo a SLA de contrato
            $estimadedDate2 = $this->slaCalculator->getEstimate(
                $approvedInfo["createdAt"],
                $contract->getSlaProduct(),
                $contract->getSlaApprover());//tiempo estimado desde que el evaluador aprobó

            $requisitionData[] = 'SLA Ejecución del proveedor';
            $requisitionData[] = $declined ? 'RECHAZA' : 'ACEPTA';
            $requisitionData[] = $this->addValues($productInfo,$approvedInfo);
            $requisitionData[] = $productInfo["sla"] ? $this->translator->trans($productInfo["sla"]->getType()) : null;
            $requisitionData[] = $acceptedInfo["createdAt"] ? DateUtils::dateIntervalFormat($approvedInfo["finishedAt"]->diff($endedAt ?? (new \DateTime))) : null;//Sla esperado
            $requisitionData[] = $approvedInfo["finishedAt"] ? $approvedInfo["finishedAt"]->format('d/m/Y H:i:s'): null; //fecha inicio de aceptar la OT
            $requisitionData[] = $endedAt ? $endedAt->format('d/m/Y H:i:s') : null; // final de certificado por el demandante
            $requisitionData[] = $estimadedDate1->format('d/m/Y H:i:s');
            $requisitionData[] = $endedAt > $estimadedDate1 || !$endedAt && (new \DateTime) > $estimadedDate1 ? DateUtils::dateIntervalFormat($estimadedDate1->diff($endedAt ?? (new \DateTime))) : null;
            $requisitionData[] = $estimadedDate2->format('d/m/Y H:i:s');
            $requisitionData[] = $endedAt > $estimadedDate2 || !$endedAt && (new \DateTime) > $estimadedDate2 ? DateUtils::dateIntervalFormat($estimadedDate2->diff($endedAt ?? (new \DateTime))) : null;
            /***** SLA ejecución proveedor ***/

            /***Evaluación del demandante obtenida***/
            $requisitionData[] = $supplierRating ? $supplierRating['punctuality'] : null;
            $requisitionData[] = $supplierRating ? $supplierRating['quality'] : null;
            $requisitionData[] = $supplierRating ? $supplierRating['service_level'] : null;
            /***Evaluación del demandante obtenida***/

            $requisitionData[] = $contract->getContractNumber();
            $requisitionData[] = $contractType($contract);
            $requisitionData[] = $product->getCode();
            $requisitionData[] = $product->getName();
            $requisitionData[] = $category;
            $requisitionData[] = $approver ? $approver->getRut() : null;
            $requisitionData[] = $approver ? $approver->getFullName() : null;
            $requisitionData[] = $requester ? $requesterOffice->getAddress() : null;
            $requisitionData[] = $requester ? $this->translator->trans($requesterOffice->getType()) : null;
            $requisitionData[] = $supplier->getRut();
            $requisitionData[] = $supplier->getName();
            $requisitionData[] = $requisition->getQuantity();
            $requisitionData[] = $this->translator->trans($requisition->getCreatedAt()->format('F'))." ".$requisition->getCreatedAt()->format('Y');
            $requisitionData[] = $totalPorColor($requisition, 'rojo');
            $requisitionData[] = $totalPorColor($requisition, 'gris');
            $requisitionData[] = $totalPorColorModelo($requisition, 'rojo', 'meson');
            $requisitionData[] = $totalPorColorModelo($requisition, 'gris', 'meson');
            $requisitionData[] = $totalPorColorModelo($requisition, 'rojo', 'cajero');
            $requisitionData[] = $totalPorColorModelo($requisition, 'gris', 'cajero');
            
            $requisitionsData[] = $requisitionData;
        }
        
        $this->columns = $requisitionsData;
    }

    protected function setHeaderInfo()
    {
        $headerInfo = [];
        $headerInfo[] = 'N° solicitud';
        $headerInfo[] = 'Tipo';
        $headerInfo[] = 'RUT Solicitante';
        $headerInfo[] = 'Solicitante';
        $headerInfo[] = 'Anexo';
        $headerInfo[] = 'Correo electrónico';
        $headerInfo[] = 'Dirección';
        $headerInfo[] = 'Comuna';
        $headerInfo[] = 'N° Región';
        $headerInfo[] = 'Región';
        $headerInfo[] = 'Cargo Solicitante';
        $headerInfo[] = 'Codigo Unidad';
        $headerInfo[] = 'Nombre Sucursal';
        $headerInfo[] = 'Centro de costo';
        $headerInfo[] = 'No Recibido';
        $headerInfo[] = 'Estado';
        $headerInfo[] = 'Estado en sistema';
        $headerInfo[] = 'Fecha de creación';
        $headerInfo[] = 'Fecha posible de entrega';
        $headerInfo[] = 'Fecha entrega real';
        $headerInfo[] = 'En SLA';
        $headerInfo[] = 'Días SLA restante';
        $headerInfo[] = 'SLA total esperado';
        $headerInfo[] = 'SLA total real';
        $headerInfo[] = 'Tpo Excedido respecto de SLA';

        foreach ($this->possibleStatuses as $status){
            $headerInfo[] = "Tipo SLA";
            $headerInfo[] = "SLA esperado";
            $headerInfo[] = "SLA unidad";
            $headerInfo[] = "SLA real";
            $headerInfo[] = "SLA fecha inicio";
            $headerInfo[] = "SLA fecha fin";
            
            if($status === ProductStatus::EVALUATED){
                $headerInfo[] = "Fecha certifica";
            }
            if($status === ProductStatus::RETURNED){
                $headerInfo[] = "N° rechazos demandante";
            }
            $headerInfo[] = 'Tpo Excedido respecto de SLA';
        }


        $headerInfo[] = 'Tipo SLA';
        $headerInfo[] = 'Proveedor acepta o rechaza';
        $headerInfo[] = 'SLA esperado';
        $headerInfo[] = 'SLA unidad';
        $headerInfo[] = 'SLA real';
        $headerInfo[] = 'SLA fecha inicio';
        $headerInfo[] = 'SLA fecha fin';
        $headerInfo[] = 'Primera fecha estimada';
        $headerInfo[] = 'Tpo Excedido respecto primera fecha';
        $headerInfo[] = 'Segunda fecha estimada';
        $headerInfo[] = 'Tpo Excedido respecto segunda fecha';

        $headerInfo[] = 'Puntualidad';
        $headerInfo[] = 'Calidad';
        $headerInfo[] = 'Nivel de servicio';

        $headerInfo[] = 'Nro Contrato';
        $headerInfo[] = 'Tipo contrato';
        $headerInfo[] = 'Codigo técnico';
        $headerInfo[] = 'Producto';
        $headerInfo[] = 'Categoría';
        $headerInfo[] = 'RUT Evaluador';
        $headerInfo[] = 'Nombre Evaluador';
        $headerInfo[] = 'Dirección unidad demandante';
        $headerInfo[] = 'Tipo unidad';
        $headerInfo[] = 'RUT Proveedor';
        $headerInfo[] = 'Nombre Proveedor';
        $headerInfo[] = 'Cantidad';
        $headerInfo[] = 'Mes creación';
        $headerInfo[] = 'Rojo';
        $headerInfo[] = 'Gris';
        $headerInfo[] = 'Mesón Rojo';
        $headerInfo[] = 'Mesón Gris';
        $headerInfo[] = 'Cajero Rojo';
        $headerInfo[] = 'Cajero Gris';

        $this->headers = $headerInfo;
    }

    protected function fillRows()
    {

        $this->fillCellValues($this->columns);

    }

    private function addValues($val1,$val2){
        $val1 = $val1["sla"] ? $val1["sla"]->getValue() : 0;
        $val2 = $val2["sla"] ? $val2["sla"]->getValue() : 0;
        return $val1 + $val2;
    }

    private function regionToRoman(Office $office)
    {
        if (!$city = $office->getCity()) {
            return null;
        }

        /** @var Category $region */
        if (!$region = $city->getParent()) {
            return null;
        }

        $regionToRoman = [
            'arica' => 'XV',
            'tarapaca' => 'I',
            'antofagasta' => 'II',
            'atacama' => 'III',
            'coquimbo' => 'IV',
            'valparaiso' => 'V',
            'libertador' => 'VI',
            'maule' => 'VII',
            'biobio' => 'VIII',
            'araucania' => 'IX',
            'rios' => 'XIV',
            'lagos' => 'X',
            'aisen' => 'XI',
            'magallanes' => 'XII',
            'metropolitana' => 'XIII',
        ];

        foreach ($regionToRoman as $slug => $no) {
            if ((bool) strstr($region->getSlug(), $slug)) {
                return $no;
            }
        }
        return null;
    }

    /**
     * @throws \PHPExcel_Exception
     */
    protected function fillColumnHeaders()
    {
        foreach ($this->headers as $key => $value){
            $this->report->setActiveSheetIndex(0)
                ->setCellValue($this->getNameFromNumber($key)."1", $value);
        }
    }

    private function fillCellValues($requisitionsData){
        $cRow = 2;
        foreach ($requisitionsData as $row){
            foreach ($row as $key => $value){
                $this->report->getActiveSheet()
                    ->setCellValue($this->getNameFromNumber($key).$cRow, $value);
            }
            ++$cRow;
        }
    }

    private function filterStates($states, $status,$contractSlas){
        $s = $result = null;
        $result = [
            "status" => $status,
            "createdAt" =>  null,
            "finishedAt" =>  null,
            "slaExpectedValue" =>  array_key_exists($status,$contractSlas) ? $contractSlas[$status]->getValue() : null,
            "slaExpectedType" =>  array_key_exists($status,$contractSlas) ? $contractSlas[$status]->getType() : null,
            "sla" => array_key_exists($status,$contractSlas) ? $contractSlas[$status] : null
        ];

        foreach ($states as $state){
            $stateN = $state->getProcessName().".".$state->getStepName();
            if($stateN != $status){
                continue;
            }
            $s[] = $state;
        }

        if(count($s) > 0 && $s !== null) {
            $result["createdAt"] =  $s[0]->getCreatedAt();
            $result["finishedAt"] =  $s[count($s) - 1]->getFinishedAt();
            $result["quantity"] =  count($s);
        }

        return $result;
    }

    private function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return $this->getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
    }
}
