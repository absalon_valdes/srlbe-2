<?php

namespace App\Export;

use App\Entity\Requisition;
use App\Entity\User;
use App\Helper\PositionTitleNormalizer;
use App\Model\VcardContact;
use App\Model\VcardRequisition;
use App\Repository\RequisitionRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Liuggio\ExcelBundle\Factory;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Router;

/**
 * Class VcardRequisitionExporter
 * @DI\Service("vcard_requisition_exporter")
 */
class VcardRequisitionExporter
{

    const CLOSED_STATUSES   = ['solicitud_completa','cerrada_sistema'];
    const OPEN_STATUSES     = ['solicitud_evaluada','ejecucion','en_despacho','aceptado','proveedor_calificado'];
    const HIDDEN_STATUSES   = ['rechazado','denegado','cerrado','borrador']; /** hidden for suppliers **/

    /**
     * @var Factory
     * @Di\Inject("phpexcel")
     */
    public $excelFactory;
    
    /**
     * @var RequisitionRepository
     * @DI\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var RequisitionRepository
     * @Di\Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @var string
     * @DI\Inject("%kernel.cache_dir%")
     */
    public $cacheDir;

    /**
     * @var Router
     * @DI\Inject("router")
     */
    public $router;
    
    /**
     * @var Serializer
     * @DI\Inject("serializer")
     */
    public $serializer;

    /**
     * @var PositionTitleNormalizer
     * @DI\Inject("position_title_normalizer")
     */
    public $positionNormalizer;
    
    public function exportSingle(Requisition $requisition)
    {
        return $this->createLink($this->createExcel([$requisition]));
    }

    public function exportOpen(User $user)
    {
        return $this->exportMany($user, false);
    }

    public function exportClosed(User $user)
    {
        return $this->exportMany($user);
    }

    private function exportMany(User $user, $closed = true)
    {

        $data = $this->requisitionRepo->createQueryBuilder('r')
            ->leftJoin('r.contract', 'c')
            ->leftJoin('c.supplier', 's')
            ->where('r.type = :type')
            ->andWhere(':user member of s.representatives')
            ->andWhere('r.status  in (:statuses)')
            ->andWhere('r.status not in (:notstatuses)')
            ->setParameter('type', Requisition::TYPE_VCARD)
            ->setParameter('statuses', $closed ? self::CLOSED_STATUSES : self::OPEN_STATUSES)
            ->setParameter('notstatuses', self::HIDDEN_STATUSES)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult()
        ;
        return $this->createLink($this->createExcel($data));
    }

    public function createLink($excel)
    {
        return $this->router->generate('report_vcard_download', [
            'fileId' => $this->createFile($excel)['id']
        ], Router::ABSOLUTE_URL);
    }

    public function createFile($excel)
    {
        $tempDir = sprintf('%s/vcard_report', $this->cacheDir);

        if (!@mkdir($tempDir, 0777, true) && !is_dir($tempDir)) {
            throw new \RuntimeException(sprintf(
                'Cannot create folder %s', $tempDir
            ));
        }

        $fileId = Uuid::uuid5(Uuid::NAMESPACE_DNS, time());
        $filePath = sprintf('%s/%s.xlsx', $tempDir, $fileId->toString());

        $writer = $this->excelFactory->createWriter($excel, 'Excel2007');
        $writer->save($filePath);

        return [
            'id' => $fileId,
            'path' => $filePath,
        ];
    }
    
    public function createExcel(array $data = array())
    {
        $headers = $this->getHeaders();
        $records = array_filter(array_map([$this, 'itemToArray'], $data));

        return $this->excelFactory->createPHPExcelObject()
            ->getActiveSheet()
            ->fromArray(array_merge($headers, $records))
            ->getParent()
        ;
    }
    
    private function getHeaders()
    {
        $headers = [
            'N° solicitud',
            'Código técnico',
            'Producto',
            'Fecha solicitud',
            'Cantidad',
            'Demandante',
            'Sucursal',
            'Nombre',
            'Cargo',
            'Dependencia',
            'Mostrar dependencia',
            'Dirección',
            'Mostrar dirección',
            'Email',
            'Código de área',
            'Teléfono',
            'Fax',
            'Teléfono móvil',
        ];
        
        for ($i = 1; $i <= 3; $i++) {
            $headers = array_merge($headers, [
                "Contacto n° $i nombre",
                "Contacto n° $i cargo",
                "Contacto n° $i código de área",
                "Contacto n° $i teléfono",
                "Contacto n° $i email",    
            ]);
        }
        
        return [$headers];
    }
    
    private function itemToArray(Requisition $requisition)
    {
        $requisition = $this->requisitionRepo->find($requisition->getId());

        $product = $requisition->getProduct();
        $requester = $requisition->getEmployee();
        $metadata = $requisition->getMetadata();
        
        if (!$requester || !$product || !$metadata) {
            return null;
        }
        
        /** @var VcardRequisition $vcard */
        $vcard = $this->serializer->deserialize($metadata['vcards'], VcardRequisition::class, 'json');
        $office = $vcard->getOffice();
        $addressOffice = $vcard->getAddressOffice() ?: $office;

        if ($addressOffice) {
            if ($realCity = $this->categoryRepo->findOneByName($addressOffice->getCity()->getName())) {
                $addressOffice->setCity($realCity);
            }
            $address = $addressOffice->getAddress();
            $addressDetail = $vcard->getAddressDetail();
            $addressCity = $addressOffice->getCity()->getName();
            $addressFull = $address.($addressDetail ? ', '.$addressDetail : '').', '.$addressCity;
        } else {
            $addressFull = '';
        }

        $data = [
            $requisition->getNumber(),
            $product->getCode(),
            $product->getName(),
            $requisition->getCreatedAt()->format('d/m/Y H:i:s'),
            $requisition->getQuantity(),
            $requester->getFullName(),
            $office->getName(),
            $vcard->getFullName(),
            $this->positionNormalizer->normalize($vcard->getPosition()),
            $vcard->isShowOffice() ? $vcard->getOffice()->getName() : '',
            $vcard->isShowOffice() ? 'Si' : 'No',
            $vcard->isShowAddress() ? $addressFull : '',
            $vcard->isShowAddress() ? 'Si' : 'No',
            $vcard->getEmail(),
            $vcard->getAreaCode(),
            $vcard->getPhone(),
            $vcard->getFax(),
            $vcard->getMobile(),
        ];

        foreach ($vcard->getContacts() as $contact) {
            $data = array_merge($data, $this->contactToArray($contact));
        }
        
        return $data;
    }
    
    private function contactToArray(VcardContact $contact)
    {
        return [
            $contact->getFullName(),
            $contact->getPosition()->getTitle(),
            $contact->getAreaCode(),
            $contact->getPhone(),
            $contact->getEmail(),
        ];
    }
}
