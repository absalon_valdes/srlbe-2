<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class VcardDataDownloadType extends AbstractType
{
    const THIS_REQUISITION = 'vcard.download.this';
    const ALL_OPEN = 'vcard.download.open';
    const ALL_CLOSED = 'vcard.download.closed';
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('choice', ChoiceType::class, [
                'choices' => [
                    self::THIS_REQUISITION,
                    self::ALL_OPEN,
                    self::ALL_CLOSED,
                ],
                'label' => false,
                'expanded' => true,
                'choice_label' => function ($v) {
                    return $v;
                },  
            ])
        ;
    }
}