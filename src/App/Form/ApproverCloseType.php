<?php

namespace App\Form;

use App\Entity\Requisition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ApproverCloseType
 * @package App\Form
 *
 * @Di\FormType
 */
class ApproverCloseType extends AbstractType
{

    /**
     * @Di\Inject("form_upload_helper")
     */
    public $uploadHelper;

    const REASON_TECHNICAL = 'approver_close.reason.technical';
    const REASON_ALREADY_DELIVERED = 'approver_close.reason.alread_delivered';
    const REASON_OTHER = 'approver_close.reason.other';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();

            /** @var Requisition $requisition */
            $requisition = $event->getData();

            /** ApproverClose metadata */
            $metadata = $requisition->getMetadata('approver_close') ?? [];

            $attachments = array_map(function ($item) {
                $item['document'] = $item['file'];
                return $item;
            }, $metadata['files'] ?? []);

            $form
                ->add('datetime', DateTimeType::class,[
                    'label' => 'Fecha y Hora Cierre',
                    'property_path' => 'metadata[approver_close][datetime]',
                    'date_widget' => 'single_text',
                    'time_widget' => 'choice',
                    'date_format' => 'dd/MM/yyyy',
                ])
                ->add('reason', ChoiceType::class, [
                    'label' => 'Motivo cierre',
                    'choices' => [
                        self::REASON_TECHNICAL,
                        self::REASON_ALREADY_DELIVERED,
                        self::REASON_OTHER,
                    ],
                    'choices_as_values' => true,
                    'choice_label' => function ($v) {
                        return $v;
                    },
                    'multiple' => false,
                    'expanded' => true,
                    'property_path' => 'metadata[approver_close][reason]'
                ])
                ->add('comments', TextareaType::class, [
                    'label' => 'Justificación',
                    'required' => false,
                    'attr' => ['disabled' => true],
                    'property_path' => 'metadata[approver_close][comments]'
                ])
                ->add('send', SubmitType::class, [
                    'label' => 'Enviar',
                    'attr' => ['class' => 'pull-right btn-warning']
                ])
                ->add('cancel', ButtonType::class, [
                    'label' => 'Cancelar',
                    'attr' => [
                        'class' => 'pull-left btn-warning',
                        'data-dismiss' => 'modal'
                    ]
                ])
                ->add('files', CollectionType::class, [
                    'entry_type' => ValidationDocType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'mapped' => false,
                    'label' => false,
                    'data' => $attachments,
                    'prototype_name' => '__validation_files__'
                ])
            ;

        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            /** @var Requisition $requisition */
            $requisition = $form->getData();

            $files = $event->getData()['files'] ?? [];
            $documents = array();
            foreach ($files as $doc) {
                if (is_null($doc['description'] ?? null) && empty($doc['document'])) {
                    continue;
                }

                $documents[] = [
                    'description' => $doc['description'],
                    'file' => $this->uploadHelper->upload([$doc['document']])
                ];
            }

            $approverClose = $requisition->getMetadata('approver_close');
            $approverClose['files'] = $documents;
            $requisition->addMetadata('approver_close', $approverClose);

        });
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Requisition::class);
    }
}