<?php

namespace App\Form;

use App\Entity\Position;
use App\Model\VcardContact;
use App\Repository\PositionRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;

/**
 * Class ContactType
 * @DI\FormType()
 */
class ContactType extends AbstractType
{
    /**
     * @DI\Inject("%area_codes%")
     */
    public $areaCodes;
    
    /**
     * @var PositionTitleNormalizer
     * @DI\Inject("position_title_normalizer")
     */
    public $positionNormalizer;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, [
                'label' => 'Nombre completo',
                'attr' => [
                    'placeholder' => 'Ingrese su nombre completo',
                    'style' => 'text-transform:capitalize'
                ]
            ])
            ->add('position', EntityType::class, [
                'label' => 'Cargo',
                'attr' => ['class' => 'betterSelect'],
                'class' => Position::class,
                'query_builder' => function (PositionRepository $repo) {
                    return $repo
                        ->createQueryBuilder('o')
                        ->orderBy('o.title', 'ASC')
                    ;
                },
                'choice_attr' => function (Position $pos) {
                    return [
                        'data-position' => $this->positionNormalizer->normalize($pos)
                    ];
                },
                'choice_label' => function (Position $pos) {
                    return $this->positionNormalizer->normalize($pos);
                }
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                'constraints' => [new Email],
                'attr' => ['placeholder' => 'Ingrese su email']
            ])
            ->add('areaCode', ChoiceType::class, [
                'label' => 'Código área',
                'choices' => $this->areaCodes,
                'choice_label' => function ($v, $k) { 
                    return $v.' - '.$k; 
                },
                'attr' => ['data-area-code-selector' => null]
            ])
            ->add('phone', IntegerType::class, [
                'label' => 'N° de teléfono',
                'attr' => [
                    'data-cphone-number' => null,
                    'placeholder' => 'Ingrese su número de teléfono'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', VcardContact::class);
    }
}