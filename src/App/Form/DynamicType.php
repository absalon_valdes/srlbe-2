<?php

namespace App\Form;

use App\Controller\Helper\FormUploadHelper;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DynamicType
 * @package App\Form
 * @DI\FormType()
 */
class DynamicType extends AbstractType
{
    /**
     * @var array
     */
    private $options;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var FormUploadHelper
     */
    private $uploader;

    /**
     * @var string
     */
    private $uploadsDir;

    /**
     * DynamicType constructor.
     *
     * @param EntityManager $em
     * @param FormUploadHelper $uploader
     * @param $uploadsDir
     * @param Container $container
     *
     * @DI\InjectParams({
     *     "em"=@DI\Inject("doctrine.orm.entity_manager"),
     *     "uploadsDir"=@DI\Inject("%uploads_dir%"),
     *     "uploader"=@DI\Inject("form_upload_helper"),
     *     "container"=@DI\Inject("service_container")
     * })
     */
    public function __construct(EntityManager $em, FormUploadHelper $uploader, $uploadsDir, Container $container)
    {
        $this->em = $em;
        $this->uploader = $uploader;
        $this->uploadsDir = $uploadsDir;
        $this->container = $container;
    }

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        foreach ($this->options['config'] as $key => $config) {
            $typeOptions = $config['options'] ?? [];
            $typeClass = $config['type'];

            if (!$typeClass = $this->getFormTypeFqcn($typeClass)) {
                continue;
            }

            if (EntityType::class === $typeClass) {
                $typeOptions = $this->getEntityTypeConfig($key, $typeOptions);
            }

            if (CollectionType::class === $typeClass) {
                $typeOptions = $this->getCollectionTypeConfig($key, $typeOptions);
            }

            if (FileType::class === $typeClass) {
                $typeOptions = $this->getFileTypeConfig($key, $typeOptions);
            }

            $typeOptions = $this->evaluateOptions($key, $typeOptions);
            $typeOptions = $this->getConstraintTypeFqn($key, $typeOptions);

            $builder->add($key, $typeClass, $typeOptions);

            if (EntityType::class == $typeClass) {
                $builder->get($key)->addModelTransformer(new CallbackTransformer(
                    function ($toManaged) use ($typeOptions) {
                        return $toManaged ? $this->em->getRepository($typeOptions['class'])
                            ->find($toManaged->getId()) : null;
                    },
                    function ($toObject) use ($typeOptions) {
                        return $toObject;
                    }
                ));
            }
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData']);
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'onPreSubmit']);
    }

    public function onPreSetData(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();

        foreach ($this->options['config'] as $key => $config) {
            $typeClass = $config['type'];

            if (!$typeClass = $this->getFormTypeFqcn($typeClass)) {
                continue;
            }

            if (FileType::class == $typeClass) {
                $this->{$key} = $data[$key] ?? [];

                if ($this->{$key}) {
                    $form->add($key.'_delete_file', ChoiceType::class, [
                        'label' => false,
                        'required' => false,
                        'mapped' => false,
                        'multiple' => true,
                        'expanded' => true,
                        'choices' => $this->{$key},
                        'choice_label' => function () {
                            return 'Eliminar';
                        }
                    ]);
                }
            }
        }
    }

    public function onPreSubmit(FormEvent $event)
    {
        $data = $event->getData();

        foreach ($this->options['config'] as $key => $config) {
            $typeClass = $config['type'];

            if (!$typeClass = $this->getFormTypeFqcn($typeClass)) {
                continue;
            }

            if (FileType::class == $typeClass) {
                foreach ($data[$key.'_delete_file'] ?? [] as $deleteFile) {
                    unset($this->{$key}[array_search($deleteFile, $this->{$key})]);
                }

                $data[$key] = $this->uploader->upload($data[$key]) ?? [];
            }
        }

        $event->setData($data);
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('config', array());
    }

    /**
     * @param $shortType
     * @return mixed|string
     */
    private function getFormTypeFqcn($shortType)
    {
        if (class_exists($shortType)) {
            return $shortType;
        }

        $builtinTypes = [
            'birthday', 'button', 'checkbox', 'choice', 'collection', 'country',
            'currency', 'datetime', 'date', 'email', 'entity', 'file', 'form',
            'hidden', 'integer', 'language', 'locale', 'money', 'number',
            'password', 'percent', 'radio', 'range', 'repeated', 'reset',
            'search', 'submit', 'textarea', 'text', 'time', 'timezone', 'url',
        ];

        if (!in_array($shortType, $builtinTypes, true)) {
            return null;
        }

        $irregularTypeFqcn = array(
            'entity' => EntityType::class,
            'datetime' => DateTimeType::class,
        );

        if (array_key_exists($shortType, $irregularTypeFqcn)) {
            return $irregularTypeFqcn[$shortType];
        }

        return sprintf('Symfony\\Component\\Form\\Extension\\Core\\Type\\%sType', ucfirst($shortType));
    }

    /**
     * @param $key
     * @param $typeOptions
     * @return mixed
     */
    private function getCollectionTypeConfig($key, $typeOptions)
    {
        $typeOptions['entry_type'] = isset($typeOptions['entry_type']) ?
            $this->getFormTypeFqcn($typeOptions['entry_type']) : self::class;

        if (!isset($typeOptions['entry_builder'])) {
            return $typeOptions;
        }

        $entryOptions = $typeOptions['entry_options'] ?? [];
        $entryOptions['config'] = $typeOptions['entry_builder'];
        $typeOptions['entry_options'] = $entryOptions;
        $typeOptions['prototype_name'] = sprintf('__%s__', $key);

        unset($typeOptions['entry_builder']);

        return $typeOptions;
    }

    /**
     * @param $key
     * @param array $typeOptions
     * @return array
     */
    private function getEntityTypeConfig($key, array $typeOptions)
    {
        if (isset($typeOptions['where'])) {
            $typeOptions['query_builder'] = function ($repo) use ($typeOptions) {
                return $repo->createQueryBuilder('o')->where($typeOptions['where']);
            };

            unset($typeOptions['where']);
        }

        if (isset($typeOptions['service_qb'])) {
            $typeOptions['query_builder'] = function ($repo) use ($typeOptions) {
                $id = $typeOptions['service_qb']['service'];
                $method = $typeOptions['service_qb']['method'];
                $service = $this->container->get($id);

                return call_user_func_array([$service, $method], [$repo]);
            };

            unset($typeOptions['service_qb']);
        }

        return $typeOptions;
    }

    /**
     * @param $key
     * @param array $typeOptions
     * @return array
     */
    private function getFileTypeConfig($key, array $typeOptions)
    {
        $typeOptions['data_class'] = null;
        return $typeOptions;
    }

    /**
     * @param $key
     * @param array $typeOptions
     * @return array
     */
    private function evaluateOptions($key, array $typeOptions)
    {
        foreach ($typeOptions as $optionKey => $typeOption) {
            if (is_array($typeOption)) {
                $this->evaluateOptions($key, $typeOption);
            }

            if (is_string($typeOption) && substr($typeOption, 0, 5) === 'eval:') {
                $typeOptions[$optionKey] = eval('return '.substr($typeOption, 5).';');
            }
        }

        return $typeOptions;
    }

    private function getConstraintTypeFqn($key, array $typeOptions)
    {
        $contraints = [];
        foreach ($typeOptions['constraints'] ?? [] as $constraint) {
            $classFqn = sprintf(
                'Symfony\\Component\\Validator\\Constraints\\%s',
                $key = array_keys($constraint)[0]
            );

            $contraints[$key] = new $classFqn($constraint[$key]);
        }

        if (!empty($constraints)) {
            $typeOptions['constraints'] = $contraints;
        }

        return $typeOptions;
    }
}
