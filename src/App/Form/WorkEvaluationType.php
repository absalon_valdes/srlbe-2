<?php

namespace App\Form;

use App\Entity\Requisition;
use App\Util\DateUtils;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class WorkEvaluationType
 * @package App\Form
 * 
 * @DI\FormType()
 */
class WorkEvaluationType extends AbstractType
{
    const EVALUATION_OK     = 'recepcion_conforme';
    const EVALUATION_NOT_OK = 'recepcion_con_reparos';
    const EVALUATION_REJECT = 'servicio_no_recibido';

    /**
     * @var TranslatorInterface
     * @DI\Inject("translator")
     */
    public $translator;

    /**
     * @var Requisition
     */
    private $requisition;
    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('evaluation', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    self::EVALUATION_OK,
                    self::EVALUATION_NOT_OK,
                    self::EVALUATION_REJECT,
                ],
                'choice_label' => function ($v) {
                    return $v;
                },  
                'choice_attr' => function($v) {
                    return [
                        'data-help-tooltip' => $this->translator->trans($v.'.help')
                    ];
                },
                'mapped' => false,
            ])
            ->add('comment', TextareaType::class, [
                'mapped' => false,
            ])
            ->add('datetime', HiddenType::class, [
                'mapped' => false,
            ])
        ;
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Requisition $requisition */
            $requisition = $this->requisition = $event->getData();
            
            /** @var array $confirmation */
           // $confirmation = $requisition->getMetadata('confirmation') ?? [];
           // $deliveredAt = $confirmation['delivered_at'] ?? null;
            
            if ($requisition->isType(Requisition::TYPE_VCARD)) {
                $confirmation = $requisition->getMetadata('confirmation') ?? json_encode([]);
                $data = json_decode($confirmation);
                $deliveredAt = $data['delivered_at'] ?? null;
                if ($deliveredAt) {
		    $deliveredAt = \DateTime::createFromFormat(\DateTime::ISO8601, $deliveredAt);
                }
            } else {
                /** @var array $confirmation */
                $confirmation = $requisition->getMetadata('confirmation') ?? [];
                $deliveredAt = $confirmation['delivered_at'] ?? null;
            }

            $event
                ->getForm()
                ->add('evaluated_at', DateTimeType::class, [
                    'label' => false,
                    'date_widget' => 'single_text',
                    'time_widget' => 'choice',
                    'date_format' => 'dd/MM/yyyy',
                    'data' => $deliveredAt instanceof \DateTime ? $deliveredAt : new \DateTime,
                    'mapped' => false,
                    'required' => false,
                ])
            ;
        });
        
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $evaluation = $event->getData();
            $evaluation['evaluated_at'] = DateUtils::dateTimeFieldToObject($evaluation['evaluated_at']);
            
            $this->requisition->addSubMetadata('evaluation', $evaluation);
        });
        
        $builder->get('datetime')
            ->addModelTransformer(new CallbackTransformer(function ($datetime) {
                /** @var \DateTime $datetime */
                return !$datetime ? time() : $datetime->getTimestamp();
            }, function ($timestamp) {
                return new \DateTime('@'.$timestamp ?? time());
            }))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'data_class' => Requisition::class,
        ]);
    }  
}
