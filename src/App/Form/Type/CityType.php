<?php
namespace App\Form\Type;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class CityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('city', EntityType::class, $this->customProvinceChoice());
    }

    private function customProvinceChoice()
    {
        $options =  array(
                'class'             => 'App:Category',
                'choice_label'      => 'name',
                'group_by'          => function ($city) {
                        $city = $city->getParent();
                        $region = $city->getParent();
                        return $region->getName();
                },
                'attr'              => array('data-widget' => 'select2'),
                'query_builder'     => function ($er) {
                        $query =  $er
                                  ->createQueryBuilder('comun')
                                  ->addSelect('city')
                                  ->addSelect('region')
                                  ->addSelect('type')
                                  ->leftJoin('comun.parent', 'city')
                                  ->leftJoin('city.parent', 'region')
                                  ->leftJoin('region.parent', 'type')
                                  ->where('type.name = :geo')
                                  ->setParameter('geo', 'Geografico');
                          return $query;
                }

           );

        return $options;
    }
}
