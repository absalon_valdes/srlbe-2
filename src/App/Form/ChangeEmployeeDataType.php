<?php

namespace App\Form;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @FormType
 */
class ChangeEmployeeDataType extends AbstractType
{
    /**
     * @Inject("user_repo")
     */
    public $userRepo;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();

            /** @var Employee $employee */
            $employee = $event->getData();

            $form
                ->add('office', EntityType::class, [
                    'class' => Office::class,
                    'attr' => ['class' => 'betterSelect'],
                    'label' => 'Sucursal',
                    'choice_attr' => function ($val) {
                        return [
                            'data-office' => json_encode($val)
                        ];
                    },
                    'query_builder' => function (EntityRepository $r) {
                        return $r->createQueryBuilder('o')
                            ->orderBy('o.name', 'asc')
                            ;
                    },
                    'data' => $employee->getOffice()->getId()
                ])
                ->add('deliveryAddress', EntityType::class, [
                    'class' => Office::class,
                    'attr' => ['class' => 'betterSelect'],
                    'required' => false,
                    'choice_label' => function (Office $k) {
                        return $k->getAddress();
                    },
                    'query_builder' => function (EntityRepository $r) {
                        return $r->createQueryBuilder('o')
                            ->orderBy('o.address', 'asc')
                        ;
                    }
                ])
                ->add('phone', TextType::class, [
                    'attr' => ['readonly' => false],
                    'data' => $employee->getPhone()
                ])
                ->add('secondPhone', NumberType::class, [
                    'label' => 'Telefono secundario',
                    'attr' => ['readonly' => false],
                    'data' => $employee->getSecondPhone(),
                    'required' => false
                ])
                ->add('alternateAddress', TextType::class, [
                    'label' => 'Confirma tu dirección',
                    'attr' => ['readonly' => false],
                    'data' => $employee->getAlternateAddress(),
                    'required' => false
                ]);
//            ->add('send', SubmitType::class, ['label' => 'Cambiar dirección', 'attr' => ['class' => 'btn btn-warning pull-left']]);

        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            /** @var Employee $employee */
            $employee = $event->getForm()->getData();

            $data = $event->getData();

            $employee->setSecondPhone($data["secondPhone"]);
            $employee->setAlternateAddress($data["alternateAddress"]);

        });
    }

    public function getName()
    {
        return 'employee_change_data';
    }
}
