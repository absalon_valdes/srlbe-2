<?php

namespace App\Form;

use App\Model\Representative;
use App\Validator\Constraints\Rut;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class PepRepresentativeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => false,
                'constraints' => [
                    new NotBlank(['groups' => 'all']),
                ]
            ])
            ->add('rut', TextType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'rut-input',
                    'help' => 'Introduzca RUT sin puntos. Ej: 9999999-9',
                ],
                'constraints' => [
                    new Rut(['groups' => 'all']), 
                    new NotBlank(['groups' => 'all']),
                ],
            ])
        ;
    } 

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Representative::class
        ]);
    }
}