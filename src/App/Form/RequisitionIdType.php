<?php

namespace App\Form;

use App\Repository\RequisitionRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RequisitionIdType
 * @package App\Form
 * 
 * @DI\FormType()
 */
class RequisitionIdType extends AbstractType implements DataTransformerInterface
{
    /**
     * @var RequisitionRepository
     * @DI\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer($this);
    }

    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return HiddenType::class;
    }

    /**
     * @inheritdoc
     */
    public function transform($requisition)
    {
        if (null === $requisition/* || !$requisition instanceof Requisition*/) {
            return '';
        }
        
        return $requisition->getId();
    }

    /**
     * @inheritdoc
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
        
        if (!$requisition = $this->requisitionRepo->find($id)) {
            throw new TransformationFailedException(sprintf(
                'Requisition with id %s not found', 
                $id
            ));
        }
        
        return $requisition;
    }
}