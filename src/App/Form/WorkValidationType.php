<?php

namespace App\Form;

use App\Entity\Requisition;
use App\Entity\User;
use App\Helper\UserHelper;
use App\Util\DateUtils;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class WorkValidationType
 * @package App\Form
 *
 * @Di\FormType
 */
class WorkValidationType extends AbstractType
{
    /**
     * @Di\Inject("form_upload_helper")
     */
    public $uploadHelper;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();

            /** @var Requisition $requisition */
            $requisition = $event->getData();

            /** Confirmation metadata */
            $confirmation = $requisition->getMetadata('confirmation') ?? [];
           
            /** @var User $user */
            $user = $this->userHelper->getCurrentUser();
            
            $attachments = array_map(function ($item) {
                $item['document'] = $item['file'];
                return $item;
            }, $confirmation['files'] ?? []);
            
            /** @var \DateTime $finishedJob */
            $finishedJob = $requisition->getJobCompletionDatetime();
             
            /** @var \DateTime $deliveredAt */
            $deliveredAt = $finishedJob ? $finishedJob : new \DateTime;
            
            /** Disable datetime */
            $readOnly = $user->hasRole('ROLE_SUPPLIER');// && !empty($confirmation);
            
            $form
                ->add('delivered_at', DateTimeType::class, [
                    'date_widget' => 'single_text',
                    'time_widget' => 'choice',
                    'date_format' => 'dd/MM/yyyy',
                    'mapped' => false,
                    'required' => false, 
                    'data' => $deliveredAt,
                    'attr' => [ 'readonly' => $readOnly ],
                ])
                ->add('files', CollectionType::class, [
                    'entry_type' => ValidationDocType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'mapped' => false,
                    'label' => false,
                    'data' => $attachments,
                    'prototype_name' => '__validation_files__'
                ])
            ;

            if (!$meta = $requisition->getMetadata('more_info')) {
                return;
            }

            foreach ($meta as $index => $moreInfo) {
                $form->add(sprintf('more_info_%s', $index), TextareaType::class, [
                    'mapped' => false,
                    'attr' => [
                        'data-index' => $index,
                        'readonly' => isset($moreInfo['response']),
                    ],
                    'label' => false,
                    'data' => $moreInfo['response'] ?? null,
                ]);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();

            /** @var Requisition $requisition */
            $requisition = $form->getData();
            
            /** @var array $certificationFiles */
            $certificationFiles = $requisition->getMetadata('confirmation')['files'] ?? [];

            /** Submitted data */
            $data = array_values(array_filter($event->getData(), function($k) {
                return preg_match('/^more_info/', $k);
            }, ARRAY_FILTER_USE_KEY));

            if ($moreInfo = $requisition->getMetadata('more_info')) {
                array_walk($moreInfo, function(&$value, $key) use ($data) {
                    $value['response'] = $data[$key];
                });

                $requisition->addMetadata('more_info', $moreInfo);
            }
            
            $files = $event->getData()['files'] ?? [];
            $documents = array();
            
            foreach ($files as $doc) {
                if (is_null($doc['description'] ?? null) && empty($doc['document'])) {
                    continue;
                }

                $documents[] = [
                    'description' => $doc['description'],
                    'file' => $this->uploadHelper->upload([$doc['document']])
                ];
            }

            $requisition->addMetadata('confirmation', [
                'delivered_at' => $this->widgetToDateTime($event->getData()),
                'files' => array_merge($certificationFiles, $documents)
            ]);
        });
    }

    /**
     * @param $data
     * @return \DateTime
     */
    private function widgetToDateTime($data)
    {
        if (!isset($data['delivered_at'])) {
            return new \DateTime();
        }
        
        if ($data['delivered_at'] instanceof \DateTime) {
            return $data['delivered_at'];
        }

        return DateUtils::dateTimeFieldToObject($data['delivered_at']);
    }

    /**
     * @inheritdoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_extra_fields' => true,
            'csrf_protection' => false,
        ]);
    }
}
