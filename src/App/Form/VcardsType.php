<?php

namespace App\Form;

use App\Entity\Office;
use App\Entity\Position;
use App\Helper\PositionTitleNormalizer;
use App\Model\VcardRequisition;
use App\Repository\PositionRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VcardsType
 * @DI\FormType()
 */
class VcardsType extends AbstractType
{
    /**
     * @DI\Inject("%area_codes%")
     */
    public $areaCodes;

    /**
     * @var PositionTitleNormalizer
     * @DI\Inject("position_title_normalizer")
     */
    public $positionNormalizer;
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, [
                'label' => 'Nombre completo',
                'attr' => [
                    'placeholder' => 'Ingrese su nombre completo',
                    'style' => 'text-transform:capitalize'
                ]
            ])
            ->add('position', EntityType::class, [
                'label' => 'Cargo',
                'attr' => ['class' => 'betterSelect'],
                'class' => Position::class,
                'query_builder' => function (PositionRepository $repo) {
                    return $repo
                        ->createQueryBuilder('o')
                        ->orderBy('o.title', 'ASC')
                    ;
                },
                'choice_attr' => function (Position $pos) {
                    return [
                        'data-position' => $this->positionNormalizer->normalize($pos)
                    ];
                },
                'choice_label' => function (Position $pos) {
                    return $this->positionNormalizer->normalize($pos);
                }
            ])
            ->add('office', EntityType::class, [
                'label' => 'Dependencia (opcional)',
                'attr' => ['class' => 'betterSelect'],
                'class' => Office::class,
                'required' => false,
                'choice_attr' => function ($val) {
                    return [
                        'data-office' => json_encode($val)
                    ];
                },
            ])
            ->add('showOffice', CheckboxType::class, [
                'label' => 'Mostrar en tarjeta',
                'required' => false,
            ])
            ->add('addressOffice', EntityType::class, [
                'label' => 'Dirección (opcional)',
                'attr' => ['class' => 'betterSelect'],
                'required' => false,
                'class' => Office::class,
                'choice_attr' => function ($val) {
                    return [
                        'data-office' => json_encode($val)
                    ];
                },
                'choice_label' => function (Office $k) {
                    return $k->getAddress();
                }
            ])
            ->add('showAddress', CheckboxType::class, [
                'label' => 'Mostrar en tarjeta',
                'required' => false,
            ])
            ->add('addressDetail', TextType::class, [
                'label' => 'Piso/Local (opcional)',
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'label' => 'Comuna',
                'mapped' => false,
                'attr' => ['readonly' => true],
            ])
            ->add('costCenter', TextType::class, [
                'label' => 'Centro de costo',
                'attr' => ['readonly' => true]
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mail',
                'attr' => ['readonly' => true],
            ])
            ->add('areaCode', ChoiceType::class, [
                'label' => 'Código área teléfono',
                'choices' => $this->areaCodes,
                'choice_label' => function ($v, $k) {
                    return $v.' - '.$k;
                },
                'attr' => [
                    'data-area-code-selector' => null
                ]
            ])
            ->add('phone', IntegerType::class, [
                'label' => 'N° de teléfono',
                'attr' => [
                    'data-phone-number' => null,
                    'placeholder' => 'Ingrese su número de teléfono'
                ]
            ])
            ->add('fax', IntegerType::class, [
                'label' => 'N° de fax',
                'required' => false,
                'attr' => [
                    'data-phone-number' => null,
                    'placeholder' => 'Ingrese su número de fax'
                ]
            ])
            ->add('mobile', IntegerType::class, [
                'label' => 'N° de celular',
                'required' => false,
                'attr' => [
                    'data-mobile-number' => null,
                    'placeholder' => 'Ingrese su número de celular'
                ]
            ])
            ->add('comments', TextareaType::class, [
                'label' => 'Observaciones',
                'required' => false,
                'attr' => ['placeholder' => 'Ingrese una observación']
            ])
            ->add('quantity', ChoiceType::class, [
                'choices' => [100 => 100, 200 => 200],
                'expanded' => true,
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            
            if (!$data->isDouble()) {
                return;
            }
            
            $form
                ->add('contacts', CollectionType::class, [
                    'entry_type' => ContactType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'label' => false,
                    'prototype_name' => '__vcards_contacts_name__',
                ])
            ;
        });

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            $contacts = $data->getContacts();
            
            $data->setContacts($contacts->map(function ($contact) use ($data) {
                return $contact->setRequisition($data);
            }));
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', VcardRequisition::class);
    }
}