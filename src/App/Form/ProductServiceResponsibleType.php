<?php

namespace App\Form;

use App\Entity\Requisition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class ProductServiceResponsible
 * @package App\Form
 */
class ProductServiceResponsibleType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('responsible', TextType::class, [
                'constraints' => new Length(['min' => 7]),
                'property_path' => 'metadata[responsible]',
                'attr' => ['help' => 'El nombre debe tener al menos 7 caracteres']
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Requisition::class);
    }
}