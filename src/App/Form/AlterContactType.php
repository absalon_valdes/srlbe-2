<?php

namespace App\Form;

use App\Model\AlterContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AlterContactType
 * @package App\Form
 */
class AlterContactType extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('last_name', TextType::class)
            ->add('second_last_name', TextType::class)
            ->add('email', EmailType::class)
            ->add('phone', TextType::class)
        ;
    }

    /**
     * @inheritDoc
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', AlterContact::class);
    }
}