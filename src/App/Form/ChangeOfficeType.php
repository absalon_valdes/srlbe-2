<?php

namespace App\Form;

use App\Entity\Office;
use App\Entity\User;
use App\Helper\SpecialOfficeHelper;
use App\Helper\UserHelper;
use App\Repository\OfficeRepository;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChangeOfficeType
 * @package App\Form
 * @Di\FormType()
 */
class ChangeOfficeType extends AbstractType
{

    /**
     * @var SpecialOfficeHelper
     * @Di\Inject("special_office_helper")
     */
    public $specialOfficeHelper;

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            /** @var FormInterface $form */
            $form = $event->getForm();

            $form
                ->add('office', EntityType::class, [
                    'class' => Office::class,
                    'attr' => ['class' => 'betterSelect'],
                    'query_builder' => function (OfficeRepository $repo)  {
                        $query = $repo->createQueryBuilder('o');
                        if ($isSpecialOffice = $this->specialOfficeHelper->checkSpecialOffice()) {
                            if($this->specialOfficeHelper->getConfig('filter_offices')){
                                $query->where('o.specialType = :type');
                                $query->setParameter('type',$isSpecialOffice);
                            }
                        }
                        return $query;
                    },
                    'choice_attr' => function ($val) {
                        return [
                            'data-office' => json_encode($val)
                        ];
                    }
                ])
                ->add('phone', TextType::class, [
                    'label' => 'Teléfono',
                    'required' => false,
                    'attr' => [
                        'class ' => 'form-control',
                        'placeholder' => 'Teléfono'
                    ],
                ]);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('csrf_protection', false);
    }
}