<?php

namespace App\Form;

use App\Entity\User;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @FormType
 */
class RequesterInfoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('rut', TextType::class, ['attr' => ['readonly' => true ] ])
            ->add('position', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('email', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('phone', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('officeType', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('officeName', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('officeCode', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('officeAddress', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('officeCity', TextType::class, ['attr' => ['readonly' => true ]])
            ->add('officeCostCenter', TextType::class, ['attr' => ['readonly' => true ]]);
    }

    public function getName()
    {
        return 'requester_info';
    }
}
