<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ProviderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rut', TextType::class, [
                'label' => 'Ingrese el rut del proveedor',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Rut del proveedor'
                ],
            ])
            ->add('name', TextType::class, [
                'label' => 'Nombre de la empresa proveedora',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Nombre de empresa proveedora'
                ],
            ])
            ->add('city', EntityType::class, array(
                'label' => 'Ciudad',
                'class' => 'App:Category',
                'choice_label' => 'name',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Nombre de empresa proveedora'
                ],
            ))
            ->add('telephono', TextType::class, [
                'label' => 'Teléfono del proveedor',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Teléfono'
                ],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Correo del proveedor',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Ingrese un email'
                ]
            ])
            ->add('sla', TextType::class, [
                'label' => 'SLA - Tiempo de entrega o de fiscalización del trabajo',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Ingrese el sla'
                ]
            ])
            ->add('cost_center', TextType::class, [
                'label' => 'Centro de costo',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Ingrese el numero de centro de costo'
                ]
            ])
            ->add('cost', TextType::class, [
                'label' => 'Costo',
                'attr' => [
                    'class ' => 'form-control',
                    'placeholder' => 'Ingrese el monto'
                ]
            ]);
    }
}