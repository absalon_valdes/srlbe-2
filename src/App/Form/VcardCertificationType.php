<?php

namespace App\Form;

use App\Model\VcardCertification;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VcardCertificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('receiver', TextType::class)
            ->add('extras', CollectionType::class, [
                'entry_type' => VcardCertificationExtraType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false,
                'prototype_name' => '__vcard_certification_extra_name__',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', VcardCertification::class);
    }
}