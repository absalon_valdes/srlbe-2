<?php

namespace App\Form;

use App\Entity\SpecialRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpecialRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('request', TextType::class, [
                'label' => '¿Qué necesita solicitar?',
                'attr' => [
                    'placeholder' => 'Nombre del pedido'
                ]
            ])
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'label' => '¿Para cuándo lo necesita?',
                'attr' => [
                    'class' => 'datepicker',
                    'data-date-autoclose' => 'true',
                    'data-date-format' => 'dd/mm/yyyy',
                    'placeholder' => 'Ingrese la fecha'
                ],
                'html5' => false,
                'format' => 'dd/MM/yyyy'
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Descripción de lo que necesita',
                'attr' => [
                    'placeholder' => 'Describa el detalle de lo que solicita'
                ]
            ])
            ->add('files', FileType::class, [
                'multiple' => true,
                'label' => 'Adjuntar foto o archivo',
                'required' => false
            ])
        ;  
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', SpecialRequest::class);
    }
}