<?php

namespace App\Form;

use App\Repository\CategoryRepository;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\FormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * @FormType
 */
class BackOfficeSearchPepGrantType extends AbstractType
{
    /**
     * @var CategoryRepository
     * @Inject("category_repo")
     */
    public $categoryRepo;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $regions = $this->categoryRepo->getChildren($this->categoryRepo->findOneByName('Geografico'), true);
        $cities = $this->categoryRepo->getLeafs($this->categoryRepo->findOneByName('Geografico'));

        $builder
            ->add('office', TextType::class)
            ->add('createdAt', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false,
                'attr' => [
                    'class' => 'datepicker',
                    'data-date-autoclose' => 'true',
                    'data-date-format' => 'dd/mm/yyyy',
                ],
            ])
            ->add('region', ChoiceType::class, [
                'attr' => ['class' => 'betterSelect'],
                'choices' => array_filter(['Todas' => 'region.all'] + $regions),
                'choice_label' => function ($category, $key, $index) {
                    if (!is_string($category)) {
                        return $category->getName();
                    }

                    return $key;
                },
            ])
            ->add('city', ChoiceType::class, [
                'attr' => ['class' => 'betterSelect'],
                'choices' => array_filter(['Todas' => 'cities.all'] + $cities),
                'choice_label' => function ($category, $key, $index) {
                    if (!is_string($category)) {
                        return $category->getName();
                    }

                    return $key;
                },
            ])
            ->add('userName', TextType::class)
            ->add('search', SubmitType::class, [
                'attr' => [
                    'class' => 'pull-right btn btn-sm btn-success',
                    'id' => 'btn_search_requisition',
                    'name' => 'btn_search_requisition',
                ],
            ]);
    }

    public function getName()
    {
        return 'search_pep_grant';
    }
}
