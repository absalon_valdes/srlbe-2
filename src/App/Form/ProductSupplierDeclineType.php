<?php

namespace App\Form;

use App\Entity\Requisition;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductSupplierDeclineType
 * @package App\Form
 */
class ProductSupplierDeclineType extends AbstractType
{
    const DECLINED_STOCK     = 'sin_stock';
    const DECLINED_PERSONNEL = 'sin_personal';
    const DECLINED_TECHNICAL = 'motivos_tecnicos';

    /**
     * @var Requisition
     */
    public $requisition;
    
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('comment', TextareaType::class, [
            'mapped' => false,
        ]);
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            /** @var Requisition $requisition */
            $requisition = $this->requisition = $event->getData();
            
            if ($requisition->isType(Requisition::TYPE_MAINTENANCE)) {
                return;
            }
            
            $event->getForm()
                ->add('reason', ChoiceType::class, [
                    'mapped' => false,
                    'expanded' => true, 
                    'choices' => [
                        self::DECLINED_STOCK,
                        self::DECLINED_PERSONNEL,
                        self::DECLINED_TECHNICAL,
                    ],
                    'choice_label' => function ($v) {
                        return $v;
                    },  
                ])
            ;
        });
        
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $this->requisition->addSubMetadata('declined', 
                $this->requisition->getSupplier()->getId()
            );
            
            $this->requisition->addSubMetadata('decline', $event->getData());
        });
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Requisition::class);
    }
}