<?php

namespace App\Loader;

/**
 * Interface LoaderInterface
 * @package App\Loader
 */
interface LoaderInterface
{
    /**
     * @param $filename
     * @return mixed
     */
    public function load($filename);

    /**
     * @return mixed
     */
    public function getType();
}
