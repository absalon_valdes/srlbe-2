<?php

namespace App\Model;

use App\Entity\Contract;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Position;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class VcardRequisition
 *
 * @Assert\Callback({"App\Validator\Callback\VcardValidationCallback", "validate"})
 * @Serializer\ExclusionPolicy("ALL")
 */
class VcardRequisition
{
    /**
     * @Serializer\Type("App\Entity\Employee")
     * @Serializer\Expose()
     */
    protected $employee;

    /** 
     * @Assert\NotBlank() 
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $fullName;
    
    /** 
     * @Assert\NotBlank()
     * @Serializer\Type("App\Entity\Position")
     * @Serializer\Expose()
     */
    protected $position;
    
    /** 
     * @Assert\NotBlank()
     * @Serializer\Type("App\Entity\Office")
     * @Serializer\Expose()
     */
    protected $office;

    /**
     * @Assert\NotBlank()
     * @Serializer\Type("App\Entity\Office")
     * @Serializer\Expose()
     */
    protected $addressOffice;

    /**
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $addressDetail;

    /**
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $costCenter;

    /**
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $areaCode;
    
    /** 
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $phone;

    /** 
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $fax;

    /** 
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $mobile;

    /**
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $comments;
    
    /** 
     * @Assert\Choice(choices={100, 200}) 
     * @Serializer\Expose()
     * @Serializer\Type("int")
     */
    protected $quantity;
    
    /** 
     * @Assert\Email() 
     * @Serializer\Expose()
     * @Serializer\Type("string")
     */
    protected $email;

    /**
     * @Assert\Valid()
     * @Serializer\Expose()
     * @Serializer\Type("ArrayCollection<App\Model\VcardContact>")
     */
    protected $contacts;

    /**
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $showAddress = true;

    /**
     * @Serializer\Expose()
     * @Serializer\Type("boolean")
     */
    protected $showOffice = true;

    protected $product;
    protected $isDouble = false;
    protected $contract;

    public function __construct(Employee $employee, Contract $contract, $isDouble = false)
    {
        $this->employee = $employee;
        $this->contract = $contract;
        
        $this->fullName = ucwords($employee->getFullName());
        $this->email = $employee->getEmail();
        $this->position = $employee->getPosition();
        $this->office = $employee->getOffice();
        $this->addressOffice = $employee->getOffice();
        $this->costCenter = $this->office->getCostCenter();
        $this->product = $contract->getProduct();
        $this->isDouble = $isDouble;
        
        $this->contacts = new ArrayCollection;
    }

    public function setEmployee(Employee $employee)
    {
        $this->employee = $employee;
    }

    public function getEmployee()
    {
        return $this->employee;
    }

    public function getFullName()
    {
        return $this->fullName;
    }

    public function setFullName($fullName)
    {
        $this->fullName = ucwords($fullName);
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function setPosition(Position $position)
    {
        $this->position = $position;
    }

    public function getOffice()
    {
        return $this->office;
    }

    public function setOffice(Office $office)
    {
        $this->office = $office;
    }

    public function setAddressOffice(Office $office)
    {
        $this->addressOffice = $office;
    }

    public function getAddressOffice()
    {
        return $this->addressOffice;
    }

    public function getCostCenter()
    {
        return $this->costCenter;
    }

    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;
    }

    public function getAreaCode()
    {
        return $this->areaCode;
    }

    public function setAreaCode($areaCode)
    {
        $this->areaCode = $areaCode;
    }
    
    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getFax()
    {
        return $this->fax;
    }

    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    public function getComments()
    {
        return $this->comments;
    }

    public function setComments($comments)
    {
        $this->comments = $comments;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getContacts()
    {
        return $this->contacts;
    }

    public function setContacts(ArrayCollection $contacts)
    {
        $this->contacts = $contacts;
    }

    public function addContact(VcardContact $contact)
    {
        $this->contacts->add($contact);
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getProduct()
    {
        return $this->product;
    }

    public function setProduct($product)
    {
        $this->product = $product;
    }

    public function isDouble()
    {
        return $this->isDouble;
    }

    public function setAddressDetail($detail)
    {
        $this->addressDetail = $detail;
    }

    public function getAddressDetail()
    {
        return $this->addressDetail;
    }

    public function isShowAddress()
    {
        return $this->showAddress;
    }

    public function setShowAddress($showAddress)
    {
        $this->showAddress = $showAddress;
    }

    public function isShowOffice()
    {
        return $this->showOffice;
    }


    public function setShowOffice($showOffice)
    {
        $this->showOffice = $showOffice;
    }

    public function equalTo(VcardRequisition $o)
    {
        return $this->position === $o->getPosition()
            && $this->office->getCity()->getParent() === $o->getOffice()->getCity()->getParent()
        ;
    }
}
