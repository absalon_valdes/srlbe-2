<?php

namespace App\Model;

use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class JobReport
 */
class JobReport implements \Serializable
{
    const CREATED = 'create';
    const UPDATED = 'update';
    const SKIPPED = 'skipped';
    const VALIDATED = 'validated';

    private $lineno = 0;
    private $data = array();
    private $action = self::SKIPPED;
    private $errors = array();
    
    /**
     * JobReport constructor.
     * @param $lineno
     * @param array $data
     */
    public function __construct($lineno, array $data)
    {
        $this->lineno = $lineno;
        $this->data = $data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param $lineno
     * @param array $data
     * @return JobReport
     */
    public static function create($lineno, array $data)
    {
        return new self($lineno, $data);
    }
    
    /**
     * @return int
     */
    public function getLineno()
    {
        return $this->lineno;
    }
    
    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }
    
    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }
    
    /**
     * @param mixed $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
    
    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
    
    /**
     * @param array $errors
     * @return $this
     */
    public function setErrors(array $errors)
    {
        foreach ($errors as $error) {
            $this->addError($error);
        }
        
        return $this;
    }
    
    /**
     * @param $violation
     */
    public function addError($violation)
    {
        $message = '';
        if(is_string($violation)) {
            $message = $violation;
        } else if ($violation instanceof ConstraintViolationInterface) {
            $message = $violation->getMessage();
        }
        
        if (!is_string($message)) {
            $message = 'Unkown error';
        }
        
        $this->action = self::SKIPPED;
        $this->errors[] = $message;
    }

    public function addErrors(ConstraintViolationList $constraintViolationList) {
        foreach ($constraintViolationList as $constraintViolation) {
            $this->errors[] = $constraintViolation->getMessage();
        }
        $this->action = self::SKIPPED;
    }
    
    /**
     * @return bool
     */
    public function willCreate()
    {
        return self::CREATED === $this->action;
    }
    
    /**
     * @return bool
     */
    public function willUpdate()
    {
        return self::UPDATED === $this->action;
    }
    
    /**
     * @return bool
     */
    public function willSkip()
    {
        return self::SKIPPED === $this->action;
    }
    
    /**
     * @inheritdoc
     */
    public function serialize()
    {
        return serialize([
            $this->lineno,
            $this->data,
            $this->errors,
            $this->action
        ]);
    }
    
    /**
     * @inheritdoc
     */
    public function unserialize($serialized)
    {
        list(
            $this->lineno,
            $this->data,
            $this->errors,
            $this->action           
        ) = unserialize($serialized);
    }
}