<?php 

namespace App\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;

/**
 * Class Company
 * @package App\Model
 */
class Company
{
    const TYPE_COMPANY = 0;
    const TYPE_PERSON  = 1;
    
    /**
     * @var string
     */
    protected $name;
    
    /**
     * @var string
     */
    protected $rut;
    
    /**
     * @var ArrayCollection
     */
    protected $shareholders;
    
    /**
     * @var ArrayCollection
     */
    protected $representatives;
    
    /**
     * @var ArrayCollection
     */
    protected $documents;
    
    /**
     * @var
     */
    protected $type;
    
    /**
     * @var bool
     */
    protected $selected = false;
    
    /**
     * @var bool
     */
    protected $isPep = false;

    /**
     * Company constructor.
     */
    public function __construct()
    {
        $this->shareholders = new ArrayCollection;
        $this->representatives = new ArrayCollection;
        $this->documents = new ArrayCollection;
    }

    /**
     * @return bool
     */
    public function isPep()
    {
        return $this->isPep;
    }

    /**
     * @param $isPep
     */
    public function setIsPep($isPep)
    {
        $this->isPep = $isPep;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     * @return mixed
     */
    public function setType($type)
    {
        return $this->type = $type;
    }

    /**
     * @param $selected
     * @return $this
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSelected()
    {
        return $this->selected;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $rut
     * @return $this
     */
    public function setRut($rut)
    {
        $this->rut = $rut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRut()
    {
        return $this->rut;
    }

    /**
     * @param ArrayCollection $documents
     * @return $this
     */
    public function setDocuments(ArrayCollection $documents)
    {
        $this->documents = $documents;

        return $this;
    }

    /**
     * @param $document
     * @return $this
     */
    public function addDocument($document)
    {
        $this->documents[] = $document;

        return $this;
    }

    /**
     * @param $document
     * @return bool
     */
    public function removeDocument($document)
    {
        return $this->documents->removeElement($document);
    }

    /**
     * @return ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param Representative $representative
     * @return $this
     */
    public function addRepresentative(Representative $representative)
    {
        $this->representatives[] = $representative;

        return $this;
    }

    /**
     * @param Representative $representative
     * @return bool
     */
    public function removeRepresentative(Representative $representative)
    {
        return $this->representatives->removeElement($representative); 
    }

    /**
     * @param $representatives
     * @return $this
     */
    public function setRepresentatives($representatives)
    {
        $this->representatives = $representatives;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRepresentatives()
    {
        return $this->representatives;
    }

    /**
     * @param Shareholder $shareholder
     * @return $this
     */
    public function addShareholder(Shareholder $shareholder)
    {
        $this->shareholders[] = $shareholder;

        return $this;
    }

    /**
     * @param Shareholder $shareholder
     * @return bool
     */
    public function removeShareholder(Shareholder $shareholder)
    {
        return $this->shareholders->removeElement($shareholder);
    }

    /**
     * @param $shareholders
     * @return $this
     */
    public function setShareholders($shareholders)
    {
        $this->shareholders = $shareholders;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getShareholders()
    {
        return $this->shareholders;
    }

    /**
     * @return bool
     */
    public function hasPeps()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->neq('pep', null));

        return !$this->getPeople()->matching($criteria)->isEmpty() || $this->isPep;
    }

    /**
     * @return ArrayCollection
     */
    public function getPeople()
    {
        return new ArrayCollection(array_merge(
            $this->shareholders->toArray(), 
            $this->representatives->toArray()
        ));
    }
}