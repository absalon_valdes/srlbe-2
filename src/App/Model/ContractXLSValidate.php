<?php
namespace App\Model;
use App\Helper\ExcelBatchProcessor;
use Liuggio\ExcelBundle\Factory;
use Symfony\Component\Config\Tests\Util\Validator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as Validate;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ContractXLSValidate {

    const XLS_HEADERS = [
        0=>'CODIGO TECNICO',
        1=>'RUT PROVEEDOR',
        2=>'NUMERO DE CONTRATO',
        3=>'FECHA INICIO',
        4=>'FECHA TERMINO',
        5=>'DOCUMENTACION COMPRA',
        6=>'CODIGO SUCURSAL',
        7=>'CENTRO DE COSTO',
        8=>'TIPO DE MONEDA',
        9=>'VALOR',
        10=>'SLA EVALUADOR',
        11=>'SLA OC',
        12=>'SLA PROVEEDOR',
        13=>'SLA PRODUCTO',
        14=>'SLA DESPACHO'
    ];

    const NAME_PROPERTIES = [
        'technicalCode'=>'Código técnico',
        'supplier'=>'Rut proveedor',
        'contractNumber'=>'Número de contrato',
        'initDate'=>'Fecha inicio',
        'finishDate'=>'Fecha termino',
        'purchaseDocumentation'=>'Documentacion de compra',
        'brandOfficeCode'=>'Código sucursal',
        'costCenter'=>'Centro de costo',
        'moneyType'=>'Tipo de moneda',
        'price'=>'Valor',
        'SlaEvaluator'=>'SLA evaluador',
        'SlaOc'=>'SLA OC',
        'SlaSupplier'=>'SLA proveedor',
        'SlaProduct'=>'SLA producto',
        'SlaOffice'=>'SLA despacho'
    ];

    /**
     * @Assert\NotBlank(message="Código técnico es requerido")
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9_-]*$/",
     *     match=true,
     *     message="Codigo técnico incorrecto"
     * )
     */
    protected $technicalCode;

    /**
     * @Assert\NotBlank(message="Rut del proveedor es requerido")
     * @Validate\Supplier()
     */
    protected $supplier;

    /**
     * @Assert\NotBlank(message="Número de contrato es requerido")
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9_-]*$/",
     *     match=true,
     *     message="Número de contrato incorrecto"
     * )
     */
    protected $contractNumber;

    /**
     * @Assert\NotBlank(message="Fecha de inicio es requerida")
     * @Assert\Date(
     *     message="Formato de fecha de inicio incorrecto"
     * )
     */
    protected $initDate;

    /**
     * @Assert\NotBlank(message="Fecha de termino es requerida")
     * @Assert\Date(
     *     message="Formato de fecha de termino incorrecto"
     * )
     */
    protected $finishDate;

    /**
     * @Assert\Choice(
     *     choices = {"Contrato", "contrato", "Orden de compra", "orden de compra", ""},
     *     message  = "Documentación de compra incorrecta"
     * )
     */
    protected $purchaseDocumentation;

    /**
     * @Assert\NotBlank(message="Código de sucursal es requerido")
     * @Assert\Regex(
     *     pattern="/^[A-Za-z0-9_-]*$/",
     *     match=true,
     *     message="Código de sucursal incorrecto"
     * )
     */
    protected $brandOfficeCode;

    /**
     * @Assert\NotBlank(message="Centro de costo es requerido")
     * @Assert\Type(type="numeric", message="Centro de costo incorrecto")
     */
    protected $costCenter;

    /**
     * @Assert\Choice(
     *     choices = {"peso", "uf", "usd", ""},
     *     message  = "Tipo de moneda incorrecto"
     * )
     */
    protected $moneyType;


    protected $price;

    /**
     * @Assert\NotBlank(message="SLA Evaluador es requerido")
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,2}(diashabiles\z|horashabiles\z|diascorridos\z|horascorridas\z)/i",
     *     match=true,
     *     message="SLA Evaluador incorrecto"
     * )
     */
    protected $SlaEvaluator;

    /**
     * @Assert\NotBlank(message="SLA OC es requerido")
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,2}(diashabiles\z|horashabiles\z|diascorridos\z|horascorridas\z)/i",
     *     match=true,
     *     message="SLA OC incorrecto"
     * )
     */
    protected $SlaOc;

    /**
     * @Assert\NotBlank(message="SLA Proveedor es requerido")
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,2}(diashabiles\z|horashabiles\z|diascorridos\z|horascorridas\z)/i",
     *     match=true,
     *     message="SLA Proveedor incorrecto"
     * )
     */
    protected $SlaSupplier;

    /**
     * @Assert\NotBlank(message="SLA Producto es requerido")
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,2}(diashabiles\z|horashabiles\z|diascorridos\z|horascorridas\z)/i",
     *     match=true,
     *     message="SLA Producto incorrecto"
     * )
     */
    protected $SlaProduct;

    /**
     * @Assert\NotBlank(message="SLA Sucursal es requerido")
     * @Assert\Regex(
     *     pattern="/^[0-9]{1,2}(diashabiles\z|horashabiles\z|diascorridos\z|horascorridas\z)/i",
     *     match=true,
     *     message="SLA Sucursal incorrecto"
     * )
     */
    protected $SlaOffice;

    /**
     * ContractXLSValidate constructor.
     */
    public function __construct($data)
    {
        $this->createFromArray($data);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTechnicalCode()
    {
        return $this->technicalCode;
    }

    /**
     * @param mixed $technicalCode
     */
    public function setTechnicalCode($technicalCode)
    {
        $this->technicalCode = $technicalCode;
    }

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param mixed $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return mixed
     */
    public function getContractNumber()
    {
        return $this->contractNumber;
    }

    /**
     * @param mixed $contractNumber
     */
    public function setContractNumber($contractNumber)
    {
        $this->contractNumber = $contractNumber;
    }

    /**
     * @return mixed
     */
    public function getInitDate()
    {
        return $this->initDate;
    }

    /**
     * @param mixed $initDate
     */
    public function setInitDate($initDate)
    {
        $this->initDate = $initDate;
    }

    /**
     * @return mixed
     */
    public function getFinishDate()
    {
        return $this->finishDate;
    }

    /**
     * @param mixed $finishDate
     */
    public function setFinishDate($finishDate)
    {
        $this->finishDate = $finishDate;
    }

    /**
     * @return mixed
     */
    public function getPurchaseDocumentation()
    {
        return $this->purchaseDocumentation;
    }

    /**
     * @param mixed $purchaseDocumentation
     */
    public function setPurchaseDocumentation($purchaseDocumentation)
    {
        $this->purchaseDocumentation = $purchaseDocumentation;
    }

    /**
     * @return mixed
     */
    public function getBrandOfficeCode()
    {
        return $this->brandOfficeCode;
    }

    /**
     * @param mixed $brandOfficeCode
     */
    public function setBrandOfficeCode($brandOfficeCode)
    {
        $this->brandOfficeCode = $brandOfficeCode;
    }

    /**
     * @return mixed
     */
    public function getCostCenter()
    {
        return $this->costCenter;
    }

    /**
     * @param mixed $costCenter
     */
    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;
    }

    /**
     * @return mixed
     */
    public function getMoneyType()
    {
        return $this->moneyType;
    }

    /**
     * @param mixed $moneyType
     */
    public function setMoneyType($moneyType)
    {
        $this->moneyType = $moneyType;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getSlaEvaluator()
    {
        return $this->SlaEvaluator;
    }

    /**
     * @param mixed $SlaEvaluator
     */
    public function setSlaEvaluator($SlaEvaluator)
    {
        $this->SlaEvaluator = $SlaEvaluator;
    }

    /**
     * @return mixed
     */
    public function getSlaOc()
    {
        return $this->SlaOc;
    }

    /**
     * @param mixed $SlaOc
     */
    public function setSlaOc($SlaOc)
    {
        $this->SlaOc = $SlaOc;
    }

    /**
     * @return mixed
     */
    public function getSlaSupplier()
    {
        return $this->SlaSupplier;
    }

    /**
     * @param mixed $SlaSupplier
     */
    public function setSlaSupplier($SlaSupplier)
    {
        $this->SlaSupplier = $SlaSupplier;
    }

    /**
     * @return mixed
     */
    public function getSlaProduct()
    {
        return $this->SlaProduct;
    }

    /**
     * @param mixed $SlaProduct
     */
    public function setSlaProduct($SlaProduct)
    {
        $this->SlaProduct = $SlaProduct;
    }

    /**
     * @return mixed
     */
    public function getSlaOffice()
    {
        return $this->SlaOffice;
    }

    /**
     * @param mixed $SlaOffice
     */
    public function setSlaOffice($SlaOffice)
    {
        $this->SlaOffice = $SlaOffice;
    }



    public function createFromArray($array) {
        foreach($array as $keyColumn=>$value) {
            switch($keyColumn) {
                case 0: //codigo tecnico
                    $this->setTechnicalCode($value);
                    break;
                case 1: //rut proveedor
                    $this->setSupplier((int)$value);
                    break;
                case 2: //numero contrato
                    $this->setContractNumber($value);
                    break;
                case 3: //fecha de inicio
                    $value = is_numeric($value) ? $this->excelDateToPHPDatetime($value):$value;
                    $this->setInitDate($value);
                    break;
                case 4: //fecha de termino
                    $value = is_numeric($value) ? $this->excelDateToPHPDatetime($value):$value;
                    $this->setFinishDate($value);
                    break;
                case 5: //documentación compra
                    $this->setPurchaseDocumentation(strtolower($value));
                    break;
                case 6: //codigo sucursal
                    $this->setBrandOfficeCode($value);
                    break;
                case 7: //centro de costo
                    $this->setCostCenter($value);
                    break;
                case 8: //tipo de moneda
                    $this->setMoneyType(strtolower($value));
                    break;
                case 9: //valor
                    $this->setPrice($value);
                    break;
                case 10: //SLA evaluador
                    $this->setSlaEvaluator($value);
                    break;
                case 11: //SLA OC
                    $this->setSlaOc($value);
                    break;
                case 12: //SLA proveedor
                    $this->setSlaSupplier($value);
                    break;
                case 13: //SLA producto
                    $this->setSlaProduct($value);
                    break;
                case 14: //SLA despacho
                    $this->setSlaOffice($value);
                    break;
            }
        }
    }

    /**
     * @param $cellsArray
     * @return array
     */
    public function getArrayObjectsFromArrayXLS($cellsArray) {
        $errors = [];
        foreach ($cellsArray as $keyRow=>$columnData) {
            $contractXlsValidate = new ContractXLSValidate($columnData);
            $errors[] = $contractXlsValidate;
        }
        return $errors;
    }

    /**
     * @param int $excelDateTime
     * @return \DateTime
     */
    public static function excelDateToPHPDatetime($excelDateTime = 0) {
        $calendarBaseLine = '1899-12-30';
        if ($excelDateTime < 60) {
            // 29th February will be treated as 28th February
            ++$excelDateTime;
        }

        return (new \DateTime($calendarBaseLine, new \DateTimeZone('UTC')))
            ->modify('+' . floor($excelDateTime) . ' days')
            ->modify('+' . floor(fmod($excelDateTime, 1) * 86400) . ' seconds');
    }

    public static function validateHeaders($headers) {
        return ContractXLSValidate::XLS_HEADERS == array_map(
            function($val)
            {
                return strtoupper($val);
            },$headers) ? true:false;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if(!empty($this->getPrice())) {
            if(!is_numeric($this->getPrice())){
                $context->buildViolation('Valor debe ser un valor numerico')
                    ->atPath('price')
                    ->setInvalidValue($this->getPrice())
                    ->addViolation();
            }
            else if($this->getPrice() < 0) {
                $context->buildViolation('Valor debe ser igual o mayor que 0')
                    ->atPath('price')
                    ->setInvalidValue($this->getPrice())
                    ->addViolation();
            }
        }
    }
}