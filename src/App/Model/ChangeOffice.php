<?php

namespace App\Model;

use App\Entity\Employee;
use App\Entity\Office;

/**
 * Class ChangeOffice
 * @package App\Model
 */
class ChangeOffice
{
    /**
     * @var Office
     */
    protected $office;

    /**
     * @var phone
     */
    protected $phone;

    /**
     * ChangeOffice constructor.
     * @param Office $office
     * @param  $phone
     */
    public function __construct(Office $office = null,$phone = null)
    {
        $this->office = $office;
        $this->phone = $phone;
    }

    /**
     * @param Employee $employee
     * @return ChangeOffice
     */
    public static function createForEmployee(Employee $employee)
    {
        return new self($employee->getOffice(),$employee->getPhone());
    }

    /**
     * @return Office
     */
    public function getOffice()
    {
        return $this->office;
    }

    /**
     * @param Office $office
     */
    public function setOffice(Office $office)
    {
        $this->office = $office;
    }

    /**
     * @return phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param  $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
}