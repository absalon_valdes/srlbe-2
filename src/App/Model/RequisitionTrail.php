<?php
/**
 * Created by PhpStorm.
 * User: rmoreno
 * Date: 23-05-18
 * Time: 19:22
 */

namespace App\Model;

/**
 * Trait RequisitionTrail
 * @package App\Model
 */

trait RequisitionTrail
{
    /**
     * @param string $status
     * @return string
     */
    function getAliasStatus(string $status){
        return implode('.',['product',$status]);
    }

    /**
     * @param string $type
     * @return string
     */
    function getAliasType(string $type){
        return implode('.',['requisition',$type]);

    }
}