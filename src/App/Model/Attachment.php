<?php

namespace App\Model;

/**
 * Class Attachment
 * 
 * @package App\Model
 */
class Attachment implements \JsonSerializable
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;

    /**
     * Attachment constructor.
     *
     * @param $name
     * @param $path
     */
    public function __construct($name, $path)
    {
        $this->name = $name;
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @inheritDoc
     */
    function __toString()
    {
        return $this->name; 
    }

    /**
     * @inheritDoc
     */
    function jsonSerialize()
    {
        return [
            'name' => $this->name,
            'path' => $this->path,
        ];
    }
}