<?php

namespace App\Helper;

use App\Model\Attachment;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class UploadMover
 * @package App\Helper
 */
class UploadMover
{
    /**
     * @param UploadedFile $file
     * @param $storage
     * @return string
     */
    public static function move(UploadedFile $file, $storage)
    {
        $org = $file->getClientOriginalName();
        $ext = $file->getClientOriginalExtension();
        $newName = sha1($org.time()).($ext ? '.'.$ext : null);
        
        $file->move($storage, $newName);
        
        return new Attachment($org, $newName);
    }
}
