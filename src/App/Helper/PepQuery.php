<?php

namespace App\Helper;

use App\Entity\PepGrant;
use App\Model\Person;
use App\Model\Shareholder;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("pep_query_helper")
 */
class PepQuery
{
    const SHARE_THRESHOLD = 10;

    /**
     * @var array
     */
    private $pepDatabase;

    /**
     * @Di\Inject("%kernel.root_dir%")
     */
    public $rootDir;

    /**
     * @param PepGrant $grant
     */
    public function checkPep(PepGrant $grant)
    {
        $companies = $grant->getCompanies();
        $companies->map(function ($company) {
            if ($company->getType() === 1) {
                $pepType = $this->validateRut($company->getRut());
                $company->setIsPep($pepType ? 'pep_type_'.$pepType[0] : false);
            }

            $company->getPeople()->map(function(Person $person) {
                $this->validate($person);
            });
        });

        // http://stackoverflow.com/questions/11084209
        $grant->setCompanies(clone $companies);
    }

    /**
     * @param Person $person
     */
    public function validate(Person $person)
    {
        $database = $this->loadPepDatabase();
        $rut = explode('-', $person->getRut())[0];

        foreach ($database as $type => $peps) {
            if ($person->isPep()) {
                continue;
            }

            $person->setPep(isset($peps[$rut]) ? 'pep_type_'.$type : null);
        }

        if ($person instanceof Shareholder && $person->isPep()) {
            if ($person->getShare() < self::SHARE_THRESHOLD) {
                $person->setPep(null);
            }
        }
    }

    public function validateRut($rut)
    {
        $database = $this->loadPepDatabase();
        $rut = explode('-', $rut)[0];
        $pepType = [];

        foreach ($database as $type => $peps) {
            if (isset($peps[$rut])) {
                $pepType[] = $type;
            }
        }

        return !empty($pepType) ? $pepType : null;
    }

    /**
     * @return mixed|null
     */
    private function loadPepDatabase()
    {
        if (!$this->pepDatabase) {
            $vcardCache = sprintf('%s/../var/data/peps.php.cache', $this->rootDir);
            $this->pepDatabase = require $vcardCache;
        }

        return $this->pepDatabase;
    }


    /**
     * @return null
     */
    public function getPEPDatabase()
    {
        return $this->loadPepDatabase();
    }
}