<?php

namespace App\Helper;

use App\Entity\Position;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class PositionTitleNormalizer
 * @DI\Service("position_title_normalizer")
 */
class PositionTitleNormalizer
{
    /**
     * @var string
     * @DI\Inject("%kernel.root_dir%")
     */
    public $rootDir;
    
    /**
     * @var array
     */
    protected $normalizedTitles;

    /**
     * @param Position $position
     * @return mixed
     */
    public function normalize(Position $position)
    {
        $this->loadNormalizedTitles();
        
        $original = $position->getTitle();
        $key = strtoupper(str_replace([' ', '-', '.', '_'], '', $original));
        $normalized = $this->normalizedTitles[$key] ?? null;
        
        return empty($normalized) ? $original : $normalized;
    }

    /**
     * @return array|mixed
     */
    private function loadNormalizedTitles()
    {
        if (!$this->normalizedTitles) {
            $this->normalizedTitles = require sprintf('%s/../var/data/positions.php.cache', $this->rootDir);
        }
        
        return $this->normalizedTitles;
    }
}