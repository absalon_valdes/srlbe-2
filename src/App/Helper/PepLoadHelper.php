<?php 

namespace App\Helper;

use PHPExcel_IOFactory;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("pep_load_helper")
 */
class PepLoadHelper
{
    /**
     * @param $filename
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function load($filename)
    {
        $type = PHPExcel_IOFactory::identify($filename);
        $reader = PHPExcel_IOFactory::createReader($type);
        $reader->setReadDataOnly(true);

        $excel = $reader->load($filename);
        $worksheet = $excel->getSheet();
        $toArray = $worksheet ? $worksheet->toArray() : [];

        if (!count($toArray)) {
            return [];
        }

        array_shift($toArray);
        array_shift($toArray);
        array_shift($toArray);

        $row = $toArray[0];
        $company = [];
        $company['name'] = $row[0];
        $company['rut']  = $row[1];
        $company['representatives'] = $this->parseRepresentatives($row);
        $company['shareholders'] = $this->parseShareholders($row);

        return $company;
    }

    /**
     * @param $row
     * @return array
     */
    public function parseRepresentatives($row)
    {
        $representatives = [];

        foreach (array_chunk(array_slice($row, 2, 6), 2) as $repr) {
            if (empty($repr[0]) || empty($repr[1])) {
                continue;
            }

            $representatives[] = [
                'name' => $repr[0],
                'rut'  => $repr[1],
            ];
        }   

        return $representatives;
    }

    /**
     * @param $row
     * @return array
     */
    public function parseShareholders($row)
    {
        $shareholders = [];

        foreach (array_chunk(array_slice($row, 8, 30), 3) as $share) {
            $percent = (int) $share[2];

            if (!is_numeric($percent) || empty($share[0]) || empty($share[1])) {
                continue;
            }

            $shareholders[] = [
                'name'  => $share[0],
                'rut'   => $share[1],
                'share' => is_numeric($percent) ? $percent : 1,
            ];
        }   

        return $shareholders;
    }
}