<?php

namespace App\Helper;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class ReportsRepository
 * @package App\Helper
 *
 * @DI\Service("reports_repository")
 */
class ReportsRepository
{
    /**
     * @var string
     * @DI\Inject("%kernel.var_dir%")
     */
    public $varDir;

    /**
     * @return array
     */
    public function findAll() : array
    {
        $files = [];

        foreach (glob($this->varDir.'/reports/*.*') as $report) {
            $timestamp = \DateTime::createFromFormat(
                'YmdHis', explode('-', basename($report))[3]
            )->getTimestamp();

            $files[$timestamp] = basename($report);
        }

        return $files;
    }
    /**
     * @param string $type
     * @return array
     */
    public function findByType($type) : array
    {
        $files = [];

        foreach (glob($this->varDir.'/reports/multiple/*.*') as $report) {
            if (strpos( basename($report), $type) !== false) {
                $timestamp = \DateTime::createFromFormat(
                    'YmdHis', explode('-', basename($report))[3]
                )->getTimestamp();

                $files[$timestamp] = basename($report);
            }
        }

        return $files;
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function find($filename,$type = null)
    {
        $path = !$type ? '/reports/' : '/reports/multiple/';
        if (!file_exists($path = $this->varDir.$path.$filename)) {
            return null;
        }

        return $path;
    }

}