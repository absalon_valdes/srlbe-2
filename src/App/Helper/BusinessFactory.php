<?php

namespace App\Helper;

use Business\Business;
use Business\Day;
use Business\Days;
use JMS\DiExtraBundle\Annotation as Di;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;

/**
 * Class BusinessFactory
 * @package App\Helper
 * 
 * @Di\Service("business_factory")
 */
class BusinessFactory
{
    /**
     * @var array
     * @Di\Inject("%app.holidays%")
     */
    public $holidays;
    
    private $transformer;

    public function __construct()
    {
        $this->transformer = new ArrayTransformer;
    }
    
    public function getBusiness()
    {
        $monToThu = [['8:50', '18:10']];

        $days = [
            new Day(Days::MONDAY, $monToThu),
            new Day(Days::TUESDAY, $monToThu),
            new Day(Days::WEDNESDAY, $monToThu),
            new Day(Days::THURSDAY, $monToThu),
            new Day(Days::FRIDAY, [['8:50', '16:30']])
        ];
        
        return new Business($days, $this->buildHolidays());
    }

    private function buildHolidays()
    {
        $holidays = [];
        
        foreach ($this->holidays as $holiday) {
            $datetime = \DateTime::createFromFormat('d/m', $holiday);
            $rule = new Rule('FREQ=YEARLY;COUNT=2', $datetime);

            foreach ($this->transformer->transform($rule) as $recurr) {
                $holidays[] = $recurr->getStart();
            }
        }
        
        return $holidays;
    }
}