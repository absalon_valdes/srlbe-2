<?php

namespace App\Helper;

use App\Entity\Office;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class SpecialOfficeHelper
 * @package App\Helper
 *
 * @DI\Service("special_office_helper")
 */
class SpecialOfficeHelper
{
    /**
     * @var UserHelper
     * @DI\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var array
     * @DI\Inject("%app.special_offices%")
     */
    public $config;

    /**
     * @return mixed
     */
    public function checkSpecialOffice()
    {
        $user = $this->userHelper->getCurrentUser();
        $currentOffice = null;

        if($user->getEmployee()){
            /** @var Office */
            $currentOffice = $user->getEmployee()->getOffice() ?? null;

            if(!$currentOffice){
                return;
            }
        }else{
            return $currentOffice;
        }

        if($currentOffice->getSpecialType()){
            return $currentOffice->getSpecialType();
        }

    }

    /**
     * @param string config
     * @return string|array|null
     */
    public function getConfig($config = null){
        $hasConfig = $this->checkSpecialOffice();

        if(!$config && $hasConfig){
            return $this->config[$hasConfig];
        }

        if($config && $hasConfig){
            return $this->config[$hasConfig][$config];
        }

        return null;
    }

    /**
     * @return array
     */
    public function getSpecialTypes($asKeyPairArray = false){
        $types = array_keys($this->config);
        if($asKeyPairArray){
            $result = ["none" => null];
            foreach ($types as $type){
                $result[$type] = $type;
            }
            $types = $result;
        }
        return $types;
    }
}