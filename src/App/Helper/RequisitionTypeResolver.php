<?php

namespace App\Helper;

use App\Entity\Category;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Repository\CategoryRepository;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequisitionTypeResolver
 * @package App\Helper
 * @Di\Service("requisition_type_resolver")
 */
class RequisitionTypeResolver
{
    /**
     * @var Category
     */
    protected $products;
    
    /**
     * @var Category
     */
    protected $maintenance;

    /**
     * @var Category
     */
    protected $repair;

    /**
     * @var Category
     */
    protected $uniform;

    /**
     * Products constructor.
     * @Di\InjectParams({
     *     "categoryRepo": @Di\Inject("category_repo")
     * })
     * @param CategoryRepository $categoryRepo
     */
    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->products    = $categoryRepo->findOneByName('Productos');
        $this->repair      = $categoryRepo->findOneByName('Reparaciones');
        $this->maintenance = $categoryRepo->findOneByName('Mantenciones');
        $this->uniform     = $categoryRepo->findOneByName('Uniformes');
    }

    /**
     * @param Product|Category $entity
     * @return string
     */
    public function getType($entity)
    {

        if ($entity instanceof Product) {
            if ($entity->belongsToCategory('Mantenciones')) {
                return Requisition::TYPE_MAINTENANCE;
            }
            
            if ($entity->belongsToCategory('Reparaciones') || $entity->belongsToCategory('reparaciones')) {
                return Requisition::TYPE_SELF_MANAGEMENT;
            }

            if ($entity->belongsToCategory('Uniformes')) {
                return Requisition::TYPE_UNIFORM;
            }
            if ($entity->belongsToCategory('Tarjetas de Presentación')) {
                return Requisition::TYPE_VCARD;
            }

            if ($entity->belongsToCategory('Productos')) {
                return Requisition::TYPE_PRODUCT;
            }
            
            /** @var Category $category */
            $entity = $entity->getCategory();
        }
        
        if (!$entity) {
            return Requisition::TYPE_PRODUCT;
        }
        
        if ($this->maintenance instanceof Category && $entity->isChildOf($this->maintenance)) {
            return Requisition::TYPE_MAINTENANCE;
        }

        if ($this->repair instanceof Category && $entity->isChildOf($this->repair)) {
            return Requisition::TYPE_SELF_MANAGEMENT;
        }

        if ($this->uniform instanceof Category && $entity->isChildOf($this->uniform)) {
            return Requisition::TYPE_UNIFORM;
        }
        
        return Requisition::TYPE_PRODUCT;
    }
}