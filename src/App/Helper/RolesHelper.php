<?php

namespace App\Helper;

use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("app_roles_helper")
 */
class RolesHelper
{
    /**
     * @Di\Inject("%security.role_hierarchy.roles%")
     */
    public $rolesHierarchy;

    /**
     * @Di\Inject("translator")
     */
    public $translator;

    public function getRoles()
    {
        $roles = [];
        $rawRoles = array_keys($this->rolesHierarchy);

        foreach ($rawRoles as $roleKey) {
            $roles[$this->translator->trans($roleKey)] = $roleKey;
        }

        return $roles;
    }
}
