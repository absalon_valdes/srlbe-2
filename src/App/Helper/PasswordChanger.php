<?php 

namespace App\Helper;

use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("password_changer")
 */
class PasswordChanger
{
    /**
     * @Di\Inject("fos_user.user_manager")
     */
    public $userManager;

    /**
     * @Di\Inject("security.password_encoder")
     */
    public $encoder;

    /**
     * @param $newPassword
     */
    public function changePassword($newPassword)
    {
        $user = $this->userHelper->getCurrentUser();

        $newPassword = $this->encoder->encodePassword($user, $newPassword);
        $user->setPassword($newPassword);
        $user->setSecured(true);

        $this->userManager->updateUser($user);
    }
}