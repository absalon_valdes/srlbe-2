<?php
namespace App\Helper;

use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("build_array_by_constants")
 */
class BuildArrayByConstants {

    /**
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * Constructs an array with constants of a class with a common needle.
     * @param string|object $class The class where the constants are declared.
     * @param $postfix
     * @param string $needle
     * @return array An associative array of the constants, or an empty array if none were found.
     * @throws \ReflectionException
     */
    public function getConstantsArray($class, $postfix = '', $needle = null)
    {
        $constants = [];
        $reflect = new \ReflectionClass($class);
        /** @var \Symfony\Component\Translation\DataCollectorTranslator $translator */
        $translator = $this->translator;

        foreach ($reflect->getConstants() as $name => $value) {
            $add = '';

            if(is_array($value) || strpos($value,'product') === false ){
                continue;
            }
            if(strpos($value,'hold') !== false){
                $add = ' (En evaluación)';
            }
            if (is_int(stripos($name, $needle))) {
                $constants[$this->translator->trans($value.$postfix).$add] = $value;
                continue;
            }

            if (!$needle) {
                $constants[$this->translator->trans($value.$postfix).$add] = $value;
            }
        }

        return $constants;
    }

}
