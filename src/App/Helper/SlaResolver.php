<?php

namespace App\Helper;

use App\Entity\Contract;
use App\Entity\Requisition;
use App\Entity\SLA;
use App\Model\Transition;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;
use Lexik\Bundle\WorkflowBundle\Entity\ModelState;

/**
 * Class SlaResolver
 * @Di\Service("sla_resolver")
 */
class SlaResolver
{
    /**
     * @var SlaCalculator
     * @Di\Inject("sla_calculator")
     */
    public $slaCalculator;

    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    public function getTotalSla(Requisition $requisition, Contract $contract)
    {
        $slas = $requisition->getSlas();
        
        if ($requisition->isCentralized()) {
            return $this->slaCalculator->getEstimate(null, 
                $slas['product.created'],
                $slas['product.ordered'],
                $slas['product.approved'],
                $slas['product.delivered']
            );
        } else {
            return $this->slaCalculator->getEstimate(null, 
                $slas['product.approved'],
                $slas['product.delivered']
            );
        }
    }

    /**
     * @param Requisition $requisition
     * @param null $recalculateFor
     * @return \stdClass
     */
    public function getSLA(Requisition $requisition, $recalculateFor = null)
    {
        $data = new \stdClass;
        $data->requisition = $requisition;
        $data->metadata = $requisition->getMetadata();
        $data->type = $requisition->getType();
        $data->product = $requisition->getProduct();
        $data->contract = $requisition->getContract();
        $data->slas = $requisition->getSlas();
        $data->timestamps = $requisition->getMetadata('timestamps');
        
        if (!$requisition->isType(Requisition::TYPE_VCARD)) {
            $data->current = $this->workflow->getCurrentState($requisition);
            $data->previous = $data->current->getPrevious();
            $data->next = $data->current->getNext();
            $data->history = $this->workflow->getHistory($requisition);
        } else {
            $data->activities = $requisition->getActivities();
        }

        $data->state = $requisition->getStatus();
        $data->centralized = $requisition->isCentralized();
        
        $rules = Requisition::TYPE_VCARD === $data->type ? $this->forVcards() : $this->forAll();
        
        $evaluated = array_map(function ($ts) use ($data) {
            return array_map(function ($callback) use ($data) {
                return $callback ? $callback($data) : null;
            }, $ts);
        }, $rules[$recalculateFor ?? $data->state] ?? []);
        
        if (isset($evaluated['all'])) {
            foreach (['requester', 'approver', 'supplier'] as $role) {
                $evaluated[$role] = array_merge($evaluated[$role] ?? [], $evaluated['all']);
            }
            
            unset($evaluated['all']);
        }

        foreach ($evaluated as $roles => $evals) {
            if (false === strpos($roles, '|')) {
                continue;
            }
            
            foreach (explode('|', $roles) as $part) {
                $evaluated[$part] = array_merge($evaluated[$part] ?? [], $evals);
            }
            
            unset($evaluated[$roles]);
        }
        
        foreach ($evaluated as $roles => $evals) {
            foreach ($evals as $k => $v) {
                if (null !== $v) {
                    continue;
                }
                
                unset($evaluated[$roles][$k]);
            }
        }
        
        return array_replace_recursive($data->timestamps ?: [], $evaluated);
    }

    private function forVcards()
    {
        return [
            'ejecucion' => [
                'all' => [
                    'started' => function ($data) {
                        return clone $data->requisition->getCreatedAt();
                    },
                    'response' => function ($data) {
                        return clone $this->slaCalculator->getSla(
                            $data->slas['product.delivered']
                        );
                    },
                    'completion' => function ($data) {
                        return clone $this->slaCalculator->getEstimate(null, 
                            $data->slas['product.delivered'],
                            $data->slas['en_despacho']
                        );
                    }
                ]
            ],
            'en_despacho' => [
                'all' => [
                    'response' => function ($data) {
                        return new \DateTime;
                    },
                    'completion' => function ($data) {
                        $started = $data->timestamps['requester']['started'];
                        $expected = $this->slaCalculator->getEstimate($started, $data->slas['product.delivered']);
                        $real = new \DateTime;
                        
                        if ($real > $expected) {
                            return;
                        }
                        
                        return clone $this->slaCalculator->getEstimate($real, $data->slas['en_despacho']);
                    }
                ]
            ],
            'evaluacion' => [
                'all' => [
                    'started' => function ($data) {
                        return clone $data->requisition->getCreatedAt();
                    },
                    'response' => function ($data) {
                        return clone $this->slaCalculator->getSla($data->slas['product.created']);
                    }
                ]
            ],
            'rechazado' => [
                'all' => [
                    'response' => function ($data) {
                        return new \DateTime;
                    }
                ]
            ]
        ];
    }

    private function forAll()
    {
        return [
            'product.created' => [
                'all' => [
                    'started' => function ($data) { 
                        return clone $data->previous->getFinishedAt(); 
                    },
                    'response' => function ($data) {
                        return clone $this->slaCalculator->getSla($data->slas['product.created']);
                    }
                ]
            ],
            'product.ordered' => [
                'all' => [
                    'response' => function () {
                        return new \DateTime;
                    },
                    'completion' => function ($data) {
                        return clone $this->slaCalculator->getEstimate(null, 
                            $data->slas['product.ordered'],
                            $data->slas['product.approved'],
                            $data->slas['product.delivered']
                        );
                    }
                ]
            ],
            'product.approved' => [
                'all' => [
                    'started' => function ($data) {
                        if ('draft' === $data->previous->getStepName()) {
                            return clone $data->previous->getFinishedAt();
                        }
                    },
                    'completion' => function ($data) {
                        if ($data->requisition->isType(Requisition::TYPE_MAINTENANCE) && 
                            $data->previous->getStepName() === 'hold') {
                            
                            return clone $this->slaCalculator->getEstimate(null,
                                $data->slas['product.approved'],
                                $data->slas['product.delivered']
                            );
                        }
                        
                        if ($data->centralized) {
                            return clone $this->slaCalculator->getEstimate(null, 
                                $data->slas['product.approved'],
                                $data->slas['product.delivered']
                            );
                        }
                    },
                    'response' => function ($data) {
                        if ($data->requisition->isType(Requisition::TYPE_MAINTENANCE) && 
                            $data->previous->getStepName() === 'hold') {
                            return new \DateTime;
                        }
                        
                        if (!$data->centralized) {
                            return clone $this->slaCalculator->getEstimate(null, 
                                $data->slas['product.approved']
                            );
                        }
                    }
                ],
            ],
            'product.rejected' => [
                'all' => [
                    'response' => function ($data){
                        return $data->previous->getFinishedAt();
                    }
                ]
            ],
            'product.appeal' => [
                'all' => [
                    'response' => function ($data) {
                        return $this->slaCalculator->getEstimate($data->previous->getFinishedAt(), $data->slas['product.approved']);
                    }
                ]
            ],
            'product.denied' => [
                'all' => [
                    'response' => function ($data){
                        return $data->previous->getFinishedAt();
                    }
                ]
            ],
            'product.accepted' => [
                'all' => [
                    'completion' => function ($data) {
                        $approved = $data->requisition->getTransition('product.approved');
                        $sla = $this->slaCalculator->getSla(
                            $data->slas['product.approved'], 
                            $approved->getDatetime()
                        );
                       
                        if ((new \DateTime) > $sla) {
                            return clone $this->slaCalculator->getEstimate(
                                $approved->getDatetime(),
                                $data->slas['product.approved'],
                                $data->slas['product.delivered']
                            );
                        }
                        
                        return clone $this->slaCalculator->getSla(
                            $data->slas['product.delivered']
                        );
                    },
                    'response' => function ($data) {
                        if (!$data->centralized) {
                            return new \DateTime;
                        }
                    }
                ]
            ],
            'product.confirmed' => [
                'all' => [
                    'response' => function ($data) {
                        return $data->previous->getFinishedAt();
                    },
                    'completion' => function ($data) {
                        return clone $data->previous->getFinishedAt();
                    }
                ]
            ],
            'product.declined' => [
            ],
            'product.hold' => [
                'all' => [
                    'response' => function () {
                        return new \DateTime;
                    }
                ]
            ],
            'product.delivered' => [
            ],
            'product.reviewed' => [
                'all' => [
                    'completion' => function ($data) {
                        return new \DateTime;
                    }
                ]
            ],
            'product.returned' => [
                'all' => [
                    'completion' => function ($data) {
                        $accepted = $data->requisition->getTransition('product.accepted');
                        $acceptedAt = $accepted->getDatetime();
                        $productSla = $data->slas['product.delivered'];
                        
                        return clone $this->slaCalculator->getEstimate($acceptedAt, $productSla);
                    }
                ]
            ],
            'product.evaluated' => [
                'all' => [
                    'completion' => function ($data) {
                        $evaluated = end($data->metadata['evaluation']);
                        $event = $data->centralized ? 'confirmed' : 'reviewed';
                        $tr = $data->requisition->getTransition('product.'.$event);
                        
                        if ($tr && $tr->getDatetime() !== $evaluated['evaluated_at']) {
                            return clone $evaluated['evaluated_at'];
                        }
                    }
                ]
            ],
            'product.closed' => [
                'all' => [
                    'completion' => function ($data) {
                        return new \DateTime;
                    }
                ]
            ],
            'product.completed' => [
            ],
        ];
    }

    function array_map_recursive($callback, $array)
    {
        $func = function ($item) use (&$func, &$callback) {
            return is_array($item) ? array_map($func, $item) : call_user_func($callback, $item);
        };

        return array_map($func, $array);
    }

    /**
     * @param Requisition $requisition
     * @return \DateTime
     */
    public function responseDate(Requisition $requisition) : \DateTime
    {
        if ($requisition->isCentralized()) {
            if ($t = $requisition->getTransition(ProductStatus::REJECTED)) {
                return $t->getDatetime();
            }
            
            if ($t = $requisition->getTransition(ProductStatus::ORDERED)) {
                return $t->getDatetime();
            } else {
                return $this->slaCalculator->getEstimate(
                    $requisition->getCreatedAt(),
                    $requisition->getSLA(ProductStatus::CREATED)
                );
            }
        } else {
            if ($t = $requisition->getTransition(ProductStatus::ACCEPTED)) {
                return $t->getDatetime();
            } else {
                return $this->slaCalculator->getEstimate(
                    $requisition->getCreatedAt(),
                    $requisition->getSLA(ProductStatus::APPROVED)
                );
            }
        }
    }

    /**
     * @param Requisition $requisition
     * @param Contract $contract
     * @param \DateTime $dateTime
     * @return \DateTime|null
     */
    public function resolveSLA(Requisition $requisition, Contract $contract = null, \DateTime $dateTime = null) : \DateTime
    {
        $status = $requisition->getStatus();
        $contract = $contract ?? $requisition->getContract();

        $returnedCallback = function () use ($requisition, $contract) {
            if ($interval = $requisition->getJobCompletionInterval()) {
                $usedSla = $this->slaCalculator->dateIntervalToSla($interval);
                $totalSla = $requisition->getSlaProduct();
                
                if (($diff = $totalSla->getValue() - $usedSla->getValue()) > 0) {
                    return $this->slaCalculator->getEstimate(
                        $requisition->getJobCompletionDatetime(),
                        SLA::create($diff, SLA::BUSINESS_DAY)
                    );
                }
            }
            
            return null;
        };
        
        $finishedCallback = function () use ($requisition) {
            return $requisition->getJobCompletionDatetime();
        };
        
        $rejectedCallback = function () use ($requisition) {
            return $requisition->getClosedAt();
        };
        
        $ordered = [
            ProductStatus::DRAFT => [
                ProductStatus::CREATED,
                ProductStatus::ORDERED,
                ProductStatus::APPROVED,
                ProductStatus::DELIVERED,
            ],
            ProductStatus::CREATED => [
                ProductStatus::CREATED,
                ProductStatus::ORDERED,
                ProductStatus::APPROVED,
                ProductStatus::DELIVERED,
            ],
            ProductStatus::ORDERED => [
                ProductStatus::ORDERED,
                ProductStatus::APPROVED,
                ProductStatus::DELIVERED,
            ],
            ProductStatus::APPROVED => [
                ProductStatus::APPROVED,
                ProductStatus::DELIVERED,
            ],
            ProductStatus::ACCEPTED => [
                //ProductStatus::ACCEPTED, 
                ProductStatus::DELIVERED
            ],
            ProductStatus::DELIVERED => [
                ProductStatus::DELIVERED 
            ],
            ProductStatus::CLOSED => $rejectedCallback,
            ProductStatus::REJECTED => $rejectedCallback,
            ProductStatus::DENIED => $rejectedCallback,
            ProductStatus::RETURNED => $returnedCallback,
            ProductStatus::REVIEWED => $finishedCallback,
            ProductStatus::CONFIRMED => $finishedCallback,
            ProductStatus::EVALUATED => $finishedCallback,
            ProductStatus::COMPLETED => $finishedCallback,
        ];
        
        if (!isset($ordered[$status])) {
            return $requisition->getStateDueDate();
        }
        
        $reference = $ordered[$status];
        $remaining = is_callable($reference) ? $reference() : $reference;
        
        if (is_array($remaining)) {
            $remaining = array_map([$requisition, 'getSla'], $remaining);
        } elseif ($remaining instanceof SLA) {
            $remaining = [$remaining];
        } elseif ($remaining instanceof \DateTime) {
            return $remaining;
        } elseif (is_bool($remaining) || null === $remaining) {
            return new \DateTime; // BC FIX
        }
        
        $statusIndex = array_search($status, array_keys($ordered), true);
        $prevStatus = array_keys($ordered)[$statusIndex > 0 ? $statusIndex - 1 : $statusIndex];
        $prevTrans = $requisition->getTransition($prevStatus);
        $dateTime = $dateTime ?? $prevTrans ? $prevTrans->getDatetime() : new \DateTime;

        array_unshift($remaining, $dateTime);
        $callable = [$this->slaCalculator, 'getEstimate'];

        return call_user_func_array($callable, $remaining);
    }

    /**
     * @param Requisition $requisition
     */
    public function updateSLA(Requisition $requisition)
    {
        $status = $requisition->getStatus();
        
        /** @var SLA $stateSla */
        if (!$stateSla = $requisition->getSLA($status)) {
            return;
        }
        
        /** @var Transition $transition */
        $transition = $requisition->getLastTransition();
        
        /** @var \DateTime $dueDate */
        $dueDate = $this->slaCalculator->getEstimate($transition->getDatetime(), $stateSla);
        
        $requisition->setStateDueDate($dueDate);
    }
}
