<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\User;
use Doctrine\ORM\Query\Expr\Join;

class PepGrantRepository extends Repository
{
    public function findOpen()
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where($qb->expr()->eq('p.requester', ':user'))
            ->orderBy($qb->expr()->desc('p.updatedAt'))
            ->setParameter('user', $this->get('user_helper')->getCurrentUser());

        return $qb->getQuery()->getResult();
    }

    public function searchPepGrant($filter)
    {
        //Useless
        unset($filter['userType']);
        if (count(array_filter($filter)) <= 2 &&
            $filter['region'] === 'region.all' &&
            $filter['city'] === 'cities.all'
        ) {
            $pepGrant['error'] = 'Debe llenar al menos un filtro, o seleccionar un estado diferente a todos';

            return $pepGrant;
        }

        $qb = $this->createQueryBuilder('r');
        $qb ->leftJoin(User::class, 'u', Join::WITH, $qb->expr()->eq('r.requester', 'u'))
            ->leftJoin(Employee::class, 'e', Join::WITH, $qb->expr()->eq('u.employee', 'e'))
            ->leftJoin(Office::class, 'o', Join::WITH, $qb->expr()->eq('e.office', 'o'))
            ->leftJoin(Category::class, 'city', Join::WITH, $qb->expr()->eq('o.city', 'city'))
            ->leftJoin(Category::class, 'region', Join::WITH, $qb->expr()->eq('region', 'city.parent'));

        //Review the filters
        if ($filter['office']) {
            $qb ->andWhere($qb->expr()->like('o.name', ':requesterOffice'))
                ->setParameter('requesterOffice', '%'.$filter['office'].'%');
        }

        if ($filter['region'] !== 'region.all') {
            $qb->andWhere($qb->expr()->eq('region', ':selectedRegion'))
                ->setParameter('selectedRegion', $filter['region']);
        }

        if ($filter['city'] !== 'cities.all') {
            $qb->andWhere($qb->expr()->eq('city', ':selectedCity'))
                ->setParameter('selectedCity', $filter['city']);
        }

        if ($filter['createdAt']) {
            $qb ->andWhere($qb ->expr()->eq('DATE(r.createdAt)', ':createdAt'))
                ->setParameter('createdAt', $filter['createdAt']->format('Y/m/d'));
        }

        if ($filter['userName']) {
            $qb ->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like($qb->expr()->concat('e.name', 'e.lastName'), ':requesterName'),
                    $qb->expr()->like('u.username', ':requesterName')
                )
            )
                ->setParameter('requesterName', '%'.$filter['userName'].'%');
        }

        return $qb->getQuery()->getResult();
    }
}
