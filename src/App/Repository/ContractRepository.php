<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Office;
use App\Entity\User;
use App\Entity\Employee;
use Doctrine\ORM\Query\Expr\Join;

/**
 * Class ContractRepository
 * @package App\Repository
 */
class ContractRepository extends Repository
{
    /**
     * @param Product $product
     * @param User|null $user
     * @return array
     */
    public function findByProduct(Product $product, User $user = null)
    {
        $user = $user ?? $this->get('user_helper')->getCurrentUser();
        $contracts =  $this
            ->queryByProduct($product, $user)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->getResult()
            ;
        //Orders results alphabetically by suppliers names
        usort($contracts, function($a, $b){
            return strcmp($a->getSupplier()->getName(), $b->getSupplier()->getName());
        });

        return $contracts;
    }

    /**
     * @param Product $product
     * @param User $user
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function queryByProduct(Product $product, User $user)
    {
        $qb = $this->createQueryBuilder('c');
        $qb
            ->join(Office::class, 'o', Join::WITH, $qb->expr()->eq('c.office', 'o'))
            ->join(Employee::class, 'e', Join::WITH, $qb->expr()->eq('e.office', 'o'))
            ->where($qb->expr()->andX(
                $qb->expr()->eq('e.user', ':user'),
                $qb->expr()->eq('c.product', ':product'),
                $qb->expr()->eq('c.expired', 0)
            ))
            ->setParameter('user', $user)
            ->setParameter('product', $product)
        ;

        return $qb;
    }

    /**
     * @param array $offices
     * @return array
     */
    public function findOffices(array $offices)
    {

        $qb = $this->createQueryBuilder('c');

        $qb->join('c.office', 'o');
        $qb->join('c.product', 'p');
        $qb->where($qb->expr()->andX(
                $qb->expr()->in('c.office', $offices),
                $qb->expr()->isNull('p.deletedAt'),
                $qb->expr()->eq('c.expired', 0)))
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $suppliers
     * @return array
     */
    public function findSuppliers(array $suppliers)
    {

        $qb = $this->createQueryBuilder('c');

        $qb->join('c.supplier', 'o');
        $qb->where($qb->expr()->andX(
            $qb->expr()->in('c.supplier', $suppliers),
            $qb->expr()->eq('c.expired', 0)))
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function lastContract()
    {

        $qb = $this->createQueryBuilder('c');
        $qb
            ->andWhere($qb->expr()->andX(
                $qb->expr()->eq('c.expired', 1)
            ));

        return $qb->getQuery()
            ->setMaxResults(1)
            ->getSingleResult();
    }
}
