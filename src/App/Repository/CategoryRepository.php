<?php

namespace App\Repository;

use App\Entity\Category;
use Behat\Transliterator\Transliterator;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use PhpOption\Option;

class CategoryRepository extends NestedTreeRepository
{
    public function findOneByNameAndParent($name, Category $parent = null)
    {
        $category = parent::findOneBy([
            'name' => $name,
            'parent' => $parent,     
        ]);
        
        return Option::fromValue($category);
    }

    public function findByParentAsArray(Category $parent)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->select("c.id, c.name as text");
        $qb->where('c.parent = :parent');
        $qb->setParameter('parent', $parent);
        return $qb->getQuery()->getArrayResult();
    }

    public function findAllContainingInSlug($search,$searchText = false)
    {
        $qb = $this->createQueryBuilder('c');
        $qb->where($qb->expr()->like('c.slug', ':slug'));
        if($searchText){
            $qb->andWhere($qb->expr()->like('c.name', ':name'));
            $qb->setParameter('name', '%'.$searchText.'%');
        }
        $qb->setParameter('slug', '%'.$search.'%');
        return $qb->getQuery()->getResult();
    }

    public function getOrCreateFromSlug($categorySlug)
    {
        $slugify = function ($i) {
            return implode('/', array_map([Transliterator::class, 'urlize'], $i));
        };
        
        $parts = array_map('trim', explode('/', $categorySlug));
        
        /** @var Category $category */
        if ($category = $this->findOneBySlug($slugify($parts))) {
            return $category;
        }
        
        $slugTree = array_map('trim', explode('/', $categorySlug));
        $prevCategory = null;

        foreach ($slugTree as $i => $value) {
            $ct = $slugify(array_slice($slugTree, 0, $i + 1));

            /** @var Category $curCat */
            $curCat = $this->findOneBySlug($ct) ?? new Category;
            
            $curCat->setName($parts[$i]);
            $curCat->setParent($prevCategory);
            
            if (!$curCat->getId()) {
                $this->_em->persist($curCat);
            }
            
            $prevCategory = $curCat;
        }
        
        $this->_em->flush();
        
        return $prevCategory;    
    }
}
