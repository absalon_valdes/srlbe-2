<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\ORM\Query\Expr\Join;

class EmployeeRepository extends Repository
{
    public function removeInactive(array $ruts)
    {
        $qb = $this->createQueryBuilder('e');
        $qb
            ->delete($this->getClassName(), 'e')
            ->where($qb->expr()->notIn('e.rut', ':ruts'))
            ->setParameter('ruts', $ruts);

        return $qb->getQuery()->getResult();
    }

    public function findForVcard($rut = null)
    {
        $qb = $this->createQueryBuilder('e');

        if (!$rut) {
            $qb
                ->join(User::class, 'u', Join::WITH, $qb->expr()->eq('u', 'e.user'))
                ->where($qb->expr()->eq('u', ':user'))
                ->setParameter('user', $this->get('user_helper')->getCurrentUser());
        } else {
            $qb
                ->where($qb->expr()->eq('e.rut', ':rut'))
                ->setParameter('rut', $rut);
        }

        return $qb->getQuery()->getSingleResult();
    }
}
