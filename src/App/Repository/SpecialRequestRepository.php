<?php

namespace App\Repository;

use App\Entity\SpecialRequest;
use App\Entity\User;
use Doctrine\DBAL\LockMode;

class SpecialRequestRepository extends Repository
{
    public function findUserCart(User $user)
    {
        return $this->findBy([
            'state' => SpecialRequest::STATE_CART,
            'requester' => $user->getId()
        ]);
    }

    public function findByAssigneeAndState(User $assignee, array $states = array())
    {
        return $this->createQueryBuilder('p')
            ->where('p.state in (:states)')
            ->andWhere('p.assignee = :assignee')
            ->setParameters([
                'states' => $states,
                'assignee' => $assignee
            ])
            ->getQuery()
            ->getResult()
        ;
    }


    public function setNextNumber( $request) {
        $em = $this->getEntityManager();
        $em->getConnection()->beginTransaction();
        try {
            $request->setNumber($this->getNextNumber());
            $em->flush();
            $em->getConnection()->commit();
        }
        catch(\Exception $e) {
            $em->getConnection()->rollBack();
            throw $e;
        }
    }

    public function getNextNumber()
    {
        $allowedClasses = [
            SpecialRequest::class
        ];
        $numbers = [];

        foreach ($allowedClasses as $cls) {
            $qb = $this->getEntityManager()->getRepository($cls)
                ->createQueryBuilder('o');

            $qb->select('o.number');

            $qb->orderBy('o.number', 'DESC');

            $result = $qb->getQuery()
                ->setLockMode(LockMode::PESSIMISTIC_READ)
                ->setMaxResults(1)
                ->getOneOrNullResult();

            $number = $result['number'];
            $numbers[] = is_numeric($number) ? $number : 10000;
        }

        return max($numbers) +1;
    }

}