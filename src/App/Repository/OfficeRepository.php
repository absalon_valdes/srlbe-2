<?php

namespace App\Repository;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\User;
use Doctrine\ORM\Query\Expr\Join;

class OfficeRepository extends Repository
{
    public function findAlternateRequester(Office $office, User $requester)
    {
        // TODO: fix
        $username = $requester->getUsername();

        $qb = $this->_em->createQueryBuilder();
        $qb->select('u');
        $qb->from(User::class, 'u');
        $qb->leftJoin(Employee::class, 'e', Join::WITH, 'e.user = u');
        $qb->leftJoin(Office::class, 'o', Join::WITH, 'e.office = o');
        $qb->where($qb->expr()->andX(
            $qb->expr()->eq('o', ':office'),
            $qb->expr()->neq('u', ':requester')
        ));
        $qb->setMaxResults(1);
        $qb->setParameter('office', $office);
        $qb->setParameter('requester', $requester);
        
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findAllOffices()
    {
        $qb = $this->createQueryBuilder('o');
        
        $query = $qb->getQuery();
        $query->useQueryCache(true);
        $query->useResultCache(true);
        
        return $query->getArrayResult();
    }
}
