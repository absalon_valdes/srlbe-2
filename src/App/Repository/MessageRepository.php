<?php

namespace App\Repository;

use App\Entity\Message;

class MessageRepository extends Repository
{
    public function getUnreadCount()
    {
        $qb = $this->createQuery();
        $qb->select($qb->expr()->count('o.id'));

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getUnread()
    {
        return $this->createQuery()->getQuery()->getArrayResult();
    }

    private function createQuery()
    {
        $qb = $this->createQueryBuilder('o');
        $qb
            ->where($qb->expr()->andX(
                $qb->expr()->eq('o.viewed', 'false'),
                $qb->expr()->eq('o.receiver', ':user')
            ))
            ->setParameter(':user', $this->get('user_helper')->getCurrentUser())
        ;

        return $qb;
    }

    public function readMessage(Message $message)
    {
        $message->setViewed(true);

        $this->persist($message, true);
    }
}
