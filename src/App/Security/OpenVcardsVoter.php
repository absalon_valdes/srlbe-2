<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\RequisitionRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class OpenVcardsVoter
 * @DI\Service()
 * @DI\Tag("security.voter")
 */
class OpenVcardsVoter extends Voter
{
    const REQUEST_VCARD = 'request_vcard';
    
    /**
     * @var RequisitionRepository
     * @DI\Inject("requisition_repo")
     */
    public $requisitionRepo;
    
    protected function supports($attribute, $subject)
    {
        return self::REQUEST_VCARD === $attribute;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        if (!$userEmployee = $user->getEmployee()) {
            return false;
        }

        $activeReqs = $this->requisitionRepo->findCountActiveVcards($user);

        if ($userEmployee === $subject && count($activeReqs)) {
            return false;
        }

        return true;
    }
}