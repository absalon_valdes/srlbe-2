<?php

namespace App\Security;

use App\Entity\Contract;
use App\Entity\Requisition;
use App\Entity\User;
use App\Entity\Supplier;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

/**
 * Class RequisitionVoter
 * @package App\Security
 * @Di\Service("requisition_voter")
 * @Di\Tag("security.voter")
 */
class RequisitionVoter extends Voter
{
    const EDIT    = 'requisition_edit';
    const VIEW    = 'requisition_view';
    const PROCEED = 'requisition_proceed';

    /**
     * @var AccessDecisionManagerInterface
     * @Di\Inject("security.access.decision_manager")
     */
    public $decisionManager;

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        return $subject instanceof Requisition && in_array($attribute, [self::EDIT, self::VIEW, self::PROCEED], true);
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $requisition, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }
        
        if ($this->decisionManager->decide($token, ['ROLE_ADMIN'])) {
            return true;
        }

        if (self::EDIT === $attribute) {
            return $this->canEdit($requisition, $user);
        }

        if (self::VIEW === $attribute) {
            return $this->canView($requisition, $user);
        }

        if (self::PROCEED === $attribute) {
            return $this->canProceed($requisition, $user);
        }

        throw new \LogicException();
    }

    /**
     * @param Requisition $requisition
     * @param User $user
     * @return bool
     */
    private function canView(Requisition $requisition, User $user)
    {
        $contract  = $requisition->getContract();
        $supplier  = $contract->getSupplier()->getRepresentatives()->get(0);
        $approver  = $requisition->getProduct()->getApprover();
        $requester = $requisition->getRequester();
        $assignee  = $requisition->getAssignee();

        return in_array($user, [$approver, $supplier, $requester, $assignee], true);
    }

    /**
     * @param Requisition $requisition
     * @param User $user
     * @return bool
     */
    private function canEdit(Requisition $requisition, User $user)
    {
        return true;
    }

    /**
     * @param Requisition $requisition
     * @param User $user
     * @return bool
     */
    private function canProceed(Requisition $requisition, User $user)
    {
        if ($user->hasRole('ROLE_SUPPLIER')) {
            return $this->supplierCanProceed($requisition, $user);
        }

        if ($user->hasRole('ROLE_REQUESTER')) {
            return $this->requesterCanProceed($requisition, $user);
        }

        if ($user->hasRole('ROLE_APPROVER')) {
            return $this->approverCanProceed($requisition, $user);
        }

        return false;
    }

    private function approverCanProceed(Requisition $requisition, User $user)
    {

    }

    private function requesterCanProceed(Requisition $requisition, User $user)
    {

    }

    private function supplierCanProceed(Requisition $requisition, User $user)
    {
        /** @var Contract $contract */
        $contract = $requisition->getContract();

        /** @var Supplier $supplier */
        $supplier = $contract->getSupplier();
        
        return !$supplier->getRepresentatives()->contains($user);
    }
}