<?php

namespace App\Security;

use App\Helper\UserHelper;
use JMS\DiExtraBundle\Annotation as Di;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * @package App\Security
 * @Di\Service("switch_user_register")
 */

class SwitchUserLogger
{
    /**
     * @Di\Inject("switch_user_logger")
     */
    public $logger;

    /**
     * @Di\Inject("security.token_storage")
     * @var TokenStorage $securityContext
     */
    public $securityContext;

    /**
     * @Di\Inject("user_helper")
     * @var UserHelper $userHelper
     */
    public $userHelper;

    /**
     * @Di\Observe("security.impersonate.start")
     * @Di\Observe("security.impersonate.end")
     * @param GenericEvent $event
     * @param $eventName
     */
    public function onSwitchUser(GenericEvent $event, $eventName)
    {
        $previousUser = $event->getSubject()['previous_user'];
        $nextUser = $event->getSubject()['next_user'];

        $switch = $eventName === 'security.impersonate.end' ? 'Fin' : 'Inicio';

        $this->logger->info(sprintf(
            '%s  - %s Usuario: %s, loggeado como %s',
            $switch,
            (new \DateTime)->format('d/m/Y H:i:s'),
            $previousUser->getUsername(),
            $nextUser->getUsername()
        ));
    }
}
