<?php

namespace App\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RegisterNotifierCompilerPass
 * @package App\DependencyInjection\Compiler
 */
class RegisterNotifierCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('notification_manager')) {
            return;
        }

        $manager = $container->getDefinition('notification_manager');

        foreach ($container->findTaggedServiceIds('notifier') as $id => $tags) {
            $manager->addMethodCall('addNotifier', array(new Reference($id)));
        }
    }
}
