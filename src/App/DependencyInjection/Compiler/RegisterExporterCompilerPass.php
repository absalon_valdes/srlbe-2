<?php

namespace App\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class RegisterExporterCompilerPass
 * @package App\DependencyInjection\Compiler
 */
class RegisterExporterCompilerPass implements CompilerPassInterface
{
    /**
     * @inheritdoc
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('exporter_aggregator')) {
            return;
        }
        
        /** @var Definition $aggregator */
        $aggregator = $container->getDefinition('exporter_aggregator');

        foreach ($container->findTaggedServiceIds('exporter') as $id => $tags) {
            foreach ($tags as $attributes) {
                $aggregator->addMethodCall('registerExporter', [$attributes['alias'], new Reference($id)]);
            }
        }
    }
}