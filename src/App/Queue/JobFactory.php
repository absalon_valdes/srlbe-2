<?php

namespace App\Queue;

use App\Helper\UserHelper;
use JMS\DiExtraBundle\Annotation as DI;
use Ramsey\Uuid\Uuid;

/**
 * Class JobFactory
 * @package App\Queue
 * 
 * @DI\Service("job_factory")
 */
class JobFactory
{
    /**
     * @var UserHelper
     * @DI\Inject("user_helper")
     */
    public $userHelper;
    
    public function createJob($queue, array $payload = [])
    {
        $job = new Job(Uuid::uuid4()->toString(), $queue, $payload);
        
        
        //$job->setCreatedBy($this->userHelper->getCurrentUser());
        
        return $job;
    }    
}