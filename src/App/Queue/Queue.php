<?php

namespace App\Queue;

use App\Entity\Job;
use App\Repository\JobRepository;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Queue
 * @DI\Service("queue")
 */
class Queue
{
    /**
     * @var JobRepository
     */
    public $repository;

    /**
     * @var Worker[]
     */
    public $workers;

    /**
     * Queue constructor.
     *
     * @DI\InjectParams({
     *     "repository"=@DI\Inject("job_repo"),
     *     "container"=@DI\Inject("service_container"),
     *     "config"=@DI\Inject("%workers%")
     * })
     *
     * @param JobRepository $repository
     * @param ContainerInterface $container
     * @param array $config
     */
    public function __construct(
        JobRepository $repository,
        ContainerInterface $container,
        array $config
    ) {
        $this->repository = $repository;

        foreach ($config as $item) {
            $this->workers[$item['id']] = $container->get($item['service']);
        }
    }

    /**
     * @return array
     */
    public function getWorkers()
    {
        return $this->workers;
    }

    /**
     * @param Job $job
     * @return Job
     */
    public function enqueue(Job $job)
    {
        $this->repository->save($job);

        return $job;
    }

    /**
     * @param Job $job
     * @return Job
     */
    public function retry(Job $job)
    {
        $this->repository->save($job->retry());

        return $job;
    }

    /**
     * @param Job $job
     * @return Job
     */
    public function stop(Job $job)
    {
        $this->repository->save($job->stop());

        return $job;
    }

    /**
     * @param Job $job
     * @return Job
     */
    public function cancel(Job $job)
    {
        $this->repository->save($job->cancel());

        return $job;
    }

    /**
     * Process queue
     */
    public function run()
    {
        $bool = true;
        while ($bool) {
            if(!$this->checkExistsByState(Job::RUNNING)) {
                while ($job = $this->repository->findNext()) {
                    $this->workers[$job->getQueue()]->execute($job);
                }
                sleep(10);
                $bool = $this->checkExistsByState(Job::QUEUED);
            }
            else {
                sleep(10);
            }
        }

        return false;
    }

    /**
     * @param $state
     * @return bool
     */
    public function checkExistsByState($state)
    {
        return $this->repository->findByState($state) ? true : false;
    }
}