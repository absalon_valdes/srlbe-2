<?php

namespace App\Queue\Workers;

use App\Entity\Contract;
use App\Entity\DateRange;
use App\Entity\Job;
use App\Entity\Money;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\SLA;
use App\Entity\Supplier;
use App\Helper\ExcelBatchProcessor;
use App\Model\JobReport;
use App\Repository\ProductRepository;
use App\Util\RutUtils;
use App\Workflow\Status\ProductStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Psr\Log\LoggerInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class ContractImporterWorker
 *
 * @DI\Service()
 */
class ContractImporterWorker extends AbstractWorker
{
    /**
     * @var LoggerInterface $logger
     * @Di\Inject("logger")
     */
    public $logger;

    /**
     * @var \Redis
     * @DI\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var array
     */
    protected $products;

    /**
     * @var array
     */
    protected $offices;

    /**
     * @var array
     */
    protected $suppliers;

    /**
     * @var array
     */
    private $pCentralized;

    /**
     * @var Connection
     */
    public $pdo;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var Job
     */
    protected $job;

    const EXCEL_DATE_FORMAT = 'Y-m-d';

    const HEADERS = [
        0=>'CODIGO TECNICO',
        1=>'RUT PROVEEDOR',
        2=>'NUMERO DE CONTRATO',
        3=>'FECHA INICIO',
        4=>'FECHA TERMINO',
        5=>'DOCUMENTACION COMPRA',
        6=>'CODIGO SUCURSAL',
        7=>'CENTRO DE COSTO',
        8=>'TIPO DE MONEDA',
        9=>'VALOR',
        10=>'SLA EVALUADOR',
        11=>'SLA OC',
        12=>'SLA PROVEEDOR',
        13=>'SLA PRODUCTO',
        14=>'SLA DESPACHO'
    ];

    /**
     * ContractImporterWorker constructor.
     *
     * @param EntityManager $em
     * @param $varDir
     * @param ValidatorInterface $validator
     * @DI\InjectParams({
     *     "em"=@DI\Inject("doctrine.orm.entity_manager"),
     *     "varDir"=@DI\Inject("%kernel.var_dir%"),
     *     "validator"=@DI\Inject("validator")
     * })
     */
    public function __construct(EntityManager $em, $varDir, ValidatorInterface $validator)
    {
        parent::__construct($em, $varDir);
        $this->validator = $validator;
        $this->pdo = $em->getConnection();
        $this->pdo->setFetchMode(\PDO::FETCH_KEY_PAIR);

        $this->products = $this->pdo->fetchAll('select code, id from products');
        $this->offices = $this->pdo->fetchAll('select code, id from offices');
        $this->suppliers = $this->pdo->fetchAll('select rut, id from suppliers');
        $this->pCentralized = $this->pdo->fetchAll('select code, centralized from products');
    }

    /**
     * @inheritDoc
     */
    public function process(Job $job)
    {
        $this->job = $job;
        $logfile = sprintf('%s/%s.log', $this->tempDir, $job->getId());
        $tempCSV = sprintf('%s/%s.csv', $this->tempDir, $job->getId());

        $cursor = 1;
        $retry = false;
        if($job->isRetried()) {
            $this->em->flush($job->setState(Job::RUNNING));
            $cursor = $job->getProcessed();
            $retry = true;
        }

        if(!$retry) {
            ExcelBatchProcessor::excelToCsv($job->getPayload('file'), $tempCSV);
            $total = (int)exec(sprintf('wc -l < "%s"', $tempCSV));
            $this->setSize($job, $total);
            $this->report($job, $logfile);
        }

        if (!file_exists($tempCSV) && !filesize($tempCSV)) {
            throw new \RuntimeException(sprintf('Cannot create file %s', $tempCSV));
        }

        try{
            $this->pdo->beginTransaction();
            $this->pdo->setAutoCommit(false);

            $logBatch = '';
            if ($handler = fopen($tempCSV, 'r')) {
                if($retry) {
                    $handler = $this->moveToLine($cursor, $handler);
                }
                while (!feof($handler)) {
                    if($cursor % 100 == 0) {
                        $this->em->refresh($job);
                        if($job->isCanceled()) {
                            $this->pdo->rollBack();
                            unlink($tempCSV);
                            unlink($logfile);
                            return;
                        }
                        else if ($job->isStopped()) {
                            $this->pdo->commit();
                            file_put_contents($logfile, $logBatch, FILE_APPEND);
                            return;
                        }
                        else if($job->isRetried()) {
                            $this->em->flush($job->setState(Job::RUNNING));
                            $this->pdo->commit();
                        }
                    }

                    if (!is_array($data = fgetcsv($handler))) {
                        break;
                    }

                    if (empty($data)) {
                        continue;
                    }

                    $report = JobReport::create($cursor++, $data);
                    $this->advance($job);

                    try {
                        $report = $this->processLine($report,$cursor);
                    } catch (\Exception $e) {
                        $report->addError($e->getMessage());
                    }

                    $logBatch .= serialize($report) . PHP_EOL;

                    if($cursor % 2000 == 0) {
                        $this->pdo->commit();
                    }

                    if ($cursor % 2000 == 0) {
                        file_put_contents($logfile, $logBatch, FILE_APPEND);
                        $logBatch = '';
                    }

                    if(in_array($this::HEADER_ERROR_MESSAGE,$report->getErrors())) {
                        $job->setSize(1);
                        break;
                    }
                }
                fclose($handler);
            }
            file_put_contents($logfile, $logBatch, FILE_APPEND);
            $this->pdo->commit();
            $this->pdo->setAutoCommit(true);
            $this->report($job, $logfile);
        }
        catch (\Exception $e) {
            $this->logger->error('Importer error:'.$e);
            $this->pdo->rollBack();
        }
        unlink($tempCSV);

        $this->redis->flushDB();
        return;
    }

    /**
     * @param JobReport $report
     * @param $line
     * @return string
     */
    private function processLine(JobReport $report,$line)
    {
        if($line == 2) {
            $this->hasCorrectHeaders(array_map([$this,'normalizeString'],$report->getData()))?:$report->addError($this::HEADER_ERROR_MESSAGE);
        }
        else {
            $report = $this->createContract($report);
        }
        return $report;
    }

    /**
     * @param array $data
     * @return array
     */
    private function normalize(array $data)
    {
        return  [
            'id_producto' => strtoupper(trim(preg_replace('/[^A-Za-z0-9\-\_]/', '', $data[0]))),
            'id_proveedor' => RutUtils::normalize((int) $data[1]),
            'id_unidad' => is_numeric(trim($data[6])) ? (int) trim($data[6]) : trim($data[6]),
            'fecha_inicio' => \DateTime::createFromFormat(self::EXCEL_DATE_FORMAT, $data[3]),
            'fecha_fin' => \DateTime::createFromFormat(self::EXCEL_DATE_FORMAT, $data[4]),
            'numero_contrato' => $data[2],
            'acuerdo_compra' => $data[5],
            'centro_costo' => $data[7],
            'precio' => $data[9],
            'moneda' => Money::getMoneyType($data[8]),
            'sla_eval_tipo' => SLA::getSlaType($data[10]),
            'sla_eval_valor' => SLA::getSlaValue($data[10]),
            'sla_order_tipo' => SLA::getSlaType($data[11]),
            'sla_order_valor' => SLA::getSlaValue($data[11]),
            'sla_accept_tipo' => SLA::getSlaType($data[12]),
            'sla_accept_valor' => SLA::getSlaValue($data[12]),
            'sla_prod_tipo' => SLA::getSlaType($data[13]),
            'sla_prod_valor' => SLA::getSlaValue($data[13]),
            'sla_delivery_tipo' => SLA::getSlaType($data[14]),
            'sla_delivery_valor' => SLA::getSlaValue($data[14]),
        ];
    }

    private function createContract(JobReport $report)
    {
        $row = $this->normalize($report->getData());
        if (!isset($row['id_proveedor'], $row['id_producto'], $row['id_unidad'])) {
            $report->addError('IDS inválidos');
            return $report;
        }
        $this->products = array_change_key_case($this->products, CASE_UPPER);
        $supplierId = $this->suppliers[$row['id_proveedor']] ?? null;
        $productId  = $this->products[$row['id_producto']] ?? null;
        $officeId   = $this->offices[$row['id_unidad']] ?? null;
        $pCentralized  = $this->pCentralized[$row['id_producto']] ?? 1;


        $start = !$row['fecha_inicio'] instanceof \DateTime ?
            \DateTime::createFromFormat('Y-m-d', trim($row['fecha_inicio'])) :
            $row['fecha_inicio'];

        $end = !$row['fecha_fin'] instanceof \DateTime ?
            \DateTime::createFromFormat('Y-m-d', trim($row['fecha_fin'])) :
            $row['fecha_fin'];

        if (!$supplierId) {
            $report->addError('Proveedor: '. $row['id_proveedor'] . ' No existe');
        }

        if (!$productId) {
            $report->addError('Producto: '. $row['id_producto'] . ' No existe');
        }

        if (!$officeId) {
            $report->addError('Sucursal: '. $row['id_unidad'] . ' No existe');
        }

        if (!$start || !$end) {
            $report->addError('Formato de fecha invalido: '. $row['fecha_inicio'] . ' '. $row['fecha_fin']);
        }

        if(count($report->getErrors())>0) {
            return $report;
        }

        $columnTypes = [
            \PDO::PARAM_INT,  // product_id
            \PDO::PARAM_INT,  // supplier_id
            \PDO::PARAM_INT,  // office_id
            \PDO::PARAM_STR,  // contract_number
            'datetime',       // validity_start
            'datetime',       // validity_end
            \PDO::PARAM_BOOL, // expired
            \PDO::PARAM_STR,  // purchase_agreement
            \PDO::PARAM_INT,  // price
            \PDO::PARAM_STR,  // currency

            \PDO::PARAM_INT,  // sla_approver_value
            \PDO::PARAM_STR,  // sla_approver_type

            \PDO::PARAM_INT,  // sla_product_value
            \PDO::PARAM_STR,  // sla_product_type

            \PDO::PARAM_INT,  // sla_accept_value
            \PDO::PARAM_STR,  // sla_accept_type

            \PDO::PARAM_INT,  // sla_order_value
            \PDO::PARAM_STR,  // sla_order_type

            \PDO::PARAM_INT,  // sla_approver_eval_value
            \PDO::PARAM_STR,  // sla_approver_eval_type

            \PDO::PARAM_INT,  // sla_requester_eval_value
            \PDO::PARAM_STR,  // sla_requester_eval_type

            \PDO::PARAM_INT,  // centralized
            \PDO::PARAM_STR,  // cost_center
        ];

        $registerData = [
            'product_id'         => $productId,
            'supplier_id'        => $supplierId,
            'office_id'          => $officeId,
            'contract_number'    => $row['numero_contrato'],
            'validity_start'     => $start,
            'validity_end'       => $end,
            'expired'            => $end < new \DateTime(),
            'purchase_agreement' => $row['acuerdo_compra'],
            'centralized'        => $pCentralized,
            'price_value'        => $row['precio'],
            'price_currency'     => $row['moneda'],
            'cost_center'        => $row['centro_costo'],
            'slas' => $slas = serialize([
                ProductStatus::CREATED   => SLA::create($row['sla_eval_valor'],   $row['sla_eval_tipo']),
                ProductStatus::ORDERED   => SLA::create($row['sla_order_valor'],  $row['sla_order_tipo']),
                ProductStatus::APPROVED  => SLA::create($row['sla_accept_valor'], $row['sla_accept_tipo']),
                ProductStatus::DELIVERED => SLA::create($row['sla_prod_valor'],   $row['sla_prod_tipo']),
                ProductStatus::CONFIRMED => SLA::create(2, SLA::BUSINESS_DAY),
                ProductStatus::REVIEWED  => SLA::create(2, SLA::BUSINESS_DAY),
                'en_despacho'            => SLA::create($row['sla_delivery_valor'], $row['sla_delivery_tipo']),
            ]),
            'deleted_at' => NULL,
            'updated_at' => (new \DateTime())->format('Y-m-d H:i:s')
        ];

        if ($id = $this->checkContractExist($supplierId, $productId, $officeId)) {
            unset(
                $columnTypes[0],
                $columnTypes[1],
                $columnTypes[2],
                $registerData['product_id'],
                $registerData['supplier_id'],
                $registerData['office_id']
            );
            $this->pdo->update('contracts', $registerData, ['id' => $id], array_values($columnTypes));
            $report->setAction(JobReport::UPDATED);
        } else {
            $registerData['created_at'] = (new \DateTime())->format('Y-m-d H:i:s');
            $this->pdo->insert('contracts', $registerData, $columnTypes);
            $report->setAction(JobReport::CREATED);
        }
        return $report;
    }

    private function checkContractExist($idSupplier, $idProduct, $idOffice)
    {
        return $this->pdo->fetchColumn(
            'select id from contracts where supplier_id = ? and product_id = ? and office_id = ?',
            [$idSupplier, $idProduct, $idOffice], 0
        );
    }

    /**
     * @param $line
     * @param $handler
     */
    private function moveToLine($line, $handler)
    {
        $count = 0;
        while(!feof( $handler)) {
            fgets($handler);
            $count++;
            if($count == $line) {
                break;
            }
        }
        return $handler;
    }
}