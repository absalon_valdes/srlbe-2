<?php

namespace App\Queue\Workers;

use App\Entity\Employee;
use App\Entity\Job;
use App\Entity\User;
use App\Helper\ExcelBatchProcessor;
use App\Model\JobReport;
use App\Repository\EmployeeRepository;
use App\Repository\OfficeRepository;
use App\Repository\PositionRepository;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class EmployeeImporterWorker
 * @package App\Queue\Workers
 *
 * @DI\Service()
 */
class EmployeeImporterWorker extends AbstractWorker
{
    const DEFAULT_ROL = 'ROLE_EMPLOYEE';
    const REQUESTER_ROL = 'ROLE_REQUESTER';

    const HEADERS = [
        0=>'RUT',
        1=>'NOMBRE',
        2=>'APELLIDO PATERNO',
        3=>'APELLIDO MATERNO',
        4=>'TELEFONO',
        5=>'PUESTO',
        6=>'CODIGO SUCURSAL',
        7=>'EMAIL USUARIO',
        8=>'SEXO'
    ];

    /**
     * @var EmployeeRepository $employeesRepo
     * @DI\Inject("employee_repo")
     */
    public $employeesRepo;

    /**
     * @var PositionRepository $positionsRepo
     * @DI\Inject("position_repo")
     */
    public $positionsRepo;

    /**
     * @var OfficeRepository $officeRepo
     * @DI\Inject("office_repo")
     */
    public $officeRepo;

    /**
     * @var UserManager
     * @DI\Inject("fos_user.user_manager")
     */
    public $userManager;
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var Job
     */
    protected $job;

    /**
     * ContractImporterWorker constructor.
     *
     * @param EntityManager $em
     * @param $varDir
     * @param ValidatorInterface $validator
     * @DI\InjectParams({
     *     "em"=@DI\Inject("doctrine.orm.entity_manager"),
     *     "varDir"=@DI\Inject("%kernel.var_dir%"),
     *     "validator"=@DI\Inject("validator")
     * })
     */
    public function __construct(EntityManager $em, $varDir, ValidatorInterface $validator)
    {
        parent::__construct($em, $varDir);
        $this->validator = $validator;
    }

    public function process(Job $job)
    {
        $this->job = $job;
        $logfile = sprintf('%s/%s.log', $this->tempDir, $job->getId());
        $tempCSV = sprintf('%s/%s.csv', $this->tempDir, $job->getId());

        $cursor = 1;

        ExcelBatchProcessor::excelToCsv($job->getPayload('file'), $tempCSV);
        $total = (int)exec(sprintf('wc -l < "%s"', $tempCSV));
        $this->setSize($job, $total);
        $this->report($job, $logfile);

        if (!file_exists($tempCSV) && !filesize($tempCSV)) {
            throw new \RuntimeException(sprintf('Cannot create file %s', $tempCSV));
        }

        try{
            $logBatch = '';
            if ($handler = fopen($tempCSV, 'r')) {

                while (!feof($handler)) {

                    if (!is_array($data = fgetcsv($handler))) {
                        break;
                    }

                    if (empty($data)) {
                        continue;
                    }

                    $report = JobReport::create($cursor++, $data);
                    $this->advance($job);

                    try {
                        $report = $this->processLine($report,$cursor);
                    } catch (\Exception $e) {
                        $report->addError($e->getMessage());
                    }

                    $logBatch .= serialize($report) . PHP_EOL;

                    if($cursor % 2000 == 0) {
                        $this->em->flush();
                    }

                    if ($cursor % 2000 == 0) {
                        file_put_contents($logfile, $logBatch, FILE_APPEND);
                        $logBatch = '';
                    }

                    if(in_array($this::HEADER_ERROR_MESSAGE,$report->getErrors())) {
                        $job->setSize(1);
                        break;
                    }
                }
                fclose($handler);
            }
            file_put_contents($logfile, $logBatch, FILE_APPEND);
            $this->em->flush();
            $this->report($job, $logfile);
        }
        catch (\Exception $e) {
            $this->logger->error('Importer error:'.$e);
        }
        unlink($tempCSV);
        return;
    }

    private function processLine(JobReport $report,$line)
    {
        if($line == 2) {
            $headers = array_map([$this,'normalizeString'], $report->getData());
            $report->setData($headers);
            $this->hasCorrectHeaders($headers) ?:$report->addError($this::HEADER_ERROR_MESSAGE);
        }
        else {
            $report = $this->createOrUpdate($report);
        }
        return $report;
    }

    private function normalize(array $data)
    {
        return [
            'rut'           => strtolower((trim($data[0]))),
            'name'          => ucwords(strtolower(trim($data[1]))),
            'lastName'      => ucfirst(strtolower($data[2])),
            'secondLastName'=> ucfirst(strtolower($data[3])),
            'phone'         => trim($data[4]),
            'position'      => $this->positionsRepo->findOneBy(['title'=>$data[5]]) ?? null,
            'office'        => $this->officeRepo->findOneBy(['code'=>$data[6]]) ?? null,
            'email'         => strtolower(trim($data[7])),
            'user'          => $this->userManager->findUserByUsername((explode('@', strtolower(trim($data[7])))[0])),
            'gender'        => strtoupper($data[8][0]),
        ];
    }

    private function createOrUpdate(JobReport $report)
    {
        $data = $this->normalize($report->getData());
        $rut = $data['rut'];
        $user = $data['user'];
        $report->setAction(JobReport::UPDATED);

        $employee = $this->employeesRepo->findOneBy(['rut'=>$rut]);
        if(!$employee) {
            $employee = new Employee();
            $employee->setRut($rut);
            $report->setAction(JobReport::CREATED);
        }

        $employee->setPosition($data['position']);
        $employee->setOffice($data['office']);
        $employee->setName($data['name']);
        $employee->setLastName($data['lastName']);
        $employee->setSecondLastName($data['secondLastName']);
        $employee->setPhone($data['phone']);
        $employee->setGender($data['gender']);

        $report = $this->validate($report,$employee,$data);

        if(count($report->getErrors()) == 0)
        {
            if (empty($user)) {
                $user = $this->createUser($data['email'], $rut, $employee);
            }else{
                $user = $this->updateRoles($user);
            }

            $employee->setUser($user);

            $report->willCreate() ?: $this->em->persist($employee);
            $report->willUpdate() ?: $this->em->merge($employee);
        }

        return $report;
    }

    private function validate(JobReport $report,Employee $employee, $data)
    {
        if($data['position'] === null) {
            $report->addError('El cargo no existe en el sistema');
        }
        if($data['office'] === null) {
            $report->addError('La sucursal no existe en el sistema');
        }
        if($data['user'] !== null) {
            if ($data['user']->getEmployee()->getId() !== $employee->getId()) {
                $report->addError('Ya existe un usuario con el email ' . $data['email']);
            }
        }
        if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $report->addError('El email no es válido');
        }

        $errors = $this->validator->validate($employee);

        if(count($errors) > 0 && count($report->getErrors()) == 0) {
            $report->addErrors($errors);
        }

        if(count($report->getErrors()) > 0) {
            !$report->willCreate() ?: $this->em->detach($employee);
            !$report->willUpdate() ?:$this->em->refresh($employee);
        }

        return $report;
    }

    private function createUser($email,$rut,Employee $employee)
    {
        $username = explode('@', $email)[0];
        $user = $this->userManager->findUserByUsername($username);

        if ($user) {
            return $user;
        }

        $user = $this->userManager->createUser();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setRoles(array(self::DEFAULT_ROL,self::REQUESTER_ROL));
        $user->setEnabled(true);
        $user->setIsEmployee(true);
        if ($oldUser = $this->checkUserWithEmployee($employee)){
            $oldUser->setEmployee(null);
            $this->userManager->updateUser($oldUser);
        }
        $user->setEmployee($employee);
        $password = explode('-', $rut)[0];
        $user->setPlainPassword($password);

        $this->userManager->updateUser($user);

        return $user;
    }

    private function updateRoles(User $user){
        if(!in_array(self::REQUESTER_ROL,$user->getRoles())){
            $user->addRole(self::REQUESTER_ROL);
        }
        if(!in_array(self::DEFAULT_ROL,$user->getRoles())){
            $user->addRole(self::DEFAULT_ROL);
        }
        $this->userManager->updateUser($user);
        return $user;
    }

    function checkUserWithEmployee($employee){
        $user = $this->userManager->findUserBy(['employee' => $employee]);
        return $user;
    }
}