<?php

namespace App\Util;

/**
 * Class DateUtils
 * @package App\Util
 */
class DateUtils
{
    /**
     * @param \DateInterval $interval
     * @return int
     */
    public static function dateIntervalToDays(\DateInterval $interval)
    {
        return $interval->y * 365 + $interval->m * 30 + $interval->d;
    }

    /**
     * @param \DateInterval $interval
     * @return string
     */
    public static function dateIntervalFormat(\DateInterval $interval)
    {
        $format = [];
        $format[] = $interval->y ? '%ya' : '';
        $format[] = $interval->m ? '%mM' : '';
        $format[] = $interval->d ? '%dd' : '';
        $format[] = $interval->h ? '%Hh' : '';
        $format[] = $interval->i ? '%im' : '';
        $format[] = $interval->s ? '%ss' : '';

        return $interval->format(trim(join(' ', $format)));
    }

    /**
     * @param $datetime
     * @return \DateTime
     */
    public static function dateTimeFieldToObject($datetime)
    {
        $date      = $datetime['date'];
        $hour      = $datetime['time']['hour'];
        $minute    = $datetime['time']['minute'];
        $formatted = sprintf('%s %02d:%02d', $date, $hour, $minute);

        return \DateTime::createFromFormat('d/m/Y H:i', $formatted);
    }

    /**
     * @param \DateInterval $interval
     * @return array
     */
    public static function dateIntervalSerialize(\DateInterval $interval)
    {
        return [
            'y' => $interval->y,
            'm' => $interval->m,
            'd' => $interval->d,
            'h' => $interval->h,
            'i' => $interval->i,
            's' => $interval->s,
            'invert' => (bool)$interval->invert,
        ];
    }

    /**
     * @param \DateInterval $a
     * @param \DateInterval $b
     * @return bool|\DateInterval
     */
    public static function dateIntervalAdd(\DateInterval $a, \DateInterval $b)
    {
        $i = new \DateTime;
        $j = clone $i;
        
        $i->add($a);
        $i->add($b);
        
        return $j->diff($i);
    }

    /**
     * @param \DateInterval $interval
     * @return int
     */
    public static function dateIntervalToSeconds(\DateInterval $interval)
    {
        return $interval->days*86400 + $interval->h*3600 + $interval->i*60 + $interval->s;
    }
}
