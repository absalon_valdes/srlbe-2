<?php

namespace App\Util;

/**
 * Class RutUtils
 * @package App\Util
 */
class RutUtils
{
    /**
     * @param $rut
     * @return string
     */
    public static function normalize($rut)
    {
        return $rut.'-'.self::calculateVerifier($rut);
    }

    /**
     * @param $rut
     * @return int|string
     */
    public static function calculateVerifier($rut)
    {
        $rut = (string) $rut;

        $factor = 2;
        $accum  = 0;
        for ($i = strlen($rut) - 1; $i >= 0; $i--) {
            $factor = $factor > 7 ? 2 : $factor;
            $accum += (int)$rut[$i] * $factor++;
        }

        $dv = 11 - ($accum % 11);
        $dv = $dv == 11 ? 0 : ($dv == 10 ? 'K' : $dv);

        return $dv;
    }

    /**
     * @param $rut
     * @param $verifier
     * @return bool
     */
    public static function isValid($rut, $verifier)
    {
        return self::calculateVerifier($rut) === $verifier;
    }
}