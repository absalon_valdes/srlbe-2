<?php

namespace App\Pipeline;

use App\Pipeline\Task\Task;

class Pipeline
{
    private $name;
    private $tasks = [];
    
    public function __construct($name, array $tasks = [])
    {
        $this->name = $name;
        $this->tasks = $tasks;
    }

    public function addTask(Task $task)
    {
        $this->tasks[] = $task;
    }
    
    public function process(array $options = [])
    {
    }
}