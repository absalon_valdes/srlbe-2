<?php

namespace App\Pipeline;

use App\Pipeline\Task\Task;

class Registry
{
    private $tasks = [];

    public function registerTask(Task $task)
    {
        $this->tasks[$task->getName()] = $task;
    }

    public function getTask($name)
    {
        return clone $this->tasks[$name];
    }
}