<?php

namespace App\Pipeline;

use App\Pipeline\Task\Task;
use Gedmo\Exception\BadMethodCallException;

class Builder
{
    private $name;
    private $start;
    private $end;
    private $tasks = [];

    private $registry;
    
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }
    
    public function create($name)
    {
        $this->name = $name;
        
        return $this;
    }

    public function src(Task $source)
    {
        $this->start = $source;
        
        return $this;
    }

    public function dest(Task $dest)
    {
        $this->end = $dest;
        
        return $this;
    }

    public function pipe(Task $pipe)
    {
        $this->tasks[] = $pipe;
        
        return $this;
    }

    public function getPipeline()
    {
        if (null === $this->start) {
            throw new BadMethodCallException();
        }
        
        if (null === $this->end) {
            throw new BadMethodCallException();
        }
        
        $tasks[] = $this->start;
        $tasks = array_merge($tasks, $this->tasks);
        $tasks[] = $this->end;
        
        return new Pipeline($this->name, $tasks);
    }
}