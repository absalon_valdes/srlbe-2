<?php

namespace App\Pipeline\Task\Writer;

use App\Pipeline\Task\AbstractTask;

class DoctrineDBALWriter extends AbstractTask
{
    public function getName()
    {
        return 'doctrine_dbal_writer';
    }

    public function execute(&$data)
    {
        $pdo = $this->container->get('database_connection');
        $pdo->beginTransaction();
        
        try {
            foreach ($data as $contract) {
                $this->process($contract, $pdo);
            }
            
            $pdo->commit();
        } catch (\Exception $e) {
            $pdo->rollBack();
        }
    }

    private function process($item, $pdo)
    {
        $tableName = $this->options['table_name'];
        $typeMap = $this->options['type_map'];
        $existsKey = $this->options['exists_key'] ?? null;

        $columnTypes = [];
        
        foreach ($item as $key => $value) {
            $type = $typeMap[$key];
            
            if ('string' === $type) {
                $columnTypes[] = \PDO::PARAM_STR;
            } elseif ('boolean' === $type) {
                $columnTypes[] = \PDO::PARAM_BOOL;
            } elseif ('datetime' === $type) {
                $columnTypes[] = 'datetime';
            } elseif ('int' === $type) {
                $columnTypes[] = \PDO::PARAM_INT;
            } else {
                $columnTypes[] = \PDO::PARAM_STR;
            }
        }
        
        if ($id = $this->checkIfExists($pdo, $tableName, $item, $existsKey)) {
            foreach ($existsKey as $i => $k) {
                unset($item[$k], $columnTypes[$i]);
            }

            $pdo->update($tableName, $item, ['id' => $id], array_values($columnTypes));
        } else {
            $pdo->insert($tableName, $item, $columnTypes);
        }
    }

    private function checkIfExists($pdo, $tableName, $item, $keys)
    {
        $qb = $pdo->createQueryBuilder();
        $qb->select('id')->from($tableName);
        
        foreach ($keys as $index => $key) {
            $qb->andWhere($key. ' = ?');
            $qb->setParameter($index, $item[$key]);
        }
        
        return $qb->execute()->fetchColumn();
    }
}