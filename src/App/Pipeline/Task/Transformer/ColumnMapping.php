<?php

namespace App\Pipeline\Task\Transformer;

use App\Pipeline\Task\AbstractTask;

class ColumnMapping extends AbstractTask
{
    public function execute(&$data)
    {
        $mapped = [];

        foreach ($data as $row) {
            $newItem = [];
            foreach ($this->options['mapping'] as $k => $i) {
                $newItem[$k] = $row[$i];
            }
            $mapped[] = $newItem;
        }
        
        $data = $mapped;
    }
}