<?php

namespace App\Pipeline\Task\Transformer;

use App\Pipeline\Task\AbstractTask;

class ColumnTransform extends AbstractTask
{
    public function execute(&$data)
    {
        $function = $this->options['function'];
        $column = $this->options['column'];
        
        $data = array_map(function ($item) use ($function, $column) {
            $args = $this->makeArgs($column, $item);
            $item[$column] = call_user_func_array($function, $args);
            return $item;
        }, $data);
    }

    private function makeArgs($column, $item)
    {
        if (!isset($this->options['arguments'])) {
            return [$item[$column]];
        }
        
        if (is_array($args = $this->options['arguments'])) {
            $args = array_map(function ($arg) use ($item) {
                if (0 === strpos($arg, '$')) {
                    return $item[substr($arg, 1)];
                }
                
                return $arg;
            }, $args);
        }
        
        return $args ?? [];
    }
}