<?php

namespace App\Pipeline\Task\Transformer;

use App\Pipeline\Task\AbstractTask;

class VirtualColumn extends AbstractTask
{
    public function execute(&$data)
    {
        $key = $this->options['key'];
        $value = $this->options['value'];

        $data = array_map(function ($item) use ($key, $value) {
            $item[$key] = $value;
            
            return $item;
        }, $data);
    }
}