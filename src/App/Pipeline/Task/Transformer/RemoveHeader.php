<?php

namespace App\Pipeline\Task\Transformer;

use App\Pipeline\Task\AbstractTask;

class RemoveHeader extends AbstractTask
{
    public function execute(&$data)
    {
        $size = $this->options['height'] ?? 1;
        
        while ($size--) {
            array_shift($data);
        } 
    }
}