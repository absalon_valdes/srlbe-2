<?php

namespace App\Pipeline\Task;

interface Task
{
    public function getName();
    public function setOptions(array $options);
    public function execute(&$data);
}