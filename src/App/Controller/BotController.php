<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BotController
 * @package App\Controller
 * @Rest\Route("/bot")
 */
class BotController extends FOSRestController
{
    /**
     * @param Request $request
     * @Rest\Post()
     */
    public function indexAction(Request $request)
    {
        $mail = \Swift_Message::newInstance();
        $mail->setSubject('Bot SRLBE');
        $mail->setBody(json_encode($request->request->all()));
        $mail->setFrom('absalon@bluecompany.cl');
        $mail->addTo('absalon@bluecompany.cl');
        
        $this->get('mailer')->send($mail);

	return $this->view(null, 200);
    }
}
