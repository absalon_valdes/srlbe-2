<?php

namespace App\Controller;

use App\Entity\Category;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as Di;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;

class Menus extends FOSRestController
{
    /**
     * @Rest\Get("/category-menu/{id}", name="display_cat_menu")
     * @param Category $category
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function categoryMenu(Category $category)
    {
        return $this->render('categorymenu.twig', [
            'category' => $category
        ]);
    }
}