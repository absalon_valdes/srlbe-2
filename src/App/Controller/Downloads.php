<?php

namespace App\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation\Inject;
use FOS\RestBundle\Controller\Annotations\Get;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class Downloads extends FOSRestController
{
    /**
     * @Inject("%products_files_dir%")
     */
    protected $productsFilesDir;

    /**
     * @Inject("%uploads_dir%")
     */
    protected $uploadDir;

    /**
     * @Get("/download/{fileName}", name="download_file", requirements={"filename": ".+"})
     * @Extra\Cache(expires="+10 days", public=true, smaxage="15000")
     * @param $fileName
     * @return BinaryFileResponse
     */
    public function downloadFile($fileName)
    {
        return $this->createResponse($this->uploadDir, $fileName);
    }

    /**
     * @Get("/downloads/product/{fileName}", name="download_product_file", requirements={"fileName": ".+"})
     * @param $fileName
     * @return BinaryFileResponse
     */
    public function downloadProductFile($fileName)
    {
        return $this->createResponse($this->productsFilesDir, $fileName);
    }

    private function createResponse($fileDir, $fileName)
    {
        if (!file_exists($filePath = $fileDir.'/'.$fileName)) {
            throw $this->createNotFoundException();
        }
        
        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($filePath);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $fileName,
            iconv('UTF-8', 'ASCII//TRANSLIT', $fileName)
        );

        return $response;
    }
}
