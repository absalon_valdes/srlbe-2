<?php

namespace App\Controller;

use App\Entity\PepGrant;
use App\Form\PepValidationType;
use App\Workflow\Status\PepStatus;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation\Inject;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/validacion-pep")
 * @Breadcrumb("Portada", route={"name"="dashboard"})
 */
class Pep extends FOSRestController
{
    /**
     * @Inject("%uploads_dir%")
     */
    protected $uploadDir;

    /**
     * @Inject("pep_query_helper")
     */
    protected $pepQuery;

    /**
     * @Inject("pep_grant_repo")
     */
    protected $pepGrantRepo;

    /**
     * @Inject("form_upload_helper")
     */
    protected $uploadHelper;

    /**
     * @Inject("pep_load_helper")
     */
    protected $pepLoadHelper;

    /**
     * @Inject("employee_repo")
     */
    protected $employeeRepo;

    /**
     * @var EventDispatcher $dispatcher
     * @Inject("event_dispatcher")
     */
    protected $dispatcher;

    /**
     * @Breadcrumb("Procesos de validación de proveedores PEP")
     * @Get(name="pep_list")
     * @View("pep/list.twig")
     */
    public function list()
    {
        return $this->pepGrantRepo->findByRequester($this->getUser());
    }

    /**
     * @Breadcrumb("Inicio del proceso de registro de validación de proveedores PEP")
     * @Method({"GET", "POST"})
     * @Route("/crear", name="pep_index")
     * @View("pep/index.twig")
     * @param Request $request
     * @return array|\FOS\RestBundle\View\View
     */
    public function index(Request $request)
    {
        $grant = $this->pepGrantRepo->create();
        $grant->setRequester($this->getUser());
        $grant->setStatus(PepStatus::CREATED);

        return $this->processForm($request, $grant);
    }

    /**
     * @Breadcrumb("Inicio del proceso de registro de validación de proveedores PEP")
     * @Security("user == grant.getRequester() or is_granted('ROLE_BACKOFFICE')")
     * @Method({"GET", "POST"})
     * @Route("/editar/{id}", name="pep_edit")
     * @View("pep/index.twig")
     * @param Request $request
     * @param PepGrant $grant
     * @return array|\FOS\RestBundle\View\View
     */
    public function edit(Request $request, PepGrant $grant)
    {
        return $this->processForm($request, $grant);
    }

    /**
     * @Post("/carga-template", name="pep_load_template")
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function loadDataFromTemplate(Request $request)
    {
        $file = $request->files->get('template');
        $file = array_values($this->uploadHelper->upload($file))[0];

        $filename = sprintf('%s/%s', $this->uploadDir, $file);
        $company = $this->pepLoadHelper->load($filename);

        return $this->view($company, 200);
    }

    /**
     * @Breadcrumb("Validación de proveedores PEP")
     * @Security("user == grant.getRequester() or is_granted('ROLE_BACKOFFICE')")
     * @Get("/confirmacion/{id}", name="pep_register")
     * @View("pep/register.twig")
     * @param PepGrant $grant
     * @return PepGrant
     */
    public function register(PepGrant $grant)
    {
        return $grant;
    }

    /**
     * @Breadcrumb("Validación de proveedores PEP")
     * @Security("user == grant.getRequester() or is_granted('ROLE_BACKOFFICE')")
     * @Get("/validacion/{id}", name="pep_checkout")
     * @View("pep/checkout.twig")
     * @param PepGrant $grant
     * @return array
     */
    public function checkout(PepGrant $grant)
    {
        $this->pepQuery->checkPep($grant);
        $grant->setStatus(PepStatus::VALIDATED);
        $this->pepGrantRepo->persist($grant, true);

        if ($grant->hasPep()) {
            $event = new GenericEvent($grant);
            $this->dispatcher->dispatch('pep.has_pep', $event);
        }
        
        return [
            'user' => $user = $this->getUser(),
            'employee' => $this->employeeRepo->findOneByUser($user),
            'data' => $grant
        ];
    }

    /**
     * @Post("/aceptar-condiciones/{id}", name="pep_accept")
     * @param PepGrant $grant
     * @return \FOS\RestBundle\View\View
     */
    public function accept(PepGrant $grant)
    {
        $grant->setStatus(PepStatus::ACCEPTED);
        $this->pepGrantRepo->persist($grant, true);

        return $this->view(null, 200);
    }

    /**
     * @Breadcrumb("Validación de proveedores PEP")
     * @Security("user == grant.getRequester() or is_granted('ROLE_BACKOFFICE')")
     * @Get("/seleccion/{id}", name="pep_selection")
     * @View("pep/selection.twig")
     * @param PepGrant $grant
     * @return array
     */
    public function selection(PepGrant $grant)
    {
        return [
            'user' => $user = $this->getUser(),
            'employee' => $this->employeeRepo->findOneByUser($user),
            'data' => $grant
        ];
    }

    /**
     * @Post("/adjudicar/{id}", name="pep_grant")
     * @Security("user == grant.getRequester() or is_granted('ROLE_BACKOFFICE')")
     * @param Request $request
     * @param PepGrant $grant
     * @return \FOS\RestBundle\View\View
     */
    public function grant(Request $request, PepGrant $grant)
    {
        $rut = $request->request->get('company')[0];
        $grant->setStatus(PepStatus::GRANTED);
        $grant->selectCompany($grant->getCompanyByRut($rut));
        $this->pepGrantRepo->persist($grant, true);

        return $this->routeRedirectView('pep_grant_preview', [
            'id' => $grant->getId()
        ]);
    }

    /**
     * @Breadcrumb("Adjudicar proveedor")
     * @Security("user == grant.getRequester() or is_granted('ROLE_BACKOFFICE')")
     * @Get("/adjudicar/{id}", name="pep_grant_preview")
     * @View("pep/grant.twig")
     * @param PepGrant $grant
     * @return array
     */
    public function preview(PepGrant $grant)
    {
        return [
            'user' => $user = $this->getUser(),
            'employee' => $this->employeeRepo->findOneByUser($user),
            'data' => $grant,
        ];
    }

    /**
     * @Post("/adjuntar-docs/{id}", name="pep_attach")
     * @param Request $request
     * @param PepGrant $grant
     * @return \FOS\RestBundle\View\View
     */
    public function attach(Request $request, PepGrant $grant)
    {
        $files = $request->files->get('attachments');
        $files = $this->uploadHelper->upload($files);

        $grant->setCertification($files);
        $grant->setStatus(PepStatus::GRANTED);
        $this->pepGrantRepo->persist($grant, true);

        return $this->view($files, 200);
    }

    /**
     * @Breadcrumb("Formulario de validación de proveedor PEP")
     * @Get("/detalle/{id}", name="pep_grant_detail")
     * @View("pep/grant.twig")
     * @param PepGrant $grant
     * @return array
     */
    public function detail(PepGrant $grant)
    {
        return [
            'user' => $user = $this->getUser(),
            'employee' => $this->employeeRepo->findOneByUser($user),
            'data' => $grant,
        ];
    }

    /**
     * @Get("/documento/{id}", name="pep_render_doc")
     * @param PepGrant $grant
     * @return Response
     */
    public function renderDocument(PepGrant $grant)
    {
        $user = $this->getUser();
        $employee = $this->employeeRepo->findOneByUser($user) ?: $user;

        $uuid = Uuid::uuid5(Uuid::NAMESPACE_DNS,
            sha1(sprintf('%s-%s', $user->getId(), json_encode($grant))));

        $html = $this->renderView('pep/form.pdf.twig', [
            'user' => $user,
            'employee' => $employee,
            'data' => $grant,
            'uuid' => $uuid,
        ]);

        if (!$document = $this->get('pdf_renderer')->render($html)) {
            throw new NotFoundHttpException('Error al renderizar formulario');
        }

        return new Response(file_get_contents($document), Response::HTTP_OK, [
            'Content-Type'        => 'application/octect-stream',
            'Content-Disposition' => 'attachment; filename="file.pdf"'
        ]);
    }

    /**
     * @param Request $request
     * @param PepGrant $grant
     * @return array|\FOS\RestBundle\View\View
     */
    private function processForm(Request $request, PepGrant $grant)
    {
        $form = $this->createForm(PepValidationType::class, $grant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->uploadDocuments($grant);
            $this->pepGrantRepo->persist($grant, true);

            return $this->routeRedirectView('pep_register', [
                'id' => $grant->getId()
            ]);
        }

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @param PepGrant $grant
     */
    private function uploadDocuments(PepGrant &$grant)
    {
        $companies = $grant->getCompanies()->map(function($company) {
            $documents = $company->getDocuments()->map(function($doc) {
                return $doc instanceof UploadedFile ? $this->uploadHelper->upload($doc) : $doc;
            });

            return $company->setDocuments($documents);
        });

        $grant->setCompanies($companies);
    }
}
