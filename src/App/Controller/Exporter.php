<?php

namespace App\Controller;

use App\Export\ExporterAggregator;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class Exporter
 * @package App\Controller
 */
class Exporter extends FOSRestController
{
    /**
     * @var ExporterAggregator
     * @DI\Inject("exporter_aggregator")
     */
    public $exporterAggregator;

    /**
     * @Get("/export/{type}", name="exporter")
     * @param $type
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function export($type)
    {
        /** @var \App\Export\Exporter $exporter */
        $exporter = $this->exporterAggregator->getExporter($type);
        
        return $exporter->export()->getResponse();
    }
}