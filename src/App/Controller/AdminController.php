<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Contract;
use App\Entity\Office;
use App\Entity\Product;
use App\Entity\ShipmentConstraint;
use App\Entity\Supplier;
use App\Entity\User;
use App\Helper\SpecialOfficeHelper;
use App\Listener\EasyAdminPreEditListener;
use App\Repository\ProductRepository;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query;
use FOS\UserBundle\Model\UserManager;
use JavierEguiluz\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\File;

/**
 * @Route("/admin")
 */
class AdminController extends BaseAdminController
{
    /**
     * @Inject("translator")
     */
    protected $translator;

    /**
     * @Inject("fos_user.user_manager")
     * @var UserManager $userManager
     */
    public $userManager;

    /**
     * @Inject("category_repo")
     */
    public $categoryRepo;

    /**
     * @Inject("%products_files_dir%")
     */
    protected $productFilesDir;


    /**
     * @var SpecialOfficeHelper
     * @Inject("special_office_helper")
     */
    public $specialOfficeHelper;


    //this functions manages the integration with FOS Bunddle with EasyAdmin
    public function createNewUserEntity()
    {
        return $this->userManager->createUser();
    }

    public function prePersistUserEntity(User $user)
    {
        $user->setIsEmployee($user->getEmployee() !== null);
        $this->userManager->updateUser($user);
    }

    public function preUpdateUserEntity(User $user)
    {
        $user->setIsEmployee($user->getEmployee() !== null);
        $this->userManager->updateUser($user);
    }
    // End of the FOS integration

    //Customization of the field Role in the user Entity
    public function createUserNewForm($entity, array $entityProperties)
    {
        $form = parent::createNewForm($entity, $entityProperties);

        return $this->createFormUser($entity, $entityProperties, $form);
    }

    public function createUserEditForm($entity, array $entityProperties)
    {
        $form = parent::createEditForm($entity, $entityProperties);

        return $this->createFormUser($entity, $entityProperties, $form);
    }

    private function createFormUser(User $entity, array $entityProperties, $form)
    {
        // change the default plain password for a traditional 2 password check
        $form->remove('plainPassword');
        $form->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'el password debe coincidir.',
            'options' => ['attr' => ['class' => 'password-field']],
            'required' => false,
            'first_options'  => ['label' => 'Password'],
            'second_options' => ['label' => 'Repetir Password'],
        ]);

        // change the default roles
        $form->remove('roles');
        $roles = $this->container->get('app_roles_helper')->getRoles();
        $form->add('roles', ChoiceType::class, [
            'choices'   => $roles,
            'required'  => false,
            'multiple'  => true,
            'attr'      => ['data-widget' => 'select2'],
        ]);

        return $form;
    }
    //END of Customization of the field Role in the user Entity

    //Customization of Product from
    public function createProductNewForm($entity, array $entityProperties)
    {
        $form = parent::createNewForm($entity, $entityProperties);

        return $this->createFormProduct($entity, $entityProperties, $form);
    }

    public function createProductEditForm($entity, array $entityProperties)
    {
        $form = parent::createEditForm($entity, $entityProperties);

        return $this->createFormProduct($entity, $entityProperties, $form);
    }

    public function prePersistProductEntity($entity)
    {
        $entity = $this->createAttachmentsPerProduct($entity);
    }

    public function preUpdateProductEntity($entity)
    {
        $entity = $this->createAttachmentsPerProduct($entity);
    }

    public function preRemoveProductEntity($entity)
    {
        $entity = $this->createAttachmentsPerProduct($entity);
    }

    private function createAttachmentsPerProduct($product)
    {
        $productImg = $product->getImages();

        $product->setImages([]);
        foreach ($productImg as $img) {
            $image = $this->fileHandler($img);
            if (!$image) {
                continue;
            }

            $product->addImage($image);
        }

        $productAttach = $product->getAttachments();
        $product->setAttachments([]);
        foreach ($productAttach as $attach) {
            $attachment = $this->fileHandler($attach);
            if (!$attachment) {
                continue;
            }

            $product->addAttachment('file', $attachment);
        }
        return $product;
    }

    private function fileHandler($file)
    {
        if (!$file) {
            return false;
        }
        
        if (is_string($file)) {
            return $file;
        }

        try {
            $uploadedFile = sprintf('%s/%s', $this->productFilesDir, $file);

            if (!is_file($uploadedFile)) {
                $original = $file->getClientOriginalName();
                $newName = sha1($original).'.'.$file->guessExtension();
                $file->move($this->productFilesDir, $newName);

                return $newName;
            }
        } catch (\Exception $e) {}

        return $file;
    }

    private function createFormProduct($entity, array $entityProperties, $form)
    {
        $form->remove('images');
        $form->remove('attachments');

        $form->add('images', FileType::class, [
            'multiple' => true,
            'required' => false,
            'data_class' => null,
            'constraints' => [
              new All([
                'constraints' => [
                    new File(
                        ['maxSize' => '2M',
                         'mimeTypes' => ['image/*'],
                         'mimeTypesMessage' => 'Ingrese un archivo valido']
                    )
                ]])]
        ]);
        $form->add('attachments', FileType::class, [
            'multiple' => true,
            'required' => false,
            'data_class' => null,
            'constraints' => [
              new All([
                'constraints' => [
                    new File(
                        [
                        'maxSize' => '2M',
                        'mimeTypes' => [
                           'application/pdf',
                           'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Ingrese un archivo valido']
                    )
                ]])]
        ]);

        return $form;
    }

    //END of Product Form

    //Customization for the Supplier Form
    public function createSupplierNewForm($entity, array $entityProperties)
    {
        $form = parent::createNewForm($entity, $entityProperties);

        return $this->createFormSupplier($entity, $entityProperties, $form);
    }

    public function createSupplierEditForm($entity, array $entityProperties)
    {
        $form = parent::createEditForm($entity, $entityProperties);

        return $this->createFormSupplier($entity, $entityProperties, $form);
    }

    private function createFormSupplier($entity, $entityProperties, $form)
    {
        $form->remove('city');
        $form->add('city', EntityType::class, $this->customProvinceChoice());

        return $form;
    }
    //END of Customization for the Supplier Form

    //Customization for the Office Form
    public function createOfficeNewForm($entity, array $entityProperties)
    {
        $form = parent::createNewForm($entity, $entityProperties);

        return $this->createFormUnit($entity, $entityProperties, $form);
    }

    public function createOfficeEditForm($entity, array $entityProperties)
    {
        $form = parent::createEditForm($entity, $entityProperties);

        return $this->createFormUnit($entity, $entityProperties, $form);
    }

    private function createFormUnit($entity, $entityProperties, $form)
    {
        $form->remove('type');
        $form->remove('city');
        $form->remove('specialType');

        $unitTypes = $this->getConstants("TYPE_", Office::class);

        $specialTypes = $this->specialOfficeHelper->getSpecialTypes(true);

        $form->add('type', ChoiceType::class, array(
            'attr'    => array('data-widget' => 'select2'),
            'choices' => $unitTypes,
        ));

        $form->add('specialType', ChoiceType::class, array(
            'attr'    => array('data-widget' => 'select2'),
            'choices' => $specialTypes,
            'required'      => false,
        ));

        $form->add('city', EntityType::class, $this->customProvinceChoice());

        return $form;
    }
    //END of Customization for the Office Form

    //Customization for the Contract Form
    public function createContractNewForm($entity, array $entityProperties)
    {
        $form = parent::createNewForm($entity, $entityProperties);

        return $this->createFormContract($entity, $entityProperties, $form);
    }

    public function createContractEditForm($entity, array $entityProperties)
    {
        $form = parent::createEditForm($entity, $entityProperties);

        return $this->createFormContract($entity, $entityProperties, $form);
    }

    private function createFormContract($entity, $entityProperties, $form)
    {
        $form->remove('slaType');
        $form->remove('slaMeasure');

        return $form
                    ->add('currency', ChoiceType::class, [
                        'attr'    => ['data-widget' => 'select2'],
                        'choices' => $this->getConstants("CURRENCY_", Contract::class),
                    ])
                    ->add('slaType', ChoiceType::class, [
                        'attr'    => ['data-widget' => 'select2'],
                        'choices' => $this->getConstants("_DAYS", Contract::class),
                    ])
                    ->add('slaMeasure', ChoiceType::class, [
                        'attr'      => ['data-widget' => 'select2'],
                        'choices'   => $this->getConstants("Unit", Contract::class),
                    ]);
    }

    private function customProvinceChoice()
    {
        return  array(
                'class'             => 'App:Category',
                'choice_label'      => 'name',
                'group_by'          => function ($city) {
                        return $city->getParent();
                },
                'attr'              => ['data-widget' => 'select2'],
                'query_builder'     => function ($er) {
                        return $er
                                  ->createQueryBuilder('city')
                                  ->addSelect('region')
                                  ->addSelect('type')
                                  ->leftJoin('city.parent', 'region')
                                  ->leftJoin('region.parent', 'type')
                                  ->where('type.name = :geo')
                                  ->setParameter('geo', 'Geografico');
                }

           );
    }

    /**
     * Constructs an array with constants of a class with a common needle.
     * @param string $needle
     * @param string|object $classOrObj The class name or object where the constants are declared.
     * @return array An associative array of the constants, or an empty array if none were found.
     */
    private function getConstants($needle, $classOrObj)
    {
        $constants = array();
        $reflect = new \ReflectionClass($classOrObj);

        foreach ($reflect->getConstants() as $name => $value) {
            if (is_int(stripos($name, $needle))) {
                $constants[$this->translator->trans($value)] = $value;
            }
        }
        return $constants;
    }
}
