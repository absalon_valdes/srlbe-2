<?php

namespace App\Controller;

use App\Entity\Requisition;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Post;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Search extends FOSRestController
{

    /**
     * @Inject("searcher")
     */
    protected $searcher;

    /**
     * @Inject("request_stack")
     */
    protected $requestStack;

    /**
     * @Method({"GET", "POST"})
     * @Route("/search", name="search")
     * @View("search.twig")
     */
    public function search()
    {
        $request = $this->requestStack->getCurrentRequest();
        $needle = $request->query->get('query');

        if (is_numeric(array_search('ROLE_REQUESTER', $this->getUser()->getRoles()))) {
            if (!trim($needle)) {
                return [
                'productsResult'  => []
                ];
            }

            $products = /*$this->serializeFiles(*/$this->searcher->findProducts($needle, $this->getUser())/*)*/;
            return [
                'productsResult'  => $products
            ];
        }

        if (!trim($needle)) {
                return [
                'requisitionsResult'  => []
                ];
        }

        $requisitions = /*$this->serializeFiles(*/$this->searcher->findRequisitions($needle, $this->getUser())/*)*/;

        return [
                'requisitionsResult' => $requisitions,
            ];
    }


    private function serializeFiles($result)
    {
        return array_map(function ($result) {
            $result['images'] = str_replace(['[', ']', '"'],'',$result['images']);
            
            return $result;
        }, $result);
    }
}
