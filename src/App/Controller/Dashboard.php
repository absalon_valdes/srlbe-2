<?php

namespace App\Controller;

use App\Entity\Requirement;
use App\Entity\Requisition;
use App\Entity\SpecialRequest;
use App\Form\BackOfficeSearchRequisitionType;
use App\Form\RequirementSearchType;
use App\Form\UserType;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\HttpFoundation\Request;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Liuggio\ExcelBundle\Factory;
use Symfony\Component\Translation\Translator;

class Dashboard extends FOSRestController
{
    private static $roleDashboard = [
        'ROLE_ADMIN'                    => 'adminDashboard',
        'ROLE_APPROVER'                 => 'approverDashboard',
        'ROLE_APPROVER_VCARD'           => 'approverVcardDashboard',
        'ROLE_REQUESTER'                => 'requesterDashboard',
        'ROLE_SUPPLIER'                 => 'supplierDashboard',
        'ROLE_EMPLOYEE'                 => 'requesterDashboard',
        'ROLE_BACKOFFICE'               => 'backofficeDashboard',
        'ROLE_HELP_DESK'                => 'helpDeskDashboard',
        'ROLE_PEP'                      => 'requesterDashboard',
        'ROLE_APPROVER_SPECIAL'         => 'approverSpecialDashboard'
    ];

    /**
     * @Inject("requisition_repo")
     */
    protected $requisitionRepo;

    /**
     * @Inject("employee_repo")
     */
    protected $empployeeRepo;

    /**
     * @Inject("pep_grant_repo")
     */
    protected $pepRepo;

    /**
     * @var Factory
     * @Inject("phpexcel")
     */
    protected $phpexcel;

    /**
     * @var Translator
     * @Inject("translator")
     */
    public $translator;

    /**
     * @var string
     * @Inject("%kernel.environment%")
     */
    public $env;

    public function GetDataRoles(Request $request)
    {
        $data = [];
        $user = $this->getUser();

        $submit = $request->request->get('user');

        if (isset($submit['user'])) {
            return $this->routeRedirectView('masking', [
                'id' => $submit['user']
            ]);
        }

        foreach (self::$roleDashboard as $role => $callback) {
            if ($user->hasRole($role)) {
                $data = array_merge($data, $this->$callback());
            }
        }

        return $data;
    }

    /**
     * @Rest\Get("/NoMostrarPopup", name="no_mostrar_popup")
     * @return mixed|null
     */
    public function NoMostrarPopup()
    {
        $this->get('session')->set('popup_completado', true);

        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Rest\Get("/descargar_excel", name="descargar_excel")
     * @Rest\View("dashboard/indexAll.twig")
     * @return mixed|null
     */
    public function DownloadExcel(Request $request)
    {
        $user = $this->getUser();
        $data = $this->GetDataRoles($request);

        $phpExcelObject = $this->phpexcel->createPHPExcelObject();

        $phpExcelObject->getProperties()
            ->setCreator('Absalon Valdes <absalon@bluecompany.cl>')
            ->setTitle('Reporte de solicitudes '.date('d-m-Y H:i:s'))
        ;

        $phpExcelObject->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Id')
            ->setCellValue('B1', 'Tipo')
            ->setCellValue('C1', 'Detalle')
            ->setCellValue('D1', 'Sucursal')
            ->setCellValue('E1', 'Centro de Costo')
            ->setCellValue('F1', 'Solicitante')
            ->setCellValue('G1', 'Fecha de solicitud')
            ->setCellValue('H1', 'Vencimiento SLA')
            ->setCellValue('I1', 'Estado')
        ;

        $datos = array();

        if ($user->hasRole('ROLE_HELP_CENTER_APPROVER')) {
            $datos = array_merge($datos, $data['help_center_pending_eval']);
            $datos = array_merge($datos, $data['openRequirements']);
            $datos = array_merge($datos, $data['closedRequirements']);
        }

        if ($user->hasRole('ROLE_REQUIREMENT_SUPERVISOR')) {
            $datos = array_merge($datos, $data['supervisor_pending_eval']);
        }

        if ($user->hasRole('ROLE_REQUIREMENT_MANAGER')) {
            $datos = array_merge($datos, $data['manager_pending_eval']);
        }

        $count_row = 2;
        foreach($datos as $row) {
            /* @var Requirement $row */

            $phpExcelObject->getActiveSheet()
                ->setCellValue('A' . $count_row, $row->getOrderNumber())
                ->setCellValue('B' . $count_row, 'Ficha Req')
                ->setCellValue('C' . $count_row, $row->getType()->getName())
                ->setCellValue('D' . $count_row, $row->getOffice()->getName())
                ->setCellValue('E' . $count_row, $row->getOffice()->getCostCenter())
                ->setCellValue('F' . $count_row, $row->getRequester()->getEmployee()->getFullName())
                ->setCellValue('G' . $count_row, date('d/m/Y', $row->getCreatedAt()->getTimestamp()))
                ->setCellValue('H' . $count_row, date('d/m/Y', $row->getStateDueDate()->getTimestamp()))
                ->setCellValue('I' . $count_row, $this->translator->trans($row->getStatus()))
            ;

            $count_row++;
        }

        foreach(range('A','I') as $columnID) {
            $phpExcelObject->getActiveSheet()->getColumnDimension($columnID)
                ->setAutoSize(true);
        }

        $phpExcelObject->getActiveSheet()->setTitle('Simple');
        $phpExcelObject->setActiveSheetIndex(0);

        $writer = $this->phpexcel->createWriter($phpExcelObject, 'Excel5');
        $response = $this->phpexcel->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $hash = "fichas" . time() . '.xls'
        );

        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'private');
        $response->headers->set('Cache-Control', 'maxage=-1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    /**
     * @Breadcrumb("Portada", route={"name"="dashboard"})
     * @Breadcrumb("Solicitudes")
     * @Rest\Get("/solicitudes", name="requisitions_index")
     * @Rest\View("dashboard/indexAll.twig")
     * @param string $type
     * @return mixed|null
     */
    public function indexAll(Request $request)
    {
        return $this->GetDataRoles($request);
    }

    /**
     * @Method({"GET", "POST"})
     * @Route("/", name="dashboard")
     * @View("dashboard/index.twig")
     */
    public function index(Request $request)
    {
        return $this->GetDataRoles($request);
    }

    public function approverDashboard()
    {
        return [];
    }

    public function approverVcardDashboard()
    {
        return [];
    }

    public function requirementRequesterDashboard()
    {
        return [];
    }

    public function requesterDashboard()
    {
        return [
            /*'requester_pending_approval' => $this->requisitionRepo->findRequesterPendingEvaluation(),
            'requester_pending_cert'     => null,
            //'requester_pending_cert'     => $this->requisitionRepo->findRequesterPendingCertification(),
            'requester_historical'       => $this->requisitionRepo->findUserHistory(),*/
        ];
    }

    public function supplierDashboard()
    {
        return [];
    }

    public function backofficeDashboard()
    {
        return [
            'requisition'        => $this->requisitionRepo->findBy(['type' => Requisition::TYPE_PRODUCT]),
            'maintenance'        => $this->requisitionRepo->findBy(['type' => Requisition::TYPE_MAINTENANCE]),
            'selfmanagement'     => $this->requisitionRepo->findBy(['type' => Requisition::TYPE_SELF_MANAGEMENT]),
            'formSearchProducts' => $this->createForm(BackOfficeSearchRequisitionType::class)->createView(),
            //'pep'                => $this->pepRepo->findAll(),
        ];
    }

    public function helpDeskDashboard()
    {
        $form = $this->createForm(UserType::class);

        return [
            'form' => $form->createView()
        ];
    }

    public function adminDashboard()
    {
        $form = $this->createForm(UserType::class);

        return [
            'form' => $form->createView()
        ];
    }

    public function approverSpecialDashboard()
    {
        $repo = $this->get('special_request_repo');
        $userId = $this->getUser()->getId();

        return [
            'pending_special_requests' => $repo->findBy([
                'assignee' => $userId,
                'state' => SpecialRequest::STATE_EVALUATION
            ]),
            'open_special_requests' => $repo->findByAssigneeAndState(
                $this->getUser(),
                [
                    SpecialRequest::STATE_IN_PROGRESS,
                    SpecialRequest::STATE_REJECTED,
                    SpecialRequest::STATE_APPEALED,
                ]
            ),
            'closed_special_requests' => $repo->findByAssigneeAndState(
                $this->getUser(),
                [
                    SpecialRequest::STATE_CLOSED,
                    SpecialRequest::STATE_REJECTED_FINAL,
                ]
            ),
        ];
    }

    /**
     * @Method({"GET"})
     * @Route("/aceptar-solicitudes", name="bulk_accept_command")
     * @return Response
     */
    public function bulkAcceptRequisitions()
    {
        $lockHandler = new LockHandler('lock.bulk.accept');
        if ($lockHandler->lock()) {
            $binPath = realpath(dirname(__DIR__, 3));
            $binPhpPath = PHP_BINDIR;
            $command = "$binPhpPath/php $binPath/bin/console s:a:req --env=$this->env > /dev/null 2>&1 &";
            exec($command);
        }

        return new Response("Command succesfully executed from the controller");
    }
}
