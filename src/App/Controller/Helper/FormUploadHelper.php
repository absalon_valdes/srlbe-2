<?php 

namespace App\Controller\Helper;

use JMS\DiExtraBundle\Annotation\Inject;
use JMS\DiExtraBundle\Annotation\Service;

/**
 * @Service("form_upload_helper")
 */
class FormUploadHelper
{
    /**
     * @Inject("%uploads_dir%")
     */
    public $uploadDir;
    
    public function upload($files)
    {
        $uploadedFiles = array();

        if (is_null($files)) {
            return null;
        }

        if (!is_array($files)) {
            $files = array($files);
        }

        foreach ($files as $file) {
            if (is_null($file)) {
                continue;
            }
            
            $original = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $newName = sha1($original.time()).($extension ? '.'.$extension : null);
            $file->move($this->uploadDir, $newName);

            $uploadedFiles[$original] = $newName;
        }

        return empty($uploadedFiles) ? null : $uploadedFiles;
    }
}