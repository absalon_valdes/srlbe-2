<?php

namespace App\Controller\Helper;

use App\Entity\Category;
use App\Helper\RedisHandler;
use App\Repository\PepGrantRepository;
use App\Repository\RequirementRepository;
use App\Repository\RequisitionRepository;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * @Di\Service("back_office_get_alerts")
 */
class BackOfficeAlerts
{
    const KEY = 'PROCESS_UPDATE';

    /**
     * @var RedisHandler $redisHandler
     * @Di\Inject("redis_handler")
     */
    public $redisHandler;

    public function getLastAlerts()
    {
        return $this->redisHandler->read(self::KEY);
    }
}
