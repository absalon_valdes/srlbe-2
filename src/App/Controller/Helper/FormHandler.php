<?php

namespace App\Controller\Helper;

use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Di\Service("form_handler")
 */
class FormHandler
{
    /**
     * @Di\Inject("form.factory")
     * @var FormFactory $formFactory
     */
    public $formFactory;

    /**
     * @param Request $request
     * @param $formType
     * @param null $data
     * @param array $options
     * @return mixed|Form
     */
    
    public function processForm(Request $request, $formType, $data = null, $options = [])
    {
        /** @var Form $form */
        $form = $this->formFactory->create($formType, $data, $options);
        $form->handleRequest($request);
        if (!$form->isValid()) {
            return $form;
        }
        
        return $form->getData();
    }
}
