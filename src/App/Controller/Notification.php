<?php

namespace App\Controller;

use App\Entity\Message;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\DiExtraBundle\Annotation as Di;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;

/**
 * @Breadcrumb("Portada", route={"name"="dashboard"}) 
 * @Extra\Route("/notificaciones")
 */
class Notification extends FOSRestController
{
    /**
     * @var \App\Repository\MessageRepository
     * @Di\Inject("message_repo")
     */
    protected $messagesRepo;

    /**
     * @Breadcrumb("Notificaciones")
     * @Rest\Get(name="notifications")
     * @Rest\View("notification/index.twig")
     */
    public function index()
    {
        return $this->messagesRepo->findByReceiver($this->getUser());
    }

    /**
     * @Breadcrumb("Notificaciones", route={"name"="notifications"})
     * @Breadcrumb("{message.subject}")
     * @Extra\Security("user == message.getReceiver()")
     * @Rest\Get("/{id}", name="notification_detail")
     * @Rest\View("notification/detail.twig", templateVar="notification")
     * @param Message $message
     * @return Message
     */
    public function detail(Message $message)
    {
        $this->messagesRepo->readMessage($message);

        /** @var Message $message */
        return $message;
    }

    /**
     * @Extra\Security("user == message.getReceiver()")
     * @Rest\Post("/{id}/read", name="notification_read")
     * @param Message $message
     * @return \FOS\RestBundle\View\View
     */
    public function markAsRead(Message $message)
    {
        $this->messagesRepo->readMessage($message);

        return $this->routeRedirectView('notifications');
    }

    /**
     * @Extra\Security("user == message.getReceiver()")
     * @Rest\Post("/{id}", name="notification_delete")
     * @param Message $message
     * @return \FOS\RestBundle\View\View
     */
    public function delete(Message $message)
    {
        $this->messagesRepo->remove($message, true);

        return $this->routeRedirectView('notifications');
    }
}
