<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Entity\Office;
use App\Entity\Position;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Export\VcardRequisitionExporter;
use App\Form\DelegateVcardType;
use App\Form\ProductAppealRejectionType;
use App\Form\ProductAppealType;
use App\Form\ProductRejectionType;
use App\Form\RequisitionType;
use App\Form\VcardCertificationType;
use App\Form\VcardDataDownloadType;
use App\Form\VcardsType;
use App\Form\VcardSupplierCertification;
use App\Form\WorkEvaluationType;
use App\Form\WorkSupplierRatingType;
use App\Helper\RequisitionTypeResolver;
use App\Helper\TransitionHelper;
use App\Model\VcardCertification;
use App\Model\VcardRequisition;
use App\Repository\ContractRepository;
use App\Repository\EmployeeRepository;
use App\Repository\OfficeRepository;
use App\Repository\ProductRepository;
use App\Repository\RequisitionRepository;
use App\Repository\UserRepository;
use App\Workflow\ProcessInstance;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View as RestView;
use JMS\DiExtraBundle\Annotation as Di;
use JMS\DiExtraBundle\Annotation\Inject;
use JMS\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Workflow\Workflow;

/**
 * @Route("/tarjetas")
 * @Breadcrumb("Portada", route={"name"="dashboard"})
 */
class VcardsController extends FOSRestController
{
    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    protected $workflow;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    protected $requisitionRepo;

    /**
     * @var RequisitionTypeResolver
     * @Di\Inject("requisition_type_resolver")
     */
    protected $typeResolver;

    /**
     * @Inject("apy_breadcrumb_trail")
     */
    protected $breadcrumb;

    /**
     * @DI\Inject("category_trail_helper")
     */
    protected $categoryTrail;

    /**
     * @var UserRepository
     * @DI\Inject("user_repo")
     */
    protected $userRepo;

    /**
     * @var EmployeeRepository
     * @DI\Inject("employee_repo")
     */
    protected $employeeRepo;

    /**
     * @var OfficeRepository
     * @DI\Inject("office_repo")
     */
    protected $officeRepo;

    /**
     * @var ProductRepository
     * @DI\Inject("product_repo")
     */
    protected $productRepo;

    /**
     * @var ContractRepository
     * @DI\Inject("contract_repo")
     */
    protected $contractRepo;

    /**
     * @var EventDispatcherInterface
     * @DI\Inject("event_dispatcher")
     */
    protected $dispatcher;

    /**
     * @var VcardRequisitionExporter
     * @DI\Inject("vcard_requisition_exporter")
     */
    protected $vcardExporter;

    /**
     * @var TransitionHelper
     * @DI\Inject("transition_helper")
     */
    public $transitionHelper;

    /**
     * @Di\Inject("form_upload_helper")
     */
    protected $uploadHelper;

    /**
     * @var string
     * @DI\Inject("%kernel.cache_dir%")
     */
    public $cacheDir;

    /**
     * @var Serializer
     * @Inject("serializer")
     */
    public $serializer;

    /**
     * @Inject("%uploads_dir%")
     */
    public $uploadDir;

    /**
     * @Inject("%double_vcards%")
     */
    public $doubleVcards;

    /**
     * @Rest\Get("/direcciones-region/{id}", name="vcard_region_offices", defaults={"id"=null})
     * @param Office $office
     * @return array
     */
    public function regionOfficesAction(Office $office = null)
    {
        return $this->getDoctrine()->getManager()
            ->getRepository(Office::class)
            ->createQueryBuilder('o')
            ->select('o.id, o.address as text')
            ->leftJoin('o.city', 'c')
            ->leftJoin('c.parent', 'r')
            ->where('r = :region')
            ->setParameter('region', $office->getCity()->getParent())
            ->getQuery()
            ->getArrayResult()
        ;
    }

    /**
     * @Rest\Get("/detalle-sucursal/{id}", name="vcard_office_detail", defaults={"id"=null})
     * @param Office $office
     * @return array
     */
    public function officeDetailsAction(Office $office = null)
    {
        return [
            'address' => $office->getAddress(),
            'floor' => $office->getAddressDetail(),
            'city' => $office->getCity()->getName(),
        ];
    }

    /**
     * @Breadcrumb("Productos")
     * @Breadcrumb("Tarjeta de presentación")
     * @ParamConverter("employee", class="App:Employee", options={
     *     "map_method_signature" = true,
     *     "repository_method" = "findForVcard"
     * })
     * @ParamConverter("vcards", class="App:Product", options={
     *     "map_method_signature" = true,
     *     "repository_method" = "findVcardsForEmployee"
     * })
     * @Method({"GET", "POST"})
     * @Route("/{rut}", name="vcard_available", defaults={"rut"=null})
     * @View("vcard/index.twig")
     * @param Request $request
     * @param Employee $employee
     * @param $vcards
     * @return array|\FOS\RestBundle\View\View
     */
    public function catalog(Request $request, Employee $employee, $vcards)
    {
        $form = $this->createForm(DelegateVcardType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->routeRedirectView('vcard_available', [
                'rut' => $form->getData()['rut']
            ]);
        }

        return [
            'employee' => $employee,
            'form' => $form->createView(),
            'products' => $vcards,
            'category' => isset($vcards[0]) ? $vcards[0]->getCategory() : null
        ];
    }

    /**
     * @Breadcrumb("Productos")
     * @Breadcrumb("Tarjeta de presentación")
     * @Extra\Security("is_granted('request_vcard', employee)")
     * @Extra\Method({"GET", "POST"})
     * @Rest\Route("/pedidos/{id}", name="vcards_request", requirements={"id"=".+", "code"=".+"})
     * @Rest\View("vcard/request.twig")
     * @param Request $request
     * @param Employee $employee
     * @return array|\FOS\RestBundle\View\View|Response
     * @internal param Product $product
     */
    public function request(Request $request, Employee $employee)
    {
        /** @var Product $product */
        $product = $this->productRepo->findOneBy([
            'code' => $request->query->get('code')
        ]);

        $contract = $this->contractRepo->findByProduct(
            $product, $employee->getUser()
        )[0];

        if ($this->typeResolver->getType($product) != Requisition::TYPE_VCARD){
            return $this->redirectToRoute('requisitions_request', [
                'slug' => $product->getCategory()->getSlug(),
                'code' => $product->getCode()
            ]);
        }

        /** @var Requisition $requisition */
        $requisition = $this->requisitionRepo->create();
        $requisition->setType(Requisition::TYPE_VCARD);
        $requisition->setProduct($product);
        $requisition->setRequester($this->getUser());
        $requisition->setStateDueDate(new \DateTime);
        $requisition->setContract($contract);
        $requisition->setStatus('borrador');

        $isDouble = in_array($product->getCode(), $this->doubleVcards, true);

        $data = new VcardRequisition($employee, $contract, $isDouble);
        $preSubmit = clone $data;

        $form = $this->createForm(VcardsType::class, $data);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $serialized = $this->serializer->serialize($data, 'json');

            $requisition->setQuantity($data->getQuantity());
            $requisition->addMetadata('vcards', $serialized);

            $assignee = $this->userRepo->findByRole('ROLE_APPROVER_VCARD');

            if (!$preSubmit->equalTo($data)) {
                $requisition->setAssignee($assignee);
                $nextState = 'enviar_a_evaluacion';
            } else {
                $supplier = $requisition->getContract()->getSupplierUser();
                $requisition->setAssignee($supplier);
                $nextState = 'enviar_a_ejecucion';
            }

            $workflow = $this->get('workflow.vcards');

            if ($workflow->can($requisition, $nextState)) {
                $workflow->apply($requisition, $nextState);
                $this->requisitionRepo->setNextNumber($requisition);
                $this->get('tionvel_interop.vcard_notification')->notify($requisition);
            }

            $this->requisitionRepo->save($requisition);
        }

        return [
            'vcards' => $form->createView(),
            'data' => $data,
            'contract' => $contract,
        ];
    }

    /**
     * @Breadcrumb("Solicitudes", route="requisitions_index")
     * @Breadcrumb("Solicitud n° {requisition.number}")
     * @Method({"GET", "POST"})
     * @Route("/detalle/{id}", name="vcards_detalle")
     * @View("vcard/detail.twig")
     * @param Request $request
     * @param Requisition $requisition
     * @return array
     */
    public function detailAction(Request $request, Requisition $requisition)
    {
        $vcards = $requisition->getMetadata('vcards');
        $certify = $requisition->getMetadata('certificacion') ?? "{}";

        $cert = new VcardCertification();
        $form = $this->createForm(VcardCertificationType::class, $cert);
        $form->handleRequest($request);

        return [
            'user' => $user = $requisition->getRequester(),
            'employee' => $employee = $this->employeeRepo->findOneByUser($user),
            'office' => $office = $employee->getOffice(),
            'alternate' => $this->officeRepo->findAlternateRequester($office, $user),
            'requisition' => $requisition,
            'vcards_data' => $this->serializer->deserialize($vcards, VcardRequisition::class, 'json'),
            'certificacion_proveedor' => $this->serializer->deserialize($certify, VcardCertification::class, 'json'),
            'rejected' => $this->createForm(ProductRejectionType::class)->createView(),
            'appeal' => $this->createForm(ProductAppealType::class)->createView(),
            'deny' => $this->createForm(ProductAppealRejectionType::class)->createView(),
            'evaluation' => $this->createForm(WorkEvaluationType::class, $requisition)->createView(),
            'supplier_rating' => $this->createForm(WorkSupplierRatingType::class)->createView(),
            'download_data' => $this->createForm(VcardDataDownloadType::class)->createView(),
            'supplier_cert' => $form->createView(),
        ];
    }

    /**
     * @Post("/{id}/aprobado", name="vcards_aprobado")
     * @param Requisition $requisition
     * @return \FOS\RestBundle\View\View|null
     */
    public function approver(Requisition $requisition)
    {
        try {
            /** @var Workflow $workflow */
            $workflow = $this->get('workflow.vcards');

            if ($workflow->can($requisition, 'en_evaluacion')) {
                $workflow->apply($requisition, 'en_evaluacion');
                $supplier = $requisition->getContract()->getSupplierUser();
                $requisition->setAssignee($supplier);
                $this->requisitionRepo->save($requisition);

                return $this->view(null, Response::HTTP_OK);
            }

        } catch (\Exception $exception) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/apelacion-aprobado", name="vcards_apelacion_aprobado")
     * @param Requisition $requisition
     * @return \FOS\RestBundle\View\View|null
     */
    public function approverAppeal(Requisition $requisition)
    {
        $errorCode = Response::HTTP_OK;

        try {
            /** @var Workflow $workflow */
            $workflow = $this->get('workflow.vcards');

            if ($workflow->can($requisition, 'en_evaluacion_apelacion')) {
                $workflow->apply($requisition, 'en_evaluacion_apelacion');
                $supplier = $requisition->getContract()->getSupplierUser();
                $requisition->setAssignee($supplier);
                $this->requisitionRepo->save($requisition);
            }
        } catch (\Exception $exception) {
            $errorCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return $this->view(null, $errorCode);
    }

    /**
     * @Post("/{id}/cerrar-apelacion", name="vcards_cerrar_apelacion")
     * @param Requisition $requisition
     * @return \FOS\RestBundle\View\View
     */
    public function closedAppel(Requisition $requisition)
    {

        try {

            /** @var Workflow $workflow */
            $workflow = $this->get('workflow.vcards');

            if ($workflow->can($requisition, 'en_apelacion_cerrada')) {
                $workflow->apply($requisition, 'en_apelacion_cerrada');
                $requisition->setAssignee($requisition->getRequester());
                $this->requisitionRepo->save($requisition);

                return $this->view(null, Response::HTTP_OK);
            }

        } catch (\Exception $exception) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/rechazar", name="vcards_rechazar")
     * @param Request $request
     * @param Requisition $requisition
     * @return RestView|null
     */
    public function rejected(Request $request, Requisition $requisition)
    {
        try {

            $form = $this->createForm(ProductRejectionType::class);
            $form->handleRequest($request);

            /** @var Workflow $workflow */
            $workflow = $this->get('workflow.vcards');

            if ($form->isValid()) {

                $data = $form->getData();

                if ($workflow->can($requisition, 'en_rechazo')) {
                    $workflow->apply($requisition, 'en_rechazo');

                    $requisition->addMetadata('rechazado', [
                        'razon' => $data['reason'],
                        'comentario' => $data['comment'],
                    ]);

                    $requisition->setAssignee($requisition->getRequester());
                    $this->requisitionRepo->save($requisition);
                    $this->get('tionvel_interop.vcard_notification')->notify($requisition);

                    return $this->view(null, Response::HTTP_OK);
                }
            }
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/apelar", name="vcards_apelar")
     * @param Request $request
     * @param Requisition $requisition
     * @return RestView|null
     */
    public function appeal(Request $request, Requisition $requisition)
    {
        try {

            $form = $this->createForm(ProductAppealType::class);
            $form->handleRequest($request);

            /** @var Workflow $workflow */
            $workflow = $this->get('workflow.vcards');

            if ($form->isValid()) {
                $data = $form->getData();

                if ($workflow->can($requisition, 'en_apelacion')) {
                    $workflow->apply($requisition, 'en_apelacion');

                    $requisition->addMetadata('apelacion', [
                        'comentario' => $data['comment'],
                        'archivos' => $this->uploadHelper->upload($data['files'])
                    ]);

                    $this->resolveAssignee($requisition);
                    $this->requisitionRepo->save($requisition);

                    return $this->view(null, Response::HTTP_OK);
                }

            }
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Post("/{id}/rechazar-apelacion", name="vcard_rechazar_apelacion")
     * @param Request $request
     * @param Requisition $requisition
     * @return \FOS\RestBundle\View\View
     */
    public function deny(Request $request, Requisition $requisition)
    {
        try {
            $form = $this->createForm(ProductAppealRejectionType::class);
            $form->handleRequest($request);

            /** @var Workflow $workflow */
            $workflow = $this->get('workflow.vcards');

            if ($form->isValid()) {
                $data = $form->getData();

                if ($workflow->can($requisition, 'en_apelacion_rechazada')) {
                    $workflow->apply($requisition, 'en_apelacion_rechazada');

                    $requisition->addMetadata('denegado', [
                        'razon' => $data['reason'],
                        'comentario' => $data['comment'],
                        'archivos' => $this->uploadHelper->upload($data['files'])
                    ]);

                    $requisition->setAssignee($requisition->getRequester());
                    $this->requisitionRepo->save($requisition);

                    return $this->view(null, Response::HTTP_OK);
                }
            }
        } catch (\Exception $e) {
            return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @Breadcrumb("Solicitudes", route="requisitions_index")
     * @Breadcrumb("Solicitud n° {requisition.number}")
     * @Post("/{id}/certificar", name="vcards_certificar")
     * @param Request $request
     * @param Requisition $requisition
     * @return RestView|null|Response
     */
    public function certify(Request $request, Requisition $requisition)
    {
        $cert = new VcardCertification();
        $form = $this->createForm(VcardCertificationType::class, $cert);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $cert->uploadFiles($this->uploadDir);
            $serialized = $this->serializer->serialize($cert, 'json');

            $workflow = $this->get('workflow.vcards');

            if ($workflow->can($requisition, 'despachar_pedido')) {
                $workflow->apply($requisition, 'despachar_pedido');
                $requisition->addMetadata('certificacion', $serialized);
                $requisition->setAssignee($requisition->getRequester());
                $this->requisitionRepo->save($requisition);
                $this->get('tionvel_interop.vcard_notification')->notify($requisition);

                return $this->routeRedirectView('dashboard');
            }
        }

        return $this->forward('App:Vcards:detail', [
            'id' => $requisition->getId()
        ]);
    }

    /**
     * @Post("/exportar/vcard/{id}", name="report_vcard")
     * @param Request $request
     * @param Requisition $requisition
     * @return RestView|string|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function vcardReport(Request $request, Requisition $requisition)
    {
        $form = $this->createForm(VcardDataDownloadType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $choice = $form->getData()['choice'];
            $user = $this->getUser();

            if (VcardDataDownloadType::THIS_REQUISITION === $choice) {
                return $this->vcardExporter->exportSingle($requisition);
            } else if (VcardDataDownloadType::ALL_CLOSED === $choice) {
                return $this->vcardExporter->exportClosed($user);
            } else if (VcardDataDownloadType::ALL_OPEN === $choice) {
                return $this->vcardExporter->exportOpen($user);
            }
        }

        return $this->view($form, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @Rest\Get("/descargar/{fileId}", name="report_vcard_download")
     * @param $fileId
     * @return BinaryFileResponse
     */
    public function downloadVcardReport($fileId)
    {
        $filename = sprintf('%s/vcard_report/%s.xlsx',
            $this->cacheDir, $fileId);

        if (!file_exists($filename)) {
            throw $this->createNotFoundException();
        }

        BinaryFileResponse::trustXSendfileTypeHeader();

        $response = new BinaryFileResponse($filename);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileId.'.xlsx',
            iconv('UTF-8', 'ASCII//TRANSLIT', $fileId.'.xlsx')
        );

        return $response;
    }

    /**
     * @return mixed
     */
    private function resolveAssignee()
    {
        $assignee = $this->userRepo->findByRole('ROLE_APPROVER_VCARD');

        if (!$assignee) {
            throw new \RuntimeException('No se ha podido determinar el usuario a asignar');
        }

        return $assignee;
    }

    /**
     * @Post("/{id}/recibir-despacho", name="recibir_despacho", defaults={"step"="recibir_despacho"})
     * @Post("/{id}/evaluar-entrega", name="evaluacion_trabajo", defaults={"step"="evaluar_entrega"})
     * @Post("/{id}/calificar-proveedor", name="calificar_proveedor", defaults={"step"="calificar_proveedor"})
     * @Post("/{id}/finalizar-solicitud", name="finalizar_solicitud", defaults={"step"="finalizar_solicitud"})
     *
     * @param Request $request
     * @param Requisition $requisition
     * @param $step
     * @return RestView
     */
    public function processStep(Request $request, Requisition $requisition, $step)
    {
        $stepFormMap = [
            'evaluar_entrega' => WorkEvaluationType::class,
            'calificar_proveedor' => WorkSupplierRatingType::class,
        ];

        $workflow = $this->get('workflow.vcards');

        if ($workflow->can($requisition, $step)) {
            $workflow->apply($requisition, $step);
            $this->requisitionRepo->save($requisition);

            if (!isset($stepFormMap[$step])) {
                return $this->view(null, Response::HTTP_OK);
            }

            return $this->handleForm($request, $requisition, $stepFormMap[$step]);
        }

        return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    private function handleForm(Request $request, Requisition $requisition, $formClass)
    {
        if (WorkEvaluationType::class === $formClass) {
            $form = $this->createForm($formClass, $requisition);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $this->requisitionRepo->save($requisition);
                return $this->view(null, Response::HTTP_OK);
            }
        }

        if (WorkSupplierRatingType::class === $formClass) {
            $form = $this->createForm($formClass);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $supplier = $requisition->getContract()->getSupplier();
                $supplier->addRating($data = $form->getData());

                $requisition->addMetadata('supplier_rating', $data);

                $this->requisitionRepo->save($requisition);
                $this->requisitionRepo->save($supplier);

                $workflow = $this->get('workflow.vcards');
                if ($workflow->can($requisition, 'finalizar_solicitud')) {
                    $workflow->apply($requisition, 'finalizar_solicitud');
                    $this->requisitionRepo->save($requisition);

                    return $this->routeRedirectView('dashboard');
                }
            }
        }

        return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
