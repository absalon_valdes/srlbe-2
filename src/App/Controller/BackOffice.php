<?php

namespace App\Controller;

use App\Controller\Helper\BackOfficeAlerts;
use App\Controller\Helper\FormHandler;
use App\Entity\Category;
use App\Entity\Requisition;
use App\Export\PepBackOfficeExporter;
use App\Export\RequirementBackOfficeExporter;
use App\Export\RequisitionBackOfficeExporter;
use App\Export\RequisitionExporter;
use App\Form\BackOfficeSearchPepGrantType;
use App\Form\BackOfficeSearchRequisitionType;
use App\Form\RequirementSearchType;
use App\Repository\CategoryRepository;
use App\Repository\PepGrantRepository;
use App\Repository\RequirementRepository;
use App\Repository\RequisitionRepository;
use App\Workflow\Status\PepStatus;
use App\Workflow\Status\ProductStatus;
use App\Workflow\Status\RequirementStatus;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as Di;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/backoffice")
 * @Extra\Security("is_granted('ROLE_BACKOFFICE')")
 */
class BackOffice extends FOSRestController
{
    /**
     * @var BackOfficeAlerts
     * @Di\Inject("back_office_get_alerts")
     */
    public $backOfficeAlerts;

    /**
     * @var FormHandler
     * @Di\Inject("form_handler")
     */
    public $formHandler;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var PepGrantRepository
     * @Di\Inject("pep_grant_repo")
     */
    public $pepRepo;

    /**
     * @var CategoryRepository $categoryRepo
     * @Di\Inject("category_repo")
     */
    public $categoryRepo;


    /**
     * @var RequisitionBackOfficeExporter $requisitionExporter 
     * @Di\Inject("backoffice.requisition.exporter")
     */
    public $requisitionExporter;

    /**
     * @var RequisitionExporter $completeRequisitionExporter
     * @Di\Inject("requisition.exporter")
     */
    public $completeRequisitionExporter;

    /**
     * @var PepBackOfficeExporter $pepExporter
     * @Di\Inject("backoffice.pep.exporter")
     */
    public $pepExporter;
    
    /**
     * @Rest\Post("/alertas", name="backoffice_alerts")
     */
    public function getAlerts()
    {
        $html = $this->render(
            'backoffice/_partial/_render_alerts.twig',
            ['alerts' => $this->backOfficeAlerts->getLastAlerts()]
        );

        return [$html];
    }

    /**
     * @Extra\Method({"POST", "GET"})
     * @Route("/pedidos", name="backoffice_products")
     * @Rest\View("backoffice/products.twig")
     */
    public function indexProducts()
    {
        return ['formSearchProducts' => $this->createForm(BackOfficeSearchRequisitionType::class)->createView()];
    }

    /**
     * @Extra\Method({"POST", "GET"})
     * @Route("/mantenimiento", name="backoffice_maintenance")
     * @Rest\View("backoffice/maintenance.twig")
     */
    public function indexMaintenance()
    {
        return ['formSearchProducts' => $this->createForm(BackOfficeSearchRequisitionType::class)->createView()];
    }

    /**
     * @Extra\Method({"POST", "GET"})
     * @Route("/autogestion", name="backoffice_self_management")
     * @Rest\View("backoffice/self_management.twig")
     */
    public function indexSelfManagement()
    {
        return ['formSearchProducts' => $this->createForm(BackOfficeSearchRequisitionType::class)->createView()];
    }

    /**
     * @Extra\Method({"POST", "GET"})
     * @Route("/pep", name="backoffice_pep")
     * @Rest\View("backoffice/pep.twig")
     */
    public function indexPep()
    {
        return ['formSearchPeps' => $this->createForm(BackOfficeSearchPepGrantType::class)->createView()];
    }

    /**
     * @Rest\Post("/buscar/pedidos/{type}", name="backoffice_search_requisition")
     */
    public function getRequisitionSearchResults($type, Request $request)
    {
        $form = $this->createForm(BackOfficeSearchRequisitionType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $results = $this->requisitionRepo->searchRequisitions($data, $type);
            $results = (!isset($results['error'])) ? $results : null;
            return [$this->render(
                'backoffice/_partial/_render_search.twig',
                [
                    'results' => $results,
                    'table_type' => 'requisition',
                    'in_sla' => $data["in_sla"]
                ]
            )];
        }

        return $this->view(null, Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    /**
     * @Extra\Method({"GET", "POST"})
     * @Route("/reporte/buscar/pedidos", name="backoffice_report_requisition")
     */
    public function getRequisitionSearchReport( Request $request)
    {
        $data = json_decode($request->query->get('data'), true);

        $data['city'] = $data['city'] !== 'Todas' ? $this->categoryRepo->findOneByName($data['city']) : 'cities.all';
        $data['region'] = $data['region'] !== 'Todas' ? $this->categoryRepo->findOneByName($data['region']) : 'region.all';
        $data['status'] = $data['status'] !== 'Todos' ? $data['status'] : 'requisition.all';
        $data['createdAt'] = !$data['createdAt'] ? null : \DateTime::createFromFormat('d/m/Y', $data['createdAt']);
        $results = $this->requisitionRepo->searchRequisitions($data, $data["type"]);

        if (isset($results['error'])) {
            $typeCreatedStatus = [
                Requisition::TYPE_PRODUCT => ProductStatus::CREATED,
                Requisition::TYPE_MAINTENANCE => ProductStatus::CREATED,
                Requisition::TYPE_SELF_MANAGEMENT => ProductStatus::APPROVED,
            ];
            $results = $this->requisitionRepo->findBy(['type' => $data["type"], 'status' => $typeCreatedStatus[$data["type"]]]);
        }
        return $this->requisitionExporter->export($data["type"], $results)->getResponse();
    }
    /**
     * @Extra\Method({"GET", "POST"})
     * @Route("/reporte-completo/buscar/pedidos", name="backoffice_complete_report_requisition")
     */
    public function getRequisitionSearchCompleteReport( Request $request)
    {
        $data = json_decode($request->query->get('data'), true);

        $data['city']   = $data['city']   !== 'Todas' ? $this->categoryRepo->findOneByName($data['city'])   : 'cities.all';
        $data['region'] = $data['region'] !== 'Todas' ? $this->categoryRepo->findOneByName($data['region']) : 'region.all';
        $data['status'] = $data['status'] !== 'Todos' ? $data['status'] : 'requisition.all';
        $data['createdAt'] = !$data['createdAt'] ? null : \DateTime::createFromFormat('d/m/Y', $data['createdAt']);
        $results = $this->requisitionRepo->searchRequisitions($data, $data["type"]);

        if (isset($results['error'])) {
            $typeCreatedStatus = [
                Requisition::TYPE_PRODUCT          => ProductStatus::CREATED,
                Requisition::TYPE_MAINTENANCE      => ProductStatus::CREATED,
                Requisition::TYPE_SELF_MANAGEMENT  => ProductStatus::APPROVED,
            ];
            $results = $this->requisitionRepo->findBy(['type' => $data["type"], 'status' => $typeCreatedStatus[$data["type"]]]);
        }

        return $this->completeRequisitionExporter->export(null, null,array_map(function($o) { return $o->getNumber(); }, $results))->getResponse();
    }
    
    /**
     * @Rest\Post("/buscar/pep", name="backoffice_search_pep")
     */
    public function getPepSearchResults(Request $request)
    {
        $data = $this->formHandler->processForm($request, BackOfficeSearchPepGrantType::class);
        if (!is_array($data)) {
            return $this->view(null, 500);
        }

        $results = $this->pepRepo->searchPepGrant($data);
        $results = (count($results) > 0) ? $results : null;
        $html = $this->render(
            'backoffice/_partial/_render_search.twig',
            [
                'results' => $results,
                'table_type' => 'pep',
            ]
        );

        return [$html];
    }

    /**
     * @Extra\Method({"GET", "POST"})
     * @Route("/reporte/buscar/pep", name="backoffice_report_pep")
     * @return array
     */
    public function getPepSearchReport(Request $request)
    {
        $data = json_decode($request->query->get('data'), true);

        $data['city']   = $data['city']   !== 'Todas' ? $this->categoryRepo->findOneByName($data['city'])   : 'cities.all';
        $data['region'] = $data['region'] !== 'Todas' ? $this->categoryRepo->findOneByName($data['region']) : 'region.all';
        $data['createdAt'] = !$data['createdAt'] ? null : \DateTime::createFromFormat('d/m/Y', $data['createdAt']);  
        unset($data['type']);
        
        $results = $this->pepRepo->searchPepGrant($data);

        if (isset($results['error'])) {
            $results = $this->pepRepo->findBy(['status' => PepStatus::CREATED]);
        }
        
        return $this->pepExporter->export(null, $results)->getResponse();
    }


    /**
     * @Rest\Get("/sub-categorias/{parent}", name="get_subcategories")
     * @param Category $parent
     * @return array
     *
     */
    public function getSubCategories(Category $parent = null)
    {
        $result = [];
        if($parent){
            $result = $this->categoryRepo->findByParentAsArray($parent);
        }
        return ['results' => $result];
    }


    /**
     * @Rest\Get("/categorias/{type}", name="get_categories_by_type")
     * @param Request $request
     * @param string $type
     * @return array
     *
     */
    public function getCategoriesByType(Request $request,$type){
        $results = [];
        $all = $this->categoryRepo->findAllContainingInSlug('productos',$request->get('q'));
        switch ($type) {
            case Requisition::TYPE_PRODUCT:
                $categories    = array_filter($all,function(Category $c){
                    return !$c->isChildOf( $this->categoryRepo->findOneByName('Reparaciones'))
                        &&  !$c->isChildOf($this->categoryRepo->findOneByName('Mantenciones'))
                        &&  !$c->isChildOf($this->categoryRepo->findOneByName('Uniformes'))
                        && !preg_match('/(limpieza|reparaciones|mantenciones|uniformes)$/', $c->getSlug()  )
                        && !strpos($c->getSlug(), 'tarjetas' )
                        && !is_null($c->getParent())
                        && $c->getLvl() === 1
                        ;
                });
                break;
            case Requisition::TYPE_SELF_MANAGEMENT:
                $categories    = array_filter($all,function(Category $c){
                    return $c->isChildOf( $this->categoryRepo->findOneByName('Reparaciones'));
                });
                break;
            case Requisition::TYPE_MAINTENANCE:
                $categories    = array_filter($all,function(Category $c){
                    return $c->isChildOf( $this->categoryRepo->findOneByName('Mantenciones'));
                });
                break;
        }

        foreach ($categories as $c){
            $results[] = ['id' => $c->getId(),'text' => $c->getName()];
        }

        return ['results' => $results];
    }
}
