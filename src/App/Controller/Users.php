<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use App\Form\EmployeeProfileType;
use App\Form\RememberPasswordType;
use App\Form\UserProfileType;
use App\Helper\UserHelper;
use App\Repository\EmployeeRepository;
use App\Repository\UserRepository;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation\Inject;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\SwitchUserRole;
use App\Entity\Employee;
use App\Entity\User;
use Doctrine\ORM\Query\Expr\Join;

/**
 * @Route("/usuario")
 */
class Users extends FOSRestController
{
    /**
     * @var UserHelper
     * @Inject("user_helper")
     */
    protected $userHelper;

    /**
     * @var UserRepository
     * @Inject("user_repo")
     */
    public $userRepo;
    /**
     * @var EmployeeRepository
     * @Inject("employee_repo")
     */
    public $employeeRepo;

    /**
     * @var EventDispatcherInterface
     * @Inject("event_dispatcher")
     */
    public $dispatcher;
    
    /**
     * @var TokenStorageInterface
     * @Inject("security.token_storage")
     */
    public $tokenStorage;

    /**
     * @Inject("%notification_templates%")
     */
    public $path;

    /**
     * @Inject("mailer")
     */
    public $mailer;

    /**
     * @Inject("%notification_sender%")
     */
    public $sender;

    /**
     * @Method({"GET", "POST"})
     * @Route("/perfil", name="user_profile")
     * @View("user/profile.twig")
     * @param Request $request
     * @return array
     * @throws \OutOfBoundsException
     * @throws \LogicException
     */
    public function profile(Request $request)
    {
        $employeeInfo = $this->employeeRepo->findOneByUser($this->getUser());

        if (!$employeeInfo) {
            $form = $this->createForm(UserProfileType::class);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $password = $form->get('plainPassword')->getData();
                $this->userHelper->changePassword($password);
                $request->getSession()->invalidate();
            }

            return [
                    'userInfo'  => $this->getUser(),
                    'form'         => $form,
                   ];
        }

        $form = $this->createForm(EmployeeProfileType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $form->get('plainPassword')->getData();
            $this->userHelper->changePassword($password);
            $request->getSession()->invalidate();
        }

        return [
                    'userInfo'  => $this->getUser(),
                    'employeeInfo' => $employeeInfo,
                    'form'         => $form,
               ];
    }

    /**
     * @Security("!user.isSecured()")
     * @Method({"GET", "POST"})
     * @Route("/primer-login", name="first_login")
     * @View("user/first-login.twig")
     * @param Request $request
     * @return \FOS\RestBundle\View\View|\Symfony\Component\Form\FormView
     */
    public function firstLogin(Request $request)
    {
        $form = $this->createForm(ChangePasswordType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $password = $form->get('password')->getData();
            $this->userHelper->changePassword($password);
            $request->getSession()->invalidate();

            return $this->routeRedirectView('fos_user_security_logout');
        }

        return $form->createView();
    }

    /**
     * @Get("/encontrar-usuario", name="find_user")
     * @param Request $request
     * @return View
     */
    public function findUser(Request $request)
    {
        $result = [];
        
        if (!empty($q = $request->get('q', null))) {
            $result = $this->userRepo->createQueryBuilder('u')
                ->select('u.id, u.username as text')
                ->where('u.username LIKE :username')
                ->orderBy('u.username', 'ASC')
                ->setParameter('username', '%'.$q.'%')
                ->getQuery()
                ->getArrayResult()
            ;
        }
        
        return ['results' => $result];
    }

    /**
     * @Get("/encontrar-usuario-xnombre", name="find_user_byname")
     * @param Request $request
     * @return View
    */
    public function findUserByName(Request $request)
    {
        $result = [];

        if (!empty($q = $request->get('q', null))) {
            $qb = $this->employeeRepo->createQueryBuilder('e');
            $result = $qb
                ->select("u.id, CONCAT(e.name,' ',e.lastName) as text")
                ->innerJoin(User::class, 'u', Join::WITH, $qb->expr()->eq('u.employee', 'e'))
                ->where(
                    $qb->expr()->orX(
                        $qb->expr()->like('e.name', ':username'),
                        $qb->expr()->like('e.lastName', ':username')
                    )
                )
                ->orderBy('text', 'ASC')
                ->setParameter('username','%'.$q.'%')
                ->getQuery()
                ->getArrayResult()
            ;
        }

        return ['results' => $result];
    }

    /**
     * @Get("/remember-pass", name="modal_remember_pass")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return View
    */
    public function rememberPass(Request $request)
    {
        $success = false;
        $form = $this->createForm(RememberPasswordType::class);
        if($request->getMethod() == 'POST'){
            $form->handleRequest($request);
            if($form->isValid()){
                if($this->sendRememberPassEmail($form->get('username')->getData())){
                    $this->get('session')->getFlashBag()->add('success','true');
                    $success = true;
                }
            }
        }
        return $this->render(
            'user/modal_remember_pass.html.twig',
            ['form' => $form->createView(),'username' => $request->get('username'),'success' => $success]
        );
    }


    private function sendRememberPassEmail($username){
        /** @var User $user */
        $user = $this->userRepo->findOneByUsername($username);

        if(!$user){
            $this->get('session')->getFlashBag()->add('error','Usuario no encontrado');
            return;
        }
        if(!$user->getIsEmployee() && in_array('ROLE_SUPPLIER',$user->getRoles())){
            $rut = $user->getUsername();
        }elseif($user->getIsEmployee()){
            $rut = explode("-",$user->getEmployee()->getRut())[0];
        }else{
            $this->get('session')->getFlashBag()->add('error','Contacte al administrador para cambiar su contraseña');
            return;
        }
        try {
            $html = $this->renderView($this->path.'/email.remember.pass.twig', ['rut' => $rut]);
            $message = \Swift_Message::newInstance()
                ->setSubject('[SRL] Tu contraseña de ingreso')
                ->setBody($html)
                ->addPart($html, 'text/html')
                ->setTo($user->getEmail())
                ->setFrom($this->sender)
            ;

            return $this->mailer->send($message);
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('error','No se ha podido enviar el correo, contacte al administrador');
            return false;
        }
    }
}
