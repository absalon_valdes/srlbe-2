<?php

namespace App\Controller;

use App\Entity\Job;
use App\Entity\UploadedFile;
use App\Form\ImporterType;
use App\Helper\ExcelBatchProcessor;
use App\Model\ContractXLSValidate;
use App\Model\QueueUpload;
use App\Queue\JobFactory;
use App\Queue\JobRepository;
use App\Queue\Queue;
use App\Queue\Workers\AbstractWorker;
use App\Queue\Workers\ContractImporterWorker;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Process\Process;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;


/**
 * Class Importer
 * @package App\Controller
 * @Extra\Security("is_granted('ROLE_ADMIN')")
 *
 * @Route("/importador")
 */
class Importer extends FOSRestController
{
    /**
     * @var array
     * @DI\Inject("%workers%")
     */
    public $workers;

    /**
     * @var string
     * @DI\Inject("%kernel.var_dir%")
     */
    public $varDir;

    /**
     * @var Queue
     * @DI\Inject("queue")
     */
    public $queue;

    /**
     * @var JobFactory
     * @DI\Inject("job_factory")
     */
    public $jobFactory;

    /**
     * @var JobRepository
     * @DI\Inject("job_repo")
     */
    public $jobRepository;

    /**
     * @var string
     * @Di\Inject("%kernel.environment%")
     */
    public $env;

    /**
     * @Method({"GET", "POST"})
     * @Route("", name="importador")
     * @View("importer/import.twig")
     * @param Request $request
     * @return array
     */
    public function importAction(Request $request)
    {
        $this->executeRunQueueCommand();
        $data = new QueueUpload();

        $form = $this->createForm(ImporterType::class, $data);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->queue->enqueue(Job::create($data->getType(), $data->toArray()));

            return $this->redirectView('importador', Response::HTTP_OK);
        }

        return [
            'form' => $form->createView(),
            'workers' => $this->workers
        ];
    }

    /**
     * @Post("/subir-archivo", name="importer_upload")
     * @param Request $request
     * @return array
     */
    public function uploadAction(Request $request)
    {
        if (!$file = $request->files->get('file')){
            return [];
        }

        $upload = UploadedFile::createFromFile(
            $file,
            $this->varDir.'/queue_files'
        );

        return [
            'file' => $upload->getName(),
            'original' => $upload->getOriginalName(),
        ];
    }

    /**
     * @Get("/trabajos", name="importer_jobs")
     * @return array
     */
    public function jobsListAction()
    {
        return [
            'data' => $this->jobRepository->findActive()
        ];
    }

    /**
     * @Get("/historico", name="importer_history")
     * @return array
     */
    public function jobsHistoryAction()
    {
        return [
            'data' => $this->jobRepository->findHistory()
        ];
    }

    /**
     * @Get("/trabajos/{id}", name="importer_job")
     * @View("importer/job.twig")
     *
     * @param Job $job
     * @return Job
     */
    public function jobAction(Job $job)
    {
        return [
            'data' => $job,
        ];
    }

    /**
     * @Get("/trabajos/{id}/report", name="importer_job_report")
     * @param Request $request
     * @param Job $job
     * @return array
     */
    public function reportAction(Request $request, Job $job)
    {
        $report = sprintf('%s/importer/%s', $this->getParameter('kernel.var_dir'), $job->getId());

        $size = $job->getSize();
        $start = $request->query->getInt('start');
        exec(sprintf('sed -n -e "%s, %sp" %s.log', $start + 1, $start+$size, $report), $data);

        return [
            'draw' => $request->query->getInt('draw'),
            'recordsTotal' => $size,
            'recordsFiltered' => $size,
            'data' => array_map('unserialize', $data),
            'error' => ''
        ];
    }

    /**
     * @Get("/descargar-archivo/{id}", name="importer_download")
     * @param $id
     * @return BinaryFileResponse
     */
    public function downloadAction($id)
    {
        if (!$job = $this->jobRepository->find($id)) {
            throw $this->createNotFoundException();
        }

        $payload = $job->getPayload();
        $filename = $payload['file'] ?? 'NONE';
        $original = $payload['original'] ?? 'NONE';

        if (!file_exists($filename)) {
            throw $this->createNotFoundException();
        }

        $response = new BinaryFileResponse($filename);
        $response->trustXSendfileTypeHeader();
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            $original,
            iconv('UTF-8', 'ASCII//TRANSLIT', $original)
        );

        return $response;
    }

    /**
     * @Get("/detener-trabajo/{id}", name="stop_job")
     * @param Job $job
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function stopJobAction(Job $job) {
        $this->queue->stop($job);
        return $this->redirectToRoute('importador');
    }

    /**
     * @Get("/reiniciar-trabajo/{id}", name="retry_job")
     * @param Job $job
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function retryJobAction(Job $job) {
        $this->queue->retry($job);
        $this->executeRunQueueCommand();
        return $this->redirectToRoute('importador');
    }

    /**
     * @Get("/cancelar-trabajo/{id}", name="cancel_job")
     * @param Job $job
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function cancelJobAction(Job $job) {
        $this->queue->cancel($job);
        return $this->redirectToRoute('importador');
    }


    public function executeRunQueueCommand()
    {
        $binPath = realpath(dirname(__DIR__, 3));
        $binPhpPath = PHP_BINDIR;
        $command = "$binPhpPath/php $binPath/bin/console queue:run --env $this->env > /dev/null 2>&1 &";
        exec($command);
    }
}
