<?php

namespace App\Workflow\Assigner;

interface AssignerInterface
{
    public function assign(AssignableInterface $object, $event);
}