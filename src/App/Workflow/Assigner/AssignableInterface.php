<?php

namespace App\Workflow\Assigner;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface AssignableInterface
 * @package App\Workflow\Assigner
 */
interface AssignableInterface
{
    /**
     * @return UserInterface
     */
    public function getAssignee();

    /**
     * @param UserInterface $user
     * @return void
     */
    public function setAssignee(UserInterface $user);
}