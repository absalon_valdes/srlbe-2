<?php

namespace App\Workflow;

use Lexik\Bundle\WorkflowBundle\Entity\ModelState;
use Lexik\Bundle\WorkflowBundle\Model\ModelInterface;
use Lexik\Bundle\WorkflowBundle\Model\ModelStateInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class ProcessModel
 * @package App\Workflow
 */
class ProcessModel implements ModelInterface, ModelStateInterface
{
    /**
     * @var ProcessTokenInterface
     */
    private $model;

    /**
     * @var array
     */
    private $states;

    /**
     * @var string
     */
    private $oid;

    /**
     * ProcessModel constructor.
     * @param ProcessTokenInterface $model
     */
    public function __construct(ProcessTokenInterface $model)
    {
        $this->model = $model;
        $this->states = array();
        $this->oid = $model->getObjectName().$model->getId();
    }

    /**
     * @return mixed
     */
    public function getWorkflowData()
    {
        return $this->model->getWorkflowData();
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->model->setStatus($status);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->model->getStatus();
    }

    /**
     * @param ModelState $modelState
     */
    public function addState(ModelState $modelState)
    {
        $this->states[] = $modelState;
    }

    /**
     * @return array
     */
    public function getStates()
    {
        return $this->states;
    }

    /**
     * @return string
     */
    public function getWorkflowIdentifier()
    {
        return Uuid::uuid5(Uuid::NAMESPACE_DNS, $this->oid)->toString();
    }

    /**
     * @return ProcessTokenInterface
     */
    public function getWorkflowObject()
    {
        return $this->model;
    }
}
