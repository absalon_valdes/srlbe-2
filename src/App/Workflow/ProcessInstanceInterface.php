<?php

namespace App\Workflow;

/**
 * Interface ProcessInstanceInterface
 * @package App\Workflow
 */
interface ProcessInstanceInterface
{
    /**
     * @param $model
     * @return mixed
     */
    public function start($model);

    /**
     * @param $model
     * @param $state
     * @return mixed
     */
    public function proceed($model, $state);

    /**
     * @param $model
     * @return mixed
     */
    public function getCurrentState($model);

    /**
     * @param $model
     * @return mixed
     */
    public function getHistory($model);

    /**
     * @param $model
     * @return mixed
     */
    public function isComplete($model);
}
