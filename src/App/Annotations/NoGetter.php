<?php

namespace App\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class NoGetter
 * @Annotation
 * @Target("PROPERTY")
 */
final class NoGetter extends Annotation
{
}