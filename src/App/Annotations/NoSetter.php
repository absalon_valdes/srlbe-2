<?php

namespace App\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class NoSetter
 * @Annotation
 * @Target("PROPERTY")
 */
final class NoSetter extends Annotation
{
}