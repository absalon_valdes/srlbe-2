<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateRange extends Constraint
{

    public $message = "Rango de fechas invalido";
    public $emptyStartDate = "La fecha de inicio esta vacia";
    public $emptyEndDate = "La fecha de fin esta vacia";

    public $hasEndDate = true;

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'daterange_validator';
    }
}
