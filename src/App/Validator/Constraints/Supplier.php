<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Supplier extends Constraint
{
    public $message = 'El proveedor no existe (%s)';
    public $invalidRegex = 'Rut del proveedor incorrecto';
}
