<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Translation\TranslatorInterface;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_admin_menu")
 */
class AdminMenu
{
    const REPORT_MENU = 'Reportes';

    /**
     * @Di\Inject("%easyadmin.config%")
     */
    public $adminMenu;

    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;
    
    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $env;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \ReflectionException
     */
    public function build(FactoryInterface $factory, array $options)
    {

        $headerMenuConf =[
            'uri' => '#',
            'attributes'=> [
                'class' => 'dropdown',
            ],
            'childrenAttributes' => [
                'class' => 'dropdown-menu',
                "style" => "z-index: 999999999999",
            ],
            'linkAttributes' => [
                "class" => "dropdown-toggle",
                "data-toggle"=>"dropdown",
                "role"=>"button",
                "aria-haspopup"=>"true",
                "aria-expanded"=>"false",
            ]
        ];

        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'class' => 'nav navbar-nav navbar-left'
            ]
        ]);

        $menu->addChild('Contenido', $headerMenuConf);
        
        foreach ($this->adminMenu['entities'] as $entity) {
            $ref = new \ReflectionClass($entity['class']);
            $name = $ref->getShortName();
            
            $menu['Contenido']->addChild($this->translator->trans($name), [
                'route' => 'easyadmin',
                'routeParameters' => ['entity' => $name]
            ]);
        }

        $menu->addChild('Importador', [
            'route' => 'importador'
        ]);

        $menu->addChild('Exportadores', $headerMenuConf);
        $menu['Exportadores']->setAttribute('class', 'dropdown');
        $menu['Exportadores']->addChild('Productos', ['route' => 'exporter', 'routeParameters' => ['type' => 'products']]);
        $menu['Exportadores']->addChild('Proveedores', ['route' => 'exporter', 'routeParameters' => ['type' => 'suppliers']]);
        $menu['Exportadores']->addChild('Sucursales', ['route' => 'exporter', 'routeParameters' => ['type' => 'offices']]);
        $menu['Exportadores']->addChild('Usuarios', ['route' => 'exporter', 'routeParameters' => ['type' => 'users']]);
        $menu['Exportadores']->addChild('Funcionarios', ['route' => 'exporter', 'routeParameters' => ['type' => 'employees']]);
        $menu['Exportadores']->addChild('PEP', ['route' => 'exporter', 'routeParameters' => ['type' => 'pep']]);
        $menu['Exportadores']->addChild('Catálogos', ['uri' => '#ver-catalogos']);

        $reportMenu = $menu->addChild(self::REPORT_MENU, $headerMenuConf);
        $reportMenu->setAttribute('class', 'dropdown');
        

        $reportMenu->addChild('BI General', [
            'uri' => '#reports_bi_general_cmmd'
        ]);

        $reportMenu->addChild('BI General Diario', [
            'uri' => '#reports_bi_general_daily'
        ]);

        $reportMenu->addChild('BI Pedidos', [
            'route' => 'report_requisition_bi_general',
            'routeParameters' => ['type' => 'requisition.product']
        ]);
        $reportMenu->addChild('BI Autogestión', [
            'route'           => 'report_requisition_bi_general',
            'routeParameters' => ['type' => 'requisition.self_management']
        ]);
        $reportMenu->addChild('BI Mantenciones', [
            'route'           => 'report_requisition_bi_general',
            'routeParameters' => ['type' => 'requisition.maintenance']
        ]);
        $reportMenu->addChild('BI Tarjetas', [
            'route'           => 'report_requisition_general_vcard'
        ]);
        $reportMenu->addChild('BI PEP', [
            'route' => 'report_pep_bi_general'
        ]);
        $reportMenu->addChild('Reporte usuarios', [
            'route' => 'report_users_general'
        ]);

        $reportMenu->addChild('BI General BEME', [
            'route'           => 'report_requisition_special_office',
            'routeParameters' => ['special_type' => 'beme']
        ]);

        $reportMenu->addChild('BI Pedidos especiales', [
            'route' => 'report_request_special_request'
        ]);

        $menu['Reportes']->addChild('BI Uniformes', [
            'route' => 'report_uniformes_bi_general'
        ]);

        $utilMenu = $menu->addChild('Utilidades', $headerMenuConf);

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_ADMIN,
            new ConfigureMenuEvent($factory, $menu)
        );

        return $menu;
    }
}
