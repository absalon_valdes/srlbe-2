<?php

namespace App\Menu;

use JMS\DiExtraBundle\Annotation as Di;
use Knp\Menu\FactoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_approver_menu")
 */
class ApproverMenu
{
    /**
     * @Di\Inject("request_stack")
     */
    public $requestStack;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    public function build(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'id' => 'main-menu',
                'class' => 'nav navbar-nav'
            ]
        ]);

        $request = $this->requestStack->getCurrentRequest();

        if ('dashboard' !== $request->get('_route') && !$options['dashboard']) {
            $menu->addChild('Inicio', [
                'route' => 'dashboard',
                'linkAttributes' => [
                    'class' => 'menu-home'
                ]
            ]);
        }

        $menu->addChild('Estado de solicitudes', [
            'route' => 'requisitions_index',
            'linkAttributes' => [
                'class' => 'menu-estado'
            ]
        ]);

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_APPROVER,
            new ConfigureMenuEvent($factory, $menu)
        );

        return $menu;
    }
}