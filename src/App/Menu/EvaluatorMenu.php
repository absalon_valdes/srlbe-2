<?php

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use JMS\DiExtraBundle\Annotation as Di;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Translation\TranslatorInterface;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_evaluator_menu")
 */
class EvaluatorMenu
{
    const MODULE_MENU = 'module';
    /**
     * @var TranslatorInterface
     * @Di\Inject("translator")
     */
    public $translator;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     * @throws \ReflectionException
     */
    public function build(FactoryInterface $factory, array $options)
    {

        $headerMenuConf =[
            'uri' => '#',
            'attributes'=> [
                'class' => 'dropdown',
            ],
            'childrenAttributes' => [
                'class' => 'dropdown-menu',
                "style" => "z-index: 999999999999",
            ],
            'linkAttributes' => [
                "class" => "dropdown-toggle",
                "data-toggle"=>"dropdown",
                "role"=>"button",
                "aria-haspopup"=>"true",
                "aria-expanded"=>"false",
            ],
        ];

        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'class' => 'nav navbar-nav navbar-left'
            ]
        ]);

        $menu->addChild(self::MODULE_MENU, array_merge([
            'route' => 'dashboard',
            'label'=>$this->translator->trans('dashboard')
        ]));

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_EVALUATOR,
            new ConfigureMenuEvent($factory, $menu)
        );

        return $menu;
    }
}
