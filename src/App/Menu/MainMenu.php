<?php

namespace App\Menu;

use App\Entity\Category;
use App\Helper\SpecialOfficeHelper;
use App\Helper\UserHelper;
use JMS\DiExtraBundle\Annotation as Di;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\Event\ConfigureMenuEvent;

/**
 * @Di\Service("menu_main_menu")
 */
class MainMenu
{
    const PROD_KEY = 'prods_menu_cache';
    const REPR_KEY = 'repai_menu_cache';
    const MANT_KEY = 'maint_menu_cache';
    const EXPIRE_T = 300; // 5 min ttl

    /**
     * @Di\Inject("category_repo")
     */
    public $categoriesRepo;

    /**
     * @Di\Inject("product_repo")
     */
    public $productsRepo;

    /**
     * @var RequestStack
     * @Di\Inject("request_stack")
     */
    public $requestStack;

    /**
     * @var \Redis
     * @Di\Inject("snc_redis.default_client")
     */
    public $redis;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @Di\Inject("%kernel.environment%")
     */
    public $environment;

    /**
     * @var SpecialOfficeHelper
     * @Di\Inject("special_office_helper")
     */
    public $specialOfficeHelper;

    /**
     * @var EventDispatcher
     * @Di\Inject("event_dispatcher")
     */
    public $eventDispatcher;

    /**
     * @param FactoryInterface $factory
     * @param array $options
     * @return \Knp\Menu\ItemInterface
     */
    public function build(FactoryInterface $factory, array $options)
    {
        $request = $this->requestStack->getCurrentRequest();

        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'id' => 'main-menu',
                'class' => 'nav navbar-nav'
            ]
        ]);

        if (!$options['dashboard'] && 'dashboard' !== $request->get('_route')) {
            $menu->addChild('Inicio', [
                'route' => 'dashboard',
                'linkAttributes' => [
                    'class' => 'menu-home'
                ]
            ]);
        }

        $this->productsMenu($menu);
        $this->maintenanceMenu($menu);
        $this->fixesMenu($menu);

        $this->requirementsMenu($menu);

        $menu->addChild('Estado de solicitudes', [
            'route' => 'requisitions_index',
            'linkAttributes' => ['class' => 'menu-estado']
        ]);

        $this->eventDispatcher->dispatch(
            ConfigureMenuEvent::CONFIGURE_MAIN,
            new ConfigureMenuEvent($factory, $menu)
        );

        return $menu;
    }

    /**
     * @param $root
     */
    private function requirementsMenu($root)
    {
        if(!$this->specialOfficeHelper->checkSpecialOffice()){
            $root->addChild('Solicitudes Especiales', [
                'labelAttributes' => ['class' => 'menu-requerimientos disable-feature']
            ]);
        }else{
            $root->addChild('Solicitudes Especiales', [
                'route' => 'solicitudes_especiales',
                'linkAttributes' => ['class' => 'menu-requerimientos']
            ]);
        }
    }

    /**
     * @param $root
     */
    private function fixesMenu($root)
    {
        $menu = $root->addChild('Autogestión', [
            'labelAttributes' => [
                'class' => 'menu-subastar '
            ]
        ]);

        if (!$this->userHelper->isGranted('ROLE_REQUESTER')) {
            return;
        }
        
        $menu->setChildrenAttributes([
            'id' => 'js-dropdown-arreglos-hover',
            'class' => 'dropdown-menu'
        ]);

        foreach (['reparaciones', 'limpieza'] as $item) {
            if (!$category = $this->categoriesRepo->findOneBySlug('productos/'.$item)) {
                return;
            }
            
            $this->addChildren($menu, self::REPR_KEY, $category, function ($c) {
                return $this->productsRepo->findHavingProducts($c);
            });
        }
    }

    /**
     * @param $root
     */
    private function maintenanceMenu($root)
    {
        $menu = $root->addChild('Mantenciones', [
            'labelAttributes' => [
                'class' => 'menu-arreglar'
            ]
        ]);
        
        if (!$this->userHelper->isGranted('ROLE_REQUESTER')) {
            return;
        } 
        
        $menu->setChildrenAttributes([
            'id' => 'js-dropdown-mantenciones-hover',
            'class' => 'dropdown-menu'
        ]);

        if (!$category = $this->categoriesRepo->findOneBySlug('productos/mantenciones')) {
            return;
        }
        
        $this->addChildren($menu, self::MANT_KEY, $category, function ($c) {
            return $this->productsRepo->findHavingProducts($c);
        });
    }

    /**
     * @param $root
     */
    private function productsMenu($root)
    {
        $menu = $root->addChild('Pedidos', [
            'labelAttributes' => [
                'class' => 'menu-pedidos '
            ]
        ]);

        if (!$this->userHelper->isGranted('ROLE_REQUESTER')) {
            return;
        } 
        
        $menu->setChildrenAttributes([
            'id' => 'js-dropdown-pedidos-hover',
            'class' => 'dropdown-menu'
        ]);

        if (!$category = $this->categoriesRepo->findOneBySlug('productos')) {
            return;
        }
        
        $this->addChildren($menu, self::PROD_KEY, $category, function ($c) {
            return $this->productsRepo->findHavingProducts($c) &&
                   !preg_match('/(limpieza|reparaciones|mantenciones)$/', $c->getSlug());
        });
    }

    /**
     * @param $root
     * @param $key
     * @param $category
     * @param \Closure $filter
     */
    private function addChildren($root, $key, Category $category, \Closure $filter)
    {
        $uid = $this->userHelper->getCurrentUser()->getId();
        $items = json_decode($this->redis->get($key.$uid)) ?? [];

        $categoryFilter = function (Category $p) use ($filter) {
            return $filter($p) ? $p->getSlug() : null;
        };

        if (empty($items)) {
            foreach ($category->getChildren() as $cat) {
                $items[$cat->getName()] = $categoryFilter($cat);
            }
            
            $this->redis->set($key.$uid, json_encode($items), self::EXPIRE_T);
        }

        foreach (array_filter((array)$items) as $key => $value) {
            $root->addChild($key, [
                'route' => 'requisitions_catalog',
                'routeParameters' => ['slug' => $value]
            ]);
        }
    }
}
