<?php

namespace App\Dashboard;

use App\Helper\UserHelper;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class AbstractQuery
 * @package App\Dashboard
 */
abstract class AbstractQuery implements QueryInterface
{
    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var EntityManager
     * @Di\Inject("doctrine.orm.entity_manager")
     */
    public $manager;

    /**
     * {@inheritdoc}
     */
    abstract public function getQuery();

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        $ref = new \ReflectionObject($this);

        $name = preg_replace('/Query$/', '', $ref->getShortName());
        $name = str_replace('_', '.', Inflector::tableize($name));

        return $name;
    }

    /**
     * @return \App\Entity\User|null
     */
    public function getUser()
    {
        return $this->userHelper->getCurrentUser();
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->manager->createQueryBuilder();
    }

    /**
     * @return array
     */
    public function getArrayResult()
    {
        try { return $this->getQuery()->getArrayResult(); }
        catch (\Exception $e) {}

        return [];
    }

    /**
     * @return array
     */
    public function getResult()
    {	
        try { return $this->getQuery()->getResult(); } 
        catch (\Exception $e) {}   
       	
	return [];
    }
}
