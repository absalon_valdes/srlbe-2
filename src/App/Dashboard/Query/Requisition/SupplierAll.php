<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Dashboard\QueryAggregator;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class SupplierAll extends AbstractQuery
{
    /**
     * @var QueryAggregator
     * @Di\Inject("dashboard_query_aggregator")
     */
    public $aggregator;

    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return array_merge(
            $this->aggregator->getQueryResult('supplier.away'),
            $this->aggregator->getQueryResult('supplier.pending'),
            $this->aggregator->getQueryResult('supplier.history'),
            $this->aggregator->getQueryResult('supplier.cert')
        );
    }

    /**
     * @return array
     */
    public function getArrayResult()
    {
        return array_merge(
            $this->aggregator->getQueryArrayResult('supplier.away'),
            $this->aggregator->getQueryArrayResult('supplier.pending'),
            $this->aggregator->getQueryArrayResult('supplier.history'),
            $this->aggregator->getQueryArrayResult('supplier.cert')
        );
    }
}
