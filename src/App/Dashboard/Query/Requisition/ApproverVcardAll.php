<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Dashboard\QueryAggregator;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class ApproverVcardAll
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class ApproverVcardAll extends AbstractQuery
{
    /**
     * @var QueryAggregator
     * @Di\Inject("dashboard_query_aggregator")
     */
    public $aggregator;

    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return array_merge(
            $this->aggregator->getQueryResult('approver.vcard.pending'),
            $this->aggregator->getQueryResult('approver.vcard.away'),
            $this->aggregator->getQueryResult('approver.vcard.cert'),
            $this->aggregator->getQueryResult('approver.vcard.history')
        );
    }

    /**
     * @return array
     */
    public function getArrayResult()
    {
        return array_merge(
            $this->aggregator->getQueryArrayResult('approver.vcard.pending')
        );
    }
}
