<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Dashboard\QueryAggregator;
use App\Entity\Contract;
use App\Entity\DeclinedRequisition;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class SupplierHistoryQuery extends AbstractQuery
{
    /**
     * @var QueryAggregator
     * @Di\Inject("dashboard_query_aggregator")
     */
    public $aggregator;
    
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->join(Supplier::class, 's', Join::WITH, $qb->expr()->eq('c.supplier', 's'))
            ->where($qb->expr()->andX(
                $qb->expr()->isMemberOf(':user', 's.representatives'),
                $qb->expr()->in('r.status', [
                    ProductStatus::CLOSED,
                    ProductStatus::COMPLETED,
                    ProductStatus::DECLINED,
                    ProductStatus::TIMEOUT,
                    ProductStatus::CLOSED_BY_APPROVER,
                ])
            ))
            ->setParameter('user', $this->getUser())
            ->orderBy($qb->expr()->desc('r.updatedAt'));

        return $qb->getQuery();
    }

    public function getArrayResult()
    {
        return array_merge(
            $this->aggregator->getQueryArrayResult('supplier.completed'),
            $this->aggregator->getQueryArrayResult('supplier.declined')
        );
    }
}
