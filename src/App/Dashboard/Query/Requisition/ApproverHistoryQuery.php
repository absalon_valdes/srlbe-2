<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Contract;
use App\Entity\Product;
use App\Entity\Requisition;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query\Expr\Join;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class ApproverHistoryQuery extends AbstractQuery
{
    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->join(Product::class, 'p', Join::WITH, $qb->expr()->eq('r.product', 'p'))
            ->join(Contract::class, 'c', Join::WITH, $qb->expr()->eq('r.contract', 'c'))
            ->where($qb->expr()->eq(':user', 'p.approver'))
            ->andWhere($qb->expr()->in('r.status', [
                ProductStatus::CLOSED,
                ProductStatus::COMPLETED,
                ProductStatus::CLOSED_BY_APPROVER,
                ProductStatus::TIMEOUT,
                //ProductStatus::DELIVERED,
                //ProductStatus::EVALUATED,
                //ProductStatus::REVIEWED,
            ]))
            //->andWhere($qb->expr()->eq('c.centralized', true))
            ->setParameter('user', $this->getUser())
        ;

        return $qb->getQuery();
    }
}
