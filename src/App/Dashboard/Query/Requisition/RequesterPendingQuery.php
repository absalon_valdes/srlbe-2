<?php

namespace App\Dashboard\Query\Requisition;

use App\Dashboard\AbstractQuery;
use App\Entity\Requisition;
use App\Entity\SpecialRequest;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequesterHistoryQuery
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class RequesterPendingQuery extends AbstractQuery
{
    public function getQuery()
    {
        $qb = $this->getQueryBuilder();
        $qb
            ->select('r')
            ->from(Requisition::class, 'r')
            ->where($qb->expr()->eq('r.requester', ':user'))
            ->andWhere($qb->expr()->eq('r.assignee', ':user'))
            ->andWhere($qb->expr()->in('r.status', [
                ProductStatus::REVIEWED,
                ProductStatus::EVALUATED,
                ProductStatus::REJECTED,
                'evaluacion',
                'aceptado',
                'solicitud_evaluada',
                'rechazado'
            ]))
            ->orderBy($qb->expr()->desc('r.updatedAt'))
            ->setParameter(':user', $this->getUser())
        ;

        return $qb->getQuery();
    }
}
