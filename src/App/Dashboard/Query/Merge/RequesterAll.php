<?php

namespace App\Dashboard\Query\Merge;

use JMS\DiExtraBundle\Annotation as Di;
use App\Dashboard\AbstractQuery;
use App\Dashboard\QueryAggregator;

/**
 * Class RequesterAll
 * @package App\Service\DashboardQuery\Query
 * @Di\Service @Di\Tag("dashboard.query")
 */
class RequesterAll extends AbstractQuery
{
    /**
     * @var QueryAggregator
     * @Di\Inject("dashboard_query_aggregator")
     */
    public $aggregator;

    /**
     * {@inheritdoc}
     */
    public function getQuery()
    {
        return null;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return array_merge(
            $this->aggregator->getQueryResult('requester.open'),
            $this->aggregator->getQueryResult('requester.pending'),
            $this->aggregator->getQueryResult('requester.history')
        );
    }

    /**
     * @return array
     */
    public function getArrayResult()
    {
        return array_merge(
            $this->aggregator->getQueryArrayResult('requester.open'),
            $this->aggregator->getQueryArrayResult('requester.pending'),
            $this->aggregator->getQueryArrayResult('requester.history')
        );
    }
}
