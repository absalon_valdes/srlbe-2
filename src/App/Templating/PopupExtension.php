<?php

namespace App\Templating;

use App\Repository\RequisitionRepository;
use JMS\DiExtraBundle\Annotation as Di;
use App\Helper\UserHelper;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * @Di\Service("popup_extension")
 * @Di\Tag("twig.extension")
 */
class PopupExtension extends \Twig_Extension
{
    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requisitionRepo;

    /**
     * @var Session
     * @Di\Inject("session")
     */
    public $session;

    /**
     * @var \Twig_Environment
     * @Di\Inject("twig")
     */
    public $twig;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * @var string
     * @Di\Inject("%poll_url%")
     */
    public $pollUrl;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_Function('mostrar_popup', [$this, 'MostrarPopup'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @return string
     */
    public function MostrarPopup()
    {
        if (!$user = $this->userHelper->getCurrentUser()) {
            return null;    
        }
        
        $findOpen = $this->requisitionRepo->findLastMonthRequisitions($user);
        $completado = $this->session->get('popup_completado', null);

        if (!$completado && 0 !== $findOpen) {
            return $this->twig->render('popup_encuesta.twig', [
                'poll_url' => $this->pollUrl
            ]);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'popup_extension';
    }
}
