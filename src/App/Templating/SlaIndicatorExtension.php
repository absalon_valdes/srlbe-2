<?php

namespace App\Templating;

use App\Entity\Requisition;
use App\Helper\SlaCalculator;
use App\Helper\SlaResolver;
use App\Helper\UserHelper;
use App\Model\Transition;
use App\Repository\RequisitionRepository;
use App\Util\DateUtils;
use App\Workflow\ProcessInstance;
use App\Workflow\Status\ProductStatus;
use JMS\DiExtraBundle\Annotation as Di;

/**
 * Class RequisitionStatusGrid
 * @package App\Templating
 *
 * @Di\Service @Di\Tag("twig.extension")
 */
class SlaIndicatorExtension extends \Twig_Extension
{
    /**
     * @var ProcessInstance
     * @Di\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var RequisitionRepository
     * @Di\Inject("requisition_repo")
     */
    public $requistionRepo;

    /**
     * @var SlaResolver
     * @Di\Inject("sla_resolver")
     */
    public $slaResolver;

    /**
     * @var UserHelper
     * @Di\Inject("user_helper")
     */
    public $userHelper;

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'sla_indicator';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_Function('sla_indicator', [$this, 'slaIndicator'], ['is_safe' => ['html']]),
            new \Twig_Function('sla_indicator_x', [$this, 'slaIndicatorX'], ['is_safe' => ['html']]),
            new \Twig_Function('requisition_timer', [$this, 'requisitionTimer'], ['is_safe' => ['html']]),
            new \Twig_Function('sla_to_seconds', [$this, 'slaToSeconds'], ['is_safe' => ['html']]),
        ];
    }

    public function requisitionTimer(Requisition $requisition, $timer, $format = 'd/m/Y H:i',$getInFormat = true)
    {
        $timers = $requisition->getMetadata('timestamps');
        
        if (null === $timers) { // OLD
            $timers = [];

            foreach (['requester', 'approver', 'supplier'] as $role) {
                $timers[$role] = [
                    'started' => $requisition->getCreatedAt(),
                    'response' => $this->slaResolver->responseDate($requisition),
                    'completion' => $requisition->inProgress() ? '-' : 
                        $this->slaResolver->resolveSLA($requisition)
                ];
                
                $requisition->addMetadata('timestamps', $timers);
            }
            
            if (!$requisition->isType('special_request')) {
                $this->requistionRepo->save($requisition);
            } 
        }
        
        $currentUser = $this->userHelper->getCurrentUser();
        $role = $requisition->getUserRole($currentUser);
        
        if (!is_array($timers) || 
            !isset($timers[$role], $timers[$role][$timer]) ||
            ($requisition->isType('special_request') && in_array($timer, ['response', 'completion'], true))
        ) {
            return '-';
        }
        
        if (!($timer = $timers[$role][$timer]) instanceof \DateTime) {
            return $timer;
        }
        
        return $getInFormat ? $timer->format($format) : $timer;
    }

    /**
     * @param Requisition $requisition
     * @return string
     */
    public function slaIndicatorX(Requisition $requisition)
    {
        $timers = $requisition->getMetadata('timestamps');
        
        if (!isset($timers['requester'])){
            return '-';
        }
        
        $timers = $timers['requester'];

	if (!$timers instanceof \DateTime) {
            return '-';
        }
        
        switch ($requisition->getStatus()) {
            case 'en_despacho':
                $slaTo = $timers['completion'];
                break;
            case 'ejecucion':
            case 'evaluacion':
                $slaTo = $timers['response'];
                break;
            default:
                $slaTo = null;
        }
        
        if (null === $slaTo) {
            return '-';
        }
        
        $result = $slaTo->diff(new \DateTime);
        $sla = DateUtils::dateIntervalFormat($result);

        return $sla ? sprintf(
            '<i class="glyph-icon flaticon-hourglass4 sla-indicator" style="color: %s"></i> <span>%s</span>',
            $result->invert ? '#00cc00' : '#cc0000', $sla ) : '-';
    }

    /**
     * @param Requisition $requisition
     * @return string
     */
    public function slaIndicator(Requisition $requisition)
    {
        if (($requisition->isRejected() && $requisition->getStatus() != ProductStatus::ORDERED)
            || in_array($requisition->getStatus(),[ProductStatus::CLOSED_BY_APPROVER]) ) {
            return '-';
        }

        if ($requisition->isClosed()) {
            $when = null;
            $result = $this->remainingSla($requisition);
	   
            if (!$result instanceof \DateTime) {
                 return '-';
            } 
	
        } elseif ($requisition->isStatus(ProductStatus::RETURNED)) {
            $when = $this->slaResolver->resolveSLA($requisition);
            $result = $when->diff(new \DateTime);
        } else {
            $when = $requisition->getStateDueDate();
            $result = $when->diff(new \DateTime);
        }

        $sla = DateUtils::dateIntervalFormat($result);

        return $sla ? sprintf(
            '<i class="glyph-icon flaticon-hourglass4 sla-indicator" style="color: %s"></i> <span>%s</span>',
            $result->invert ? '#00cc00' : '#cc0000', $sla ) : '-';

    }

    /**
     * @param Requisition $requisition
     * @return \DateInterval
     */
    private function remainingSla(Requisition $requisition)
    {
        $accepted = $requisition->getTransition(ProductStatus::ACCEPTED);
        $delivered = $requisition->getTransition(ProductStatus::DELIVERED);
    
	if ((!$accepted instanceof \DateTime) || (!$delivered instanceof \DateTime)) {
            return '-';
        }
   
        // BC 
        if (!$accepted instanceof Transition && !$delivered instanceof Transition) {
            return (new \DateTime)->diff(new \DateTime);
        }
        $diff = $delivered->getDatetime();
        $diff = $diff->diff($accepted->getDatetime());
        return $diff;
    }


    /**
     * @param Requisition $requisition
     * @return string
     */
    public function slaToSeconds(Requisition $requisition)
    {
        if ($requisition->isRejected()) {
            return '0';
        }

        if ($requisition->isClosed()) {
            $when = null;
            $result = $this->remainingSla($requisition);
        } elseif ($requisition->isStatus(ProductStatus::RETURNED)) {
            $when = $this->slaResolver->resolveSLA($requisition);
            $result = $when->diff(new \DateTime);
        } else {
            $when = $requisition->getStateDueDate();
            $result = $when->diff(new \DateTime);
        }

        if (!$result instanceof \DateTimeInterval) {
           return '0'; 
        }

        $s = DateUtils::dateIntervalToSeconds($result);
        return !$result->invert ? ( -1 ) * $s : $s;
    }
}
