<?php

namespace App;

use App\DependencyInjection\Compiler;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class App extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new Compiler\ProcessDefinitionCompilerPass);
        $container->addCompilerPass(new Compiler\RegisterNotifierCompilerPass);
        $container->addCompilerPass(new Compiler\RegisterRepositoryCompilerPass($this));
        $container->addCompilerPass(new Compiler\RegisterDashboardQueryCompilerPass);
        $container->addCompilerPass(new Compiler\RegisterExporterCompilerPass);
    }
}
