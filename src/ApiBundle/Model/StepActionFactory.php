<?php

namespace ApiBundle\Model;

use App\Entity\Requisition;
use App\Workflow\ProcessInstance;
use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class StepActionFactory
 * @DI\Service("step.action.factory")
 */
class StepActionFactory
{
    /**
     * @var ValidatorInterface
     * @DI\Inject("validator")
     */
    public $validator;

    /**
     * @var ProcessInstance
     * @DI\Inject("workflow.product")
     */
    public $workflow;

    /**
     * @var EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $manager;

    public function create(Requisition $requisition, $step, $config, Request $request)
    {
        $stepAction = new StepAction($requisition, $step, $config, $request);
        $stepAction->setWorkflow($this->workflow);
        $stepAction->setValidator($this->validator);
        $stepAction->setManager($this->manager);
        
        return $stepAction;
    }
}