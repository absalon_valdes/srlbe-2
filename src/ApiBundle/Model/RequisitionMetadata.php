<?php

namespace ApiBundle\Model;

use App\Entity\Requisition;
use App\Workflow\Status\ProductStatus;
use Behat\Transliterator\Transliterator;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class RequisitionMetadata
 * @DI\Service("requisition.metadata.formatter")
 */
class RequisitionMetadata
{
    /**
     * @var Router
     * @DI\Inject("router")
     */
    public $router;

    /**
     * @var TranslatorInterface
     * @DI\Inject("translator")
     */
    public $translator;
    
    public function getMetadata(Requisition $requisition)
    {
        $serialized = [];
        $metadata = $requisition->getMetadata();
        
        if (isset($metadata['completed'])) {
            // put at the end
            $completed = $metadata['completed'];
            unset($metadata['completed']);
            $metadata['completed'] = $completed;
        }
        
        if ($requisition->getTransition(ProductStatus::ACCEPTED)) {
            $metadata['work_order_file'] = $this->router->generate(
                'requisition_work_order_pdf', [
                'id' => $requisition->getId()
            ]);
        }
        
        foreach ($metadata as $key => $value) {
            $label = '';

            if (in_array($key, [
                'inbox_status',
                'timestamps',
                'work_order',
                'declined',
                'decline_comment',
                'supplier.sla.start',
                'supplier.sla.end',
                'supplier_change',
                'inventory_code',
                'comments',
                'description',
                'sla_update',
                'approve_timestamp',
                'deliver_timestamp',
                'review_timestamp',
                'decline_timestamp',
                'decline_rejection',
                'declined_rejection_uid',
                'close_timestamp',
                'approver_close',
                'approver_meta',
                'install_code',
                'replaces',
                'install_replaces',
                'delivery_address_2'
                //'completed'
            ], true)) {
                continue;
            }

            if ('purchase_order' === $key) {
                $label = 'Documento de orden de compra';
                $value['files'] = array_map(function ($file) {
                    return $this->router->generate('download_file', ['fileName' => $file]);
                }, $value['files'] ?? []);
            }

            if ('responsible' === $key) {
                $label = 'Responsable del servicio';
                $value = ['name' => $value];
            }

            if ('evaluation' === $key) {
                $label = 'Certificación del demandante';
                
                if (!is_array($value)) {
                    $value = [$value];
                }
                
                if (isset($value['evaluation'])) {
                    $value = [$value];
                }

                $value = array_map(function ($eval) {
                    unset($eval['datetime']);
                    $eval['label'] = $this->translator->trans($eval['evaluation']);

                    return $eval;
                }, $value);
            }
            
            if ('work_order_file' === $key) {
                $label = 'Documento de orden de trabajo';
                $wo = $requisition->getMetadata('work_order');
                $docname = Transliterator::urlize(sprintf('Documento de orden de trabajo no %s', $wo));
                $value = ['files' => [$docname.'.pdf' => $value]];
            }

            if ('supplier_rating' === $key) {
                $label = 'Calificación del proveedor';
                $criteria = [];

                foreach ($value as $k => $v) {
                    $criteria[] = [
                        'label' => $this->translator->trans($k),
                        'value' => $v,
                    ];
                }
                
                $value = $criteria;
            }

            if ('rejection' === $key) {
                $label = 'Rechazo del evaluador';
                if (isset($value['reason'])) {
                    $value['label'] = $this->translator->trans($value['reason']);
                }
            }

            if ('appeal' === $key) {
                $label = 'Apelación';
            }

            if ('decline' === $key) {
                $label = 'Rechazo de solicitud';
                
                $value = array_map(function ($val) {
                    if (isset($val['reason'])) {
                        $val['label'] = $this->translator->trans($val['reason']);
                    }
                    
                    return $val;
                }, $value);
            }

            if ('confirmation' === $key) {
                $label = 'Certificación del proveedor';

                if (0 === count($value['files'])) {
                    unset($value['files']);
                } else {
                    foreach ($value['files'] as $k => $v) {
                        if ('' === $v['description']) {
                            unset($value['files'][$k]);
                        }        
                    }
                    
                    $value['files'] = array_values($value['files']);
                }
            }

            if ('supplier_change' === $key) {
                $label = 'Cambio de proveedor';
                $value = ['comments' => $value];
            }
            
            if ('denial' === $key) {
                $label = 'Rechazo de apelación';
                $value['label'] = $this->translator->trans($value['reason']);
            }
            
            if ('response' === $key) {
                $label = 'Respuesta a rechazo de apelación';
                $value['label'] = $this->translator->trans($value['evaluation']);
            }
            
            if ('closed' === $key) {
                $label = 'Solicitud cerrada';
                $datetime = new \DateTime;
                $datetime->setTimestamp($value['timestamp']);
                
                $value['closed_at'] = $datetime;
                $value['closed_by'] = $value['username'];
                
                unset($value['timestamp'], $value['username']);
            }

            if ('completed' === $key) {
                $label = 'Solicitud completa';
                $datetime = new \DateTime;
                $datetime->setTimestamp($value['timestamp']);
                
                $value['completed_at'] = $datetime;
                $value['completed_by'] = $value['username'];
                
                unset($value['timestamp'], $value['username']);
            }
            
            if ('more_info' === $key) {
                $label = 'Información adicional';
            }

            if ('decline_rejection' === $key) {
                $label = 'Rechazo de rechazo del proveedor'; 
            }
            
            if ('details' === $key) {
                $label = 'Detalle de items';
                
                if (isset($value['codigos_inventario'])) {
                    $value['codigo_inventario'] = array_values($value['codigo_inventario']);
                }
                
                if (isset($value['caracteristicas'])) {
                    foreach ($value['caracteristicas'] as $i => $car) {
                        if (!isset($car['codigo_inventario'])) {
                            continue;
                        }
                        
                        $value['caracteristicas'][$i]['codigo_inventario'] = array_values($car['codigo_inventario']);
                    }
                }
            }

            if('delivery_address_2' === $key){
                $label = 'Dirección de despacho';
            }
            
            if ('' === $label) {
                throw new \Exception($key.' - '.$requisition->getId());
            }
            
            $serialized[] = Metadata::create($key, $label, $value); 
        }
        
        return $serialized;
    }
}
