<?php

namespace ApiBundle\Model;

use App\Entity\Requisition;
use App\Workflow\ProcessInstance;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class StepAction
{
    /** @var ValidatorInterface */
    protected $validator;

    /** @var ProcessInstance */
    protected $workflow;
    
    /** @var string */
    protected $step;
    
    /** @var array */
    protected $config;
    
    /** @var Request */
    protected $request;
    
    /** @var Requisition */
    protected $requisition;
    
    /** @var EntityManager */
    protected $manager;

    public function __construct(Requisition $requisition, $step, $config, Request $request)
    {
        $this->requisition = $requisition;
        $this->step = $step;
        $this->config = $config;
        $this->request = $request;
    }

    public function setWorkflow(ProcessInstance $workflow)
    {
        $this->workflow = $workflow;
    }

    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function setManager(EntityManager $manager)
    {
        $this->manager = $manager;
    }

    public function execute()
    {
        $config = $this->config[$this->step];
        $payload = $this->request->request->all();

        if (count($errors = $this->validate($payload))) {
            return $errors;
        }
        
        /*
        $action = $this->workflow->proceed($this->requisition, $this->step);
        
        if (!$action->getSuccessful()) {
            return $action->getErrors();
        }
        */
        
        if (isset($config['callback'])) {
            $config['callback']($this->requisition, $payload);
        }
        
        try {
            $this->manager->persist($this->requisition);
            $this->manager->flush($this->requisition);
        } catch (DBALException $e) {
            return $e;
        }
        
        return null;
    }

    private function validate(array $data = [])
    {
        $errors = [];
        $config = $this->config[$this->step];

        foreach ($config['attrs'] as $name => $attr) {
            if (!isset($attr['constraints'])) {
                continue;
            }
            
            if (!isset($data[$name])) {
                $data[$name] = null;
            }
            
            $constraints = $this->replaceConstraintKeys($attr['constraints']);
            
            foreach ($constraints as $key => $constraint) {
                if ('file' === $constraint) {
                    $data[$name] = new Base64EncodedFile($data[$name]);
                }

                if (Collection::class === $key) {
                    $fields = [];

                    foreach ($constraint['fields'] as $k => $f) {
                        foreach ($f as $c => $o) {
                            $fields[$k][] = new $c($o);
                        }
                    }
                    
                    $constraint['fields'] = $fields;
                }
                
                $errors[] = $this->validateObject($data, $name, new $key($constraint));
            }
        }
        
        return $errors;
    }

    private function validateObject(array $data, $property, Constraint $constraint)
    {
        return $this->validator->startContext()
            ->atPath($property)
            ->validate($data[$property], $constraint)
            ->getViolations()
        ;
    }

    private function getValidatorFQCN($constraint)
    {
        $classMap = [
            'choice' => Choice::class,
            'collection' => Collection::class,
            'datetime' => DateTime::class,
            'file' => File::class,
            'length' => Length::class,
            'not_blank' => NotBlank::class,
            'not_null' => NotNull::class,
        ];
        
        return $classMap[$constraint] ?? null;
    }

    private function replaceConstraintKeys($attrs)
    {
        $converted = [];

        foreach ($attrs as $key => $val) {
            if ($k = $this->getValidatorFQCN($key)) {
                $key = $k;
            }
            
            if (is_array($val)) {
                $val = $this->replaceConstraintKeys($val);
            }
            
            $converted[$key] = $val;
        }
        
        return $converted;
    }
}