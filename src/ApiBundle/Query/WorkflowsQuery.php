<?php

namespace ApiBundle\Query;

use App\Entity\Contract;
use App\Entity\Requisition;
use App\Entity\Supplier;
use App\Helper\UserHelper;
use App\Workflow\Status\ProductStatus;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class WorkflowsQuery
 * @package ApiBundle\Query
 * @DI\Service("workflows.query")
 */
class WorkflowsQuery
{
    /**
     * @var EntityManager
     * @DI\Inject("doctrine.orm.entity_manager")
     */
    public $manager;

    /**
     * @var UserHelper
     * @DI\Inject("user_helper")
     */
    public $userHelper;
    
    public function supplierRequisitionsQuery($state = 'all', $sort, $order)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->manager->createQueryBuilder();
        $qb = $qb->select('r')
            ->from(Requisition::class, 'r')
            ->join(Contract::class, 'c', Join::WITH, 'c = r.contract')
            ->join(Supplier::class, 's', Join::WITH, 's = c.supplier')
        ;
        
        switch ($state) {
            case 'pending':
                $stats = [
                    ProductStatus::APPROVED,
                    ProductStatus::ACCEPTED,
                    ProductStatus::RETURNED,
                ];
                break;
            case 'progress':
                $stats = [
                    ProductStatus::CONFIRMED,
                    ProductStatus::REVIEWED,
                ];
                break;
            case 'history':
                $stats = [
                    ProductStatus::COMPLETED,
                    ProductStatus::EVALUATED,
                    ProductStatus::CLOSED, 
                ];
                break;
            case 'certify':
                $stats = [
                    ProductStatus::DELIVERED,
                ];
                break;
            default:
                $stats = [
                    ProductStatus::APPROVED,
                    ProductStatus::ACCEPTED,
                    ProductStatus::DELIVERED,
                    ProductStatus::RETURNED,
                    ProductStatus::COMPLETED,
                    ProductStatus::EVALUATED,
                    //ProductStatus::CLOSED, 
                ];
        }
        
        $qb->where($qb->expr()->in('r.status', $stats));
        $qb->andWhere(':user MEMBER OF s.representatives');
        
        $qb->orderBy('r.'.$sort, $order);
        $qb->setParameter('user', $this->userHelper->getCurrentUser());
        
        return $qb->getQuery();
    }
}