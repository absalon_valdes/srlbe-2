<?php

namespace ApiBundle\Representation;

use Doctrine\ORM\Query;
use Hateoas\Configuration\Route;
use Hateoas\Representation\Factory\PagerfantaFactory;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

/**
 * Class PaginatedRepresentation
 * @package ApiBundle\Representation
 */
class PaginatedRepresentation
{
    public static function create($data, $route, $limit, $page)
    {
        $adapter = self::getAdapter($data);
        
        $pager = new Pagerfanta($adapter);
        $pager->setMaxPerPage($limit);
        $pager->setCurrentPage($page);

        $factory = new PagerfantaFactory();
        
        return $factory->createRepresentation($pager, new Route($route));
    }

    private static function getAdapter($data)
    {
        if ($data instanceof Query) {
            return new DoctrineORMAdapter($data);
        }
        
        if (is_array($data)) {
            return new ArrayAdapter($data);
        }

        throw new \InvalidArgumentException;
    }
}