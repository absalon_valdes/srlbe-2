<?php

namespace ApiBundle\Hateoas;

use ApiBundle\Model\TransitionsConfig;
use App\Entity\Requisition;
use Hateoas\Configuration\Metadata\ClassMetadataInterface;
use Hateoas\Configuration\Relation;
use Hateoas\Configuration\Route;
use JMS\DiExtraBundle\Annotation as DI;

/**
 * Class RequisitionActionsProvider
 * @DI\Service("requisition.actions.provider")
 */
class RequisitionActionsProvider
{
    public function addRelations($object, ClassMetadataInterface $classMetadata)
    {
        if (Requisition::class !== get_class($object)) {
            return null;    
        }
        
        /** @var Requisition $object */
        $state = $object->getStatus();
        
        $availables = TransitionsConfig::getRules($object);
        
        if (!array_key_exists($state, $availables)) {
            return [];
        }
        
        return $this->createActionRel($state, $availables[$state], $object);
    }

    private function createActionRel($from, array $actionCfg, Requisition $requisition)
    {
        $actions = [];
        
        foreach ($actionCfg as $step => $cfg) {
            if (!$cfg['precondition']) {
                continue;
            }
            
            $route = new Route('workflow_step', [
                'id' => $requisition->getId(),
                'from' => bin2hex($from),
                'step' => bin2hex($step),
            ]);
            
            $attrs = [];
            $attrs['title'] = $cfg['title'];
            
            $modelAttrs = [];

            foreach ($cfg['attrs'] ?? [] as $key => $attr) {
                if (!$attr['precondition']) {
                    continue;
                }
                
                unset($attr['precondition']);
                $modelAttrs[$key] = $attr;
            }
            
            if (count($modelAttrs)) {
                $attrs['attrs'] = $modelAttrs;
            }
            
            $actions[] = new Relation('actions', $route, null, $attrs);
        }
        
        return $actions;
    }
}