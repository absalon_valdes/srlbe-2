<?php

namespace ApiBundle\Controller;

use ApiBundle\Model\RequisitionMetadata;
use App\Export\RequisitionExporter;
use ApiBundle\Representation\PaginatedRepresentation;
use App\Entity\Requisition;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ReportController
 * @package App\Controller\Api
 * @Rest\Route("/report")
 */
class ReportController extends FOSRestController
{
    /**
     * @var RequisitionExporter
     * @DI\Inject("requisition.exporter")
     * @Security("has_role('ROLE_REPORTS')")
     */
    protected $requisitionExporter;
    
    /**
     * @ApiDoc(
     *     section="Report",
     *     description="Obtiene reporte especificado",
     *     statusCodes={200="Exito"}
     * )
     * @Rest\Get(name="report_index")
     * @Rest\QueryParam(name="type", nullable=true, requirements="(requisition.product|requisition.self_management|requisition.maintenance)", default="", description="Tipo de reporte")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="100", description="Items por página")
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Página")
     * @Rest\View(serializerGroups={"Default"})
     * @param ParamFetcherInterface $paramFetcher
     * @return static
     */
    public function processListAction(ParamFetcherInterface $paramFetcher)
    {
        $type = $paramFetcher->get('type');
        $data = $this->requisitionExporter->export($type)->toArray();
        $limit = (int) $paramFetcher->get('limit');
        $page = (int) $paramFetcher->get('page');
        return PaginatedRepresentation::create($data, 'report_index', $limit, $page);
    }
}