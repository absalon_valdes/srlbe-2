<?php

namespace ApiBundle\Controller;

use ApiBundle\Query\MessagesQuery;
use ApiBundle\Representation\PaginatedRepresentation;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use JMS\DiExtraBundle\Annotation as DI;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class MessageController
 * @package App\Controller\Api
 * @Rest\Route("/messages")
 * @Security("has_role('ROLE_SUPPLIER')")
 */
class MessageController extends FOSRestController
{
    /**
     * @var MessagesQuery
     * @DI\Inject("messages.query")
     */
    protected $query;
    
    /**
     * @ApiDoc(
     *     section="Message",
     *     description="Obtiene lista de notificaciones del usuario actual",
     *     statusCodes={200="Exito"}
     * )
     * @Rest\Get(name="messages_index")
     * @Rest\QueryParam(name="state", nullable=true, requirements="(viewed|pending)", default="", description="Estado del mensaje")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="5", description="Items por página")
     * @Rest\QueryParam(name="page", requirements="\d+", default="1", description="Página")
     * @Rest\QueryParam(name="order", nullable=true, requirements="(asc|desc)", default="desc", description="Dirección de ordenamiento")
     * @Rest\QueryParam(name="sort", nullable=true, requirements="[a-zA-Z0-9]+", default="createdAt", description="Campo de ordenamiento")
     * @Rest\View(serializerGroups={"Default"})
     * @param ParamFetcherInterface $paramFetcher
     * @return static
     */
    public function processListAction(ParamFetcherInterface $paramFetcher)
    {
        $limit = (int) $paramFetcher->get('limit');
        $page = (int) $paramFetcher->get('page');
        
        $state = $paramFetcher->get('state');
        $sort = $paramFetcher->get('sort');
        $order = $paramFetcher->get('order');
        
        $query = $this->query->supplierMessagesQuery($state, $sort, $order);
        
        return PaginatedRepresentation::create($query, 'messages_index', $limit, $page);
    }


    /**
     * @ApiDoc(
     *     section="Message",
     *     description="Obtiene detalle de una notificación"
     * )
     * @Rest\Get("/{id}", name="message_detail")
     * @param Message $message
     * @return static
     */
    public function processDetailAction(Message $message)
    {
        return $message;
    }
}