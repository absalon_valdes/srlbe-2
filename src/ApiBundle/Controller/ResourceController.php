<?php

namespace ApiBundle\Controller;

use App\Entity\Office;
use App\Entity\Product;
use App\Entity\Supplier;
use App\Entity\Message;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Class ResourceController
 * @package ApiBundle\Controller
 * @Rest\Route("/resources")
 * @Security("has_role('ROLE_SUPPLIER')")
 */
class ResourceController
{
    /**
     * @Rest\Get("/products/{id}", name="resource_product")
     * @param Product $product
     * @return Product
     */
    public function productAction(Product $product)
    {
        return $product;
    }

    /**
     * @Rest\Get("/suppliers/{id}", name="resource_supplier")
     * @param Supplier $supplier
     * @return Supplier
     */
    public function supplierAction(Supplier $supplier)
    {
        return $supplier;
    }

    /**
     * @Rest\Get("/offices/{id}", name="resource_office")
     * @param Office $office
     * @return Office
     */
    public function officeAction(Office $office)
    {
        return $office;
    }
    
    /**
     * @Rest\Get("/messages/{id}", name="resource_message")
     * @param Message $message
     * @return Message
     */
    public function messageAction(Message $message)
    {
        return $message;
    }
}